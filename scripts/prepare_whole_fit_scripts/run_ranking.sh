#syst
function run_syst {
  #run_folder="../../abs-xsec-with-trexfiles-local/v12/v12/syst-all/"
  run_folder="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v31/syst-all-fit-data-mu-blinded/"
  #run_folder="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v_ljet_finer_binning_1/v1/syst-all-fit-data-mu-blinded/"
  trex_config_path="../generate_config_file/syst-all"
  trex_folder="../../TRExFitter/"
  pushd ../generate_config_file/
  #rm -rf *.config
  #source run_generate_syst.sh && fit_asimov
  popd
  ./submit_ranking_condor.py  --config-file $trex_config_path/tty1l_pt_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder} --submit
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_eta_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr1_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr2_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drphb_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drlj_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dEtall_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dPhill_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptj1_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptll_all_syst.config --run-folder ${run_folder}
}

run_syst
