#!/usr/bin/env python
"""
This script reads from trex-config file and creates a set of systematics
This is needed for runnning ranking separately

also

This script reads trex-config file and makes a set of NP category and sub-category
This is needed for grouping the samples

"""

import argparse

class Read_trex_config:

  def __init__(self,trex_config_file):
    self.file = trex_config_file

  def get_NP_names(self): # take the config file and return set of systematic names
    systematic_list = []
    file_ = open(self.file)
    for line in file_.readlines():
      if "NuisanceParameter" in line:
        split_line = line.split(":") # split the line with spaces and the second argument would be the systematics names 
        systematic_name = split_line[1]
        systematic_name_quote_removed = systematic_name.replace('"','') # remove if double quote present in the stirng
        systematic_name_single_quote_removed = systematic_name_quote_removed.replace("'","") # remove if single quote present in the stirng
        systematic_name_quote_space_removed = systematic_name_single_quote_removed.replace(" ","")
        systematic_name_quote_space_newline_removed = systematic_name_quote_space_removed.replace("\n","")
        systematic_list.append(systematic_name_quote_space_newline_removed)
    return set(systematic_list)
  
  def get_sub_category_names(self): # take the config file and return set of systematic names
    subcategory_list = []
    file_ = open(self.file)
    for line in file_.readlines():
      if "SubCategory" in line:
        split_line = line.split(":") # split the line with spaces and the second argument would be the systematics names 
        subcategory_name = split_line[1]
        subcategory_list.append(subcategory_name)
    return set(subcategory_list)




if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--trex-config-file", type = str, help="name of the trex-config-file")
  args = parser.parse_args()
  config_file_reader = Read_trex_config(args.trex_config_file)
  set_syst_names = config_file_reader.get_NP_names()
  print(set_syst_names)
  print("lenth of set: {}".format(len(set_syst_names)))
  set_category_names = config_file_reader.get_sub_category_names()
  print(set_category_names)