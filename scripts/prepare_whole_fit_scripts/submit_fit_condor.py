#!/usr/bin/env python
"""
script to submit trex-fitter job in htcondor; because I have 11 variables; finding 11 pcs to submit it is pathetic

exe_tty2l_pt_all_syst.config.sh should be like this:
  #!/usr/bin/env bash
  cd ${trex_fitter_path} #/afs/cern.ch/user/b/bmondal/work/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/TRExFitter
  mkdir -p build
  cd build
  source ../setup.sh
  cmake ..
  make -j 8
  cd ${folder_where_to_run}
  cp ${config_file_with_path} ${folder_where_to_run}
  trex-fitter u  ${config_file}
  trex-fitter h  ${config_file}
  trex-fitter wf  ${config_file}
  trex-fitter dp  ${config_file}
  trex-fitter r  ${config_file}
"""
import os
from subprocess import call
import argparse

submit = False
process_grouped_impact = False


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

class SubmitCondor:
  def __init__(self, config_file, run_folder):
    self.config_file_name_with_path = config_file
    self.run_folder = run_folder
    tmp_list = self.config_file_name_with_path.split("/")
    self.config_file_name = tmp_list[len(tmp_list) - 1]
    self.trex_fitter_path = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-ljet/TRExFitter"
    self.write_ranking = False
  
  def create_bash_executable(self):
    self.bash_file_name = "{}/exe_{}.sh".format(self.run_folder, self.config_file_name)
    file_ = open(self.bash_file_name, "w")
    file_.write("#!/usr/bin/env bash \n")
    file_.write("ulimit -n 20096 \n")
    file_.write("cd {}\n".format(self.trex_fitter_path))
    file_.write("mkdir -p build; cd build \n")
    file_.write("source ../setup.sh \n")
    #file_.write("cmake .. \n")
    #file_.write("make -j 8 \n")
    file_.write("cd {} \n".format(self.run_folder))
    #file_.write("cp {} {}/ \n".format(self.config_file_name_with_path, self.run_folder))
    file_.write("sed -i \'s/StatOnlyFit: TRUE/StatOnlyFit: FALSE/g\' {} \n".format(self.config_file_name))
    file_.write("trex-fitter u {} \n".format(self.config_file_name))
    file_.write("trex-fitter h {} \n".format(self.config_file_name))
    file_.write("trex-fitter wf {} \n".format(self.config_file_name))
    file_.write("sed -i  \'s/StatOnlyFit: FALSE/StatOnlyFit: TRUE/g\' {} \n".format(self.config_file_name))
    file_.write("trex-fitter f {} \n".format(self.config_file_name))
    file_.write("sed -i \'s/StatOnlyFit: TRUE/StatOnlyFit: FALSE/g\' {} \n".format(self.config_file_name))
    if process_grouped_impact:
      file_.write("trex-fitter i {} \n".format(self.config_file_name))
    if process_grouped_impact:
      file_.write("trex-fitter i {} \"GroupedImpact=combine\"\n".format(self.config_file_name))
    #file_.write("trex-fitter dp {} \n".format(self.config_file_name))
    if(self.write_ranking): file_.write("trex-fitter r {} \n".format(self.config_file_name))

  def submit_condor(self):
    # create bash script firs
    self.create_bash_executable()
    self.condor_submit_script = "{}/condor_{}.sub".format(self.run_folder, self.config_file_name)
    file_ = open(self.condor_submit_script, "w")
    file_.write("executable = {} \n".format(self.condor_submit_script))
    file_.write("should_transfer_files = Yes \n")
    file_.write("when_to_transfer_output = ON_EXIT \n")
    if(self.write_ranking): file_.write("+MaxRuntime =  600 \n")
    else: file_.write("+MaxRuntime =  2400 \n")
    file_.write("request_cpus = 2 \n")
    file_.write("queue \n")

    #print("{}".format(self.bash_file_name))
    call("chmod +x {}".format(self.bash_file_name), shell=True)
    call("cp {} {}/ \n".format(self.config_file_name_with_path, self.run_folder),shell=True)
    tmp_str = self.condor_submit_script.split("/")
    condor_submit_script_name = tmp_str[len(tmp_str) - 1]
    cmd_string = "cd {}; condor_submit {}; cd - \n".format(self.run_folder,condor_submit_script_name)
    if submit:
      print(">>>>>>>>>>> submitting with commonad: {} <<<<<<<<<<<<<<<<<<<<\n".format(cmd_string))
      call(cmd_string, shell = True)



if __name__ == "__main__":
  parser = argparse.ArgumentParser(description ='submit trex-fitting job in condor')
  parser.add_argument("--config-file", required=True, help="config file name with path", type=os.path.abspath)
  parser.add_argument("--run-folder", required=True, help="run folder", type=os.path.abspath)
  parser.add_argument("--trex-folder", required=True, help="trex folder path; /afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-ljet/TRExFitter", type=os.path.abspath)
  parser.add_argument("--submit", type=str2bool, nargs='?',
                        const=True, default=False,
                        help="submit; no dry")
  parser.add_argument("--write-ranking", type=str2bool, nargs='?',
                        const=True, default=False,
                        help="submit; no dry")

  args = parser.parse_args()
  config_file_with_path = args.config_file
  run_folder = args.run_folder
  submit = args.submit
  print(">>> copied running scripts {} in :{} <<<\n".format(config_file_with_path, run_folder))
  fit_pt = SubmitCondor(config_file_with_path, run_folder)
  fit_pt.trex_fitter_path = args.trex_folder
  if(args.write_ranking): fit_pt.write_ranking = True
  fit_pt.submit_condor()
