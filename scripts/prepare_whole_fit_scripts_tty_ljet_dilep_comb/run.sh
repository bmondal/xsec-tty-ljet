#syst
function run_syst {
  run_folder="../../abs-xsec-with-trexfiles-local/v12/v_ljet_dilep_comb/v2/syst-all-not-using-multifit/"
  mkdir -p $run_folder
  trex_folder="../../TRExFitter/"
  trex_config_file_path="../generate_config_file/syst-all"
  pushd ../generate_config_file/
  rm -rf *.config
  source run_generate_syst.sh && fit_asimov
  popd
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_pt_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_eta_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  #./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_dr_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  #./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_dr1_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  #./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_drphb_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  #./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_drlj_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  #./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_ptj1_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
}
function run_syst_real_data_mu_blinded() {
  run_folder="../../abs-xsec-with-trexfiles-local/v12/v18/syst-all-fit-data-mu-blinded/"
  mkdir -p $run_folder
  trex_folder="../../TRExFitter/"
  trex_config_file_path="../generate_config_file/syst-all-fit-data-mu-blinded/"
  pushd ../generate_config_file/
  rm -rf *.config
  source run_generate_syst.sh && fit_data_mu_blinded
  popd
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_pt_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_eta_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_dr_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_dr1_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_drphb_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_drlj_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_ptj1_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
}

#stat
function run_stat {
  run_folder="../../abs-xsec-with-trexfiles-local/v12/v16/stat-all/"
  mkdir -p $run_folder
  trex_folder="../../TRExFitter/"
  trex_config_file_path="../generate_config_file/stat-all/"
  pushd ../generate_config_file/
  rm -rf *.config
  source run_generate_stat.sh && generate_config_stat_all_var
  popd
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_pt_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_eta_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_dr_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_drphb_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_drlj_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty1l_ptj1_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # reweighted ones
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/
}
function run_stat_reweighted {
  run_folder="../../abs-xsec-with-trexfiles-local/v12/v9/stat-all/"
  trex_folder="../../TRExFitter/"
  pushd ../generate_config_file/
  rm -rf *.config
  source run_generate_stat.sh &&  generate_config_reweighted_all_var
  popd
  ### >>>>>>>>>>>>>>>>>>>>> pt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_pt_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_pt_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_pt_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_pt_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> eta <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_eta_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_eta_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_eta_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_eta_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> dr <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_dr_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_dr_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_dr_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_dr_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> drphb <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_drphb_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_drphb_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_drphb_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_drphb_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> drlj <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_drlj_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_drlj_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_drlj_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_drlj_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> pt j1 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_ptj1_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_ptj1_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_ptj1_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty1l_ptj1_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}

}

#run_stat
#run_stat_reweighted
run_syst
#run_syst_real_data_mu_blinded
