#!/usr/bin/env python
"""
script to submit trex-fitter job in htcondor; because I have 11 variables; finding 11 pcs to submit it is pathetic

exe_tty2l_pt_all_syst.config.sh should be like this:
  #!/usr/bin/env bash
  cd ${trex_fitter_path} #/afs/cern.ch/user/b/bmondal/work/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/TRExFitter
  mkdir -p build
  cd build
  source ../setup.sh
  cmake ..
  make -j 8
  cd ${folder_where_to_run}
  cp ${config_file_with_path} ${folder_where_to_run}
  trex-fitter u  ${config_file}
  trex-fitter h  ${config_file}
  trex-fitter wf  ${config_file}
  trex-fitter dp  ${config_file}
  trex-fitter r  ${config_file}
"""
import os
from subprocess import call
import argparse

from Read_trex_config import *

submit = False

gamma_np_list = [
#"gamma_shape_stat_Unfolding_SR1_Truth_bin_1_SR1_bin_0"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_1_SR1_bin_1"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_2_SR1_bin_0"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_2_SR1_bin_1"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_2_SR1_bin_2"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_3_SR1_bin_0"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_3_SR1_bin_1"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_3_SR1_bin_2"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_3_SR1_bin_3"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_4_SR1_bin_2"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_4_SR1_bin_3"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_4_SR1_bin_4"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_5_SR1_bin_1"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_5_SR1_bin_3"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_5_SR1_bin_4"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_5_SR1_bin_5"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_6_SR1_bin_4"
#,"gamma_shape_stat_Unfolding_SR1_Truth_bin_6_SR1_bin_5"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_1_SR2_bin_0"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_1_SR2_bin_1"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_2_SR2_bin_0"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_2_SR2_bin_1"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_2_SR2_bin_2"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_3_SR2_bin_1"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_3_SR2_bin_2"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_3_SR2_bin_3"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_4_SR2_bin_1"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_4_SR2_bin_2"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_4_SR2_bin_3"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_4_SR2_bin_4"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_5_SR2_bin_3"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_5_SR2_bin_4"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_5_SR2_bin_5"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_6_SR2_bin_4"
#,"gamma_shape_stat_Unfolding_SR2_Truth_bin_6_SR2_bin_5"
"gamma_stat_SR1_bin_0"
,"gamma_stat_SR1_bin_1"
,"gamma_stat_SR1_bin_2"
,"gamma_stat_SR1_bin_3"
,"gamma_stat_SR1_bin_4"
,"gamma_stat_SR1_bin_5"
,"gamma_stat_SR2_bin_0"
,"gamma_stat_SR2_bin_1"
,"gamma_stat_SR2_bin_2"
,"gamma_stat_SR2_bin_3"
,"gamma_stat_SR2_bin_4"
,"gamma_stat_SR2_bin_5"
,"gamma_stat_SR3_bin_0"
,"gamma_stat_SR3_bin_1"
,"gamma_stat_SR3_bin_2"
,"gamma_stat_SR3_bin_3"
,"gamma_stat_SR3_bin_4"
,"gamma_stat_SR3_bin_5"
,"gamma_stat_SR4_bin_0"
,"gamma_stat_SR4_bin_1"
,"gamma_stat_SR4_bin_2"
,"gamma_stat_SR4_bin_3"
,"gamma_stat_SR4_bin_4"
,"gamma_stat_SR4_bin_5"

]


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

class SubmitCondor:
  def __init__(self, config_file, run_folder):
    self.config_file_name_with_path = config_file
    self.run_folder = run_folder
    tmp_list = self.config_file_name_with_path.split("/")
    self.config_file_name = tmp_list[len(tmp_list) - 1]
    self.trex_fitter_path = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/TRExFitter"
    self.write_ranking = False
    self.trex_config_reader = Read_trex_config(self.config_file_name_with_path) # config file reader
  
  def create_bash_executable_for_np(self,np_name): # create bash executalbe for running fit
    self.bash_file_name = "exe_{}.{}.sh".format(self.config_file_name, np_name)
    self.bash_file_path_name = "{}/exe_{}.{}.sh".format(self.run_folder, self.config_file_name, np_name)
    file_ = open(self.bash_file_path_name, "w")
    file_.write("#!/usr/bin/env bash \n")
    file_.write("ulimit -n 20096\n")
    #file_.write("shopt -s expand_aliases \n")
    #file_.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \n")
    #file_.write("alias setupATLAS=\'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \' \n")
    #file_.write("setupATLAS; asetup StatAnalysis,0.1.1 \n")
    file_.write("cd {}\n".format(self.trex_fitter_path))
    file_.write("mkdir -p build; cd build \n")
    file_.write("setuproot \n")
    file_.write("source ../setup.sh \n")
    file_.write("cmake .. \n")
    file_.write("make -j 8 \n")
    file_.write("cd {} \n".format(self.run_folder))
    #file_.write("cp {} {}/ \n".format(self.config_file_name_with_path, self.run_folder))
    file_.write("trex-fitter r {} Ranking={}\n".format(self.config_file_name, np_name))
    #file_.write("trex-fitter u {} \n".format(self.config_file_name))
    #file_.write("trex-fitter h {} \n".format(self.config_file_name))
    #file_.write("trex-fitter wf {} \n".format(self.config_file_name))
    #file_.write("trex-fitter dp {} \n".format(self.config_file_name))
    file_.close()

  def submit_condor(self):
    set_of_NPs  = self.trex_config_reader.get_NP_names()  # set of NP names
    for np in  set_of_NPs:
      # create bash script firs
      self.create_bash_executable_for_np(np)
      self.condor_submit_script = "{}/condor_{}.{}.sub".format(self.run_folder, self.config_file_name, np)
      file_ = open(self.condor_submit_script, "w")
      file_.write("executable = {} \n".format(self.bash_file_name))
      file_.write("should_transfer_files = Yes \n")
      file_.write("when_to_transfer_output = ON_EXIT \n")
      file_.write("+MaxRuntime =  3600 \n")
      file_.write("request_cpus = 1 \n")
      file_.write("queue \n")
      file_.close()

      #print("{}".format(self.bash_file_name))
      call("chmod +x {}".format(self.bash_file_path_name), shell=True)
      tmp_str = self.condor_submit_script.split("/")
      condor_submit_script_name = tmp_str[len(tmp_str) - 1]
      cmd_string = "pushd {}; condor_submit {}; popd \n".format(self.run_folder,condor_submit_script_name)
      if submit:
        print(">>>>>>>>>>> submitting with commonad: {} <<<<<<<<<<<<<<<<<<<<\n".format(cmd_string))
        call(cmd_string, shell = True)
    # submit for gammas
    for np in gamma_np_list:
      # create bash script firs
      self.create_bash_executable_for_np(np)
      self.condor_submit_script = "{}/condor_{}.{}.sub".format(self.run_folder, self.config_file_name, np)
      file_ = open(self.condor_submit_script, "w")
      file_.write("executable = {} \n".format(self.bash_file_name))
      file_.write("should_transfer_files = Yes \n")
      file_.write("when_to_transfer_output = ON_EXIT \n")
      file_.write("+MaxRuntime =  3600 \n")
      file_.write("request_cpus = 1 \n")
      file_.write("queue \n")

      #print("{}".format(self.bash_file_name))
      call("chmod +x {}".format(self.bash_file_path_name), shell=True)
      tmp_str = self.condor_submit_script.split("/")
      condor_submit_script_name = tmp_str[len(tmp_str) - 1]
      cmd_string = "cd {}; condor_submit {}; cd - \n".format(self.run_folder,condor_submit_script_name)
      if submit:
        print(">>>>>>>>>>> submitting with commonad: {} <<<<<<<<<<<<<<<<<<<<\n".format(cmd_string))
        call(cmd_string, shell = True)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description ='submit trex-fitting job in condor')
  parser.add_argument("--config-file", required=True, help="config file name with path", type=os.path.abspath)
  parser.add_argument("--run-folder", required=True, help="run folder", type=os.path.abspath)
  parser.add_argument("--submit", type=str2bool, nargs='?',
                        const=True, default=False,
                        help="submit; no dry")
  args = parser.parse_args()
  config_file_with_path = args.config_file
  run_folder = args.run_folder
  submit = args.submit
  print(">>> copied running scripts {} in :{} <<<\n".format(config_file_with_path, run_folder))
  fit_pt = SubmitCondor(config_file_with_path, run_folder)
  fit_pt.write_ranking = False
  fit_pt.submit_condor()
