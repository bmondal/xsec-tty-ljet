#!/usr/bin/env bash
out_file_efake="efake_yield_v280723_tmp.txt"
out_file_hfake="hfake_yield_v280723_tmp.txt"
out_file_prompt="prompt_yield_v220823.txt"
function run_efake() {
  ## tty_prod
  echo -e "event yield"
  echo -e "------------------------------------------"
  echo "efake_ttgamma_NLO_prod"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_tty_NLO_prod.Fine_Binning_1.root
  echo "efake_ttgamma_LO_dec"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_tty_LO_dec.Fine_Binning_1.root
  echo "efake_Wgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_wgamma.Fine_Binning_1.root
  echo "efake_Zgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_zgamma.Fine_Binning_1.root
  echo "efake_Wjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_wjets.Fine_Binning_1.root
  echo "efake_Zjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_zjets.Fine_Binning_1.root
  echo "efake_Wtgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_wty.Fine_Binning_1.root
  echo "efake_Wt_inclusive"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_wt_inclusive.Fine_Binning_1.root
  echo "efake_ttbar"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_ttbar.Fine_Binning_1.root
  echo "efake_singletop"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_singletop.Fine_Binning_1.root
  echo "efake_ttV"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_ttv.Fine_Binning_1.root
  echo "efake_diboson"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_diboson.Fine_Binning_1.root
}

function run_hfake() {
  ## tty_prod
  echo -e "event yield"
  echo -e "------------------------------------------"
  echo "hfake_ttgamma_NLO_prod"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_tty_NLO_prod.Fine_Binning_1.root
  echo "hfake_ttgamma_LO_dec"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_tty_LO_dec.Fine_Binning_1.root
  echo "hfake_Wgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_wgamma.Fine_Binning_1.root
  echo "hfake_Zgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_zgamma.Fine_Binning_1.root
  echo "hfake_Wjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_wjets.Fine_Binning_1.root
  echo "hfake_Zjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_zjets.Fine_Binning_1.root
  echo "hfake_Wtgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_wty.Fine_Binning_1.root
  echo "hfake_Wt_inclusive"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_wt_inclusive.Fine_Binning_1.root
  echo "hfake_ttbar"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_ttbar.Fine_Binning_1.root
  echo "hfake_singletop"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_singletop.Fine_Binning_1.root
  echo "hfake_ttV"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_ttv.Fine_Binning_1.root
  echo "hfake_diboson"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_diboson.Fine_Binning_1.root
}

function run_prompt() {
  ## tty_prod
  echo -e "event yield"
  echo -e "------------------------------------------"
  echo "prompt_Wty"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.Wty.root
  echo "prompt_Wt_inclusive"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.wt_inclusive.root
  echo "prompt_wjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.wjets.root
  echo "prompt_wgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.wgamma.root
  echo "prompt_zjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.zjets.root
  echo "prompt_zgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.zgamma.root
  echo "prompt_ttbar"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.ttbar.root
  echo "prompt_diboson"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.diboson.root
  echo "prompt_ttv"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.ttv.root
  #echo "prompt_ttz"
  #python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.ttz.root
  #echo "prompt_ttw"
  #python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.ttw.root
  echo "prompt_singletop"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.singletop.root
}


# be mindful I am using local eos folder here
#run_efake $HOME/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/ > $out_file_efake

#run_hfake $HOME/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/ > $out_file_hfake

run_prompt $HOME/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/ > $out_file_prompt
