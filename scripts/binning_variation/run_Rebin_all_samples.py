#!/usr/bin/env python3
import subprocess
import os
import threading

#new_binning = "binning_1"
new_binning = "nominal_from_fine"
#path_to_fine_binning = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v15_v2/"
#path_to_fine_binning = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v12_v1/"
#path_to_fine_binning = "/home/bm863639/work/ttgamma-analysis/analysis_framework/ljet_analysis/run1/test_onelepton/"
#path_to_fine_binning = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/"
path_to_fine_binning = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples_tmp/v12/Unfolding_inputs_v13_v1/"
#path_to_fine_binning = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/lepfake/"
#path_to_fine_binning = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/vtmp/vtmp/"
n_cpus = 55

def rebin(CR, subfolder, binning_name, optimized_binning, changer_pt, changer_others,corrupted_files):
    #path = os.path.expanduser(f"~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v15//{CR}/{subfolder}")
    path = os.path.expanduser(f"{path_to_fine_binning}/{CR}/{subfolder}")
    #path = os.path.expanduser(f"~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v15//{CR}/{subfolder}")
    #path = os.path.expanduser(f"/Users/buddhadeb/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/tmp//{CR}/{subfolder}")
    subprocess.run(["./Rebin_all_samples.py", "--top_dir", path, "--binning_name", binning_name, "--new_binning_name", optimized_binning, "--optimized_binning", optimized_binning, "--max_rel_err_val_changer_pt", str(changer_pt), "--max_rel_err_val_changer_others", str(changer_others), "--output_files_for_corrupted_folders", corrupted_files, "--n_cpus", str(n_cpus)])

def test():
    binning_name = "Fine_Binning_1"
    optimized_binning = new_binning
    changer_pt = 0.0
    changer_others = 0.0
    path = "tmp/"
    subprocess.run(["./Rebin_all_samples.py", "--top_dir", path, "--binning_name", binning_name, "--new_binning_name", optimized_binning, "--optimized_binning", optimized_binning, "--max_rel_err_val_changer_pt", str(changer_pt), "--max_rel_err_val_changer_others", str(changer_others)])



def rebin_tty_CR(subfolder,corrupted_files):
    CR = "tty_CR"
    binning_name = "Fine_Binning_1"
    optimized_binning = new_binning
    changer_pt = 0.0
    changer_others = 0.0
    rebin(CR, subfolder, binning_name, optimized_binning, changer_pt, changer_others,corrupted_files)

def rebin_tty_dec_CR(subfolder,corrupted_files):
    CR = "tty_dec_CR"
    binning_name = "Fine_Binning_1"
    optimized_binning = new_binning
    changer_pt = 0.0
    changer_others = 0.0
    rebin(CR, subfolder, binning_name, optimized_binning, changer_pt, changer_others,corrupted_files)

def rebin_fakes_CR(subfolder,corrupted_files):
    CR = "fakes_CR"
    binning_name = "Fine_Binning_1"
    optimized_binning = new_binning
    changer_pt = 0.0
    changer_others = 0.0
    rebin(CR, subfolder, binning_name, optimized_binning, changer_pt, changer_others,corrupted_files)

def rebin_other_photons_CR(subfolder,corrupted_files):
    CR = "other_photons_CR"
    binning_name = "Fine_Binning_1"
    optimized_binning = new_binning
    changer_pt = 0.0
    changer_others = 0.0
    rebin(CR, subfolder, binning_name, optimized_binning, changer_pt, changer_others,corrupted_files)



def run_tty_CR_over_specific_dirs(txt_file,corrupted_files): # run over specific dirs provided in txt file
    file_ = open(txt_file,"r")
    for line in file_.readlines():
      if line.strip(): # check if the line is empty
        #if "nominal_9999" in line: continue # ignoring nominal_999 because this folder contains more files
        rebin_tty_CR(line.strip(),corrupted_files)

def run_tty_dec_CR_over_specific_dirs(txt_file,corrupted_files): # run over specific dirs provided in txt file
    file_ = open(txt_file,"r")
    for line in file_.readlines():
      if line.strip():
        #if "nominal_9999" in line: continue # ignoring nominal_999 because this folder contains more files
        rebin_tty_dec_CR(line.strip(),corrupted_files)

def run_fakes_CR_over_specific_dirs(txt_file,corrupted_files): # run over specific dirs provided in txt file
    file_ = open(txt_file,"r")
    for line in file_.readlines():
      if line.strip():
        #if "nominal_9999" in line: continue # ignoring nominal_999 because this folder contains more files
        rebin_fakes_CR(line.strip(),corrupted_files)

def run_other_photons_CR_over_specific_dirs(txt_file,corrupted_files): # run over specific dirs provided in txt file
    file_ = open(txt_file,"r")
    for line in file_.readlines():
      if line.strip():
        #if "nominal_9999" in line: continue # ignoring nominal_999 because this folder contains more files
        rebin_other_photons_CR(line.strip(),corrupted_files)



if __name__=="__main__":
  #rebin_sig("/") # if you don't wanna run over nominal just use "/"
  #rebin_bkg("/")
  #test()
  run_over_specific_folders = False
  run_over_all_folders = True

  corrupted_files_tty_CR = "corrupted_folders_tty_CR.txt"
  corrupted_files_tty_dec_CR =  "corrupted_folders_tty_dec_CR.txt"
  corrupted_files_fakes_CR = "corrupted_folders_fakes_CR.txt"
  corrupted_files_other_photons_CR = "corrupted_folders_other_photons_CR.txt"

  if run_over_specific_folders:
    folders_to_process_tty_CR= "maybe_corrupted_folders_tty_CR.txt"
    folders_to_process_tty_dec_CR= "maybe_corrupted_folders_tty_dec_CR.txt"
    folders_to_process_fakes_CR= "maybe_corrupted_folders_fakes_CR.txt"
    folders_to_process_other_photons_CR= "maybe_corrupted_folders_other_photons_CR.txt"
    run_tty_CR_over_specific_dirs(folders_to_process_tty_CR,corrupted_files_tty_CR)
    run_tty_dec_CR_over_specific_dirs(folders_to_process_tty_dec_CR,corrupted_files_tty_dec_CR)
    run_fakes_CR_over_specific_dirs(folders_to_process_fakes_CR,corrupted_files_fakes_CR)
    run_other_photons_CR_over_specific_dirs(folders_to_process_other_photons_CR,corrupted_files_other_photons_CR)
    ## Create two threads
    #thread1 = threading.Thread(target=run_tty_CR_over_specific_dirs, args=(folders_to_process_tty_CR,corrupted_files_tty_CR))
    #thread2 = threading.Thread(target=run_tty_dec_CR_over_specific_dirs, args=(folders_to_process_tty_dec_CR,corrupted_files_tty_dec_CR))
    #thread3 = threading.Thread(target=run_fakes_CR_over_specific_dirs, args=(folders_to_process_fakes_CR,corrupted_files_fakes_CR))
    #thread4 = threading.Thread(target=run_other_photons_CR_over_specific_dirs, args=(folders_to_process_other_photons_CR,corrupted_files_other_photons_CR))

    # Start the threads
    #thread1.start()
    #thread2.start()
    #thread3.start()
    #thread4.start()

  if run_over_all_folders:
    rebin_tty_CR("/",corrupted_files_tty_CR)
    rebin_tty_dec_CR("/",corrupted_files_tty_dec_CR)
    rebin_fakes_CR("/",corrupted_files_fakes_CR)
    rebin_other_photons_CR("/",corrupted_files_other_photons_CR)
    ## Create two threads
    #thread1 = threading.Thread(target=rebin_tty_CR, args=("/",corrupted_files_tty_CR))
    #thread2 = threading.Thread(target=rebin_tty_dec_CR, args=("/",corrupted_files_tty_dec_CR))
    #thread3 = threading.Thread(target=rebin_fakes_CR, args=("/",corrupted_files_fakes_CR))
    #thread4 = threading.Thread(target=rebin_other_photons_CR, args=("/",corrupted_files_other_photons_CR))

    # Start the threads
    #thread1.start()
    #thread2.start()
    #thread3.start()
    #thread4.start()

    #rebin_tty_CR("/", corrupted_files_tty_CR)
    #rebin_tty_dec_CR("/",corrupted_files_tty_dec_CR)
    #rebin_fakes_CR("/",corrupted_files_fakes_CR)
    #rebin_other_photons_CR("/",corrupted_files_other_photons_CR)

