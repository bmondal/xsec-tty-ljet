#!/usr/bin/env python3

import ROOT
import os
import multiprocessing
#from multiprocessing import Manager
from array import  *
import numpy as np

import argparse
import sys
import math

sys.path.append(os.path.abspath(os.path.expanduser("/home/bm863639/work/ttgamma-analysis/analysis_framework/ljet_analysis/scripts/Bin_optimization/")))
from Bin_Optimizer import *

ROOT.gROOT.SetBatch(True)

debug = False 

# to not add the histograms to the internal registry
ROOT.TH1.AddDirectory(False)


# dictionary to map 2d hist to 1d hist
dict_2d_to_1d_hist = {
     "h2_drlj_reco_part_full_weighted": "hist_part_drlj_full_weighted"
    , "h2_drphb_reco_part_full_weighted": "hist_part_drphb_full_weighted"
    , "h2_j1_pt_reco_part_full_weighted": "hist_part_j1_pt_full_weighted"
    , "h2_ph_drphl1_reco_part_full_weighted": "hist_part_ph_drphl1_full_weighted"
    , "h2_ph_eta_reco_part_full_weighted": "hist_part_ph_eta_full_weighted"
    , "h2_ph_pt_reco_part_full_weighted": "hist_part_ph_pt_full_weighted"
}

dict_1d_fout_to_1d_hist = {
     "h1_fout_ph_eta_reco_part_full_weighted": "hist_part_ph_eta_full_weighted",
     "h1_fout_ph_pt_reco_part_full_weighted": "hist_part_ph_pt_full_weighted",
     "h1_fout_drlj_reco_part_full_weighted": "hist_part_drlj_full_weighted",
     "h1_fout_drphb_reco_part_full_weighted": "hist_part_drphb_full_weighted",
     "h1_fout_j1_pt_reco_part_full_weighted": "hist_part_j1_pt_full_weighted",
     "h1_fout_ph_drphl1_reco_part_full_weighted": "hist_part_ph_drphl1_full_weighted"

}

import ROOT
from array import array




def print_th2d_bin_contents(histogram):
    nx = histogram.GetNbinsX()+1 # to print also the overflow bin
    ny = histogram.GetNbinsY()+1
    for j in range(ny, 0, -1):
        row = []
        for i in range(1, nx+1):
            row.append(histogram.GetBinContent(i, j))
        print(*row)
    print()

def get_x_bin_edges(histogram):
    list = []
    for bin in range(1,histogram.GetNbinsX()+1):
      list.append(histogram.GetXaxis().GetBinLowEdge(bin))
    list.append(histogram.GetXaxis().GetBinUpEdge(histogram.GetNbinsX()))
    return list

def get_y_bin_edges(histogram):
    list = []
    for bin in range(1,histogram.GetNbinsY()+1):
      list.append(histogram.GetYaxis().GetBinLowEdge(bin))
    list.append(histogram.GetYaxis().GetBinUpEdge(histogram.GetNbinsY()))
    return list

def add_overflow_to_previous_1D(histogram):
    nx = histogram.GetNbinsX()

    # Underflow
    histogram.SetBinContent(1, histogram.GetBinContent(1) + histogram.GetBinContent(0))
    histogram.SetBinError(1, (histogram.GetBinError(1) ** 2 + histogram.GetBinError(0) ** 2) ** 0.5)
    histogram.SetBinContent(0, 0)
    histogram.SetBinError(0, 0)

    # Overflow
    histogram.SetBinContent(nx, histogram.GetBinContent(nx) + histogram.GetBinContent(nx+1))
    histogram.SetBinError(nx, (histogram.GetBinError(nx) ** 2 + histogram.GetBinError(nx+1) ** 2) ** 0.5)
    histogram.SetBinContent(nx+1, 0)
    histogram.SetBinError(nx+1, 0)

    return histogram

#def add_overflow_to_previous(histogram):
#    nx = histogram.GetNbinsX()
#    ny = histogram.GetNbinsY()
#    for i in range(1,nx+2):
#        for j in range(1,ny+2):
#            if i == nx+1:
#                #if debug: print("histoname {} \t binx, biny, bincontent: {} \t {} \t {} \n".format(histogram.GetName(), i, j,
#                #                                                                         histogram.GetBinContent(i, j)))
#                histogram.SetBinContent(i-1, j, histogram.GetBinContent(i-1, j) + histogram.GetBinContent(i, j))
#                histogram.SetBinError(i-1, j, (histogram.GetBinError(i-1, j) ** 2 + histogram.GetBinError(i, j) ** 2) ** 0.5)
#            if j == ny+1:
#                #if debug: print("histoname {} \t binx, biny, bincontent: {} \t {} \t {} \n".format(histogram.GetName(), i, j,
#                #                                                                         histogram.GetBinContent(i, j)))
#                histogram.SetBinContent(i, j-1, histogram.GetBinContent(i, j-1) + histogram.GetBinContent(i, j))
#                histogram.SetBinError(i, j-1, (histogram.GetBinError(i, j-1) ** 2 + histogram.GetBinError(i, j) ** 2) ** 0.5)
#    # add the right-top diagonal corner overflow bin to the previous diagonal bin
#    # (nx+1, ny+1) to (nx,ny)
#    histogram.SetBinContent(nx,ny, histogram.GetBinContent(nx,ny) + histogram.GetBinContent(nx+1, ny+1))
#    histogram.SetBinError(nx,ny, (histogram.GetBinError(nx,ny) ** 2 + histogram.GetBinError(nx+1, ny+1) ** 2) ** 0.5)
#
#    # now set the overflow bins to zero
#    for i in range(1,nx+2):
#        for j in range(1,ny+2):
#            if i == nx+1:
#                histogram.SetBinContent(i, j, 0)
#                histogram.SetBinError(i, j, 0)
#            if j == ny+1:
#                histogram.SetBinContent(i, j, 0)
#                histogram.SetBinError(i, j, 0)
#
#
#    return histogram


def add_overflow_to_previous(histogram):
    nx = histogram.GetNbinsX()
    ny = histogram.GetNbinsY()

    for i in range(nx+2):  # Including underflow and overflow bins
        for j in range(ny+2):  # Including underflow and overflow bins

            # Skip the corners to handle them separately later
            if (i, j) in [(0, 0), (nx+1, 0), (0, ny+1), (nx+1, ny+1)]:
                continue

            # Overflow in x
            if i == nx+1:
                histogram.SetBinContent(i-1, j, histogram.GetBinContent(i-1, j) + histogram.GetBinContent(i, j))
                histogram.SetBinError(i-1, j, (histogram.GetBinError(i-1, j) ** 2 + histogram.GetBinError(i, j) ** 2) ** 0.5)
                histogram.SetBinContent(i, j, 0)
                histogram.SetBinError(i, j, 0)
            
            # Overflow in y
            if j == ny+1:
                histogram.SetBinContent(i, j-1, histogram.GetBinContent(i, j-1) + histogram.GetBinContent(i, j))
                histogram.SetBinError(i, j-1, (histogram.GetBinError(i, j-1) ** 2 + histogram.GetBinError(i, j) ** 2) ** 0.5)
                histogram.SetBinContent(i, j, 0)
                histogram.SetBinError(i, j, 0)
            
            # Underflow in x
            if i == 0:
                histogram.SetBinContent(i+1, j, histogram.GetBinContent(i+1, j) + histogram.GetBinContent(i, j))
                histogram.SetBinError(i+1, j, (histogram.GetBinError(i+1, j) ** 2 + histogram.GetBinError(i, j) ** 2) ** 0.5)
                histogram.SetBinContent(i, j, 0)
                histogram.SetBinError(i, j, 0)
            
            # Underflow in y
            if j == 0:
                histogram.SetBinContent(i, j+1, histogram.GetBinContent(i, j+1) + histogram.GetBinContent(i, j))
                histogram.SetBinError(i, j+1, (histogram.GetBinError(i, j+1) ** 2 + histogram.GetBinError(i, j) ** 2) ** 0.5)
                histogram.SetBinContent(i, j, 0)
                histogram.SetBinError(i, j, 0)

    # Diagonal Overflow: Upper Right corner
    histogram.SetBinContent(nx, ny, histogram.GetBinContent(nx, ny) + histogram.GetBinContent(nx+1, ny+1))
    histogram.SetBinError(nx, ny, (histogram.GetBinError(nx, ny) ** 2 + histogram.GetBinError(nx+1, ny+1) ** 2) ** 0.5)
    histogram.SetBinContent(nx+1, ny+1, 0)
    histogram.SetBinError(nx+1, ny+1, 0)
    
    # Diagonal Underflow: Bottom Left corner
    histogram.SetBinContent(1, 1, histogram.GetBinContent(1, 1) + histogram.GetBinContent(0, 0))
    histogram.SetBinError(1, 1, (histogram.GetBinError(1, 1) ** 2 + histogram.GetBinError(0, 0) ** 2) ** 0.5)
    histogram.SetBinContent(0, 0, 0)
    histogram.SetBinError(0, 0, 0)

    return histogram




def rebin_2d_histogram(h2, xedges, yedges, new_hist_name):
    # Create a new histogram with the new bin edges
    nx = len(xedges) - 1
    ny = len(yedges) - 1
    h2_rebinned = ROOT.TH2D(new_hist_name, "", nx, np.asarray(xedges), ny, np.asarray(yedges))

    # Initialize the new histogram with the same axis labels as the original
    h2_rebinned.GetXaxis().SetTitle(h2.GetXaxis().GetTitle())
    h2_rebinned.GetYaxis().SetTitle(h2.GetYaxis().GetTitle())

    # first set the bin error to zero; because in the loop it adds uncertainty on top of this
    h2_rebinned.SetBinError(0,0)
    # Loop over the original histogram bins
    for xbin in range(1, h2.GetNbinsX() + 2): 
        for ybin in range(1, h2.GetNbinsY() + 2):
            xcenter = h2.GetXaxis().GetBinCenter(xbin)
            ycenter = h2.GetYaxis().GetBinCenter(ybin)
            z = h2.GetBinContent(xbin, ybin)
            error = h2.GetBinError(xbin, ybin)
            # Find the corresponding bin in the new histogram
            new_xbin = h2_rebinned.GetXaxis().FindBin(xcenter)
            new_ybin = h2_rebinned.GetYaxis().FindBin(ycenter)
            # Add the bin contents and errors to the corresponding bin in the new histogram
            newbin = h2_rebinned.GetBin(new_xbin, new_ybin)
            h2_rebinned.AddBinContent(newbin, z)
            h2_rebinned.SetBinError(newbin,
                                    math.sqrt(h2_rebinned.GetBinError(newbin) ** 2 + error ** 2))

    # Add overflow bin contribution to the last bin of the new histogram
    if debug: 
      print("Before adding overflow to previous bin for histogram {}\n".format(h2_rebinned.GetName()))
      print_th2d_bin_contents(h2_rebinned) 
    h2_rebinned = add_overflow_to_previous(h2_rebinned)
    if debug: 
      print("After adding overflow to previous bin for histogram {}\n".format(h2_rebinned))
      print_th2d_bin_contents(h2_rebinned) 


    # FIXME: some bug in the following logic
    #overflow_x = h2.GetBinContent(h2.GetNbinsX() + 1, 0)
    #overflow_y = h2.GetBinContent(0, h2.GetNbinsY() + 1)
    #overflow_z = h2.GetBinContent(h2.GetNbinsX() + 1, h2.GetNbinsY() + 1)
    #if debug: print("overflow x, y, z: {} \t {} \t {}\n".format(overflow_x, overflow_y, overflow_z))
    #error_overflow_x = h2.GetBinError(h2.GetNbinsX() + 1, 0)
    #error_overflow_y = h2.GetBinError(0, h2.GetNbinsY() + 1)
    #error_overflow_z = h2.GetBinError(h2.GetNbinsX() + 1, h2.GetNbinsY() + 1)
    #h2_rebinned.AddBinContent(h2_rebinned.GetNbinsX(), overflow_x)
    #h2_rebinned.AddBinContent(h2_rebinned.GetNbinsY(), overflow_y)
    #h2_rebinned.AddBinContent(h2_rebinned.GetNbinsX() * h2_rebinned.GetNbinsY(), overflow_z)
    #h2_rebinned.SetBinError(h2_rebinned.GetNbinsX(),
    #                        math.sqrt(h2_rebinned.GetBinError(h2_rebinned.GetNbinsX()) ** 2 + error_overflow_x ** 2))
    #h2_rebinned.SetBinError(h2_rebinned.GetNbinsY(),
    #                        math.sqrt(h2_rebinned.GetBinError(h2_rebinned.GetNbinsY()) ** 2 + error_overflow_y ** 2))
    #h2_rebinned.SetBinError(h2_rebinned.GetNbinsX() * h2_rebinned.GetNbinsY(),
    #                        math.sqrt(h2_rebinned.GetBinError(
    #                            h2_rebinned.GetNbinsX() * h2_rebinned.GetNbinsY()) ** 2 + error_overflow_z ** 2))

    return h2_rebinned




def calculate_response_matrix(reco_hist, truth_hist, migration_hist):
    response_matrix = migration_hist.Clone()
    N_R = reco_hist.GetNbinsX()
    N_T = truth_hist.GetNbinsX()
    N_mig_R = migration_hist.GetNbinsX()
    N_mig_T = migration_hist.GetNbinsY()
    dumTcontent = -1.e6
    rbin_total = 0
    res_value = [[0 for x in range(1000)] for y in range(1000)]
    res_error = [[0 for x in range(1000)] for y in range(1000)]

    if ((N_R != N_T) or N_R == 0):
        print("Unequal/empty truth and/or reco")

    '''response matrix calculation...'''
    for bin_r in range(1, N_R + 1):
        rbin_total = 0.
        for bin_t in range(1, N_T + 2): # take into account the overflow for the truth as well
            rbin_total += migration_hist.GetBinContent(bin_r, bin_t)
        for bin_t in range(1, N_T + 1):
            dumTcontent = truth_hist.GetBinContent(bin_t)
            if not (rbin_total == 0 or dumTcontent == 0):
                res_value[bin_r - 1][bin_t - 1] = 1. * (migration_hist.GetBinContent(bin_r, bin_t)) * (
                            (reco_hist.GetBinContent(bin_r)) / (dumTcontent * rbin_total))
                res_error[bin_r - 1][bin_t - 1] = 1. * (migration_hist.GetBinError(bin_r, bin_t)) * (
                            (reco_hist.GetBinContent(bin_r)) / (dumTcontent * rbin_total))
            response_matrix.SetBinContent(bin_r, bin_t, res_value[bin_r - 1][bin_t - 1])
            response_matrix.SetBinError(bin_r, bin_t, res_error[bin_r - 1][bin_t - 1])

    return response_matrix

def log_corrupted_files(log,filename):
    file_ = open(filename)
    for string in log:
        file_.write(string)
    file_.close()


class Rebin_all_samples:
    # A changer for changing the value of the param which changes the max rel err

    def __init__(self):
        self.max_rel_err_val_changer_pt = 0.00
        self.max_rel_err_val_changer_others = 0.00
        self.dict_of_hists = { # dict of hist name and the max relative error for binning optimization
             "hist_part_drlj_full_weighted": 0.30+ self.max_rel_err_val_changer_others
            , "hist_part_drphb_full_weighted": 0.30+ self.max_rel_err_val_changer_others
            , "hist_part_j1_pt_full_weighted": 0.20+ self.max_rel_err_val_changer_pt
            , "hist_part_ph_drphl1_full_weighted": 0.30+ self.max_rel_err_val_changer_others
            , "hist_part_ph_eta_full_weighted": 0.30+ self.max_rel_err_val_changer_others
            , "hist_part_ph_pt_full_weighted": 0.20+ self.max_rel_err_val_changer_pt
        }

        self.dict_hist_name_and_bin_boundaries = {}  # dictionary of hist name and bin boundaries
        #self.log_corrupted_files = [] # store the corrupted strings with filename
        #self.manager = Manager()
        #self.log_corrupted_files = self.manager.list()  # create a manager list


    def update_dict_of_hists(self):
        self.dict_of_hists = { # dict of hist name and the max relative error for binning optimization
             "hist_part_drlj_full_weighted": 0.30+ self.max_rel_err_val_changer_others
            , "hist_part_drphb_full_weighted": 0.30+ self.max_rel_err_val_changer_others
            , "hist_part_j1_pt_full_weighted": 0.20+ self.max_rel_err_val_changer_pt
            , "hist_part_ph_drphl1_full_weighted": 0.30+ self.max_rel_err_val_changer_others
            , "hist_part_ph_eta_full_weighted": 0.30+ self.max_rel_err_val_changer_others
            , "hist_part_ph_pt_full_weighted": 0.20+ self.max_rel_err_val_changer_pt
        }


    def set_max_rel_err_val_changer_pt(self, val):
        self.max_rel_err_val_changer_pt = val

    def set_max_rel_err_val_changer_others(self, val):
        self.max_rel_err_val_changer_others = val

    """Fill the dictionary, which contains histo name and bin boundaries.
       This bin boundaries we get after bin optimization
    """

    def make_dict_hist_bin_bounderies(self, sample_root_file="./nominal_9999/histograms.ttgamma_prod.Fine_Binning_1.root"):
        root_file_obj = ROOT.TFile.Open(sample_root_file)
        # Find optimized binning for all the histos and store it in dicitonary
        for histo in self.dict_of_hists:
            histo_particle_ph_pt = root_file_obj.Get("particle/{}".format(histo))
            optimized_histo_particle_ph_pt = do_binning_optimization(histo_particle_ph_pt, self.dict_of_hists[histo])
            bin_boundaries = get_bin_boundaries(
                optimized_histo_particle_ph_pt) # make dict with  key name as particle level hist name
            bin_boundaries_list = array('d', bin_boundaries)
            self.dict_hist_name_and_bin_boundaries[optimized_histo_particle_ph_pt.GetName()] = bin_boundaries_list # make dict with  key name as particle level hist name
            self.dict_hist_name_and_bin_boundaries[
                optimized_histo_particle_ph_pt.GetName().replace("_part_", "_reco_")] = bin_boundaries_list # also make dict with key name as reco level hist name

    def put_manual_binning_dict(self):
        self.dict_hist_name_and_bin_boundaries["hist_part_ph_pt_full_weighted"] = array('d',[20, 40, 60, 75, 95, 115, 135, 160, 200, 260, 500])
        self.dict_hist_name_and_bin_boundaries["hist_reco_ph_pt_full_weighted"] = array('d',[20, 40, 60, 75, 95, 115, 135, 160, 200, 260, 500])

        self.dict_hist_name_and_bin_boundaries["hist_part_ph_eta_full_weighted"] = array('d',[0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.37])
        self.dict_hist_name_and_bin_boundaries["hist_reco_ph_eta_full_weighted"] = array('d',[0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.37])

        self.dict_hist_name_and_bin_boundaries["hist_part_ph_drphl1_full_weighted"] = array('d',[0.4,0.8,1.2,1.6,2.0,2.4,2.8,3.4])
        self.dict_hist_name_and_bin_boundaries["hist_reco_ph_drphl1_full_weighted"] = array('d',[0.4,0.8,1.2,1.6,2.0,2.4,2.8,3.4])

        self.dict_hist_name_and_bin_boundaries["hist_part_j1_pt_full_weighted"] = array('d',[25,80,140,200,300,450])
        self.dict_hist_name_and_bin_boundaries["hist_reco_j1_pt_full_weighted"] = array('d',[25,80,140,200,300,450])

        self.dict_hist_name_and_bin_boundaries["hist_part_drphb_full_weighted"] = array('d',[0.4,0.8,1.3,1.9,2.6,3.4])
        self.dict_hist_name_and_bin_boundaries["hist_reco_drphb_full_weighted"] = array('d',[0.4,0.8,1.3,1.9,2.6,3.4])

        self.dict_hist_name_and_bin_boundaries["hist_part_drlj_full_weighted"] = array('d',[0.4,0.8,1.3,1.9,2.6,3.4])
        self.dict_hist_name_and_bin_boundaries["hist_reco_drlj_full_weighted"] = array('d',[0.4,0.8,1.3,1.9,2.6,3.4])



    def read_binning_from_config(self):
        pass


    def print_dict_hist_name_and_bin_boundaries(self):
        print("=== INFO: \t Optimized histogram names and bin boundaries")
        for hist in self.dict_hist_name_and_bin_boundaries:
            print(
                "=== Histogram name: {}, Bin boundaries: {} \n".format(hist, self.dict_hist_name_and_bin_boundaries[hist]))
            #for bin in  self.dict_hist_name_and_bin_boundaries[hist]:
            #    print(bin)
            #print("=== length of the list: {} \n ".format(len(list(self.dict_hist_name_and_bin_boundaries[hist]))))

    def process_file(self, args):
        filename, dirpath, binning, new_binning = args
        try:
          # check if the file is a ROOT file and contains "Fine_Binning" in its name
          if filename.endswith('.root') and binning in filename:
              if debug: print(">>> Info::Rebin_all_samples::rebin_and_save_to_file() processing file {}/{}\n".format(dirpath,filename))
              # Open the original ROOT file
              original_filename = filename
              original_file = ROOT.TFile(os.path.join(dirpath, original_filename))
    
              # Create a new ROOT file to store the updated histograms
              new_filename = filename.replace("Fine_Binning_1", new_binning)
              new_file = ROOT.TFile(os.path.join(dirpath, new_filename), "RECREATE")
    
              # if filename contains ttgamma_prod then use Reco, particle else its only Reco
              if ".ttgamma_dec." in filename and "NormVar" in filename: # there is ttgamma_dec Normvar samples, these don't have particle level
                directory_list = ["Reco","particle"]
                #directory_list = ["Reco"]
              elif ".ttgamma_prod." in filename or ".ttgamma_prod_" in filename or ".tty_prod_" in filename or ".ttgamma_dec." in filename or ".ttgamma_dec_" in filename or ".tty_dec_" in filename or ".ttgamma_nominal." in filename or ".ttgamma_nominal_H7." in filename:
                directory_list = ["Reco", "particle"]
              else:
                directory_list = ["Reco"]
              
              for dir in directory_list:
                  directory = original_file.Get(dir)  # get Reco or particle dir
                  new_file.mkdir(dir)  # create dir in outfile
                  dir_obj_from_file_reweighted = new_file.Get(
                      dir)  # this is the directory obj from the outfile. In this object we will write our new histos. this will be saved
                  for key in directory.GetListOfKeys():
                      key_name = key.GetName()
                      histo_obj = original_file.Get("{}/{}".format(dir, key_name))
                      new_hist = histo_obj.Clone()
                      # don't do naything fro th2d
                      if "h2" in key_name: continue
                      if "h_fout_ph_pt_full_weighted" in key_name: continue
    
                      # only do for reweighted /unfolding histos
                      if not "weighted" in key_name: continue
                      # weighting Reco and particle is different.
                      # if "reco" in key_name:
                      if (dir == "Reco"):
                          new_bins = self.dict_hist_name_and_bin_boundaries[new_hist.GetName()]
                          new_hist_name = new_hist.GetName()
                          hist_reweighted_tmp = new_hist.Rebin(len(new_bins) - 1, new_hist_name, new_bins)
                          hist_reweighted = add_overflow_to_previous_1D(hist_reweighted_tmp)
                          dir_obj_from_file_reweighted.WriteObject(hist_reweighted, "{}".format(key_name), "overwrite")
                      if (dir == "particle"):
                          new_bins = self.dict_hist_name_and_bin_boundaries[new_hist.GetName()]
                          new_hist_name = new_hist.GetName()
                          hist_reweighted_tmp = new_hist.Rebin(len(new_bins) - 1, new_hist_name, new_bins)
                          hist_reweighted = add_overflow_to_previous_1D(hist_reweighted_tmp)
                          dir_obj_from_file_reweighted.WriteObject(hist_reweighted, "{}".format(key_name),
                                                                   "overwrite")
              # now rebin the 2d histograms for migration matrices
              if ".ttgamma_prod." in filename or ".ttgamma_prod_" in filename or ".tty_prod_" in filename or ".ttgamma_dec." in filename or ".ttgamma_dec_" in filename or ".tty_dec_" in filename or ".ttgamma_nominal." in filename or ".ttgamma_nominal_H7." in filename:
                #if "NormVar" in filename: continue # there is ttgamma_dec Normvar samples, these don't have particle level
                for key in original_file.GetListOfKeys():
                    key_name = key.GetName()

                    if "h1_fout" in key_name and not "_response_" in key_name: # only the migration matrices
                      histo_obj = original_file.Get("{}".format(key_name))
                      new_hist = histo_obj.Clone()
                      new_bins = self.dict_hist_name_and_bin_boundaries[dict_1d_fout_to_1d_hist[new_hist.GetName()]]
                      new_hist_name = new_hist.GetName()
                      hist_reweighted_tmp = new_hist.Rebin(len(new_bins) - 1, new_hist_name, new_bins)
                      hist_reweighted = add_overflow_to_previous_1D(hist_reweighted_tmp)
                      new_file.WriteObject(hist_reweighted, "{}".format(key_name),"overwrite")

                    if "h2" in key_name and not "_response_" in key_name: # only the migration matrices
                        # do the rebinnng for migration matrices
                        histo_2d_obj = original_file.Get("{}".format(key_name))
                        new_hist_2d = histo_2d_obj.Clone()
                        corresponding_1d_hist_name = dict_2d_to_1d_hist[
                            new_hist_2d.GetName()]  # get corresponding 1d hist name, so that we can get the bin boundaries
                        new_hist_name = corresponding_1d_hist_name
                        """There could be following issue when rebinning, if the binning is not that fine enough
                        the new bin edges can fall in between two bins of the previous histogram. In that case
                        the Rebin function takes the lower bin edge as new binning. since I am doing rebinning 
                        of 2d histograms manually, I should take the exact bin edges after Rebin. that is why
                        the below line finds the bin edges after the histogram has been rebinned
                        """
                        histo_obj = new_file.Get("{}/{}".format("particle", new_hist_name))
                        new_bins = get_x_bin_edges(histo_obj)
                        hist_reweighted = rebin_2d_histogram(new_hist_2d, new_bins, new_bins,
                                                             new_hist_name)
                        new_file.WriteObject(hist_reweighted, "{}".format(key_name), "overwrite")
    
              new_file.Close()
              original_file.Close()
        except Exception as e:
          print(f"Error processing file during rebinning: {os.path.join(dirpath, filename)}")
          #self.log_corrupted_files.append("failed during rebin calculation {}/{}\n".format(os.path.basename(dirpath),filename))
          print(f"Exception: {e}")


    def rebin_and_save_to_file(self, top_dir, binning, new_binning, n_cpus):
        """params:
           binning: string in the filename with binning information
           new_binning: rename the file with new binning name
        """
        with multiprocessing.Pool(n_cpus) as pool:
          file_args = []
          for dirpath, dirnames, filenames in os.walk(top_dir):
              for filename in filenames:
                  file_args.append((filename, dirpath, binning, new_binning))
          pool.map(self.process_file, file_args)

    def calculate_and_save_response_matrices(self, top_dir, optimized_binning, n_cpus):
    
        file_args = []
    
        for dirpath, dirnames, filenames in os.walk(top_dir):
            for filename in filenames:
                file_args.append((filename, dirpath, optimized_binning))
    
        with multiprocessing.Pool(n_cpus) as pool:
            pool.map(self.calculate_response_for_file, file_args)
    
    def calculate_response_for_file(self, args):
        filename, dirpath, optimized_binning= args
    
        # Check if the file is a ROOT file and contains ...
        if filename.endswith('.root') and optimized_binning in filename and (".ttgamma_prod." in filename or ".ttgamma_prod_" in filename or ".tty_prod_" in filename or ".ttgamma_dec." in filename or ".ttgamma_dec_" in filename or ".tty_dec_" in filename ):
            if debug: print("Info::Rebin_all_samples::calculate_and_save_response_matrices() processing file {} \n".format(os.path.join(dirpath,filename)))
    
            try:
              f = ROOT.TFile(os.path.join(dirpath, filename), "update")
              # ph_pt
              reco_hist_pt = f.Get("Reco/hist_reco_ph_pt_full_weighted")
              truth_hist_pt = f.Get("particle/hist_part_ph_pt_full_weighted")
              migration_hist_pt = f.Get("h2_ph_pt_reco_part_full_weighted")
              response_matrix_pt = calculate_response_matrix(reco_hist_pt, truth_hist_pt, migration_hist_pt)
              response_matrix_pt.SetName("h2_response_matrix_ph_pt")
              response_matrix_pt.Write("h2_response_matrix_ph_pt")
    
              # ph_eta
              reco_hist_eta = f.Get("Reco/hist_reco_ph_eta_full_weighted")
              truth_hist_eta = f.Get("particle/hist_part_ph_eta_full_weighted")
              migration_hist_eta = f.Get("h2_ph_eta_reco_part_full_weighted")
              response_matrix_eta = calculate_response_matrix(reco_hist_eta, truth_hist_eta,
                                                              migration_hist_eta)
              response_matrix_eta.SetName("h2_response_matrix_ph_eta")
              response_matrix_eta.Write("h2_response_matrix_ph_eta")
    
              # dryl1
              reco_hist_dryl1 = f.Get("Reco/hist_reco_ph_drphl1_full_weighted")
              truth_hist_dryl1 = f.Get("particle/hist_part_ph_drphl1_full_weighted")
              migration_hist_dryl1 = f.Get("h2_ph_drphl1_reco_part_full_weighted")
              response_matrix_dryl1 = calculate_response_matrix(reco_hist_dryl1, truth_hist_dryl1,
                                                                migration_hist_dryl1)
              response_matrix_dryl1.SetName("h2_response_matrix_ph_dryl1")
              response_matrix_dryl1.Write("h2_response_matrix_ph_dryl1")
    
              # drphb
              reco_hist_drphb = f.Get("Reco/hist_reco_drphb_full_weighted")
              truth_hist_drphb = f.Get("particle/hist_part_drphb_full_weighted")
              migration_hist_drphb = f.Get("h2_drphb_reco_part_full_weighted")
              response_matrix_drphb = calculate_response_matrix(reco_hist_drphb, truth_hist_drphb,
                                                                migration_hist_drphb)
              response_matrix_drphb.SetName("h2_response_matrix_drphb")
              response_matrix_drphb.Write("h2_response_matrix_drphb")
              # drlj
              reco_hist_drlj = f.Get("Reco/hist_reco_drlj_full_weighted")
              truth_hist_drlj = f.Get("particle/hist_part_drlj_full_weighted")
              migration_hist_drlj = f.Get("h2_drlj_reco_part_full_weighted")
              response_matrix_drlj = calculate_response_matrix(reco_hist_drlj, truth_hist_drlj,
                                                               migration_hist_drlj)
              response_matrix_drlj.SetName("h2_response_matrix_drlj")
              response_matrix_drlj.Write("h2_response_matrix_drlj")
    
              # ph_pt j1
              reco_hist_j1_pt = f.Get("Reco/hist_reco_j1_pt_full_weighted")
              truth_hist_j1_pt = f.Get("particle/hist_part_j1_pt_full_weighted")
              migration_hist_j1_pt = f.Get("h2_j1_pt_reco_part_full_weighted")
              response_matrix_j1_pt = calculate_response_matrix(reco_hist_j1_pt, truth_hist_j1_pt,
                                                                migration_hist_j1_pt)
              response_matrix_j1_pt.SetName("h2_response_matrix_j1_pt")
              response_matrix_j1_pt.Write("h2_response_matrix_j1_pt")
    
              f.Close()
    
            except Exception as e:
                print(f"Error processing file during response calculation: {os.path.join(dirpath, filename)}")
                #self.log_corrupted_files.append("failed during response calculation {}/{}\n".format(os.path.basename(dirpath),filename))
                print(f"An error occurred: {e}")



def print_configurations(args):
    print("Top directory: ", args.top_dir)
    print("Original binning name: ", args.binning_name)
    print("New binning name: ", args.new_binning_name)
    print("Optimized binning name: ", args.optimized_binning)
    print("Maximum relative error value for pt: ", args.max_rel_err_val_changer_pt)
    print("Maximum relative error value for other variables: ", args.max_rel_err_val_changer_others)
    print("Number of CPU threads", args.n_cpus)


if __name__ == "__main__":
    """
    ********************** KEEP THIS CODE FOR REFERENCE ********************
    # Make binning dictionary
    rebin_all_samples_obj = Rebin_all_samples()
    rebin_all_samples_obj.make_dict_hist_bin_bounderies()
    #rebin_all_samples_obj.print_dict_hist_name_and_bin_boundaries()
    CR="CR_sig"
    top_dir = "/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning/{CR}/nominal_9999/".format(CR=CR)
    #top_dir = "/nominal_9999/"
    binning_name = "Fine_Binning_1"
    new_binning_name = "Optimized_Binning_1"
    rebin_all_samples_obj.rebin_and_save_to_file(os.path.abspath(top_dir), binning_name, new_binning_name)
    optimized_binning="Optimized_Binning_1"
    calcualte_and_save_response_matrices(top_dir, optimized_binning)
    """

    parser = argparse.ArgumentParser(description='Rebin and save histograms')
    parser.add_argument('--top_dir', type=str,
                        default='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning/CR_sig/nominal_9999/',
                        help='Top directory')
    parser.add_argument('--binning_name', type=str, default='Fine_Binning_1',
                        help='Original binning name')
    parser.add_argument('--new_binning_name', type=str, default='Optimized_Binning_1',
                        help='New binning name')
    parser.add_argument('--optimized_binning', type=str, default='Optimized_Binning_1',
                        help='Optimized binning name')
    parser.add_argument('--max_rel_err_val_changer_pt', type=float, default=0.0,
                        help='Maximum relative error value for pt')
    parser.add_argument('--max_rel_err_val_changer_others', type=float, default=0.0,
                        help='Maximum relative error value for other variables')
    parser.add_argument('--output_files_for_corrupted_folders', type=str, 
                        help='output_files_for_corrupted_folders')
    parser.add_argument('--n_cpus', type=int, default='4',
                        help='number of threads you want to run on')


    args = parser.parse_args()

    print_configurations(args)

    # Make binning dictionary
    rebin_all_samples_obj = Rebin_all_samples()
    rebin_all_samples_obj.set_max_rel_err_val_changer_pt(args.max_rel_err_val_changer_pt)
    rebin_all_samples_obj.set_max_rel_err_val_changer_others(args.max_rel_err_val_changer_others)
    rebin_all_samples_obj.update_dict_of_hists() # this will refresh the setting of params
    #rebin_all_samples_obj.make_dict_hist_bin_bounderies()
    rebin_all_samples_obj.put_manual_binning_dict()
    #rebin_all_samples_obj.print_dict_hist_name_and_bin_boundaries()
    top_dir = args.top_dir
    n_cpus = args.n_cpus
    binning_name = args.binning_name
    new_binning_name = args.new_binning_name
    print("Info:: Rebin_all_samples.py:: runnning with directory {} \t binning_name {} \t new_binning_name {}\n".format(os.path.abspath(top_dir), binning_name, new_binning_name))
    rebin_all_samples_obj.rebin_and_save_to_file(os.path.abspath(top_dir), binning_name, new_binning_name, n_cpus)
    optimized_binning = args.optimized_binning
    rebin_all_samples_obj.calculate_and_save_response_matrices(top_dir, optimized_binning, n_cpus)

