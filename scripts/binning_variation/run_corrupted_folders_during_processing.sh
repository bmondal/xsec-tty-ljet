#!/usr/bin/env bash
rm -rf maybe_corrupted_folders*.txt

./Find_folders_which_doesnt_contain_nfiles.py --directory ~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/tty_CR/ --nFiles 74 --outfile maybe_corrupted_folders_tty_CR.txt

./Find_folders_which_doesnt_contain_nfiles.py --directory ~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/tty_dec_CR/ --nFiles 74 --outfile maybe_corrupted_folders_tty_dec_CR.txt

./Find_folders_which_doesnt_contain_nfiles.py --directory ~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/fakes_CR/ --nFiles 74 --outfile maybe_corrupted_folders_fakes_CR.txt

./Find_folders_which_doesnt_contain_nfiles.py --directory ~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/other_photons_CR/ --nFiles 74 --outfile maybe_corrupted_folders_other_photons_CR.txt

