def rebin_all_samples(self, top_dir):
    # Recursively search for ROOT files in the top-level directory
    for dirpath, dirnames, filenames in os.walk(top_dir):
        for filename in filenames:
            # Check if the file is a ROOT file and contains "Fine_Binning" in its name
            if filename.endswith('.root') and "Fine_Binning" in filename:
                # Open the original ROOT file
                original_file = ROOT.TFile(os.path.join(dirpath, filename))

                # Create a new ROOT file to store the updated histograms
                new_filename = "updated_" + filename
                new_file = ROOT.TFile(os.path.join(dirpath, new_filename), "RECREATE")
                new_file.cd()

                # Loop over all keys in the original file
                for key in original_file.GetListOfKeys():
                    # Get the object associated with the key
                    obj = key.ReadObj()

                    # Check if the object is a 1D histogram
                    if isinstance(obj, ROOT.TH1) and not isinstance(obj, ROOT.TH2):
                        # Clone the 1D histogram
                        new_hist = obj.Clone()
                        #new_hist.Rebin(len(new_bins) - 1, "", new_bins)
                        # Rebin the 1D histogram
                        try:
                          new_bins = self.dict_hist_name_and_bin_boundaries[new_hist.GetName()]
                          new_hist_name = new_hist.GetName()
                          new_hist.Rebin(len(new_bins) - 1, new_hist_name, new_bins)
                          #rebinned_hist = rebin_hist_1d_preserve_name(new_hist, new_bins)
                          #new_hist.WriteObject(rebinned_hist, "{}".format(key.GetName()), "overwrite")
                          new_hist.Write()
                          print(" Rebinning succeded")
                        except:
                          print("some error in rebinning, probably key error")
                    # Check if the object is a 2D histogram
                    elif isinstance(obj, ROOT.TH2):
                        # Clone the 2D histogram
                        new_hist = obj.Clone()
                        #new_hist.Rebin2D(len(new_bins) - 1, new_bins, len(new_bins) - 1, new_bins)
                        try:
                            corresponding_1d_hist_name = dict_2d_to_1d_hist[
                                new_hist.GetName()]  # get corresponding 1d hist name, so that we can get the bin boundaries
                            new_bins = self.dict_hist_name_and_bin_boundaries[corresponding_1d_hist_name]
                            new_hist.Rebin2D(len(new_bins) - 1, new_bins, len(new_bins) - 1, new_bins)
                            #rebinned_hist = rebin_hist_2d_preserve_name(new_hist, new_bins, new_bins)
                            #new_hist.WriteObject(rebinned_hist, "{}".format(key.GetName()), "overwrite")
                            # obj.Rebin2D(len(new_bins) - 1, new_bins, len(new_bins) - 1, new_bins)
                            new_hist.Write()
                            print(" Rebinning succeded")
                        except:
                            print("some error in rebinning, probably key error")
                    elif isinstance(obj, ROOT.TDirectory):
                        new_file.mkdir(obj.GetName())
                        # Recursively loop over all objects in the directory
                        for sub_key in obj.GetListOfKeys():
                            sub_obj = sub_key.ReadObj()
                            if isinstance(sub_obj, ROOT.TH1) and not isinstance(sub_obj, ROOT.TH2):
                                sub_hist = sub_obj.Clone()
                                try:
                                    new_bins = self.dict_hist_name_and_bin_boundaries[sub_hist.GetName()]
                                    new_hist_name = sub_hist.GetName()
                                    sub_hist.Rebin(len(new_bins) - 1, new_hist_name, new_bins)
                                    #rebinned_hist = rebin_hist_1d_preserve_name(sub_obj, new_bins)
                                    #usb_obj.WriteObject(rebinned_hist, "{}".format(key.GetName()), "overwrite")
                                    # sub_obj.Rebin(len(new_bins) - 1, "{}".format(new_hist_name), new_bins)
                                    sub_hist.Write()
                                    print(" Rebinning succeded")
                                except:
                                    print("some error in rebinning, probably key error")
                            elif isinstance(sub_obj, ROOT.TH2):
                                sub_hist = sub_obj.Clone()
                                try:
                                    corresponding_1d_hist_name = dict_2d_to_1d_hist[
                                        sub_hist.GetName()]  # get corresponding 1d hist name, so that we can get the bin boundaries
                                    new_bins = self.dict_hist_name_and_bin_boundaries[corresponding_1d_hist_name]
                                    sub_hist.Rebin2D(len(new_bins) - 1, new_bins, len(new_bins) - 1, new_bins)
                                    #rebinned_hist = rebin_hist_2d_preserve_name(sub_obj, new_bins, new_bins)
                                    #sub_obj.WriteObject(rebinned_hist, "{}".format(key.GetName()), "overwrite")
                                    # sub_obj.Rebin2D(len(new_bins) - 1, new_bins, len(new_bins) - 1, new_bins)
                                    sub_hist.Write()
                                    print(" Rebinning succeded")
                                except:
                                    print("some error in rebinning, probably key error")
                    else:
                        # If the object is not a histogram, do nothing
                        pass
                    # Write the updated histograms to the new file
                    #new_hist.Write()

                # Close the original file and new file
                original_file.Close()
                new_file.Close()






    # Recursively search for ROOT files in the top-level directory
    for dirpath, dirnames, filenames in os.walk(top_dir):
        for filename in filenames:
            # Check if the file is a ROOT file and contains "Fine_Binning" in its name
            if filename.endswith('.root') and "Fine_Binning" in filename:
                # Open the ROOT file
                f = ROOT.TFile(os.path.join(dirpath, filename), "UPDATE")

                # Loop over all keys in the file
                for key in f.GetListOfKeys():
                    # Get the object associated with the key
                    obj = key.ReadObj()

                    # Check if the object is a 1D histogram
                    if isinstance(obj, ROOT.TH1) and not isinstance(obj, ROOT.TH2):
                        # Rebin the 1D histogram
                        try:
                          new_bins = self.dict_hist_name_and_bin_boundaries[obj.GetName()]
                          new_hist_name = obj.GetName()
                          rebinned_hist = rebin_hist_1d_preserve_name(obj, new_bins)
                          obj.WriteObject(rebinned_hist, "{}".format(key.GetName()), "overwrite")
                          print(" Rebinning succeded")
                        except:
                          print("some error in rebinning, probably key error")
                    # Check if the object is a 2D histogram
                    elif isinstance(obj, ROOT.TH2):
                        # Rebin the 2D histogram
                        try:
                          corresponding_1d_hist_name = dict_2d_to_1d_hist[
                              sub_obj.GetName()]  # get corresponding 1d hist name, so that we can get the bin boundaries
                          new_bins = self.dict_hist_name_and_bin_boundaries[corresponding_1d_hist_name]
                          rebinned_hist = rebin_hist_2d_preserve_name(obj, new_bins, new_bins)
                          obj.WriteObject(rebinned_hist, "{}".format(key.GetName()), "overwrite")
                          #obj.Rebin2D(len(new_bins) - 1, new_bins, len(new_bins) - 1, new_bins)
                          print(" Rebinning succeded")
                        except:
                            print("some error in rebinning, probably key error")
                    elif isinstance(obj, ROOT.TDirectory):
                        # Recursively loop over all objects in the directory
                        for sub_key in obj.GetListOfKeys():
                            sub_obj = sub_key.ReadObj()
                            if isinstance(sub_obj, ROOT.TH1) and not isinstance(sub_obj, ROOT.TH2):
                                try:
                                  new_bins = self.dict_hist_name_and_bin_boundaries[sub_obj.GetName()]
                                  new_hist_name = sub_obj.GetName()
                                  rebinned_hist = rebin_hist_1d_preserve_name(sub_obj, new_bins)
                                  usb_obj.WriteObject(rebinned_hist, "{}".format(key.GetName()), "overwrite")
                                  #sub_obj.Rebin(len(new_bins) - 1, "{}".format(new_hist_name), new_bins)
                                  print(" Rebinning succeded")
                                except:
                                    print("some error in rebinning, probably key error")
                            elif isinstance(sub_obj, ROOT.TH2):
                                try:
                                  corresponding_1d_hist_name = dict_2d_to_1d_hist[sub_obj.GetName()] # get corresponding 1d hist name, so that we can get the bin boundaries
                                  new_bins = self.dict_hist_name_and_bin_boundaries[corresponding_1d_hist_name]
                                  rebinned_hist = rebin_hist_2d_preserve_name(sub_obj, new_bins, new_bins)
                                  sub_obj.WriteObject(rebinned_hist, "{}".format(key.GetName()), "overwrite")
                                  #sub_obj.Rebin2D(len(new_bins) - 1, new_bins, len(new_bins) - 1, new_bins)
                                  print(" Rebinning succeded")
                                except:
                                    print("some error in rebinning, probably key error")
                    else:
                        # If the object is not a histogram, do nothing
                        pass

                # Write the updated histograms to the file
                f.Write()
                # Close the file
                f.Close()