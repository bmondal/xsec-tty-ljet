#!/usr/bin/python

'''
- Plot unfolded spectrum and truth spectrum in a single plot
- Also make the ration plot of Unfolded/Truth 

'''

'''
  c.SetRightMargin(0.09);
  c.SetLeftMargin(0.35);
  c.SetBottomMargin(0.35);
'''

debug = False
draw_non_linear = True 
unfolding_name = "tty_pt"
#unfolding_name = "tty_drphl"
luminosity = 140.1


import argparse
import ROOT
import ctypes

from ROOT import gROOT

histo_dict={}
histo_dict["tty1l_pt_all_stat"] = "hist_part_ph_pt_full_weighted"
histo_dict["tty1l_eta_all_stat"] = "hist_part_ph_eta_full_weighted"
histo_dict["tty1l_dr_all_stat"] = "hist_part_ph_drphl1_full_weighted"
histo_dict["tty1l_drphb_all_stat"] = "hist_part_drphb_full_weighted"
histo_dict["tty1l_drlj_all_stat"] = "hist_part_drlj_full_weighted"
histo_dict["tty1l_ptj1_all_stat"] = "hist_part_j1_pt_full_weighted"

def extract_name_from_path(path_): # this will    /afs/cern.ch./.../tty2l_pt_all_stat_linear_reweighted_y1 and retrun  tty2l_pt_all_stat. we need this later for key matching from dict
  splitted_string = path_.split("/")
  trex_folder_name = splitted_string[len(splitted_string) -2]
  split_trex_folder=trex_folder_name.split("_")
  folder = split_trex_folder[0] # folder will contain folder name without "linear_reweighted_bla_bla"
  for i in range(1, len(split_trex_folder)-3 ):
    folder = folder+'_'+split_trex_folder[i]
  return folder


def graph_to_hist(graph):
  n_points = graph.GetN()
  hist = ROOT.TH1D()
  for i in range(0, n_points):
    x=ctypes.c_double()
    y=ctypes.c_double()
    graph.GetPoint(i,x,y)
    hist.Fill(x,y)
  return hist
 
def divide_by_bin_width(hist_truth):
  print(">>>>>>>>>>> hist name: {} <<<<<<<<<<<<<<<<<<<<<\n".format(hist_truth.GetName()))
  for i in range(1, hist_truth.GetNbinsX() + 1):
    bin_content = 0
    bin_width = 0
    value = 0
    bin_content = hist_truth.GetBinContent(i)
    bin_width = hist_truth.GetBinWidth(i)
    value = bin_content/(bin_width*luminosity)
    print("bin content: {0}; bin width: {1}; value: {2}".format(bin_content, bin_width, value))
    hist_truth.SetBinContent(i, value)
    hist_truth.SetBinError(i, 0)
  return hist_truth


class StressTest:
  #def __init__(self,path_truth,path_linear_y1, path_linear_y_1, path_nonlinear_y1, path_nonlinear_y_1, path_truth_linear_y1, path_truth_linear_y_1, path_truth_nonlinear_y1, path_truth_nonlinear_y_1):
  def __init__(self,path_truth,path_linear_y1, path_linear_y_1, path_nonlinear_y1, path_nonlinear_y_1):
    # ----------- configurable parameters ----------------------------
    self.path_linear_y1 = path_linear_y1 
    self.path_linear_y_1 = path_linear_y_1
    self.path_nonlinear_y1 = path_nonlinear_y1 
    self.path_nonlinear_y_1 = path_nonlinear_y_1

    self.path_truth = path_truth 
    #-------------
    #self.path_truth_linear_y1= path_truth_linear_y1
    #self.path_truth_linear_y_1= path_truth_linear_y_1
    #self.path_truth_nonlinear_y_1= path_truth_nonlinear_y1
    #self.path_truth_nonlinear_y_1= path_truth_nonlinear_y_1
  
    #self.path_unfolded_hist_y1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_linear_y1,unfolding_name)
    #self.path_unfolded_hist_y_1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_linear_y_1,unfolding_name)
    #self.path_unfolded_hist_nonlinear_y1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_nonlinear_y1,unfolding_name)
    #self.path_unfolded_hist_nonlinear_y_1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_nonlinear_y_1,unfolding_name)

    self.path_unfolded_hist_y1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_linear_y1)
    self.path_unfolded_hist_y_1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_linear_y_1)
    self.path_unfolded_hist_nonlinear_y1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_nonlinear_y1)
    self.path_unfolded_hist_nonlinear_y_1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_nonlinear_y_1)


    # truth # this is hack, bottomo commented out one is for tty_prod stress test
    self.path_truth_hist = "{}/histograms.ttgamma_prod.root".format(self.path_truth)
    self.path_truth_hist_linear_y1 = "{}/histograms.ttgamma_prod.linear_reweighted_y1.0.root".format(self.path_truth)
    self.path_truth_hist_linear_y_1 = "{}/histograms.ttgamma_prod.linear_reweighted_y-1.0.root".format(self.path_truth)
    self.path_truth_hist_nonlinear_y1 = "{}/ttgamma_prod_reweighted_y1.0.root".format(self.path_truth)
    self.path_truth_hist_nonlinear_y_1 = "{}/ttgamma_prod_reweighted_y-1.0.root".format(self.path_truth)

    #self.path_truth_hist_linear_y1 = "{}/histograms.ttgamma_incl_dec.linear_reweighted_y1.0.root".format(self.path_truth)
    #self.path_truth_hist_linear_y_1 = "{}/histograms.ttgamma_incl_dec.linear_reweighted_y-1.0.root".format(self.path_truth)
    #self.path_truth_hist_nonlinear_y1 = "{}/histograms.ttgamma_incl_dec_reweighted_y1.0.root".format(self.path_truth)
    #self.path_truth_hist_nonlinear_y_1 = "{}/histograms.ttgamma_incl_dec_reweighted_y-1.0.root".format(self.path_truth)
  
  def process_line(self,line):
    arr = line.split(" ")
    if debug: print(arr)
    return int(arr[0]), float(arr[1]), float(arr[2]), float(arr[3])
  
  def unfolded_hist_from_fit_file(self,file_path, empty_hist):
    hist = empty_hist.Clone()
    file = open(file_path)
    readline = False
    for line in file.readlines():
      if line.strip():
        if(line[0] == '1'): 
          readline = True 
          if debug: print("start of reading line\n")
        if readline:
          if debug: print(line)
          bin_number, nominal, err_up, err_down = self.process_line(line)
          hist.SetBinContent(bin_number, nominal)
          hist.SetBinError(bin_number, err_up)
          print("Bin number: {0}, nominal: {1}, up: {2}, down: {3} \n".format(bin_number, nominal, err_up, err_down))
      else:
        if debug: print("line is empty")
        break
    return hist
 
  def createCanvasPads(self):
    c = ROOT.TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    #pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    #pad2.SetGridx()
    #pad2.SetGridy()
    pad2.Draw()
  
    return c, pad1, pad2
  
  
  def correct_underflow_overflow_bin(self,hist):
    underflow_content = hist.GetBinContent(0)
    overflow_content = hist.GetBinContent(hist.GetNbinsX()+1)
    hist.SetBinContent(1, hist.GetBinContent(1)+underflow_content)
    hist.SetBinContent(hist.GetNbinsX(), hist.GetBinContent(hist.GetNbinsX())+overflow_content)
    return hist
  
  def run(self):
    ROOT.gROOT.SetStyle('ATLAS')
    file_truth = ROOT.TFile.Open(self.path_truth_hist)
    file_truth_linear_y1 = ROOT.TFile.Open(self.path_truth_hist_linear_y1)
    file_truth_linear_y_1 = ROOT.TFile.Open(self.path_truth_hist_linear_y_1)
    file_truth_nonlinear_y1 = ROOT.TFile.Open(self.path_truth_hist_nonlinear_y1)
    file_truth_nonlinear_y_1 = ROOT.TFile.Open(self.path_truth_hist_nonlinear_y_1)

    hist_name = histo_dict[extract_name_from_path(self.path_linear_y1)] # get the histo name from the path
    hist_truth_tmp = file_truth.Get("particle/{}".format(hist_name)) # get the hist here
    hist_truth_linear_y1_tmp = file_truth_linear_y1.Get("particle/{}".format(hist_name)) # get the hist here
    hist_truth_linear_y_1_tmp = file_truth_linear_y_1.Get("particle/{}".format(hist_name)) # get the hist here
    hist_truth_nonlinear_y1_tmp = file_truth_nonlinear_y1.Get("particle/{}".format(hist_name)) # get the hist here
    hist_truth_nonlinear_y_1_tmp = file_truth_nonlinear_y_1.Get("particle/{}".format(hist_name)) # get the hist here

    hist_truth_tmp_tmp = hist_truth_tmp
    hist_truth_linear_y1_tmp_tmp = hist_truth_linear_y1_tmp
    hist_truth_linear_y_1_tmp_tmp = hist_truth_linear_y_1_tmp
    hist_truth_nonlinear_y1_tmp_tmp = hist_truth_nonlinear_y1_tmp
    hist_truth_nonlinear_y_1_tmp_tmp = hist_truth_nonlinear_y_1_tmp

    #hist_truth_tmp_tmp = self.correct_underflow_overflow_bin(hist_truth_tmp) # correct overflow and underflow bin
    #hist_truth_linear_y1_tmp_tmp = self.correct_underflow_overflow_bin(hist_truth_linear_y1_tmp) # correct overflow and underflow bin 
    #hist_truth_linear_y_1_tmp_tmp = self.correct_underflow_overflow_bin(hist_truth_linear_y_1_tmp) # correct overflow and underflow bin 
    #hist_truth_nonlinear_y1_tmp_tmp = self.correct_underflow_overflow_bin(hist_truth_nonlinear_y1_tmp) # correct overflow and underflow bin
    #hist_truth_nonlinear_y_1_tmp_tmp = self.correct_underflow_overflow_bin(hist_truth_nonlinear_y_1_tmp) # correct overflow and underflow bin
    # divide by bin width
    hist_truth = divide_by_bin_width(hist_truth_tmp_tmp)
    hist_truth_linear_y1 = divide_by_bin_width(hist_truth_linear_y1_tmp_tmp) 
    hist_truth_linear_y_1 = divide_by_bin_width(hist_truth_linear_y_1_tmp_tmp)
    hist_truth_nonlinear_y1 = divide_by_bin_width(hist_truth_nonlinear_y1_tmp_tmp)
    hist_truth_nonlinear_y_1 = divide_by_bin_width(hist_truth_nonlinear_y_1_tmp_tmp)
  
    #graph_y1 = unfolded_plot_canvas_y1.FindObject("Graph_from_hist_part_ph_pt_full_weighted")
    #print(graph_y1.ls())
    #graph_y_1 = unfolded_plot_canvas_y_1.FindObject("Graph_from_hist_part_ph_pt_full_weighted")
    #print(graph_y_1.ls())
    #hist_y1 = graph_to_hist(graph_y1)
    #hist_y_1 = graph_to_hist(graph_y_1)
  
    hist_y1 = self.unfolded_hist_from_fit_file(self.path_unfolded_hist_y1, hist_truth) # passing hist_truth as a sample hist; it will be cloned there
    hist_y_1 = self.unfolded_hist_from_fit_file(self.path_unfolded_hist_y_1, hist_truth)
    hist_nonlinear_y1 = self.unfolded_hist_from_fit_file(self.path_unfolded_hist_nonlinear_y1, hist_truth)
    hist_nonlinear_y_1 = self.unfolded_hist_from_fit_file(self.path_unfolded_hist_nonlinear_y_1, hist_truth)
  
    hist_clone_y1 = hist_y1.Clone()
    hist_clone_y_1 = hist_y_1.Clone()
    hist_clone_nonlinear_y1 = hist_nonlinear_y1.Clone()
    hist_clone_nonlinear_y_1 = hist_nonlinear_y_1.Clone()

    # clone the reweighted particle level ones
    hist_truth_linear_y1_clone = hist_truth_linear_y1.Clone()
    hist_truth_linear_y_1_clone = hist_truth_linear_y_1.Clone()
    hist_truth_nonlinear_y1_clone = hist_truth_nonlinear_y1.Clone()
    hist_truth_nonlinear_y_1_clone = hist_truth_nonlinear_y_1.Clone()

    # for unfolded linear, non-linear
    hist_y1.SetMarkerColor(4)
    hist_y1.SetLineColor(4)
    hist_y1.SetMarkerStyle(20)
    hist_y1.SetMarkerSize(0.9)
    hist_y_1.SetMarkerColor(6)
    hist_y_1.SetLineColor(6)
    hist_y_1.SetMarkerStyle(20)
    hist_y_1.SetMarkerSize(0.9)
    if draw_non_linear: 
      hist_nonlinear_y1.SetMarkerColor(45)
      hist_nonlinear_y1.SetLineColor(45)
      hist_nonlinear_y1.SetMarkerStyle(20)
      hist_nonlinear_y1.SetMarkerSize(0.9)
      hist_nonlinear_y_1.SetMarkerColor(3)
      hist_nonlinear_y_1.SetLineColor(3)
      hist_nonlinear_y_1.SetMarkerStyle(20)
      hist_nonlinear_y_1.SetMarkerSize(0.9)
    # for truth linear non-linear
    hist_truth_linear_y1.SetLineColor(4)
    hist_truth_linear_y_1.SetLineColor(6)
    hist_truth_nonlinear_y1.SetLineColor(45)
    hist_truth_nonlinear_y_1.SetLineColor(3)


    hist_truth_clone = hist_truth.Clone()
    hist_truth.SetLineColor(2)
    canvas, pad1, pad2 = self.createCanvasPads()
    pad1.cd()
    hist_y1.GetXaxis().SetLabelFont(63)
    hist_y1.GetXaxis().SetLabelSize(14)
    hist_y1.GetYaxis().SetLabelFont(63)
    hist_y1.GetYaxis().SetLabelSize(14)
    hist_y1.GetYaxis().SetTitleSize(0.05)
    #hist_y1.GetXaxis().SetTitle("p_{T}(#gamma) [GeV]")
    hist_y1.GetYaxis().SetTitle("events / GeV")

    hist_y1.SetMaximum(hist_y1.GetMaximum() * 1.5)  # this adujst that the histogram doesn't get covered by the legend

    hist_y1.SetStats(False)
    hist_y1.Draw("hist PE")
    hist_y_1.Draw("hist same PE")
    if draw_non_linear: hist_nonlinear_y1.Draw("hist same PE")
    if draw_non_linear: hist_nonlinear_y_1.Draw("hist same PE")
    hist_truth.Draw("hist same E")
    hist_truth_linear_y1.Draw("hist same E")
    hist_truth_linear_y_1.Draw("hist same E")
    if draw_non_linear: hist_truth_nonlinear_y1.Draw("hist same E")
    if draw_non_linear: hist_truth_nonlinear_y_1.Draw("hist same E")
    # legend
    legend = ROOT.TLegend(0.5,0.6, 0.9,0.9)
    legend.AddEntry(hist_y1, "1 + X(pseudo-data unfolded)", "P")
    legend.AddEntry(hist_y_1, " 1 - X(pseudo-data unfolded)", "P")
    legend.AddEntry(hist_truth_linear_y1, "1 + X(truth reweighted)", "L")
    legend.AddEntry(hist_truth_linear_y_1, " 1 - X(truth reweighted)", "L")

    if draw_non_linear:
      legend.AddEntry(hist_nonlinear_y1, "1 + 1*Obs(pseudo-data unfolded)", "P")
      legend.AddEntry(hist_nonlinear_y_1, " 1 - 1*Obs(pseudo-data unfolded)", "P")
      legend.AddEntry(hist_truth_nonlinear_y1, "1 + 1*Obs(truth reweighted)", "L")
      legend.AddEntry(hist_truth_nonlinear_y_1, " 1 - 1*Obs(truth reweighted)", "L")

    legend.AddEntry(hist_truth, " truth", "L")
    legend.Draw()
    #latex = ROOT.TLatex()
    #latex.SetTextSize(0.030)
    #latex.SetTextAlign(13) #align at top
    #latex.DrawLatex(.2,.9,"#it{ATLAS} Internal")
    #latex.DrawLatex(.2,.8,"#sqrt{S} = 13 TeV, 139 fb^{-1}")
    #latex.DrawLatex(.2,.7," dilepton channel")
    pad2.cd()
    hist_clone_y1.Divide(hist_truth_clone)
    hist_clone_nonlinear_y1.Divide(hist_truth_clone)
    hist_clone_y1.GetYaxis().SetRangeUser(0.35,1.65)
    hist_clone_y_1.Divide(hist_truth_clone)
    hist_clone_nonlinear_y_1.Divide(hist_truth_clone)
    hist_clone_y1.SetMarkerColor(4)
    hist_clone_y1.SetLineColor(4)
    hist_clone_y1.SetMarkerStyle(20)
    hist_clone_y1.SetMarkerSize(0.9)
    hist_clone_y_1.SetMarkerColor(6)
    hist_clone_y_1.SetLineColor(6)
    hist_clone_y_1.SetMarkerStyle(20)
    hist_clone_y_1.SetMarkerSize(0.9)
    if draw_non_linear: 
      hist_clone_nonlinear_y1.SetMarkerColor(45)
      hist_clone_nonlinear_y1.SetLineColor(45)
      hist_clone_nonlinear_y1.SetMarkerStyle(20)
      hist_clone_nonlinear_y1.SetMarkerSize(0.9)
      hist_clone_nonlinear_y_1.SetMarkerColor(3)
      hist_clone_nonlinear_y_1.SetLineColor(3)
      hist_clone_nonlinear_y_1.SetMarkerStyle(20)
      hist_clone_nonlinear_y_1.SetMarkerSize(0.9)

    hist_truth_linear_y1_clone.Divide(hist_truth_clone)
    hist_truth_linear_y_1_clone.Divide(hist_truth_clone)
    hist_truth_nonlinear_y1_clone.Divide(hist_truth_clone)
    hist_truth_nonlinear_y_1_clone.Divide(hist_truth_clone)
    hist_truth_linear_y1_clone.SetLineColor(4)
    #hist_truth_linear_y1_clone.SetLineStyle(3)
    hist_truth_linear_y_1_clone.SetLineColor(6)
    #hist_truth_linear_y_1_clone.SetLineStyle(3)
    hist_truth_nonlinear_y1_clone.SetLineColor(45)
    hist_truth_nonlinear_y_1_clone.SetLineColor(3)

    hist_clone_y1.GetXaxis().SetLabelFont(63)
    hist_clone_y1.GetXaxis().SetLabelSize(14)
    hist_clone_y1.GetYaxis().SetLabelFont(63)
    hist_clone_y1.GetYaxis().SetLabelSize(14)
    hist_clone_y1.GetYaxis().SetTitleSize(0.09)
    hist_clone_y1.GetXaxis().SetTitleSize(0.08)
    hist_clone_y1.GetYaxis().SetTitleOffset(0.4)  # Adjust offset for better appearance
    hist_clone_y1.GetYaxis().SetTitle("Unfolded / Truth")
    #hist_clone_y1.GetXaxis().SetTitle("p_{T}(#gamma)")

    hist_clone_y1.SetStats(False)
    hist_clone_y1.Draw("hist LEP")
    hist_clone_y_1.Draw("hist same LEP")
    if draw_non_linear: hist_clone_nonlinear_y1.Draw("hist same PE")
    if draw_non_linear: hist_clone_nonlinear_y_1.Draw("hist same PE")
    hist_truth_linear_y1_clone.Draw("hist same e ")
    hist_truth_linear_y_1_clone.Draw("hist same e")
    if draw_non_linear: hist_truth_nonlinear_y1_clone.Draw("hist same e")
    if draw_non_linear: hist_truth_nonlinear_y_1_clone.Draw("hist same e ")

    save_name = extract_name_from_path(self.path_linear_y1)+".pdf"
    save_name_root = extract_name_from_path(self.path_linear_y1)+".root"
    save_name_c = extract_name_from_path(self.path_linear_y1)+".C"
    canvas.SaveAs(save_name)
    canvas.SaveAs(save_name_root)
    canvas.SaveAs(save_name_c)
  
    # print chiSquare
    #chisquare = hist_truth.GetChiSquare(hist)
    #print("chi square : {}".format(chisquare))


if __name__== '__main__':
  # paths where the unfolded and truth files are stored
  #path_linear_y1 = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/abs-xsec-with-trexfiles/v12/v12/stat-all/tty2l_pt_all_stat_linear_reweighted_y1/"
  #path_linear_y_1 = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/abs-xsec-with-trexfiles/v12/v12/stat-all/tty2l_pt_all_stat_linear_reweighted_y-1/"
  #path_nonlinear_y1 = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/abs-xsec-with-trexfiles/v12/v12/stat-all/tty2l_pt_all_stat_nonlinear_reweighted_y1/"
  #path_nonlinear_y_1 = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/abs-xsec-with-trexfiles/v12/v12/stat-all/tty2l_pt_all_stat_nonlinear_reweighted_y-1/"
  #path_truth = "/afs/cern.ch/user/b/bmondal/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v7/NN06/CR_sig/nominal_9999/"

  parser = argparse.ArgumentParser(description ='create trex-config file for ph pt')
  parser.add_argument("--path-trex-fit-linear-y1", type=str, help="config file name")
  parser.add_argument("--path-trex-fit-linear-y-1", type=str, help="config file name")
  parser.add_argument("--path-trex-fit-nonlinear-y1", type=str, help="config file name")
  parser.add_argument("--path-trex-fit-nonlinear-y-1", type=str, help="config file name")
  parser.add_argument("--path-truth", type=str, help="config file name")
  args = parser.parse_args()
  # for pt
  stress_test_pt = StressTest(args.path_truth, args.path_trex_fit_linear_y1, args.path_trex_fit_linear_y_1, args.path_trex_fit_nonlinear_y1, args.path_trex_fit_nonlinear_y_1)
  stress_test_pt.run()
