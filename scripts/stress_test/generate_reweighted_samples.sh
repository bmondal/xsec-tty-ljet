# Generate non linear reweighted smaples
function non_linear_reweight {
  CR=""
  path_to_unfolding_inputs="${HOME}/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/"
  #rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/ttgamma_prod"
  #rootfile_all_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.all_MCs.root"
  #rootfile_data="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.data.root"
  # ---
  # delete all_MCs(all the simulation added) and create again; (remember we need this for non-linear reweighting)
  # Go to tty_CR and create a all_MCs adding all the MCs
  pushd "$path_to_unfolding_inputs/tty_CR/nominal_9999/" || exit
  rm  histograms.all_MCs.root
  # hadding all the histograms
  hadd histograms.all_MCs.root histograms.ttgamma_prod.root histograms.Wty.root histograms.ttv.root histograms.ttgamma_dec.root histograms.efake_ttv.root histograms.wgamma.root histograms.efake_ttbar.root histograms.hfake_ttv.root histograms.singletop.root histograms.hfake_ttbar.root histograms.efake_tty_NLO_prod.root histograms.zgamma.root histograms.efake_diboson.root histograms.ttbar.root histograms.hfake_tty_NLO_prod.root histograms.efake_wt_inclusive.root histograms.hfake_wjets.root histograms.efake_wty.root histograms.efake_zjets.root histograms.hfake_wt_inclusive.root histograms.efake_tty_LO_dec.root histograms.diboson.root histograms.hfake_tty_LO_dec.root histograms.hfake_zjets.root histograms.hfake_wty.root histograms.hfake_singletop.root histograms.zjets.root histograms.hfake_diboson.root histograms.wt_inclusive.root histograms.wjets.root histograms.efake_zgamma.root histograms.efake_wjets.root histograms.hfake_wgamma.root histograms.efake_singletop.root histograms.hfake_zgamma.root histograms.efake_wgamma.root
  popd || exit
  # Go to tty_dec_CR and create a all_MCs adding all the MCs
  pushd "$path_to_unfolding_inputs/tty_dec_CR/nominal_9999/" || exit
  rm  histograms.all_MCs.root
  # hadding all the histograms
  hadd histograms.all_MCs.root histograms.ttgamma_prod.root histograms.Wty.root histograms.ttv.root histograms.ttgamma_dec.root histograms.efake_ttv.root histograms.wgamma.root histograms.efake_ttbar.root histograms.hfake_ttv.root histograms.singletop.root histograms.hfake_ttbar.root histograms.efake_tty_NLO_prod.root histograms.zgamma.root histograms.efake_diboson.root histograms.ttbar.root histograms.hfake_tty_NLO_prod.root histograms.efake_wt_inclusive.root histograms.hfake_wjets.root histograms.efake_wty.root histograms.efake_zjets.root histograms.hfake_wt_inclusive.root histograms.efake_tty_LO_dec.root histograms.diboson.root histograms.hfake_tty_LO_dec.root histograms.hfake_zjets.root histograms.hfake_wty.root histograms.hfake_singletop.root histograms.zjets.root histograms.hfake_diboson.root histograms.wt_inclusive.root histograms.wjets.root histograms.efake_zgamma.root histograms.efake_wjets.root histograms.hfake_wgamma.root histograms.efake_singletop.root histograms.hfake_zgamma.root histograms.efake_wgamma.root
  popd || exit
  # Go to fakes_CR and create a all_MCs adding all the MCs
  pushd "$path_to_unfolding_inputs/fakes_CR/nominal_9999/" || exit
  rm  histograms.all_MCs.root
  # hadding all the histograms
  hadd histograms.all_MCs.root histograms.ttgamma_prod.root histograms.Wty.root histograms.ttv.root histograms.ttgamma_dec.root histograms.efake_ttv.root histograms.wgamma.root histograms.efake_ttbar.root histograms.hfake_ttv.root histograms.singletop.root histograms.hfake_ttbar.root histograms.efake_tty_NLO_prod.root histograms.zgamma.root histograms.efake_diboson.root histograms.ttbar.root histograms.hfake_tty_NLO_prod.root histograms.efake_wt_inclusive.root histograms.hfake_wjets.root histograms.efake_wty.root histograms.efake_zjets.root histograms.hfake_wt_inclusive.root histograms.efake_tty_LO_dec.root histograms.diboson.root histograms.hfake_tty_LO_dec.root histograms.hfake_zjets.root histograms.hfake_wty.root histograms.hfake_singletop.root histograms.zjets.root histograms.hfake_diboson.root histograms.wt_inclusive.root histograms.wjets.root histograms.efake_zgamma.root histograms.efake_wjets.root histograms.hfake_wgamma.root histograms.efake_singletop.root histograms.hfake_zgamma.root histograms.efake_wgamma.root
  popd || exit
  # Go to other_photons_CR and create a all_MCs adding all the MCs
  pushd "$path_to_unfolding_inputs/other_photons_CR/nominal_9999/" || exit
  rm  histograms.all_MCs.root
  # hadding all the histograms
  hadd histograms.all_MCs.root histograms.ttgamma_prod.root histograms.Wty.root histograms.ttv.root histograms.ttgamma_dec.root histograms.efake_ttv.root histograms.wgamma.root histograms.efake_ttbar.root histograms.hfake_ttv.root histograms.singletop.root histograms.hfake_ttbar.root histograms.efake_tty_NLO_prod.root histograms.zgamma.root histograms.efake_diboson.root histograms.ttbar.root histograms.hfake_tty_NLO_prod.root histograms.efake_wt_inclusive.root histograms.hfake_wjets.root histograms.efake_wty.root histograms.efake_zjets.root histograms.hfake_wt_inclusive.root histograms.efake_tty_LO_dec.root histograms.diboson.root histograms.hfake_tty_LO_dec.root histograms.hfake_zjets.root histograms.hfake_wty.root histograms.hfake_singletop.root histograms.zjets.root histograms.hfake_diboson.root histograms.wt_inclusive.root histograms.wjets.root histograms.efake_zgamma.root histograms.efake_wjets.root histograms.hfake_wgamma.root histograms.efake_singletop.root histograms.hfake_zgamma.root histograms.efake_wgamma.root
  popd || exit
  # ------

  # lets add all regions data and MC; from this we will create the weights; the reason we do this because same weights should be applied to all the regions as we have only one particle level
  rm all_data.root all_mc.root
  hadd all_mc.root "$path_to_unfolding_inputs/tty_CR/nominal_9999/histograms.all_MCs.root"  "$path_to_unfolding_inputs/tty_dec_CR/nominal_9999/histograms.all_MCs.root" "$path_to_unfolding_inputs/fakes_CR/nominal_9999/histograms.all_MCs.root" "$path_to_unfolding_inputs/other_photons_CR/nominal_9999/histograms.all_MCs.root"
  hadd all_data.root "$path_to_unfolding_inputs/tty_CR/nominal_9999/histograms.data.root"  "$path_to_unfolding_inputs/tty_dec_CR/nominal_9999/histograms.data.root" "$path_to_unfolding_inputs/fakes_CR/nominal_9999/histograms.data.root" "$path_to_unfolding_inputs/other_photons_CR/nominal_9999/histograms.data.root"

  # Generate reweighted histograms for tty_CR
  CR="tty_CR"
  rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_dec.root"
  outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/ttgamma_prod"
  #outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_dec"
  # ttgamma_prod NN06 CR_sig Y = 1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc "$rootfile_signal_mc" --rootfile_all_data all_data.root  --outfile $outfile --param_Y 1
  # ttgamma_prod NN06 CR_sig Y = -1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc "$rootfile_signal_mc" --rootfile_all_data all_data.root --outfile $outfile --param_Y -1

  # Generate reweighted histograms for tty_dec_CR
  CR="tty_dec_CR"
  rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_dec.root"
  outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/ttgamma_prod"
  #outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_dec"
  # ttgamma_prod NN06 CR_sig Y = 1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc "$rootfile_signal_mc" --rootfile_all_data all_data.root  --outfile $outfile --param_Y 1
  # ttgamma_prod NN06 CR_sig Y = -1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc "$rootfile_signal_mc" --rootfile_all_data all_data.root --outfile $outfile --param_Y -1

  # Generate reweighted histograms for tty_dec_CR
  CR="fakes_CR"
  rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_dec.root"
  outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/ttgamma_prod"
  #outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_dec"
  # ttgamma_prod NN06 CR_sig Y = 1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc "$rootfile_signal_mc" --rootfile_all_data all_data.root  --outfile $outfile --param_Y 1
  # ttgamma_prod NN06 CR_sig Y = -1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc "$rootfile_signal_mc" --rootfile_all_data all_data.root --outfile $outfile --param_Y -1

    # Generate reweighted histograms for tty_dec_CR
  CR="other_photons_CR"
  rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_signal_mc="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_dec.root"
  outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/ttgamma_prod"
  #outfile="$path_to_unfolding_inputs/${CR}/nominal_9999/histograms.ttgamma_dec"
  # ttgamma_prod NN06 CR_sig Y = 1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc "$rootfile_signal_mc" --rootfile_all_data all_data.root  --outfile $outfile --param_Y 1
  # ttgamma_prod NN06 CR_sig Y = -1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc "$rootfile_signal_mc" --rootfile_all_data all_data.root --outfile $outfile --param_Y -1
}

# Generate linear reweighted smaples
function linear_reweight {
  # ttgamma_prod NN06 CR_sig Y = 1
  CR="tty_CR"
  path_to_unfolding_inputs="/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/"
  rootfile_mc="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_mc="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_dec.root"
  outfile="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_prod"
  #outfile="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_dec"
  ./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y 1
  #
  ## ttgamma_prod NN06 CR_sig Y = -1
  ./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y -1
  #
  CR="tty_dec_CR"
  rootfile_mc="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_mc="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_dec.root"
  outfile="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_prod"
  #outfile="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_dec"
  
  ## ttgamma_prod Y = 1
  ./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y 1
  #
  ## ttgamma_prod Y = -1
  ./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y -1
  
  CR="fakes_CR"
  rootfile_mc="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_mc="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_dec.root"
  outfile="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_prod"
  #outfile="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_dec"
  
  ## ttgamma_prod Y = 1
  ./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y 1
  #
  ## ttgamma_prod Y = -1
  ./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y -1
  
  CR="other_photons_CR"
  rootfile_mc="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_mc="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_dec.root"
  outfile="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_prod"
  #outfile="${path_to_unfolding_inputs}/${CR}/nominal_9999/histograms.ttgamma_dec"
  
  ## ttgamma_prod NN06 CR_bkg Y = 1
  ./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y 1
  #
  ## ttgamma_prod NN06 CR_bkg Y = -1
  ./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y -1
}

# Generate linear reweighted smaples
linear_reweight
#non_linear_reweight
