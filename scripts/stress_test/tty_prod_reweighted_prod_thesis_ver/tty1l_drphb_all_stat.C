#ifdef __CLING__
#pragma cling optimize(0)
#endif
void tty1l_drphb_all_stat()
{
//=========Macro generated from canvas: c/canvas
//=========  (Sat May 18 15:22:22 2024) by ROOT version 6.28/04
   TCanvas *c = new TCanvas("c", "canvas",0,0,800,800);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c->SetHighLightColor(2);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetTickx(1);
   c->SetTicky(1);
   c->SetLeftMargin(0.16);
   c->SetRightMargin(0.05);
   c->SetTopMargin(0.05);
   c->SetBottomMargin(0.16);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: pad1
   TPad *pad1 = new TPad("pad1", "pad1",0,0.3,1,1);
   pad1->Draw();
   pad1->cd();
   pad1->Range(-0.2075949,24.70917,3.589873,192.0246);
   pad1->SetFillColor(0);
   pad1->SetBorderMode(0);
   pad1->SetBorderSize(2);
   pad1->SetTickx(1);
   pad1->SetTicky(1);
   pad1->SetLeftMargin(0.16);
   pad1->SetRightMargin(0.05);
   pad1->SetTopMargin(0.05);
   pad1->SetBottomMargin(0);
   pad1->SetFrameBorderMode(0);
   pad1->SetFrameBorderMode(0);
   Double_t xAxis1[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__1 = new TH1D("hist_part_drphb_full_weighted__1","",5, xAxis1);
   hist_part_drphb_full_weighted__1->SetBinContent(1,82.1878);
   hist_part_drphb_full_weighted__1->SetBinContent(2,121.0013);
   hist_part_drphb_full_weighted__1->SetBinContent(3,122.4392);
   hist_part_drphb_full_weighted__1->SetBinContent(4,82.473);
   hist_part_drphb_full_weighted__1->SetBinContent(5,35.45);
   hist_part_drphb_full_weighted__1->SetBinError(1,7.1399);
   hist_part_drphb_full_weighted__1->SetBinError(2,6.9961);
   hist_part_drphb_full_weighted__1->SetBinError(3,5.6201);
   hist_part_drphb_full_weighted__1->SetBinError(4,4.0802);
   hist_part_drphb_full_weighted__1->SetBinError(5,3.1718);
   hist_part_drphb_full_weighted__1->SetMaximum(183.6588);
   hist_part_drphb_full_weighted__1->SetEntries(3589753);
   hist_part_drphb_full_weighted__1->SetStats(0);
   hist_part_drphb_full_weighted__1->SetLineColor(4);
   hist_part_drphb_full_weighted__1->SetMarkerColor(4);
   hist_part_drphb_full_weighted__1->SetMarkerStyle(20);
   hist_part_drphb_full_weighted__1->SetMarkerSize(0.9);
   hist_part_drphb_full_weighted__1->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__1->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__1->GetXaxis()->SetLabelFont(63);
   hist_part_drphb_full_weighted__1->GetXaxis()->SetLabelSize(14);
   hist_part_drphb_full_weighted__1->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__1->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__1->GetYaxis()->SetTitle("events / GeV");
   hist_part_drphb_full_weighted__1->GetYaxis()->SetLabelFont(63);
   hist_part_drphb_full_weighted__1->GetYaxis()->SetLabelSize(14);
   hist_part_drphb_full_weighted__1->GetYaxis()->SetTitleSize(0.05);
   hist_part_drphb_full_weighted__1->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__1->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__1->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__1->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__1->Draw("hist PE");
   Double_t xAxis2[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__2 = new TH1D("hist_part_drphb_full_weighted__2","",5, xAxis2);
   hist_part_drphb_full_weighted__2->SetBinContent(1,50.6602);
   hist_part_drphb_full_weighted__2->SetBinContent(2,90.0244);
   hist_part_drphb_full_weighted__2->SetBinContent(3,112.4315);
   hist_part_drphb_full_weighted__2->SetBinContent(4,100.033);
   hist_part_drphb_full_weighted__2->SetBinContent(5,69.5651);
   hist_part_drphb_full_weighted__2->SetBinError(1,6.4035);
   hist_part_drphb_full_weighted__2->SetBinError(2,6.3591);
   hist_part_drphb_full_weighted__2->SetBinError(3,5.3866);
   hist_part_drphb_full_weighted__2->SetBinError(4,4.3008);
   hist_part_drphb_full_weighted__2->SetBinError(5,3.6437);
   hist_part_drphb_full_weighted__2->SetEntries(3589753);
   hist_part_drphb_full_weighted__2->SetLineColor(6);
   hist_part_drphb_full_weighted__2->SetMarkerColor(6);
   hist_part_drphb_full_weighted__2->SetMarkerStyle(20);
   hist_part_drphb_full_weighted__2->SetMarkerSize(0.9);
   hist_part_drphb_full_weighted__2->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__2->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__2->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__2->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__2->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__2->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__2->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__2->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__2->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__2->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__2->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__2->Draw("hist same PE");
   Double_t xAxis3[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__3 = new TH1D("hist_part_drphb_full_weighted__3","",5, xAxis3);
   hist_part_drphb_full_weighted__3->SetBinContent(1,67.9966);
   hist_part_drphb_full_weighted__3->SetBinContent(2,112.7556);
   hist_part_drphb_full_weighted__3->SetBinContent(3,130.4914);
   hist_part_drphb_full_weighted__3->SetBinContent(4,101.0982);
   hist_part_drphb_full_weighted__3->SetBinContent(5,59.6117);
   hist_part_drphb_full_weighted__3->SetBinError(1,6.8938);
   hist_part_drphb_full_weighted__3->SetBinError(2,6.8759);
   hist_part_drphb_full_weighted__3->SetBinError(3,5.7562);
   hist_part_drphb_full_weighted__3->SetBinError(4,4.3661);
   hist_part_drphb_full_weighted__3->SetBinError(5,3.5334);
   hist_part_drphb_full_weighted__3->SetEntries(3589753);
   hist_part_drphb_full_weighted__3->SetLineColor(45);
   hist_part_drphb_full_weighted__3->SetMarkerColor(45);
   hist_part_drphb_full_weighted__3->SetMarkerStyle(20);
   hist_part_drphb_full_weighted__3->SetMarkerSize(0.9);
   hist_part_drphb_full_weighted__3->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__3->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__3->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__3->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__3->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__3->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__3->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__3->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__3->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__3->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__3->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__3->Draw("hist same PE");
   Double_t xAxis4[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__4 = new TH1D("hist_part_drphb_full_weighted__4","",5, xAxis4);
   hist_part_drphb_full_weighted__4->SetBinContent(1,64.4319);
   hist_part_drphb_full_weighted__4->SetBinContent(2,97.8598);
   hist_part_drphb_full_weighted__4->SetBinContent(3,104.1257);
   hist_part_drphb_full_weighted__4->SetBinContent(4,81.2854);
   hist_part_drphb_full_weighted__4->SetBinContent(5,45.2937);
   hist_part_drphb_full_weighted__4->SetBinError(1,6.6904);
   hist_part_drphb_full_weighted__4->SetBinError(2,6.5084);
   hist_part_drphb_full_weighted__4->SetBinError(3,5.2532);
   hist_part_drphb_full_weighted__4->SetBinError(4,4.0261);
   hist_part_drphb_full_weighted__4->SetBinError(5,3.284);
   hist_part_drphb_full_weighted__4->SetEntries(3589753);
   hist_part_drphb_full_weighted__4->SetLineColor(3);
   hist_part_drphb_full_weighted__4->SetMarkerColor(3);
   hist_part_drphb_full_weighted__4->SetMarkerStyle(20);
   hist_part_drphb_full_weighted__4->SetMarkerSize(0.9);
   hist_part_drphb_full_weighted__4->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__4->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__4->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__4->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__4->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__4->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__4->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__4->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__4->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__4->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__4->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__4->Draw("hist same PE");
   Double_t xAxis5[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__5 = new TH1D("hist_part_drphb_full_weighted__5","",5, xAxis5);
   hist_part_drphb_full_weighted__5->SetBinContent(1,66.21537);
   hist_part_drphb_full_weighted__5->SetBinContent(2,105.3082);
   hist_part_drphb_full_weighted__5->SetBinContent(3,117.3104);
   hist_part_drphb_full_weighted__5->SetBinContent(4,91.19364);
   hist_part_drphb_full_weighted__5->SetBinContent(5,52.44972);
   hist_part_drphb_full_weighted__5->SetEntries(3589748);
   hist_part_drphb_full_weighted__5->SetLineColor(2);
   hist_part_drphb_full_weighted__5->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__5->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__5->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__5->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__5->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__5->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__5->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__5->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__5->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__5->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__5->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__5->Draw("hist same E");
   Double_t xAxis6[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__6 = new TH1D("hist_part_drphb_full_weighted__6","",5, xAxis6);
   hist_part_drphb_full_weighted__6->SetBinContent(1,79.45845);
   hist_part_drphb_full_weighted__6->SetBinContent(2,118.4718);
   hist_part_drphb_full_weighted__6->SetBinContent(3,121.2208);
   hist_part_drphb_full_weighted__6->SetBinContent(4,84.35412);
   hist_part_drphb_full_weighted__6->SetBinContent(5,41.95978);
   hist_part_drphb_full_weighted__6->SetEntries(3589755);
   hist_part_drphb_full_weighted__6->SetLineColor(4);
   hist_part_drphb_full_weighted__6->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__6->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__6->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__6->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__6->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__6->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__6->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__6->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__6->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__6->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__6->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__6->Draw("hist same E");
   Double_t xAxis7[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__7 = new TH1D("hist_part_drphb_full_weighted__7","",5, xAxis7);
   hist_part_drphb_full_weighted__7->SetBinContent(1,52.9723);
   hist_part_drphb_full_weighted__7->SetBinContent(2,92.14471);
   hist_part_drphb_full_weighted__7->SetBinContent(3,113.4001);
   hist_part_drphb_full_weighted__7->SetBinContent(4,98.03316);
   hist_part_drphb_full_weighted__7->SetBinContent(5,62.93967);
   hist_part_drphb_full_weighted__7->SetEntries(3589755);
   hist_part_drphb_full_weighted__7->SetLineColor(6);
   hist_part_drphb_full_weighted__7->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__7->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__7->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__7->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__7->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__7->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__7->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__7->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__7->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__7->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__7->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__7->Draw("hist same E");
   Double_t xAxis8[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__8 = new TH1D("hist_part_drphb_full_weighted__8","",5, xAxis8);
   hist_part_drphb_full_weighted__8->SetBinContent(1,68.55032);
   hist_part_drphb_full_weighted__8->SetBinContent(2,113.1122);
   hist_part_drphb_full_weighted__8->SetBinContent(3,130.3282);
   hist_part_drphb_full_weighted__8->SetBinContent(4,100.9498);
   hist_part_drphb_full_weighted__8->SetBinContent(5,58.8308);
   hist_part_drphb_full_weighted__8->SetEntries(3589754);
   hist_part_drphb_full_weighted__8->SetLineColor(45);
   hist_part_drphb_full_weighted__8->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__8->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__8->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__8->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__8->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__8->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__8->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__8->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__8->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__8->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__8->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__8->Draw("hist same E");
   Double_t xAxis9[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__9 = new TH1D("hist_part_drphb_full_weighted__9","",5, xAxis9);
   hist_part_drphb_full_weighted__9->SetBinContent(1,63.88043);
   hist_part_drphb_full_weighted__9->SetBinContent(2,97.50431);
   hist_part_drphb_full_weighted__9->SetBinContent(3,104.2926);
   hist_part_drphb_full_weighted__9->SetBinContent(4,81.43744);
   hist_part_drphb_full_weighted__9->SetBinContent(5,46.06864);
   hist_part_drphb_full_weighted__9->SetEntries(3589754);
   hist_part_drphb_full_weighted__9->SetLineColor(3);
   hist_part_drphb_full_weighted__9->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__9->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__9->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__9->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__9->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__9->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__9->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__9->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__9->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__9->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__9->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__9->Draw("hist same E");
   
   TLegend *leg = new TLegend(0.5,0.6,0.9,0.9,NULL,"brNDC");
   leg->SetTextFont(62);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("hist_part_drphb_full_weighted","1 + X(pseudo-data unfolded)","P");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(0.9);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hist_part_drphb_full_weighted"," 1 - X(pseudo-data unfolded)","P");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(0.9);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hist_part_drphb_full_weighted","1 + X(truth reweighted)","L");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hist_part_drphb_full_weighted"," 1 - X(truth reweighted)","L");
   entry->SetLineColor(6);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hist_part_drphb_full_weighted","1 + 1*Obs(pseudo-data unfolded)","P");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(45);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(0.9);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hist_part_drphb_full_weighted"," 1 - 1*Obs(pseudo-data unfolded)","P");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(0.9);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hist_part_drphb_full_weighted","1 + 1*Obs(truth reweighted)","L");
   entry->SetLineColor(45);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hist_part_drphb_full_weighted"," 1 - 1*Obs(truth reweighted)","L");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hist_part_drphb_full_weighted"," truth","L");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   pad1->Modified();
   c->cd();
  
// ------------>Primitives in pad: pad2
   TPad *pad2 = new TPad("pad2", "pad2",0,0.05,1,0.3);
   pad2->Draw();
   pad2->cd();
   pad2->Range(-0.2075949,0.025,3.589873,1.65);
   pad2->SetFillColor(0);
   pad2->SetBorderMode(0);
   pad2->SetBorderSize(2);
   pad2->SetTickx(1);
   pad2->SetTicky(1);
   pad2->SetLeftMargin(0.16);
   pad2->SetRightMargin(0.05);
   pad2->SetTopMargin(0);
   pad2->SetBottomMargin(0.2);
   pad2->SetFrameBorderMode(0);
   pad2->SetFrameBorderMode(0);
   Double_t xAxis10[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__10 = new TH1D("hist_part_drphb_full_weighted__10","",5, xAxis10);
   hist_part_drphb_full_weighted__10->SetBinContent(1,1.241219);
   hist_part_drphb_full_weighted__10->SetBinContent(2,1.14902);
   hist_part_drphb_full_weighted__10->SetBinContent(3,1.04372);
   hist_part_drphb_full_weighted__10->SetBinContent(4,0.9043723);
   hist_part_drphb_full_weighted__10->SetBinContent(5,0.6758853);
   hist_part_drphb_full_weighted__10->SetBinError(1,0.1078284);
   hist_part_drphb_full_weighted__10->SetBinError(2,0.06643449);
   hist_part_drphb_full_weighted__10->SetBinError(3,0.04790794);
   hist_part_drphb_full_weighted__10->SetBinError(4,0.04474216);
   hist_part_drphb_full_weighted__10->SetBinError(5,0.06047315);
   hist_part_drphb_full_weighted__10->SetMinimum(0.35);
   hist_part_drphb_full_weighted__10->SetMaximum(1.65);
   hist_part_drphb_full_weighted__10->SetEntries(1047.837);
   hist_part_drphb_full_weighted__10->SetStats(0);
   hist_part_drphb_full_weighted__10->SetLineColor(4);
   hist_part_drphb_full_weighted__10->SetMarkerColor(4);
   hist_part_drphb_full_weighted__10->SetMarkerStyle(20);
   hist_part_drphb_full_weighted__10->SetMarkerSize(0.9);
   hist_part_drphb_full_weighted__10->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__10->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__10->GetXaxis()->SetLabelFont(63);
   hist_part_drphb_full_weighted__10->GetXaxis()->SetLabelSize(14);
   hist_part_drphb_full_weighted__10->GetXaxis()->SetTitleSize(0.08);
   hist_part_drphb_full_weighted__10->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__10->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__10->GetYaxis()->SetTitle("Unfolded / Truth");
   hist_part_drphb_full_weighted__10->GetYaxis()->SetLabelFont(63);
   hist_part_drphb_full_weighted__10->GetYaxis()->SetLabelSize(14);
   hist_part_drphb_full_weighted__10->GetYaxis()->SetTitleSize(0.09);
   hist_part_drphb_full_weighted__10->GetYaxis()->SetTitleOffset(0.4);
   hist_part_drphb_full_weighted__10->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__10->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__10->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__10->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__10->Draw("hist LEP");
   Double_t xAxis11[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__11 = new TH1D("hist_part_drphb_full_weighted__11","",5, xAxis11);
   hist_part_drphb_full_weighted__11->SetBinContent(1,0.7650821);
   hist_part_drphb_full_weighted__11->SetBinContent(2,0.8548656);
   hist_part_drphb_full_weighted__11->SetBinContent(3,0.9584103);
   hist_part_drphb_full_weighted__11->SetBinContent(4,1.09693);
   hist_part_drphb_full_weighted__11->SetBinContent(5,1.32632);
   hist_part_drphb_full_weighted__11->SetBinError(1,0.09670715);
   hist_part_drphb_full_weighted__11->SetBinError(2,0.06038559);
   hist_part_drphb_full_weighted__11->SetBinError(3,0.0459175);
   hist_part_drphb_full_weighted__11->SetBinError(4,0.04716118);
   hist_part_drphb_full_weighted__11->SetBinError(5,0.06947034);
   hist_part_drphb_full_weighted__11->SetEntries(1129.016);
   hist_part_drphb_full_weighted__11->SetLineColor(6);
   hist_part_drphb_full_weighted__11->SetMarkerColor(6);
   hist_part_drphb_full_weighted__11->SetMarkerStyle(20);
   hist_part_drphb_full_weighted__11->SetMarkerSize(0.9);
   hist_part_drphb_full_weighted__11->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__11->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__11->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__11->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__11->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__11->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__11->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__11->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__11->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__11->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__11->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__11->Draw("hist same LEP");
   Double_t xAxis12[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__12 = new TH1D("hist_part_drphb_full_weighted__12","",5, xAxis12);
   hist_part_drphb_full_weighted__12->SetBinContent(1,1.0269);
   hist_part_drphb_full_weighted__12->SetBinContent(2,1.07072);
   hist_part_drphb_full_weighted__12->SetBinContent(3,1.11236);
   hist_part_drphb_full_weighted__12->SetBinContent(4,1.10861);
   hist_part_drphb_full_weighted__12->SetBinContent(5,1.136549);
   hist_part_drphb_full_weighted__12->SetBinError(1,0.1041118);
   hist_part_drphb_full_weighted__12->SetBinError(2,0.06529308);
   hist_part_drphb_full_weighted__12->SetBinError(3,0.04906811);
   hist_part_drphb_full_weighted__12->SetBinError(4,0.04787724);
   hist_part_drphb_full_weighted__12->SetBinError(5,0.06736737);
   hist_part_drphb_full_weighted__12->SetEntries(1222.583);
   hist_part_drphb_full_weighted__12->SetLineColor(45);
   hist_part_drphb_full_weighted__12->SetMarkerColor(45);
   hist_part_drphb_full_weighted__12->SetMarkerStyle(20);
   hist_part_drphb_full_weighted__12->SetMarkerSize(0.9);
   hist_part_drphb_full_weighted__12->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__12->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__12->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__12->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__12->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__12->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__12->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__12->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__12->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__12->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__12->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__12->Draw("hist same PE");
   Double_t xAxis13[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__13 = new TH1D("hist_part_drphb_full_weighted__13","",5, xAxis13);
   hist_part_drphb_full_weighted__13->SetBinContent(1,0.9730656);
   hist_part_drphb_full_weighted__13->SetBinContent(2,0.9292701);
   hist_part_drphb_full_weighted__13->SetBinContent(3,0.8876084);
   hist_part_drphb_full_weighted__13->SetBinContent(4,0.8913495);
   hist_part_drphb_full_weighted__13->SetBinContent(5,0.8635641);
   hist_part_drphb_full_weighted__13->SetBinError(1,0.10104);
   hist_part_drphb_full_weighted__13->SetBinError(2,0.06180333);
   hist_part_drphb_full_weighted__13->SetBinError(3,0.04478034);
   hist_part_drphb_full_weighted__13->SetBinError(4,0.04414891);
   hist_part_drphb_full_weighted__13->SetBinError(5,0.06261234);
   hist_part_drphb_full_weighted__13->SetEntries(943.0359);
   hist_part_drphb_full_weighted__13->SetLineColor(3);
   hist_part_drphb_full_weighted__13->SetMarkerColor(3);
   hist_part_drphb_full_weighted__13->SetMarkerStyle(20);
   hist_part_drphb_full_weighted__13->SetMarkerSize(0.9);
   hist_part_drphb_full_weighted__13->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__13->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__13->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__13->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__13->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__13->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__13->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__13->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__13->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__13->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__13->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__13->Draw("hist same PE");
   Double_t xAxis14[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__14 = new TH1D("hist_part_drphb_full_weighted__14","",5, xAxis14);
   hist_part_drphb_full_weighted__14->SetBinContent(1,1.2);
   hist_part_drphb_full_weighted__14->SetBinContent(2,1.125);
   hist_part_drphb_full_weighted__14->SetBinContent(3,1.033333);
   hist_part_drphb_full_weighted__14->SetBinContent(4,0.925);
   hist_part_drphb_full_weighted__14->SetBinContent(5,0.8);
   hist_part_drphb_full_weighted__14->SetEntries(5.083333);
   hist_part_drphb_full_weighted__14->SetLineColor(4);
   hist_part_drphb_full_weighted__14->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__14->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__14->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__14->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__14->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__14->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__14->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__14->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__14->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__14->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__14->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__14->Draw("hist same e ");
   Double_t xAxis15[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__15 = new TH1D("hist_part_drphb_full_weighted__15","",5, xAxis15);
   hist_part_drphb_full_weighted__15->SetBinContent(1,0.8);
   hist_part_drphb_full_weighted__15->SetBinContent(2,0.875);
   hist_part_drphb_full_weighted__15->SetBinContent(3,0.9666667);
   hist_part_drphb_full_weighted__15->SetBinContent(4,1.075);
   hist_part_drphb_full_weighted__15->SetBinContent(5,1.2);
   hist_part_drphb_full_weighted__15->SetEntries(4.916667);
   hist_part_drphb_full_weighted__15->SetLineColor(6);
   hist_part_drphb_full_weighted__15->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__15->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__15->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__15->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__15->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__15->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__15->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__15->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__15->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__15->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__15->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__15->Draw("hist same e");
   Double_t xAxis16[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__16 = new TH1D("hist_part_drphb_full_weighted__16","",5, xAxis16);
   hist_part_drphb_full_weighted__16->SetBinContent(1,1.035263);
   hist_part_drphb_full_weighted__16->SetBinContent(2,1.074106);
   hist_part_drphb_full_weighted__16->SetBinContent(3,1.110969);
   hist_part_drphb_full_weighted__16->SetBinContent(4,1.106983);
   hist_part_drphb_full_weighted__16->SetBinContent(5,1.121661);
   hist_part_drphb_full_weighted__16->SetEntries(5.448982);
   hist_part_drphb_full_weighted__16->SetLineColor(45);
   hist_part_drphb_full_weighted__16->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__16->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__16->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__16->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__16->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__16->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__16->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__16->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__16->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__16->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__16->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__16->Draw("hist same e");
   Double_t xAxis17[6] = {0.4, 0.8, 1.3, 1.9, 2.6, 3.4}; 
   
   TH1D *hist_part_drphb_full_weighted__17 = new TH1D("hist_part_drphb_full_weighted__17","",5, xAxis17);
   hist_part_drphb_full_weighted__17->SetBinContent(1,0.9647371);
   hist_part_drphb_full_weighted__17->SetBinContent(2,0.9258943);
   hist_part_drphb_full_weighted__17->SetBinContent(3,0.8890312);
   hist_part_drphb_full_weighted__17->SetBinContent(4,0.8930167);
   hist_part_drphb_full_weighted__17->SetBinContent(5,0.8783391);
   hist_part_drphb_full_weighted__17->SetEntries(4.551018);
   hist_part_drphb_full_weighted__17->SetLineColor(3);
   hist_part_drphb_full_weighted__17->GetXaxis()->SetTitle("min #Delta R(#gamma,b)");
   hist_part_drphb_full_weighted__17->GetXaxis()->SetRange(1,56);
   hist_part_drphb_full_weighted__17->GetXaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__17->GetXaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__17->GetXaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__17->GetYaxis()->SetTitle(" Events");
   hist_part_drphb_full_weighted__17->GetYaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__17->GetYaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__17->GetZaxis()->SetLabelFont(42);
   hist_part_drphb_full_weighted__17->GetZaxis()->SetTitleOffset(1);
   hist_part_drphb_full_weighted__17->GetZaxis()->SetTitleFont(42);
   hist_part_drphb_full_weighted__17->Draw("hist same e ");
   pad2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
