"Wy_XsecNorm" : "NuisanceParameter: "Wy_XsecNorm", "Title: "W#gamma norm. uncert.",
"Wty_XsecNorm" : "Title: "Wt#gamma norm. uncert.", "NuisanceParameter:Wty_XsecNorm",
"Wt_XsecNorm" : "Title: "Wt norm. uncert.", "NuisanceParameter:Wt_XsecNorm",                                              # Added

"ty_XsecNorm" : "Title: "t#gamma (t-chan.) norm. uncert.", "NuisanceParameter:ty_XsecNorm",                               # Added
"Singletop_XsecNorm" : "Title: "t#gamma (s-chan.) norm. uncert.", "NuisanceParameter:Singletop_XsecNorm",

"JET_JER_DataVsMC_AFII" : "Title: "jet jer data vs mc AFII", "NuisanceParameter: "JET_JER_DataVsMC_AFII",                 # don't see this in the ntuple; only available for Wty
"JET_RelativeNonClosure_AFII" : "Title: "jet RelativeNonClosure AFII", "NuisanceParameter: "JET_RelativeNonClosure_AFII", # don't see this in the ntuple; only available for Wty
"JET_PunchThrough_AFII" : "Title: "jet punch through AFII", "NuisanceParameter: "JET_PunchThrough_AFII",                   # this is also for Wty

"syst_Wty_Herwig7" : "Title: "Wty Herwig", "NuisanceParameter:syst_Wty_Herwig7",
"syst_Wty_mu_R" : "Title: "Wty mu R", "NuisanceParameter:syst_Wty_mu_R",
"syst_Wty_mu_F" : "Title: "Wty mu F", "NuisanceParameter:syst_Wty_mu_F",

"syst_Wty_overlap_Herwig7" : "Title: "Wt Herwig7", "NuisanceParameter:syst_Wty_overlap_Herwig7",                          # Added this
"syst_Wty_overlap_mu_R" : "Title: "Wt mu R", "NuisanceParameter:syst_Wty_overlap_mu_R",                                   # Added this
"syst_Wty_overlap_mu_F" : "Title: "Wt mu F", "NuisanceParameter:syst_Wty_overlap_mu_F",                                   # Added this


# i have 
Wty -- fine
ty(s+t) -- split this
Wy -- fine
Zy -- fine
ttV -- fine
diboson -- fine
ttbar -- fine

-- add Wt xsec
-- 

