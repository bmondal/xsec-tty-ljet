This repository is for creating a dictionary of naming used in tty and ttZ trex-fitter config files so that later we can change to same naming for combined fit.

main.py is the file for creating dictionary or other sort of things

The final dictionary I have created is  np_dict_tty_ttZ.py
np_dict_tty_ttZ.v1.py the same file but for archieving this file
output_np_dict_tty.txt -> dictionary created from main.py using tty trex-focnfig file
output_np_dict_ttZ_incl.txt ->  dictionary created from main.py using ttZ_inclusive trex-config file
output_np_dict_ttZ.txt.txt ->  dictionary created from main.py using ttZ_differential trex-config file

example_string_match.py -> example file for checking how string matching works

not_found_entries.txt -> intermeditate file automatically created by main.py and also later used for which files didn't get matched

confirmed_entries.txt -> file created by main.py for caching some searches so that we don't have start from the begining
