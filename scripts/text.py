import csv


import os
import difflib

import os

def extract_string(file_path):
    extracted_string = os.path.basename(file_path)
    return extracted_string


root_folder = '/home/buddha/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v10_nominal_binning_v1/tty_CR'
subdirectories = []

# Get all subdirectories
for subdir, dirs, files in os.walk(root_folder):
    for dir in dirs:
        subdirectories.append(os.path.join(subdir, dir))


with open('tty1l_pt_all_syst.txt', 'r') as f:
    list_NPs = []
    for line in f:
        columns = line.strip().split()
        if len(columns) != 4: continue
        #print(len(columns))
        if (columns[0] == "CORRELATION_MATRIX"): break
        if "gamma" in columns[0] or "Unfolding_Bin" in columns[0] or "_norm" in columns[0]: continue
        #print("{} \t {} \t {} \t {} \n".format(columns[0], columns[1], columns[2], columns[3]))
        if len(columns) == 4:
            try:
                col3 = abs(float(columns[2]))
                col4 = abs(float(columns[3]))
                min_val = min(col3, col4)
                if min_val < 0.98:
                    #print(columns[0])
                    list_NPs.append(columns[0])
            except ValueError:
                continue




list_of_nps_that_needs_reprocessing = []
list_NPs_didnt_match = []
for np in list_NPs:
  # Define the name you want to find the closest match for
  name = np

  closest_match = difflib.get_close_matches(name, subdirectories, n=1, cutoff=0.20)
  if closest_match:
      closest_match = closest_match[0]
      print("{} \t {}\n".format(closest_match, np))
      list_of_nps_that_needs_reprocessing.append(extract_string(closest_match)) 
  else:
    print("No close match found \t {}\n".format(np))
    list_NPs_didnt_match.append(np) 


out_filename = "np_list_need_to_reprocess.txt"
file_ = open(out_filename,"w")
for np in  list_of_nps_that_needs_reprocessing:
  ## write this to a file
  file_.write("{}\n".format(np))
file_.write("--------- nps didn't find folder \n")
for np in  list_NPs_didnt_match:
  file_.write("{}\n".format(np))
file_.close()

