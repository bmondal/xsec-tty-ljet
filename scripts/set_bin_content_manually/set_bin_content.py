import argparse
import ROOT

parser = argparse.ArgumentParser()
parser.add_argument("--file", help="Path to the root file", default="tty_dec_CR/nominal_9999/histograms.lepfake_negative_bin_corrected.mujet.root")
parser.add_argument("--hist_name", help="Name of the histogram", default="hist_reco_ph_eta_full_weighted")
parser.add_argument("--bin_number_to_overwrite", help="Bin number to overwrite", type=int, default=5)
parser.add_argument("--bin_value_to_overwrite", help="Bin value to overwrite", type=float, default=1.0)
parser.add_argument("--bin_error_to_overwrite", help="Bin error to overwrite", type=float, default=1.0)
args = parser.parse_args()

tfile=ROOT.TFile.Open(args.file,"UPDATE")
obj = tfile.Get("Reco")
hist = obj.Get("{}".format(args.hist_name))
print("bin content before: {}\n".format(hist.GetBinContent(args.bin_number_to_overwrite)))
print("bin error before: {}\n".format(hist.GetBinError(args.bin_number_to_overwrite)))
hist.SetBinContent(args.bin_number_to_overwrite,args.bin_value_to_overwrite)
hist.SetBinError(args.bin_number_to_overwrite,args.bin_error_to_overwrite)
print("bin content after: {}\n".format(hist.GetBinContent(args.bin_number_to_overwrite)))
print("bin error after: {}\n".format(hist.GetBinError(args.bin_number_to_overwrite)))
obj.WriteObject(hist,hist.GetName(),"overwrite")
tfile.Close()
