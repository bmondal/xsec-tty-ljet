#!/usr/bin/env python3

from difflib import SequenceMatcher

threshold = 0.00
def is_similar(s1, s2, threshold=threshold):
    return SequenceMatcher(None, s1, s2).ratio() > threshold

def find_matches(dict_syst_ttZ, syst_tty, threshold):
    matches = []
    for syst_ttZ in dict_syst_ttZ:
        ratio = SequenceMatcher(None, syst_tty, syst_ttZ).ratio()
        if ratio > threshold:
            matches.append((syst_ttZ, ratio))
    return matches

def find_np_names_return_list(config_file_name):
  inside_systematic_block = False
  with open('{}'.format(config_file_name), 'r') as file: # replace 'filename.txt' with your actual file name
    values = [] # this contains nuisance_parameter and title
    syst_name=""
    np_name=""
    title=""
    dict_nps = {}
    previous_line_was_empty = True 
    for line in file:
      #print(line)
      line = line.strip() # remove leading/trailing whitespaces
      if line == '':  
        inside_systematic_block = False # if a newline is found then it is outside systematic block
        if not(syst_name=="") and (not previous_line_was_empty):
          if (np_name=="") and (len(values)==1):
            np_name=syst_name
            values.append("{}:{}".format("NuisanceParameter",np_name))
          #print("===Info:: key: {0}, {1}, {2} ===== \n".format(syst_name, values[0], values[1]))
          dict_nps[syst_name]=values
        syst_name=""
        np_name=""
        title=""
        values = [] # resetting this
        previous_line_was_empty = True

      if line.startswith('Systematic') or line.startswith('UnfoldingSystematic'):
        previous_line_was_empty = False 
        inside_systematic_block = True
        syst_name = line.split(":")[1]
        syst_name = syst_name.strip().strip('"')
        if '"' in syst_name:
          print(syst_name)
          print("********************************************************** error ****************************** \n")
      if line.startswith('SubCategory') and inside_systematic_block:
        previous_line_was_empty = False 
        np_name = line.split(":")[1]
        np_name = np_name.strip('"')
        values.append("{}:{}".format("NuisanceParameter",np_name))
      if line.startswith('Title') and inside_systematic_block:
        previous_line_was_empty = False 
        title = line.split(":")[1]
        title = title.strip('"')
        values.append("{}:{}".format("Title",title))
  return dict_nps 

def write_np_dict_style(dict_nps,output_file): # write in output file in np_dict.py style which is used for writing config file
  file_= open(output_file, 'w')
  for syst in dict_nps:
    file_.write("\"{0}\" : \"{1}\", \"{2}\",\n".format(syst, dict_nps[syst][0], dict_nps[syst][1]))
  file_.close() 
    

def run():
  run_matching = False 
  config_file_tty = "tty1l_pt_all_syst.config"
  config_file_tty_incl = "ttgamma_4_70_0_ljets_4classes_syst_fit_full_syst.config"
  config_file_ttZ = "ttZ3l_combined_Z_pT_v05042023_3L4L.config"
  config_file_ttZ_incl = "ttZ_3L_systs_FF.cfg"

  output_file_tty =  "output_np_group_dict_tty.txt"
  output_file_tty_incl =  "output_np_group_dict_tty_incl.txt"
  output_file_ttZ =  "output_np_group_dict_ttZ.txt"
  output_file_ttZ_incl =  "output_np_group_dict_ttZ_incl.txt"
  output_file_with_np_dict_tty_ttZ = "output_np_group_dict_tty_ttZ.txt"
  dict_syst_tty = find_np_names_return_list(config_file_tty)
  #dict_syst_tty_incl = find_np_names_return_list(config_file_tty_incl)
  #dict_syst_ttZ = find_np_names_return_list(config_file_ttZ)
  #dict_syst_ttZ_incl = find_np_names_return_list(config_file_ttZ_incl)


  write_np_dict_style(dict_syst_tty,output_file_tty)
  #write_np_dict_style(dict_syst_tty_incl,output_file_tty_incl)
  #write_np_dict_style(dict_syst_ttZ,output_file_ttZ)
  #write_np_dict_style(dict_syst_ttZ_incl,output_file_ttZ_incl)

  ## No need to run below code if not needed
  if not run_matching: return

  out_file = open(output_file_with_np_dict_tty_ttZ,"w")
  not_found_entries_file = "not_found_entries.txt"
  not_found_entries = []

  from operator import itemgetter
  import os
  
  confirmed_entries_file = "confirmed_entries.txt"
  if os.path.isfile(confirmed_entries_file):
      with open(confirmed_entries_file, "r") as file:
          confirmed_entries = file.read().splitlines()
  else:
      confirmed_entries = []
  
  last_match = None
  
  for syst_tty in dict_syst_tty:
      if any(syst_ttZ in syst_tty for syst_ttZ in dict_syst_ttZ):
          for syst_ttZ in dict_syst_ttZ:
              if syst_ttZ in syst_tty:
                  print(f'The string in tty "{syst_tty}" matches to ttZ "{syst_ttZ}".')
                  last_match = "\"{0}\" : \"{1}\",\"{2}\",\n".format(syst_tty, syst_ttZ, dict_syst_ttZ[syst_ttZ][0])
                  if last_match not in confirmed_entries:
                      out_file.write(last_match)
                      with open(confirmed_entries_file, "a") as file:
                          file.write(last_match)
                  break
          continue 
  
      matches = []
      for syst_ttZ in dict_syst_ttZ:
          ratio = SequenceMatcher(None, syst_tty, syst_ttZ).ratio()
          print(f'Similarity between "{syst_tty}" and "{syst_ttZ}": {ratio*100}%')  # Debug line
          if ratio > threshold:
              matches.append((syst_ttZ, ratio))
  
      matches.sort(key=itemgetter(1), reverse=True)  
  
      for i, (match, ratio) in enumerate(matches[:5]):
          print(f'{i+1}. String tty "{syst_tty}" has a similarity of {ratio*100}% to ttZ "{match}".')
  
      while True:
          response = input('Enter the number of the match you accept, "n" to reject all, or "u" to undo the last match: ')
          if response.isdigit() and 1 <= int(response) <= len(matches[:5]):
              match = matches[int(response) - 1][0]
              print('Match accepted.')
              last_match = "\"{0}\" : \"{1}\",\"{2}\",\n".format(syst_tty, match, dict_syst_ttZ[match][0])
              if last_match not in confirmed_entries:
                  out_file.write(last_match)
                  with open(confirmed_entries_file, "a") as file:
                      file.write(last_match)
                  break  
          elif response.lower() == 'n':
              print('All matches rejected.')
              not_found_entries.append(syst_tty)
              break  
          elif response.lower() == 'u' and last_match is not None:
              print('Undoing last match.')
              out_file.seek(0)
              lines = out_file.readlines()
              out_file.seek(0)
              out_file.truncate()
              out_file.writelines([line for line in lines if line.strip('\n') != last_match.strip('\n')])
              with open(confirmed_entries_file, "r") as file:
                  lines = file.readlines()
              with open(confirmed_entries_file, "w") as file:
                  file.writelines([line for line in lines if line.strip('\n') != last_match.strip('\n')])
              last_match = None
          else:
              print('Invalid response, please enter a valid number, "n" to reject all, or "u" to undo the last match.')
  
  out_file.close()

  # Writing the not found entries into the file
  with open(not_found_entries_file, "w") as file:
    for entry in not_found_entries:
        file.write(entry + '\n')



if __name__== "__main__":
  run()
