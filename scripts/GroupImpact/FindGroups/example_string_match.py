#!/usr/bin/env python3 
from difflib import SequenceMatcher

def is_similar(s1, s2, threshold=0.95):
    return SequenceMatcher(None, s1, s2).ratio() > threshold

# List of strings to compare
strings = ['Hello, world!', 'Hello, world!!', 'Hi, world!', 'Hello, universe!', 'Hello, world!']

# The string to compare with
base_string = 'Hello, world!'

for s in strings:
    if is_similar(base_string, s):
        print(f'The string "{s}" is at least 95% similar to "{base_string}".')
        while True:  # Loop until a valid response is given
            response = input('Do you accept this match? (yes/no): ').lower()
            if response == 'y':
                print('Match accepted.')
                break  # Exit the loop
            elif response == 'n':
                print('Match not accepted.')
                break  # Exit the loop
            else:
                print('Invalid response, please answer with "yes" or "no".')

