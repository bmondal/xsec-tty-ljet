##*************************** CONFIGURATION *******************
normalized_xsec_unfolding= False 
normalized_bin_n_minus_1 = False 
absolute_xsec_unfolding = True 
input_histograms = "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/"

write_bin_width = True
unfolding_name = True

use_data = True
write_ttbar_af2 = True
write_pdf_ref_sample = True 
debug = False
all_syst =True
exp_tree_syst = True
exp_weight_syst = True
data_driven_syst = True
modelling_syst = True
bkg_norm_syst = True
btag_lepton_syst = True
muR_muF_syst = True
met_syst = True
lumi_syst = True
use_real_data = True
use_reweighted_pseudodata = False
reweighted_data_name = ""
fit_blind = True

