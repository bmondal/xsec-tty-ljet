### Command line options; how to use

---
the commnad line options for the GenerateUnfoldingTrexConfig...py


--filename : trex-fitter config file name \
--path-to-unfolding-hist : path to the unfolding histograms (unfolding inputs) example:~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v9/ \
--write-data : whether to write data or not. If not used it won't write the data block.
This will lead to the use of ASIMOV only fit \
--use-real-data : If you want to write read data in the config file data block. also this block writes such a way that
during fitting mu  will be hidden. \
--write-syst : write systematic blocks \
--fit-blind : This is **False** by default. This is exact translation of "FitBlind" in the Trex-fitter. \
--use_reweighted_pseudodata : Use reweighted MC as pseudo data in the data block \
--reweighted_data_name : If only one sample is reweighted, give the name of it. Lets say only tty_prod sample
is reweighted, other samples stay same except this one. \
