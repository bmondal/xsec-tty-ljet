#!/usr/bin/env python

# Defintion of the NormFactor class, which holds a norm factor
# title and a dictionary with options. Options can be added via
# NormFactor.Option(key, val).
class NormFactor():
    def __init__(self, title):
        self.title = title
        self.options = dict()
        self.options["Title"] = "Signal Strength"

    def Option(self, key, val):
        self.options[key] = val

    def Print(self):
        ret = "NormFactor: %s\n" % self.title
        for key,value in sorted(self.options.iteritems()):
            ret += "    %s: %s\n" % (key, value)
        return ret

    def __call__(self):
        return self.Print()

    def __repr__(self):
        return self.Print()

def Header():
    h = '\n'
    h += "# -------------------------- #\n"
    h += "# ------- NormFactor ------- #\n"
    h += "# -------------------------- #\n"
    return h

#def Default():
#    norm = NormFactor("SigXsecOverSM")
#    norm.Option("Title", "\"Signal Strength\"")
#    norm.Option("Nominal", "1")
#    norm.Option("Min", "-5")
#    norm.Option("Max", "5")
#    norm.Option("Samples", "tty, Other_tty, Wty, Other_Wty")
#    return norm

def norm_factor_1():
    norm = NormFactor("SigXsecOverSM_ttbar_wy")
    norm.Option("Title", "\"Signal Strength\"")
    norm.Option("Type", "OVERALL")
    norm.Option("OverallUp", "0.2")
    norm.Option("OverallDown", "-0.2")
    norm.Option("Samples", "prompty_ttbar, prompty_wgamma")
    return norm

def norm_factor_2():
    norm = NormFactor("SigXsecOverSM_wty_ttv_zy_diboson")
    norm.Option("Title", "\"Signal Strength\"")
    norm.Option("Type", "OVERALL")
    norm.Option("OverallUp", "0.5")
    norm.Option("OverallDown", "-0.5")
    norm.Option("Samples", "Wty, prompty_ttv, prompty_zgamma, prompty_diboson")


    return norm


if __name__ == '__main__':
    norm = Default()
    print(Header())
    print(norm.Print())