from getInfoFromRootFile import *
import NormFactor
##*************************** CONFIGURATION *******************
use_data = True 
#job_name = ""
# path to the unfolding input histograms
#path_to_input_histos = "~/eos/physics_analysis/tty/Dilepton/Unfolding_samples/tmp/Unfolding_inputs_v12_vHTCondor_splitted_by_processes_v1/"
path_to_input_histos = "~/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_vHTCondor_splitted_by_processes_v2/"
#path_to_input_histos = "/eos/bmondal/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v2/"
one_ntuple_file_for_reading_trees = "~/eos/physics_analysis/tty/Dilepton/dilepton_mini_ntuple_v12_v1_htcondor//mc16e_TOPQ1_singletop_tchan410659.1.root"
response_matrix_path = path_to_input_histos + "/nominal/"
response_matrix_file = "histograms.ttgamma_prod"
#response_matrix_file = "histograms.ttgamma_nominal"
response_matrix_name = "h2_response_matrix_ph_pt"
histo_path = path_to_input_histos+"/nominal/"
truth_distribution_file = "histograms.ttgamma_prod"
#truth_distribution_file = "histograms.ttgamma_nominal"
truth_distribution_name = "particle/hist_part_ph_pt_full_weighted"
truth_distribution_path = path_to_input_histos+"/nominal/"
histo_name = "Reco/hist_reco_ph_pt_full_weighted"
number_of_reco_bins = 6 
xaxisTitle = "p_T(#gamma) [GeV]"
yaxisTitle = "#frac{d#sigma}{dp_{T}} [fb #times GeV^{-1}]"
variable_title = "Reco level p_{T}{#gamma} [GeV]"
tau=2.0
debug = False
all_syst = False

##*************************** CONFIGURATION *******************
class GenerateTrexConfig:
  def __init__(self, filename):
    self.job_name = filename.split(".")[0]
    self.config = filename
    self.file = open(self.config, 'w')

  def write_norm_factor(self):
    self.file.write(" \n")
    self.file.write("NormFactor: SigXsecOverSM \n")
    self.file.write("  Max: 5 \n")
    self.file.write("  Min: -5 \n")
    self.file.write("  Nominal: 1 \n")
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Title: \"Signal Strength\" \n")
    self.file.write(" \n")


  def write_bkg_norm_uncertainty(self):
    self.file.write(" \n")
    self.file.write("Systematic: ttbar_and_wy_norm \n")
    self.file.write("  Title: \"ttbar and Wgamma prompt Cross-section\" \n")
    self.file.write("  Type: OVERALL \n")
    self.file.write("  OverallUp: 0.2 \n")
    self.file.write("  OverallDown: -0.2 \n")
    self.file.write("  Samples: prompty_ttbar, prompty_wgamma \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: ttbar_and_wy_norm \n")
    self.file.write("  Title: \"ttbar and Wgamma prompt Cross-section\" \n")
    self.file.write("  Type: OVERALL \n")
    self.file.write("  OverallUp: 0.5 \n")
    self.file.write("  OverallDown: -0.5 \n")
    self.file.write("  Samples: Wty, prompty_ttv, prompty_zgamma, prompty_diboson \n")
    self.file.write(" \n")
    

  def write_addition_met_syst(self):
    self.file.write(" \n")
    syst_name = "MET_SoftTrk_ResoPara"
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixPathUp: \"{}/{}\" \n".format(path_to_input_histos, syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  NuisanceParameter: {} \n".format(syst_name))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Title: {} \n".format(syst_name))
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec, prompty_wjets,prompty_wgamma,prompty_zjets,prompty_zgamma,prompty_ttbar,prompty_diboson,prompty_ttv,prompty_singletop,Wty\n")
    self.file.write("  HistoPathUp: \"{}/{}\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(syst_name))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Title: \"{}\" \n".format(syst_name))
    self.file.write("  Type: HISTO \n")
    self.file.write(" \n")
    syst_name = "MET_SoftTrk_ResoPerp"
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixPathUp: \"{}/{}\" \n".format(path_to_input_histos, syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  NuisanceParameter: {} \n".format(syst_name))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Title: {} \n".format(syst_name))
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec, prompty_wjets,prompty_wgamma,prompty_zjets,prompty_zgamma,prompty_ttbar,prompty_diboson,prompty_ttv,prompty_singletop,Wty\n")
    self.file.write("  HistoPathUp: \"{}/{}\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(syst_name))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Title: \"{}\" \n".format(syst_name))
    self.file.write("  Type: HISTO \n")
    self.file.write(" \n")


  def write_muR_muF(self):
    #Wty muR
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("Wty_muR"))
    self.file.write("  Samples:  Wty\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("Wty_muR"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.tWy_muR_up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.tWy_muR_down"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Title: \"{}\" \n".format("Wty_muR"))
    self.file.write("  Type: HISTO \n")
    #Wty muF
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("Wty_muF"))
    self.file.write("  Samples:  Wty\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("Wty_muF"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.tWy_muF_up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.tWy_muF_down"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Title: \"{}\" \n".format("Wty_muF"))
    self.file.write("  Type: HISTO \n")
    #ttbar muR
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("ttbar_muR"))
    self.file.write("  Samples:  prompty_ttbar\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_muR"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.tt_muR_up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.tt_muR_down"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Title: \"{}\" \n".format("ttbar_muR"))
    self.file.write("  Type: HISTO \n")
    #ttbar muF
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("ttbar_muF"))
    self.file.write("  Samples:  prompty_ttbar\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_muF"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.tt_muF_up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.tt_muF_down"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Title: \"{}\" \n".format("ttbar_muF"))
    self.file.write("  Type: HISTO \n")
    #tty_dec muR
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("tty_dec_muR"))
    self.file.write("  Samples:  tty_dec\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_dec_muR"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.tty_dec_muR_up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.tty_dec_muR_down"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Title: \"{}\" \n".format("tty_dec_muR"))
    self.file.write("  Type: HISTO \n")
    #tty_dec muF
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("tty_dec_muF"))
    self.file.write("  Samples:  tty_dec\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_dec_muF"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.tty_dec_muF_up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.tty_dec_muF_down"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Title: \"{}\" \n".format("tty_dec_muF"))
    self.file.write("  Type: HISTO \n")
    #tty_prod muR
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_muR"))
    self.file.write("  Samples:  tty_prod\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_prod_muR"))
    self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.tty_prod_muR_up"))
    self.file.write("  ResponseMatrixFileDown: {} \n".format("histograms.tty_prod_muR_down"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Title: \"{}\" \n".format("tty_prod_muR"))
    self.file.write("  Type: HISTO \n")
    #tty_prod muF
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_muF"))
    self.file.write("  Samples:  tty_prod\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_prod_muF"))
    self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.tty_prod_muF_up"))
    self.file.write("  ResponseMatrixFileDown: {} \n".format("histograms.tty_prod_muF_down"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Title: \"{}\" \n".format("tty_prod_muF"))
    self.file.write("  Type: HISTO \n")
    self.file.write(" \n")

  # write luminosity uncertainty in the config
  def write_luminosity(self):
    self.file.write(" \n")
    self.file.write("Systematic: lumi \n")
    self.file.write("  Title: Luminosity \n")
    self.file.write("  Type: OVERALL \n")
    self.file.write("  OverallUp: +0.017 \n")
    self.file.write("  OverallDown: -0.017 \n")
    self.file.write("  Category: lumi \n")
    self.file.write("  Regions: all \n")
    self.file.write("  Samples: all \n")
    self.file.write(" \n")

  def write_config_modelling(self):
    # ttgamma_prod var3c
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_var3c"))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.ttgamma_prod_var3cUp"))
    self.file.write("  ResponseMatrixFileDown: {} \n".format("histograms.ttgamma_prod_var3cDown"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_prod_var3c"))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format("tty_prod_var3c"))
    self.file.write("  Type: HISTO \n")
    #tty_dec var3c
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("tty_dec_var3c"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_dec_var3c"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.tty_dec_var3c_up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.tty_dec_var3c_down"))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format("tty_dec_var3c"))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: tty_dec\n")
    #ttbar var3c
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("ttbar_var3c"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_var3c"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.tt_var3c_up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.tt_var3c_down"))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format("ttbar_var3c"))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty_ttbar\n")
#    # ttgamma_prod Herwig7
#    self.file.write(" \n")
#    self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_Herwig7"))
#    self.file.write("  Samples: tty_prod\n")
#    self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.ttgamma_prod_H7"))
#    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_prod_Herwig7"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("tty_prod_Herwig7"))
#    self.file.write("  Type: HISTO \n")
    # tty_dec Herwig7
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("tty_dec_Herwig7"))
    self.file.write("  Samples: tty_dec\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_dec_Herwig7"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.ttgamma_dec_H7"))
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("tty_dec_Herwig7"))
    self.file.write("  Type: HISTO \n")
#    # prompty ttbar Herwig7
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("prompty_ttbar_Herwig7"))
#    self.file.write("  Samples: prompty_ttbar\n")
#    self.file.write("  HistoFileUp: {} \n".format("histograms.promptY_ttbar_H7"))
#    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_Herwig7"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("prompty_Herwig7"))
#    self.file.write("  Type: HISTO \n")
    # Wty Herwig7
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("Wty_Herwig7"))
    self.file.write("  Samples:  Wty\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("Wty_Herwig7"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.Wty_H7"))
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("Wty_Herwig7"))
    self.file.write("  Type: HISTO \n")
    # efake tty_prod Herwig7
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("efake_tty_prod_Herwig7"))
    self.file.write("  Samples:  efake\n")
    #self.file.write("  NuisanceParameter: \"{}\" \n".format("efake_tty_prod_Herwig7"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_prod_Herwig7")) # correlated
    self.file.write("  HistoFileUp: {} \n".format("histograms.efake_tty_prod_H7"))
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("efake_tty_prod_Herwig7"))
    self.file.write("  Type: HISTO \n")
    # efake tty_dec Herwig7
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("efake_tty_dec_Herwig7"))
    self.file.write("  Samples:  efake\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_dec_Herwig7")) # naming used to correlate the systematics
    self.file.write("  HistoFileUp: {} \n".format("histograms.efake_tty_dec_H7"))
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("efake_tty_dec_Herwig7"))
    self.file.write("  Type: HISTO \n")
    # efake ttbar Herwig7
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("efake_ttbar_Herwig7"))
    self.file.write("  Samples:  efake\n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_Herwig7")) # correlated
    self.file.write("  HistoFileUp: {} \n".format("histograms.efake_ttbar_H7"))
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("efake_ttbar_Herwig7"))
    self.file.write("  Type: HISTO \n")
    # efake ttbar hdamp_var 
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("efake_ttbar_hdamp_var"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_hdamp_var")) # correlated
    self.file.write("  HistoFileUp: {} \n".format("histograms.efake_ttbar_hdamp_var"))
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("efake_ttbar_hdamp_var"))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: efake \n")
    # efake ttbar aMC 
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("efake_ttbar_aMC"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_aMC")) # correlated
    self.file.write("  HistoFileUp: {} \n".format("histograms.efake_ttbar_aMC"))
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("efake_ttbar_aMC"))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: efake \n")
    # efake tty_prod var3c 
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("efake_tty_prod_var3c"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_prod_var3c")) # correlated
    self.file.write("  HistoFileUp: {} \n".format("histograms.efake_tty_prod_var3c_Up"))
    self.file.write("  HistoFileDown: {} \n".format("histograms.efake_tty_prod_var3c_Down"))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format("efake_tty_prod_var3c"))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: efake \n")
#    # hfake tty_prod Herwig7
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("hfake_tty_prod_Herwig7"))
#    self.file.write("  Samples:  hfake\n")
#    self.file.write("  NuisanceParameter: \"{}\" \n".format("hfake_tty_prod_Herwig7"))
#    self.file.write("  HistoFileUp: {} \n".format("histograms.hfake_tty_prod_H7"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("hfake_tty_prod_Herwig7"))
#    self.file.write("  Type: HISTO \n")
#    # hfake tty_dec Herwig7
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("hfake_tty_dec_Herwig7"))
#    self.file.write("  Samples:  hfake\n")
#    self.file.write("  NuisanceParameter: \"{}\" \n".format("hfake_tty_dec_Herwig7"))
#    self.file.write("  HistoFileUp: {} \n".format("histograms.hfake_tty_dec_H7"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("hfake_tty_dec_Herwig7"))
#    self.file.write("  Type: HISTO \n")
#    # hfake tty_prod var3c 
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("hfake_tty_prod_var3c"))
#    self.file.write("  NuisanceParameter: \"{}\" \n".format("hfake_tty_prod_var3c"))
#    self.file.write("  HistoFileUp: {} \n".format("histograms.hfake_tty_prod_var3cUp"))
#    self.file.write("  HistoFileDown: {} \n".format("histograms.hfake_tty_prod_var3cDown"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("hfake_tty_prod_var3c"))
#    self.file.write("  Type: HISTO \n")
#    self.file.write("  Samples: hfake \n")
#    # MadGraph 
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("Wty_MadGraph"))
#    self.file.write("  NuisanceParameter: \"{}\" \n".format("Wty_MadGraph"))
#    self.file.write("  HistoFileUp: {} \n".format("histograms.Wty_MadGraph"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("Wty_MadGraph"))
#    self.file.write("  Type: HISTO \n")
#    self.file.write("  Samples: Wty\n")
    # promptY ttbar hdamp_var 
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("prompty_ttbar_hdamp_var"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_hdamp_var")) # correlated
    self.file.write("  HistoFileUp: {} \n".format("histograms.promptY_ttbar_hdamp_var"))
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("prompty_ttbar_hdamp_var"))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty_ttbar \n")
    self.file.write("ReferenceSample: ttbar_AFII \n")
    # prompY ttbar aMC
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("prompty_ttbar_aMC"))
    self.file.write("  HistoFileUp: {} \n".format("histograms.promptY_ttbar_aMC"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format("ttbar_aMC")) # correlated
    self.file.write("  Symmetrisation: ONESIDED \n")
    self.file.write("  Title: \"{}\" \n".format("prompty_ttbar_aMC"))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty_ttbar \n")
    self.file.write("ReferenceSample: ttbar_AFII \n")
#    # DS 
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("Wty_DS"))
#    self.file.write("  HistoFileUp: {} \n".format("histograms.Wty_DS"))
#    self.file.write("  NuisanceParameter: \"{}\" \n".format("Wty_DS"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("Wty_DS"))
#    self.file.write("  Type: HISTO \n")
#    self.file.write("  Samples: Wty \n")

  #def write_config_datadriven_syst(self, syst_name):
  def write_config_datadriven_syst(self):
#    self.file.write(" \n")
#    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
#    self.file.write("  Samples: tty_prod \n")
#    self.file.write("  ResponseMatrixPathUp: \"{}/{}Up\" \n".format(path_to_input_histos, syst_name))
#    self.file.write("  ResponseMatrixPathDown: \"{}/{}Down\" \n".format(path_to_input_histos, syst_name))
#    self.file.write("  NuisanceParameter: {} \n".format(syst_name))
#    self.file.write("  Symmetrisation: TWOSIDED \n")
#    self.file.write("  Title: {} \n".format(syst_name))
    self.file.write(" \n")
    syst_name="efakeSF_"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  HistoPathDown: \"{}/{}Down\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  HistoPathUp: \"{}/{}Up\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(syst_name))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format(syst_name))
    #self.file.write("  Samples: tty_dec, efake, hfake,prompty,Wty\n")#ToDo it should not matter if I include the other samples
    self.file.write("  Samples: efake \n")#ToDo it should not matter if I include the other samples
    self.file.write("  Type: HISTO \n")

    self.file.write(" \n")
    syst_name="hfakeSF_"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  HistoPathDown: \"{}/{}Down\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  HistoPathUp: \"{}/{}Up\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(syst_name))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format(syst_name))
    #self.file.write("  Samples: tty_dec, efake, hfake,prompty,Wty\n")#ToDo it should not matter if I include the other samples
    self.file.write("  Samples: hfake \n")#ToDo it should not matter if I include the other samples
    self.file.write("  Type: HISTO \n")


  def write_config_obj_syst(self, syst_name, up_name, down_name):
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  ResponseMatrixPathUp: \"{}/{}{}\" \n".format(path_to_input_histos, syst_name,up_name))
    self.file.write("  ResponseMatrixPathDown: \"{}/{}{}\" \n".format(path_to_input_histos, syst_name,down_name))
    self.file.write("  NuisanceParameter: {} \n".format(syst_name))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: {} \n".format(syst_name))
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec,prompty_wjets,prompty_wgamma,prompty_zjets,prompty_zgamma,prompty_ttbar,prompty_diboson,prompty_ttv,prompty_singletop,Wty\n")
    self.file.write("  HistoPathDown: \"{}/{}{}\" \n".format(path_to_input_histos,syst_name,down_name))
    self.file.write("  HistoPathUp: \"{}/{}{}\" \n".format(path_to_input_histos,syst_name,up_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(syst_name))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format(syst_name))
    self.file.write("  Type: HISTO \n")


  def write_config_syst(self, syst_name):
    ##*** The reason there is no HistoPathUp/Down in the Unfolding systematics is that
    ##    The particle level will be folded with the response matrix to give the reco spectra?
    ##***
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixPathUp: \"{}/{}up\" \n".format(path_to_input_histos, syst_name))
    self.file.write("  ResponseMatrixPathDown: \"{}/{}down\" \n".format(path_to_input_histos, syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  NuisanceParameter: {} \n".format(syst_name))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: {} \n".format(syst_name))
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec, prompty_wjets,prompty_wgamma,prompty_zjets,prompty_zgamma,prompty_ttbar,prompty_diboson,prompty_ttv,prompty_singletop,Wty\n")
    self.file.write("  HistoPathDown: \"{}/{}down\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  HistoPathUp: \"{}/{}up\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(syst_name))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format(syst_name))
    self.file.write("  Type: HISTO \n")

  def write_config_syst_JET_JER_pseudoData(self, syst_name):
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  ResponseMatrixPathUp: \"{}/{}up_PseudoData\" \n".format(path_to_input_histos, syst_name))
    self.file.write("  ResponseMatrixPathDown: \"{}/{}down_PseudoData\" \n".format(path_to_input_histos, syst_name))
    self.file.write("  NuisanceParameter: {} \n".format(syst_name))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: {} \n".format(syst_name))
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec, prompty_wjets,prompty_wgamma,prompty_zjets,prompty_zgamma,prompty_ttbar,prompty_diboson,prompty_ttv,prompty_singletop,Wty\n")
    self.file.write("  HistoPathDown: \"{}/{}down\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  HistoPathUp: \"{}/{}up\" \n".format(path_to_input_histos,syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(syst_name))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Title: \"{}\" \n".format(syst_name))
    self.file.write("  Type: HISTO \n")

  def write_config(self):
    self.file = open(self.config, 'w')
    self.file.write("Job: \"{}\"\n".format(self.job_name))
    self.file.write("  CmeLabel: \"13 TeV\" \n")
    self.file.write("  POI: \"SigXsecOverSM\" \n")
    self.file.write("  ReadFrom: HIST \n")
    self.file.write("  HistoPath: {}\n".format(histo_path))
    self.file.write("  Label: \"Dilepton\" \n")
    self.file.write("  LumiLabel: \"139 fb^{-1}\" \n")
    self.file.write("  Lumi: 139. \n")
    self.file.write("  PlotOptions: \"LEFT,NOXERR,NOENDERR\" \n")
    self.file.write("  DebugLevel: 0 \n")
    self.file.write("  MCstatThreshold: 0.001 \n")
    self.file.write("  MCstatConstraint: \"Poisson \n")
    self.file.write("  SystControlPlots: TRUE \n")
    self.file.write("  SystPruningShape: 0.01 \n")
    self.file.write("  SystPruningNorm: 0.01 \n")
    self.file.write("  SystLarge: 0.90 \n")
    self.file.write("  CorrelationThreshold: 0.20 \n")
    self.file.write("  HistoChecks: NOCRASH \n")
    self.file.write("  SplitHistoFiles: TRUE \n")
    self.file.write("  ImageFormat: \"png\" \n")
    self.file.write("  SystCategoryTables: TRUE \n")
    self.file.write("  RankingPlot: \"all\" \n")
    self.file.write("  RankingMaxNP: 10 \n")
    self.file.write("  DoSummaryPlot: FALSE \n")
    self.file.write("  DoTables: TRUE \n")
    self.file.write("  DoSignalRegionsPlot: TRUE \n")
    self.file.write("  DoPieChartPlot: TRUE \n")









#    self.file.write("  CmeLabel: \"13 TeV\" \n")
#    self.file.write("  HistoPath: {}\n".format(histo_path))
#    self.file.write("  LegendNColumns: 1 \n")
#    self.file.write("  LegendX1: 0.6 \n")
#    self.file.write("  LumiLabel: \"139 fb^{-1}\" \n")
#    self.file.write("  MCstatThreshold: NONE\n")
#    self.file.write("  PlotOptions: YIELDS\n")
#    self.file.write("  ReadFrom: HIST\n")
#    self.file.write("  POI: SigXsecOverSM \n")
#    self.file.write("  SystControlPlots: TRUE \n")
    self.file.write(" \n")
    self.file.write("Fit: \"myFit\" \n")
    #self.file.write("  BinnedLikelihoodOptimization: TRUE \n")
    self.file.write("  FitType: SPLUSB \n")
    self.file.write("  FitRegion: CRSR\n")
    self.file.write("  POIAsimov: 1.0 \n")
    self.file.write("  UseMinos: SigXsecOverSM \n")
    self.file.write(" \n")
    # Region
    self.file.write("Region: \"SR\" \n")
    self.file.write("  HistoName: {} \n".format(histo_name))
    self.file.write("  Label: \"ee+e#mu+#mu#mu\" \n")
    #self.file.write("  NumberOfRecoBins: {} \n".format(number_of_reco_bins))
    self.file.write("  Type: SIGNAL \n")
    self.file.write("  VariableTitle: \"{}\" \n".format(variable_title))
    self.file.write(" \n")

    # Reference sample
    self.file.write(" \n")
    self.file.write("Sample: \"ttbar_AFII\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.promptY_ttbar_AFII\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: GHOST \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")

    # tty prod
    self.file.write("Sample: \"tty_prod\" \n")
    self.file.write("  HistoFile: {} \n".format(response_matrix_file))
    self.file.write("  FillColor: 2\n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"tty_prod\" \n")
    self.file.write(" \n")
#    self.file.write("UnfoldingSample: \"tty_nominal\" \n")
#    self.file.write("  ResponseMatrixFile: {} \n".format(response_matrix_file))
#    self.file.write("  FillColor: 8 \n")
#    self.file.write("  LineColor: 31 \n")
#    self.file.write("  Regions: SR \n")
#    self.file.write("  Title: \"tty_NLO_nominal\" \n")
    if use_data:
      self.file.write(" \n")
      self.file.write("Sample: \"data\" \n")
      self.file.write("  FillColor: 9 \n")
      self.file.write("  HistoFile: \"histograms.data\" \n")
      self.file.write("  LineColor: 33 \n")
      self.file.write("  Regions: SR \n")
      self.file.write("  Title: \"data\" \n")
      self.file.write("  Type: data \n")
      self.file.write(" \n")
    # tty dec
    self.file.write("Sample: \"tty_dec\" \n")
    self.file.write("  FillColorRGB: 228,131,131 \n")
    self.file.write("  HistoFile: \"histograms.ttgamma_dec\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"t#bar{t}#gamma(dec)\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    # Wty
    self.file.write("Sample: \"Wty\" \n")
    self.file.write("  FillColorRGB: 77,175,74 \n")
    self.file.write("  HistoFile: \"histograms.Wty\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Wt#gamma\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    # prompty
    self.file.write("Sample: \"prompty_wjets\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.wjets\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    self.file.write("Sample: \"prompty_wgamma\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.wgamma\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    self.file.write("Sample: \"prompty_zjets\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.zjets\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    self.file.write("Sample: \"prompty_zgamma\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.zgamma\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    self.file.write("Sample: \"prompty_ttbar\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.ttbar\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    self.file.write("Sample: \"prompty_diboson\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.diboson\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    self.file.write("Sample: \"prompty_ttv\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.ttv\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    self.file.write("Sample: \"prompty_singletop\" \n")
    self.file.write("  FillColorRGB: 255,255,51 \n")
    self.file.write("  HistoFile: \"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"Prompt #gamma bkg\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    # efake
    self.file.write("Sample: \"efake\" \n")
    self.file.write("  FillColorRGB: 255,127,0 \n")
    self.file.write("  HistoFile: \"histograms.efake\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"efake\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
    #hfake
    self.file.write("Sample: \"hfake\" \n")
    self.file.write("  FillColorRGB: 152,78,163 \n")
    self.file.write("  HistoFile: \"histograms.hfake\" \n")
    self.file.write("  LineColor: 31 \n")
    self.file.write("  Regions: SR \n")
    self.file.write("  Title: \"hfake\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
 

    #self.file.write("NormFactor: SigXsecOverSM \n")
    #self.file.write("  Max: 10 \n")
    #self.file.write("  Min: -10 \n")
    #self.file.write("  Nominal: 1.0 \n")
    #self.file.write("  Regions: SR \n")
    #self.file.write("  Tau: {} \n".format(tau))
    #self.file.write("  Samples: NONE \n")
    #self.file.write("  #Title: \"#mu(WZ+l)\" \n")
    #self.file.write(" \n")
    obj = ReadFromRootFile(one_ntuple_file_for_reading_trees)
    tree_list = obj.get_list_of_trees() # list containing tree names
    list_of_obj_syst = obj.get_list_of_uncer_from_nominal()
    self.write_norm_factor()
    #list_of_obj_syst = ["weight_pileup_UP","weight_pileup_DOWN","weight_leptonSF_EL_SF_Trigger_UP","weight_leptonSF_EL_SF_Trigger_DOWN","weight_leptonSF_EL_SF_Reco_UP","weight_leptonSF_EL_SF_Reco_DOWN","weight_leptonSF_EL_SF_ID_UP","weight_leptonSF_EL_SF_ID_DOWN","weight_leptonSF_EL_SF_Isol_UP","weight_leptonSF_EL_SF_Isol_DOWN","weight_leptonSF_MU_SF_Trigger_STAT_UP","weight_leptonSF_MU_SF_Trigger_STAT_DOWN","weight_leptonSF_MU_SF_Trigger_SYST_UP","weight_leptonSF_MU_SF_Trigger_SYST_DOWN","weight_leptonSF_MU_SF_ID_STAT_UP","weight_leptonSF_MU_SF_ID_STAT_DOWN","weight_leptonSF_MU_SF_ID_SYST_UP","weight_leptonSF_MU_SF_ID_SYST_DOWN","weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP","weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN","weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP","weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN","weight_leptonSF_MU_SF_Isol_STAT_UP","weight_leptonSF_MU_SF_Isol_STAT_DOWN","weight_leptonSF_MU_SF_Isol_SYST_UP","weight_leptonSF_MU_SF_Isol_SYST_DOWN","weight_leptonSF_MU_SF_TTVA_STAT_UP","weight_leptonSF_MU_SF_TTVA_STAT_DOWN","weight_leptonSF_MU_SF_TTVA_SYST_UP","weight_leptonSF_MU_SF_TTVA_SYST_DOWN","weight_photonSF_ID_UP","weight_photonSF_ID_DOWN","weight_photonSF_effIso_UP","weight_photonSF_effIso_DOWN","weight_jvt_UP","weight_jvt_DOWN","weight_bTagSF_DL1r_85_eigenvars_B_up","weight_bTagSF_DL1r_85_eigenvars_C_up","weight_bTagSF_DL1r_85_eigenvars_Light_up","weight_bTagSF_DL1r_85_eigenvars_B_down","weight_bTagSF_DL1r_85_eigenvars_C_down","weight_bTagSF_DL1r_85_eigenvars_Light_down","weight_bTagSF_DL1r_85_extrapolation_up","weight_bTagSF_DL1r_85_extrapolation_down","weight_bTagSF_DL1r_85_extrapolation_from_charm_up","weight_bTagSF_DL1r_85_extrapolation_from_charm_down"]

    list_of_datadriven_syst = ["efakeSF_Up","efakeSF_Down","hfakeSF_Up","hfakeSF_Down"]
    #self.write_config_fakes()
    if(all_syst):
      for tree in tree_list:
        # only use up and generate down with that tree name
        if(tree == 'nominal'): continue
        if 'MET_SoftTrk_ResoP' in tree: continue # added by write_additonal_met_syst()
        if not 'down' in tree:
          if not "PseudoData" in tree:
            self.write_config_syst(tree[:-2])
          if "PseudoData" in tree:
            self.write_config_syst_JET_JER_pseudoData(tree[:-13]) # in case of "JET_JER_EffectiveNP_3__1down_PseudoData" cut till *3_1 then add other part at the end 
  
      match_list=['down', 'DOWN', 'Down']
      Dict = {"UP":"DOWN", "Up":"Down", "up":"down"}
      match_list_btag = ["DL1r_70","DL1r_77","DL1r_Continuous"]
      ignore_list = ["leptonFake", "efake", "hfake"]
      for branch in list_of_obj_syst: # generate object systematics
        if any(x in branch for x in match_list_btag): continue
        if any(x in branch for x in ignore_list): continue
        if not any(x in branch for x in match_list):
          split_branch_str = branch.split("_") # split the string branch and get last two letter, it should be "UP/Up/up"
  
          str_containting_up = split_branch_str[len(split_branch_str)-1]
          if(debug): print("{}: {}".format(str_containting_up, Dict[str_containting_up]) )
          self.write_config_obj_syst(branch[:-2], str_containting_up, Dict[str_containting_up])
  
      # generate for data driven syst 
      #for syst in list_of_datadriven_syst: 
      #  if not any(x in syst for x in match_list):
      #    self.write_config_datadriven_syst(syst[:-2])
      self.write_config_datadriven_syst()
      # write config for sample modelling
      self.write_config_modelling()
      # adding normalization uncertainty
      self.write_bkg_norm_uncertainty()
      ## write lumi
      self.write_luminosity()
      ## write muR_muF
      self.write_muR_muF()
      ## write additonal met syst
      self.write_addition_met_syst()

if __name__=='__main__':
  config_file_name= "data_mc_comp.config"
  #config_file_name= "tty2l_pt_emu_all_syst.config"
  gen_trex_file = GenerateTrexConfig(config_file_name)
  gen_trex_file.write_config()
