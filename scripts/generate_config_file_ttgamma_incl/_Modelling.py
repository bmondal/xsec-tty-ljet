from np_dict_tty_ttZ_v1 import *
class Modelling:
  def write_config_modelling(self):
#    # ttgamma_prod var3c
    if(self.ttgamma_prod_var3c):
      self.file.write(" \n")
      self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_var3c"))
      self.file.write("  Samples: tty_incl \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.ttgamma_incl_prod_var3cUp"))
      self.file.write("  ResponseMatrixFileDown: {} \n".format("histograms.ttgamma_incl_prod_var3cDown"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_var3c"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_var3c"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_var3c"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_var3c"]["Title"]))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    #tty_dec var3c
    if (self.ttgamma_dec_var3c):
      self.file.write(" \n")
      self.file.write("UnfoldingSystematic: {} \n".format("tty_dec_var3c"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_var3c"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_var3c"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_var3c"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_var3c"]["Title"]))
      self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.tty_incl_dec_var3c_up"))
      self.file.write("  ResponseMatrixFileDown: {} \n".format("histograms.tty_incl_dec_var3c_down"))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: tty_incl\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("\n")

    #tty_dec fsr 
      #self.file.write(" \n")
      #self.file.write("UnfoldingSystematic: {} \n".format("tty_dec_fsr"))
      #self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_fsr"]["ttZ_np_name"]))
      #self.file.write("  Category: {} \n".format(np_dict["tty_dec_fsr"]["Category"]))
      #self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_fsr"]["SubCategory"]))
      #self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_fsr"]["Title"]))
      #self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.tty_incl_dec_fsr_up"))
      #self.file.write("  ResponseMatrixFileDown: {} \n".format("histograms.tty_incl_dec_fsr_down"))
      #self.file.write("  Symmetrisation: TWOSIDED \n")
      #self.file.write("  Type: HISTO \n")
      #self.file.write("  Samples: tty_incl\n")
      #self.file.write("  Regions: {} \n".format(self.regions))
      #self.file.write("\n")


    #promptY ttbar var3c
    if(self.prompty_ttbar_var3c):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("ttbar_var3c"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_var3c"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["ttbar_var3c"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_var3c"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_var3c"]["Title"]))
      #self.file.write("  HistoFileUp: {} \n".format("histograms.tt_var3c_up"))
      #self.file.write("  HistoFileDown: {} \n".format("histograms.tt_var3c_down"))
      self.file.write("  HistoFilesUp: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tt_var3c_up"))
      self.file.write("  HistoFilesDown: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tt_var3c_down"))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: prompty\n")
      self.file.write("  Regions: {} \n".format(self.regions))

    # ttgamma_prod Herwig7
    if(self.ttgamma_prod_herwig7):
      self.file.write(" \n")
      self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_Herwig7"))
      self.file.write("  Samples: tty_incl\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.ttgamma_incl_prod_H7"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_Herwig7"]["Title"]))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # tty_dec Herwig7
    if(self.ttgamma_dec_herwig7):
      self.file.write(" \n")
      self.file.write("UnfoldingSystematic: {} \n".format("tty_dec_Herwig7"))
      self.file.write("  Samples: tty_incl\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_Herwig7"]["Title"]))
      self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.ttgamma_incl_dec_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # prompty ttbar Herwig7
    if(self.prompty_ttbar_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("prompty_ttbar_Herwig7"))
      self.file.write("  Samples: prompty\n")
      self.file.write("  HistoFilesUp: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.promptY_ttbar_H7"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_ttbar_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["prompty_ttbar_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_ttbar_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_ttbar_Herwig7"]["Title"]))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # Wty Herwig7
    if(self.prompty_wty_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("prompty_Wty_Herwig7"))
      #self.file.write("  Samples:  Wty\n")
      self.file.write("  Samples: prompty\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_Wty_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["prompty_Wty_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_Wty_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_Wty_Herwig7"]["Title"]))
      #self.file.write("  HistoFileUp: {} \n".format("histograms.Wty_H7"))
      self.file.write("  HistoFilesUp: \"{}\",\"histograms.wjets\",\"histograms.wt_inclusive\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.Wty_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")

    # Wt Herwig7
    if(self.prompty_wty_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("prompty_Wt_Herwig7"))
      #self.file.write("  Samples:  Wty\n")
      self.file.write("  Samples: prompty\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_Wt_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["prompty_Wt_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_Wt_Herwig7"]["SubCategory"]))
      #self.file.write("  HistoFileUp: {} \n".format("histograms.Wty_H7"))
      self.file.write("  HistoFilesUp: \"{}\",\"histograms.wjets\",\"histograms.Wty\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.wt_inclusive_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      #self.file.write("  Title: \"{}\" \n".format("Wty_Herwig7"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_Wt_Herwig7"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # efake tty_prod Herwig7
    if(self.efake_ttgamma_prod_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_tty_prod_Herwig7"))
      self.file.write("  Samples:  efake_tty_NLO_prod \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_Herwig7"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_Herwig7"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_Herwig7"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_Herwig7"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {} \n".format("histograms.efake_tty_prod_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
    # efake tty_dec Herwig7
    if(self.efake_ttgamma_dec_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_tty_dec_Herwig7"))
      self.file.write("  Samples:  efake_tty_LO_dec\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_Herwig7"]["ttZ_np_name"])) # naming used to correlate the systematics
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_Herwig7"]["Category"])) # naming used to correlate the systematics
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_Herwig7"]["SubCategory"])) # naming used to correlate the systematics
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_Herwig7"]["Title"])) # naming used to correlate the systematics
      self.file.write("  HistoFileUp: {} \n".format("histograms.efake_tty_dec_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
    # efake ttbar Herwig7
    if(self.efake_ttbar_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_ttbar_Herwig7"))
      self.file.write("  Samples:  efake_ttbar\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_Herwig7"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["ttbar_Herwig7"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_Herwig7"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_Herwig7"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {} \n".format("histograms.efake_ttbar_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
    # efake ttbar hdamp_var 
    if(self.efake_ttbar_hdamp):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_ttbar_hdamp_var"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_hdamp_var"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["ttbar_hdamp_var"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_hdamp_var"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_hdamp_var"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {} \n".format("histograms.efake_ttbar_hdamp_var"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: efake_ttbar \n")
      self.file.write("  Regions: {} \n".format(self.regions))
    # efake ttbar aMC 
    if(self.efake_ttbar_aMC):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_ttbar_aMC"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_aMC"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["ttbar_aMC"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_aMC"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_aMC"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {} \n".format("histograms.efake_ttbar_aMC"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: efake_ttbar \n")
      self.file.write("  Regions: {} \n".format(self.regions))
    # efake tty_prod var3c 
    if(self.efake_ttgamma_prod_var3c):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_tty_prod_var3c"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_var3c"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_var3c"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_var3c"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_var3c"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {} \n".format("histograms.efake_tty_prod_var3c_Up"))
      self.file.write("  HistoFileDown: {} \n".format("histograms.efake_tty_prod_var3c_Down"))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: efake_tty_NLO_prod \n")
      self.file.write("  Regions: {} \n".format(self.regions))
    # NOTE: we don't need modelling variation for hfake
#    # hfake tty_prod Herwig7
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("hfake_tty_prod_Herwig7"))
#    self.file.write("  Samples:  hfake\n")
#    self.file.write("  NuisanceParameter: {} \n".format("hfake_tty_prod_Herwig7"))
#    self.file.write("  Category: {} \n".format("hfake_tty_prod_Herwig7"))
#    self.file.write("  SubCategory: {} \n".format("hfake_tty_prod_Herwig7"))
#    self.file.write("  HistoFileUp: {} \n".format("histograms.hfake_tty_prod_H7"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("hfake_tty_prod_Herwig7"))
#    self.file.write("  Type: HISTO \n")
#    # hfake tty_dec Herwig7
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("hfake_tty_dec_Herwig7"))
#    self.file.write("  Samples:  hfake\n")
#    self.file.write("  NuisanceParameter: {} \n".format("hfake_tty_dec_Herwig7"))
#    self.file.write("  Category: {} \n".format("hfake_tty_dec_Herwig7"))
#    self.file.write("  SubCategory: {} \n".format("hfake_tty_dec_Herwig7"))
#    self.file.write("  HistoFileUp: {} \n".format("histograms.hfake_tty_dec_H7"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("hfake_tty_dec_Herwig7"))
#    self.file.write("  Type: HISTO \n")
#    # hfake tty_prod var3c 
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("hfake_tty_prod_var3c"))
#    self.file.write("  NuisanceParameter: {} \n".format("hfake_tty_prod_var3c"))
#    self.file.write("  Category: {} \n".format("hfake_tty_prod_var3c"))
#    self.file.write("  SubCategory: {} \n".format("hfake_tty_prod_var3c"))
#    self.file.write("  HistoFileUp: {} \n".format("histograms.hfake_tty_prod_var3cUp"))
#    self.file.write("  HistoFileDown: {} \n".format("histograms.hfake_tty_prod_var3cDown"))
#    self.file.write("  Symmetrisation: ONESIDED \n")
#    self.file.write("  Title: \"{}\" \n".format("hfake_tty_prod_var3c"))
#    self.file.write("  Type: HISTO \n")
#    self.file.write("  Samples: hfake \n")
    # MadGraph 
#    if(self.prompty_wty_aMC):
#      self.file.write(" \n")
#      self.file.write("Systematic: {} \n".format("prompty_Wty_MadGraph"))
#      self.file.write("  NuisanceParameter: {} \n".format("Wty_MadGraph"))
#      self.file.write("  Category: {} \n".format("Wty_MadGraph"))
#      self.file.write("  SubCategory: {} \n".format("Wty_MadGraph"))
#      self.file.write("  Symmetrisation: ONESIDED \n")
#      self.file.write("  Title: \"{}\" \n".format("Wty_MadGraph"))
#      self.file.write("  Type: HISTO \n")
#      #self.file.write("  Samples: Wty\n")
#      self.file.write("  Samples: prompty\n")
    # promptY ttbar hdamp_var 
    if(self.prompty_ttbar_hdamp):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("prompty_ttbar_hdamp_var"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_ttbar_hdamp_var"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["prompty_ttbar_hdamp_var"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_ttbar_hdamp_var"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_ttbar_hdamp_var"]["Title"])) # correlated
      #self.file.write("  HistoFileUp: {} \n".format("histograms.promptY_ttbar_hdamp_var"))
      self.file.write("  HistoFilesUp: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.promptY_ttbar_hdamp_var"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: prompty\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  ReferenceSample: ttbar_AFII \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # prompY ttbar pt_hard 
    if(self.prompty_ttbar_pt_hard):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("prompty_ttbar_pt_hard"))
      #self.file.write("  HistoFileUp: {} \n".format("histograms.promptY_ttbar_aMC"))
      self.file.write("  HistoFilesUp: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.promptY_ttbar_pt_hard"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_ttbar_pt_hard"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["prompty_ttbar_pt_hard"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_ttbar_pt_hard"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_ttbar_pt_hard"]["Title"])) # correlated
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: prompty\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  ReferenceSample: ttbar_AFII \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # DS 
#    if(self.prompty_wty_DS):
#      self.file.write(" \n")
#      self.file.write("Systematic: {} \n".format("prompty_Wty_DS"))
#      #self.file.write("  HistoFileUp: {} \n".format("histograms.Wty_DS"))
#      self.file.write("  NuisanceParameter: {} \n".format("Wty_DS"))
#      self.file.write("  Category: {} \n".format("Wty_DS"))
#      self.file.write("  SubCategory: {} \n".format("Wty_DS"))
#      self.file.write("  Symmetrisation: ONESIDED \n")
#      self.file.write("  Title: \"{}\" \n".format("Wty_DS"))
#      self.file.write("  Type: HISTO \n")
#      #self.file.write("  Samples: Wty \n")
#      self.file.write("  Samples: prompty\n")
