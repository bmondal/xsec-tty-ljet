#!/usr/bin/env python
from ROOT import TFile
import os
debug=False

def remove_last_element(input_string):
  new_string = input_string.split("_")
  string = ''
  for x in range(0,len(new_string)-1):
      part=new_string[x]
      if (string == ''):
          string = string + part
      else:
          string = string +"_"+ part
  print(string)
  return string

def get_formatted_string(input_string, n_elements):
  new_string = input_string.split("_")
  string = ''
  for x in range(0, n_elements):
      part=new_string[x]
      if (string == ''):
          string = string + part
      else:
          string = string +"_"+ part
  print(string)
  return string



'''
Get information from root file, for example list of trees present, ....
'''
class ReadFromRootFile:
  def __init__(self, file):
    self.filename = file
    self.f  = TFile.Open("{}".format(self.filename))
    self.tree_list = []
    self.nominal_exp_syst_list = []

  ## get list of trees from the root file
  def get_list_of_trees(self):
    #f = TFile.Open("{}".format(self.filename))
    keys = self.f.GetListOfKeys()
    for key in keys:
      self.tree_list.append(key.GetName())
      if debug: print(key.GetName())
    return self.tree_list

  ## get list of experimental systematic uncertianties branches from the nominal tree
  def get_list_of_uncer_from_nominal(self):
    #f = TFile.Open("{}".format(self.filename))
    tree = self.f.Get("nominal")
    branches = tree.GetListOfBranches()
    matches = ["_down", "_up", "_Down", "_Up", "_DOWN", "_UP"]
    for branch in branches:
      if any(x in branch.GetName() for x in matches): ## object systematic tree will surely contain the 'up' and 'down' in the name
        self.nominal_exp_syst_list.append(branch.GetName())
        if debug: print(branch.GetName())
    return self.nominal_exp_syst_list

  ## write piece of cpp code to get the event weight corresponding to different object syst variation
  def object_weight_write_code_for_me(self):
    list_of_objeff_syst = self.get_list_of_uncer_from_nominal()
    thisdir = os.getenv('PWD')
    ## edit the cpp file Algorithm.cpp

    #  // apply the sample weight
    #  m_eventWeight = p_sampleWeight;
    #
    #  // apply the MC weight, if requested
    #  m_eventWeight = 1;
    #  if(!p_isMC) return;
    #  m_eventWeight = m_data->weight_mc*m_data->weight_pileup*m_data->weight_leptonSF*m_data->weight_photonSF*m_data->weight_jvt*m_data->event_norm*m_data->event_lumi*m_data->weight_bTagSF_DL1r_85;
    #  if(additionalFlagForWeight == ""){
    #  }
    #  if(additionalFlagForWeight == "") return; // don't execute the below ones if not needed
    #  if( (additionalFlagForWeight != "") && (additionalFlagForWeight == "weight_pileup_UP")){
    #    std::cout<<"========= WARNING wieght_pileup_UP ==============="<<std::endl;
    #    m_eventWeight = m_eventWeight*m_data->weight_pileup_UP/m_data->weight_pileup;
    #  }
    #  else if( (additionalFlagForWeight != "") && (additionalFlagForWeight == "weight_pileup_DOWN")){
    #    std::cout<<"========= WARNING wieght_pileup_DOWN ==============="<<std::endl;
    #    m_eventWeight = m_eventWeight*m_data->weight_pileup_DOWN/m_data->weight_pileup;
    #  }

    f= open('{}/../src/Algorithm.cpp.backup'.format(thisdir),'r')
    wFile = open("{}/../src/Algorithm.cpp".format(thisdir),"w")
    lines = f.readlines()
    for line in lines:
     wFile.write(line)
     if "Algorithm::calculateWeight_nominal()" in line:
      wFile.write("  // apply the sample weight \n")
      wFile.write("  m_eventWeight = p_sampleWeight; \n")
      wFile.write("  // apply the MC weight, if requested \n")
      wFile.write("  m_eventWeight = 1; \n")
      wFile.write("  if(!p_isMC) return; \n")
      wFile.write("    m_eventWeight = m_data->weight_mc*m_data->weight_pileup*m_data->weight_leptonSF*m_data->weight_photonSF*m_data->weight_jvt*m_data->event_norm*m_data->event_lumi*m_data->weight_bTagSF_DL1r_85; \n")
      wFile.write("  if(additionalFlagForWeight == \"\"){ \n")
      wFile.write("  } \n")
      wFile.write("  if(additionalFlagForWeight == \"\") return; // don't execute the below ones if not needed \n")
      for syst in list_of_objeff_syst:
        if "leptonFake" in syst: continue # don't need for lepton fake
        if "EL_SF" in syst:
          wFile.write("  if( (additionalFlagForWeight != \"\") && (is_ejet()) && (additionalFlagForWeight == \"{0}\"))\n".format(syst))
        elif "MU_SF" in syst:
          wFile.write("  if( (additionalFlagForWeight != \"\") && (is_mujet()) && (additionalFlagForWeight == \"{0}\"))\n".format(syst))
        else:
          wFile.write("  if( (additionalFlagForWeight != \"\") && (additionalFlagForWeight == \"{0}\"))\n".format(syst))
        wFile.write("  { \n")
        #wFile.write("    std::cout<<\"========= WARNING {} used for event weight calculation ===============\"<<std::endl; \n".format(syst))
        if "fakeSF" in syst:
          wFile.write(" if((m_data->{0} != 0) && (m_data->{1} != 0) && std::isfinite(m_data->{0}) && std::isfinite(m_data->{1}))    m_eventWeight = m_eventWeight*m_data->{0}/m_data->{1}; \n".format(syst,get_formatted_string(syst, 1 ))) ## make weight_bTagSF_DL1r_85_eigenvars_B_up to weight_bTagSF_DL1r_85, n_elements is 3 in this case
        elif not "bTagSF" in syst: 
          if "SIMPLIFIED" in syst:
            wFile.write(" if((m_data->{0}->at(0) != 0) && (m_data->{1} != 0) && std::isfinite(m_data->{0}->at(0)) && std::isfinite(m_data->{1}))   m_eventWeight = m_eventWeight*m_data->{0}->at(0)/m_data->{1}; \n".format(syst,get_formatted_string(syst,2)))
          else:
            wFile.write("  if((m_data->{0} != 0) && (m_data->{1} != 0)  && std::isfinite(m_data->{0}) && std::isfinite(m_data->{1}))   m_eventWeight = m_eventWeight*m_data->{0}/m_data->{1}; \n".format(syst,get_formatted_string(syst,2)))

        elif "eigenvars" in syst:
          wFile.write("  if((m_data->{0}->at(0) != 0) && (m_data->{1} != 0) && std::isfinite(m_data->{0}->at(0)) && std::isfinite(m_data->{1}))   m_eventWeight = m_eventWeight*m_data->{0}->at(0)/m_data->{1}; \n".format( syst,get_formatted_string(syst, 4 ))) ## make weight_bTagSF_DL1r_85_eigenvars_B_up to weight_bTagSF_DL1r_85, n_elements is 3 in this case
        else:
          wFile.write("  if((m_data->{0} != 0) && (m_data->{1} != 0) && std::isfinite(m_data->{0}) && std::isfinite(m_data->{1}))   m_eventWeight = m_eventWeight*m_data->{0}/m_data->{1}; \n".format(syst,get_formatted_string(syst, 4 ))) ## make weight_bTagSF_DL1r_85_eigenvars_B_up to weight_bTagSF_DL1r_85, n_elements is 3 in this case
        wFile.write("  } \n")

      wFile.write("} \n")
      break




if __name__=="__main__":
  input_file_for_reading_tree = "/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/fit_v12_ljets_injected/mc16a_TOPQ1_ttgamma_NLO_prod504554_ejets.1.root"
  obj = ReadFromRootFile(input_file_for_reading_tree)
  #for list in obj.get_list_of_trees():
  #  print(list)
  #weight_syst = obj.get_list_of_uncer_from_nominal() # list containing tree names
  #for syst in weight_syst:
  #  print(syst)
  #obj.object_weight_write_code_for_me()
