from getInfoFromRootFile import *
from np_title_dict import *
from Config import *
import argparse

import _JobNSamples  # job and sample blocks
import _BkgNorm # background normalization uncertainty block
import _Modelling # modelling uncertainty block
import _MuRMuFpdf # muR, muF, pdf uncertainty block
import _WriteOtherSyst

def get_last_part(string):
  split_string = string.split("/")
  return split_string[-1]


def get_title(np_name):
  for key in np_dict.keys():
    if np_name in key:
      return np_dict[key]


class GenerateTrexConfig(_JobNSamples.JobNSamples, _BkgNorm.BkgNorm, _Modelling.Modelling, _MuRMuFpdf.MuRMuFpdf, _WriteOtherSyst.WriteOtherSyst):
  def __init__(self, filename):
    file_path_name = filename
    file_name = get_last_part(file_path_name)
    self.job_name = file_name.split(".")[0]
    self.config = file_path_name
    self.file = open(self.config, 'w')

    self.use_data = False 
    self.write_ttbar_af2 = True 
    self.write_pdf_ref_sample = True 
    self.debug = False
    self.all_syst =False 
    self.exp_tree_syst = False
    self.exp_weight_syst = False
    self.data_driven_syst = False
    self.modelling_syst = False
    self.bkg_norm_syst = False
    self.btag_lepton_syst = False
    self.muR_muF_syst = False
    self.met_syst = False 
    self.lumi_syst = False 
    self.use_real_data = False
    self.use_reweighted_pseudodata = False
    self.reweighted_data_name = ""
    self.fit_blind = True
    
    ##*************************** CONFIGURATION *******************
    # path to the unfolding input histograms
    #self.path_to_input_folder= "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/"
    self.path_to_input_folder= input_histograms
    self.path_to_input_histos_tty_prod_CR = self.path_to_input_folder + "tty_CR"
    self.path_to_input_histos_tty_dec_CR = self.path_to_input_folder + "tty_dec_CR"
    self.path_to_input_histos_fakes_CR = self.path_to_input_folder + "fakes_CR"
    self.path_to_input_histos_other_photons_CR= self.path_to_input_folder + "other_photons_CR"
    # one root file for reading branches from tree
    self.one_ntuple_file_for_reading_trees = "{}/../../share/mc16a_TOPQ1_singletop_schan410644.1.root".format(os.getcwd())

    # response matrix paths
    self.response_matrix_path = self.path_to_input_histos_tty_prod_CR 
    self.response_matrix_path_SR1 = self.path_to_input_histos_tty_prod_CR + "/nominal_9999/"
    self.response_matrix_path_SR2 = self.path_to_input_histos_tty_dec_CR + "/nominal_9999/"
    self.response_matrix_path_SR3 = self.path_to_input_histos_fakes_CR+ "/nominal_9999/"
    self.response_matrix_path_SR4 = self.path_to_input_histos_other_photons_CR+ "/nominal_9999/"

    # response matrix file
    self.response_matrix_file = "histograms.ttgamma_incl"
    self.response_matrix_file_name = "histograms.ttgamma_incl"
    self.response_matrix_name = "h2_response_matrix_ph_eta"
    # histo paths
    self.histo_path_SR1 = self.path_to_input_histos_tty_prod_CR+"/nominal_9999/"
    self.histo_path_SR2 = self.path_to_input_histos_tty_dec_CR+"/nominal_9999/"
    self.histo_path_SR3 = self.path_to_input_histos_fakes_CR+"/nominal_9999/"
    self.histo_path_SR4 = self.path_to_input_histos_other_photons_CR+"/nominal_9999/"
    # truth distribution
    self.truth_distribution_file = "histograms.ttgamma_incl"
    self.truth_distribution_file_H7 = "histograms.ttgamma_incl_H7"
    self.truth_distribution_file_tty_nominal = "histograms.ttgamma_nominal"
    self.truth_distribution_file_tty_nominal_H7 = "histograms.ttgamma_nominal_H7"
    self.truth_distribution_name = "particle/hist_part_ph_eta_full_weighted"
    self.truth_distribution_path = self.path_to_input_histos_tty_prod_CR+"/nominal_9999/"
    # histo name
    self.histo_name = "Reco/hist_reco_ph_eta_full_weighted"
    # number of bins
    self.number_of_reco_bins = 8 
    self.regions = "SR1,SR2,SR3,SR4"
    # title
    self.xaxisTitle = "|#eta(#gamma)|"
    if absolute_xsec_unfolding: self.yaxisTitle = "#frac{d#sigma}{d|#eta(#gamma)|} [fb]"
    if normalized_xsec_unfolding: self.yaxisTitle = "#frac{1}{#sigma}#times#frac{d#sigma}{d|#eta(#gamma)|}"
    self.variable_title = "|#eta(#gamma)| "
    self.debug = False
    self.all_syst = False

  def refresh_config(self):
    # modelling configuration
    if(self.modelling_syst):
      self.ttgamma_prod_var3c = True
      self.ttgamma_prod_herwig7 = True
      self.efake_ttgamma_prod_herwig7 = False
      self.efake_ttgamma_prod_var3c= False
      self.ttgamma_dec_var3c = True 
      self.ttgamma_dec_herwig7 = True
      self.efake_ttgamma_dec_herwig7 = False 
      self.prompty_ttbar_var3c = True 
      self.prompty_ttbar_herwig7 = True
      self.prompty_ttbar_hdamp= True
      self.prompty_ttbar_aMC= True
      self.efake_ttbar_herwig7 = False 
      self.efake_ttbar_hdamp = False 
      self.efake_ttbar_aMC = False 
      self.prompty_wty_herwig7 = True 
      self.prompty_wty_aMC = False
      self.prompty_wty_DS = False
    # scale uncertainties
    if(self.muR_muF_syst):
      self.wty_muR_muF = True 
      self.ttbar_muR_muF = True
      self.ttgamma_dec_muR_muF = True
      self.ttgamma_prod_muR_muF = True 

    if self.all_syst:
      self.met_syst = True 
      self.lumi_syst = True 
      self.write_ttbar_af2 = True
      self.exp_tree_syst = True
      self.exp_weight_syst = True
      self.data_driven_syst = True
      self.modelling_syst = True
      self.bkg_norm_syst = True
      self.muR_muF_syst = True
      self.btag_lepton_syst = True
      # turn on modelling
      self.ttgamma_prod_var3c = True
      self.ttgamma_prod_herwig7 = True
      self.efake_ttgamma_prod_herwig7 = False
      self.efake_ttgamma_prod_var3c= False
      self.ttgamma_dec_var3c = True 
      self.ttgamma_dec_herwig7 = True
      self.efake_ttgamma_dec_herwig7 = False 
      self.prompty_ttbar_var3c = True
      self.prompty_ttbar_herwig7 = True
      self.prompty_ttbar_hdamp= True
      self.prompty_ttbar_pt_hard= True
      self.efake_ttbar_herwig7 = False
      self.efake_ttbar_hdamp = False
      self.efake_ttbar_aMC = False
      self.prompty_wty_herwig7 = True 
      self.prompty_wty_aMC = True
      self.prompty_wty_DS = True
      # turn all scale
      self.wty_muR_muF = True 
      self.ttbar_muR_muF = True
      self.ttgamma_dec_muR_muF = True
      self.ttgamma_prod_muR_muF = True
      # pdfs
      self.pdf_tty_prod = True
      self.pdf_tty_dec = True

    # naming
    self.prompty_group_name = "Prompt y"

    # list of prompt samples


if __name__=='__main__':
  parser = argparse.ArgumentParser(description ='create trex-config file for ph pt')
  parser.add_argument("--filename", type=str, help="config file name")
  parser.add_argument("--path-to-unfolding-hist", type=str, help="path to the unfolding histograms folder; example:~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v6/")
  parser.add_argument("--write-data", action='store_true', help="write data sample in the file")
  parser.add_argument("--use-real-data", action='store_true', help="use pseudo-data")
  parser.add_argument("--write-syst", action='store_true', help="write syst")
  parser.add_argument("--fit-blind", action='store_false', help="write syst")
  parser.add_argument("--use_reweighted_pseudodata", action='store_true', help="write syst")
  parser.add_argument("--reweighted_data_name", type=str, help="reweighted data name")
  args = parser.parse_args()
  #config_file_name= "tty2l_pt_all_syst.config"
  config_file_path_name=args.filename
  gen_trex_file = GenerateTrexConfig(config_file_path_name)
  gen_trex_file.path_to_input_folder = args.path_to_unfolding_hist
  if(args.write_data):
    gen_trex_file.use_data = True
  if(args.write_syst):
    gen_trex_file.all_syst = True
  if(not args.fit_blind):
    gen_trex_file.fit_blind= False
  if(args.use_real_data):
    gen_trex_file.use_real_data = True
  if(args.use_reweighted_pseudodata):
    gen_trex_file.use_reweighted_pseudodata = True
    gen_trex_file.reweighted_data_name = args.reweighted_data_name
  gen_trex_file.refresh_config()
  gen_trex_file.write_whole_config()
  print(">>>> Finished generating config: {} <<<<<\n".format(config_file_path_name))
