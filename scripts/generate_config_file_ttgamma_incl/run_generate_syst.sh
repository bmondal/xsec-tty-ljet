#!/usr/bin/env bash
# This script is for generating file and copyting all the config file to a appropriate folder for running unfolding
function fit_asimov() {
  path_to_unfolding_inputs="~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/" # this doesn't work now
  save_folder="./syst-all"
  mkdir -p $save_folder
  python GenerateUnfoldingTrexConfig_pt.py --filename $save_folder/tty1l_pt_all_syst.config --write-syst  --path-to-unfolding-hist $path_to_unfolding_inputs 
  python GenerateUnfoldingTrexConfig_eta.py --filename $save_folder/tty1l_eta_all_syst.config --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_dr.py --filename $save_folder/tty1l_dr_all_syst.config --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_ptj1.py --filename $save_folder/tty1l_ptj1_all_syst.config  --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_drphb.py --filename $save_folder/tty1l_drphb_all_syst.config  --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_drlj.py --filename $save_folder/tty1l_drlj_all_syst.config  --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs
}
function fit_data_mu_blinded() {
  save_folder="./syst-all-fit-data-mu-blinded"
  mkdir -p $save_folder
  path_to_unfolding_inputs="~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/" # this doesn't work now
  python GenerateUnfoldingTrexConfig_pt.py --filename $save_folder/tty1l_pt_all_syst.config --write-syst  --path-to-unfolding-hist $path_to_unfolding_inputs --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_eta.py --filename $save_folder/tty1l_eta_all_syst.config --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_dr.py --filename $save_folder/tty1l_dr_all_syst.config --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_ptj1.py --filename $save_folder/tty1l_ptj1_all_syst.config  --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_drphb.py --filename $save_folder/tty1l_drphb_all_syst.config  --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_drlj.py --filename $save_folder/tty1l_drlj_all_syst.config  --write-syst --path-to-unfolding-hist $path_to_unfolding_inputs --write-data --use-real-data --fit-blind
}


