import ROOT
ROOT.gStyle.SetOptStat(0)

debug = False


regions_title_dict = {"tty_CR":"SR",
  "tty_dec_CR":"t#bar{t}#gamma dec CR",
  "fakes_CR":"fakes CR",
  "other_photons_CR":"Other #gamma CR"}

def main():
  #regions = ["tty_CR","tty_dec_CR","fakes_CR","other_photons_CR"]
  regions = ["tty_CR"]
  variables = ["ph_pt","ph_eta","ph_drphl1","drphb","drlj","j1_pt"]
  for var in variables:
    for CR in regions:
      # Path to your root file
      file_reg1 = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v9/{}/nominal_9999/histograms.ttgamma_incl.nominal_from_fine.root".format(CR)

      # Histogram names
      hist_reco_name = "Reco/hist_reco_{}_full_weighted".format(var)
      hist_particle_name = "particle/hist_part_{}_full_weighted".format(var)
      hist_mig_name = "h2_{}_reco_part_full_weighted".format(var)
      hist_fout_name = "h1_fout_{}_reco_part_full_weighted".format(var)
      
      # Open the file and get the histograms
      tfile_reg1 = ROOT.TFile.Open(file_reg1)
      hist_particle = tfile_reg1.Get(hist_particle_name)
      hist_reco = tfile_reg1.Get(hist_reco_name)
      hist_mig = tfile_reg1.Get(hist_mig_name)
      hist_fout = tfile_reg1.Get(hist_fout_name)
      
      # Create a 1D histogram to store the diagonal of the migration matrix
      create_mig_diag_1D_hist = hist_particle.Clone()
      
      # Loop over the bins of the migration matrix and fill the diagonal into the 1D histogram
      for bin_i in range(1, hist_mig.GetNbinsX()+1):
        for bin_j in range(1, hist_mig.GetNbinsY()+1):
          if bin_i == bin_j:
            create_mig_diag_1D_hist.SetBinContent(bin_i, hist_mig.GetBinContent(bin_i, bin_j))
            create_mig_diag_1D_hist.SetBinError(bin_i, hist_mig.GetBinError(bin_i, bin_j))
      
      # Create the efficiency histogram by dividing the diagonal histogram by the particle-level histogram
      efficiency_hist = create_mig_diag_1D_hist.Clone()
      efficiency_hist.Divide(hist_particle)

      # Create a histogram to store fout and calculate 1-fout
      #new_fout_hist = hist_fout.Clone()
      #new_fout_hist = create_mig_diag_1D_hist.Clone()
      one_minus_fout_hist= create_mig_diag_1D_hist.Clone()

      #new_fout_hist.Divide(hist_reco)
      one_minus_fout_hist.Divide(hist_reco)
      
      #one_minus_fout_hist = new_fout_hist.Clone()
      #one_minus_fout_hist = new_fout_hist.Clone()
      #for i in range(1, new_fout_hist.GetNbinsX() + 1): 
      #  old_content = new_fout_hist.GetBinContent(i)
      #  new_content = 1 - old_content
      #  one_minus_fout_hist.SetBinContent(i, new_content)
      #  
      #  # Optionally, set the new bin errors
      #  old_error = new_fout_hist.GetBinError(i)
      #  new_error = old_error  # Adjust based on your error propagation strategy
      #  one_minus_fout_hist.SetBinError(i, new_error)

      hist_C = hist_reco.Clone()
      hist_C.Divide(hist_particle)
      hist_C.SetMarkerColor(46)
      hist_C.SetLineColor(46)

      one_minus_fout_hist.SetMarkerStyle(20)
      one_minus_fout_hist.SetMarkerSize(1.0)
      hist_C.SetMarkerStyle(20)
      hist_C.SetMarkerSize(1.0)
      efficiency_hist.SetMarkerStyle(20)
      efficiency_hist.SetMarkerSize(1.0)



      # Create a canvas and draw the efficiency histogram
      canvas = ROOT.TCanvas()
      one_minus_fout_hist.SetMarkerColor(30)
      one_minus_fout_hist.SetLineColor(30)
      one_minus_fout_hist.GetYaxis().SetRangeUser(0, 1.4)
      one_minus_fout_hist.GetYaxis().SetTitle("Factor")
      one_minus_fout_hist.Draw(" P E")
      efficiency_hist.SetMarkerColor(38)
      efficiency_hist.SetLineColor(38)
      efficiency_hist.Draw("P E same")
      #hist_C.Draw("P E same")

      # Create a TLegend object
      legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)  # Adjust the coordinates to place the legend where you want
      #legend.SetHeader("{}".format(regions_title_dict[CR]), "C")  # Optional: Set a header for the legend
      legend.AddEntry(efficiency_hist, "Efficiency", "lep")  # Add an entry for your histogram
      #legend.AddEntry(hist_C, "C", "lep")  # Add an entry for your histogram
      legend.AddEntry(one_minus_fout_hist, "Acceptance", "lep")  # Add an entry for your histogram
      legend.Draw()


      canvas.SaveAs("{}_{}_efficiency_acceptance.png".format(var,CR))

# Call the main function
if __name__ == "__main__":
  main()

