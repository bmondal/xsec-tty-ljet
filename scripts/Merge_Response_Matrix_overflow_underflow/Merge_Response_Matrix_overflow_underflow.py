import ROOT
import os
import argparse

def calculate_response_matrix(reco_hist, truth_hist, migration_hist):
    response_matrix = migration_hist.Clone()
    N_R = reco_hist.GetNbinsX()
    N_T = truth_hist.GetNbinsX()
    N_mig_R = migration_hist.GetNbinsX()
    N_mig_T = migration_hist.GetNbinsY()
    dumTcontent = -1.e6
    rbin_total = 0
    res_value = [[0 for x in range(1000)] for y in range(1000)]
    res_error = [[0 for x in range(1000)] for y in range(1000)]

    if ((N_R != N_T) or N_R == 0):
        print("Unequal/empty truth and/or reco")

    '''response matrix calculation...'''
    for bin_r in range(1, N_R + 1):
        rbin_total = 0.
        for bin_t in range(1, N_T + 1):
            rbin_total += migration_hist.GetBinContent(bin_r, bin_t)
        for bin_t in range(1, N_T + 1):
            dumTcontent = truth_hist.GetBinContent(bin_t)
            if not (rbin_total == 0 or dumTcontent == 0):
                res_value[bin_r - 1][bin_t - 1] = 1. * (migration_hist.GetBinContent(bin_r, bin_t)) * (
                            (reco_hist.GetBinContent(bin_r)) / (dumTcontent * rbin_total))
                res_error[bin_r - 1][bin_t - 1] = 1. * (migration_hist.GetBinError(bin_r, bin_t)) * (
                            (reco_hist.GetBinContent(bin_r)) / (dumTcontent * rbin_total))
            response_matrix.SetBinContent(bin_r, bin_t, res_value[bin_r - 1][bin_t - 1])
            response_matrix.SetBinError(bin_r, bin_t, res_error[bin_r - 1][bin_t - 1])

    return response_matrix

top_dir=""

def calcualte_and_save_response_matrices(filename):
    """binning: str; Update the files containing this string
    """
    for dirpath, dirnames, filenames in os.walk(top_dir):
        for filename in filenames:
            # Check if the file is a ROOT file and contains "Fine_Binning" in its name
            if filename.endswith('.root') and  (".ttgamma_prod." in filename or ".tty_prod_" in filename):
                # Open the original ROOT file
                f = ROOT.TFile(os.path.join(dirpath, filename), "update")
                # ph_pt
                reco_hist_pt = f.Get("Reco/hist_reco_ph_pt_full_weighted")
                truth_hist_pt = f.Get("particle/hist_part_ph_pt_full_weighted")
                migration_hist_pt = f.Get("h2_ph_pt_reco_part_full_weighted")
                response_matrix_pt = calculate_response_matrix(reco_hist_pt, truth_hist_pt, migration_hist_pt)
                response_matrix_pt.SetName("h2_response_matrix_ph_pt")
                response_matrix_pt.Write("h2_response_matrix_ph_pt")

                # ph_eta
                reco_hist_eta = f.Get("Reco/hist_reco_ph_eta_full_weighted")
                truth_hist_eta = f.Get("particle/hist_part_ph_eta_full_weighted")
                migration_hist_eta = f.Get("h2_ph_eta_reco_part_full_weighted")
                response_matrix_eta = calculate_response_matrix(reco_hist_eta, truth_hist_eta,
                                                                migration_hist_eta)
                response_matrix_eta.SetName("h2_response_matrix_ph_eta")
                response_matrix_eta.Write("h2_response_matrix_ph_eta")

                # dryl1
                reco_hist_dryl1 = f.Get("Reco/hist_reco_ph_drphl1_full_weighted")
                truth_hist_dryl1 = f.Get("particle/hist_part_ph_drphl1_full_weighted")
                migration_hist_dryl1 = f.Get("h2_ph_drphl1_reco_part_full_weighted")
                response_matrix_dryl1 = calculate_response_matrix(reco_hist_dryl1, truth_hist_dryl1,
                                                                  migration_hist_dryl1)
                response_matrix_dryl1.SetName("h2_response_matrix_ph_dryl1")
                response_matrix_dryl1.Write("h2_response_matrix_ph_dryl1")

                # drphb
                reco_hist_drphb = f.Get("Reco/hist_reco_drphb_full_weighted")
                truth_hist_drphb = f.Get("particle/hist_part_drphb_full_weighted")
                migration_hist_drphb = f.Get("h2_drphb_reco_part_full_weighted")
                response_matrix_drphb = calculate_response_matrix(reco_hist_drphb, truth_hist_drphb,
                                                                  migration_hist_drphb)
                response_matrix_drphb.SetName("h2_response_matrix_drphb")
                response_matrix_drphb.Write("h2_response_matrix_drphb")
                # drlj
                reco_hist_drlj = f.Get("Reco/hist_reco_drlj_full_weighted")
                truth_hist_drlj = f.Get("particle/hist_part_drlj_full_weighted")
                migration_hist_drlj = f.Get("h2_drlj_reco_part_full_weighted")
                response_matrix_drlj = calculate_response_matrix(reco_hist_drlj, truth_hist_drlj,
                                                                 migration_hist_drlj)
                response_matrix_drlj.SetName("h2_response_matrix_drlj")
                response_matrix_drlj.Write("h2_response_matrix_drlj")

                # ph_pt j1
                reco_hist_j1_pt = f.Get("Reco/hist_reco_j1_pt_full_weighted")
                truth_hist_j1_pt = f.Get("particle/hist_part_j1_pt_full_weighted")
                migration_hist_j1_pt = f.Get("h2_j1_pt_reco_part_full_weighted")
                response_matrix_j1_pt = calculate_response_matrix(reco_hist_j1_pt, truth_hist_j1_pt,
                                                                  migration_hist_j1_pt)
                response_matrix_j1_pt.SetName("h2_response_matrix_j1_pt")
                response_matrix_j1_pt.Write("h2_response_matrix_j1_pt")


                f.Close()



# Function to merge overflow and underflow bins of a TH2D histogram
def merge_overflow_underflow(histo):
    nx = histo.GetNbinsX()
    ny = histo.GetNbinsY()
    # Get the overflow and underflow bin content and error
    overflow_content = histo.GetBinContent(nx+1, ny+1)
    overflow_error = histo.GetBinError(nx+1, ny+1)
    underflow_content = histo.GetBinContent(0, 0)
    underflow_error = histo.GetBinError(0, 0)
    # Add overflow and underflow bin content and error to the last bin
    histo.SetBinContent(nx, ny, histo.GetBinContent(nx, ny) + overflow_content)
    histo.SetBinError(nx, ny, histo.GetBinError(nx, ny) + overflow_error)
    histo.SetBinContent(1, 1, histo.GetBinContent(1, 1) + underflow_content)
    histo.SetBinError(1, 1, histo.GetBinError(1, 1) + underflow_error)
    # Set overflow and underflow bin content and error to zero
    histo.SetBinContent(nx+1, ny+1, 0)
    histo.SetBinError(nx+1, ny+1, 0)
    histo.SetBinContent(0, 0, 0)
    histo.SetBinError(0, 0, 0)

"""
## Function to loop over folders and merge overflow and underflow bins of histograms
#def merge_histograms():
#    # Get the current directory
#    current_dir = os.getcwd()
#    # Get the list of subdirectories in the current directory
#    subdirs = [d for d in os.listdir(current_dir) if os.path.isdir(d)]
#    # Loop over the subdirectories
#    for subdir in subdirs:
#        # Check if the file "histograms.ttgamma_prod.root" exists in the subdirectory
#        file_path = subdir + "/histograms.ttgamma_prod.root"
#        if os.path.isfile(file_path):
#            # Open the root file and get the list of histograms
#            file = ROOT.TFile(file_path, "UPDATE")
#            histo_list = [key.GetName() for key in file.GetListOfKeys() if key.GetClassName() == "TH2D"]
#            # Loop over the histograms and merge overflow and underflow bins
#            for histo_name in histo_list:
#                if "response" not in histo_name:
#                    histo = file.Get(histo_name)
#                    merge_overflow_underflow(histo)
#                    histo.Write()
#            # Close the root file
#            file.Close()
"""
# Function to loop over folders and merge overflow and underflow bins of histograms
def merge_histograms():
    for dirpath, dirnames, filenames in os.walk(top_dir):
        for filename in filenames:
            # Check if the file is a ROOT file and contains "Fine_Binning" in its name
            if filename.endswith('.root') and  (".ttgamma_prod." in filename or ".tty_prod_" in filename):
                print(">>> file processing {} <<<<<<\n".format(os.path.join(dirpath,filename)))
                # Open the root file and get the list of histograms
                file = ROOT.TFile(os.path.join(dirpath,filename), "UPDATE")
                histo_list = [key.GetName() for key in file.GetListOfKeys() if key.GetClassName() == "TH2D"]
                # Loop over the histograms and merge overflow and underflow bins
                for histo_name in histo_list:
                    if "response" in histo_name:
                        histo = file.Get(histo_name)
                        file.Delete(histo_name)
                    else:
                        histo = file.Get(histo_name)
                        merge_overflow_underflow(histo)
                        histo.Write()
                # Close the root file
                file.Close()
#
## Function to loop over folders and merge overflow and underflow bins of histograms
#def merge_histograms():
#    # Get the current directory
#    current_dir = os.getcwd()
#    # Get the list of subdirectories in the current directory
#    subdirs = [d for d in os.listdir(current_dir) if os.path.isdir(d)]
#    # Loop over the subdirectories
#    for subdir in subdirs:
#        # Check if the file "histograms.ttgamma_prod.root" exists in the subdirectory
#        file_path = subdir + "/histograms.ttgamma_prod.root"
#        if os.path.isfile(file_path):
#            # Open the root file and get the list of histograms
#            file = ROOT.TFile(file_path, "READ")
#            new_file_path = file_path.replace(".root", "overflow_corrected.root")
#            new_file = ROOT.TFile(new_file_path, "RECREATE")
#            histo_list = [key.GetName() for key in file.GetListOfKeys() if key.GetClassName() == "TH2D"]
#            # Loop over the histograms and merge overflow and underflow bins
#            for histo_name in histo_list:
#                if "response" not in histo_name:
#                    histo = file.Get(histo_name)
#                    merge_overflow_underflow(histo)
#                    histo.Write()
#
# Function to loop over folders and merge overflow and underflow bins of histograms
#def merge_histograms():
#    # Get the current directory
#    current_dir = os.getcwd()
#    # Get the list of subdirectories in the current directory
#    subdirs = [d for d in os.listdir(current_dir) if os.path.isdir(d)]
#    # Loop over the subdirectories
#    for subdir in subdirs:
#        # Check if the file "histograms.ttgamma_prod.root" exists in the subdirectory
#        file_path = subdir + "/histograms.ttgamma_prod.root"
#        if os.path.isfile(file_path):
#            # Open the root file and get the list of histograms
#            file = ROOT.TFile(file_path, "READ")
#            new_file_path = file_path.replace(".root", "overflow_corrected.root")
#            new_file = ROOT.TFile(new_file_path, "RECREATE")
#            keys = file.GetListOfKeys()
#            for key in keys:
#                class_name = key.GetClassName()
#                name = key.GetName()
#                if class_name == "TDirectoryFile":
#                    dir = file.Get(name)
#                    dir.Write()
#                elif class_name == "TH2D":
#                    if "response" not in name:
#                        histo = file.Get(name)
#                        nx = histo.GetNbinsX()
#                        ny = histo.GetNbinsY()
#                        # Get the overflow and underflow bin content and error
#                        overflow_content = histo.GetBinContent(nx+1, ny+1)
#                        overflow_error = histo.GetBinError(nx+1, ny+1)
#                        underflow_content = histo.GetBinContent(0, 0)
#                        underflow_error = histo.GetBinError(0, 0)
#                        # Add overflow and underflow bin content and error to the last bin
#                        histo.SetBinContent(nx, ny, histo.GetBinContent(nx, ny) + overflow_content)
#                        histo.SetBinError(nx, ny, histo.GetBinError(nx, ny) + overflow_error)
#                        histo.SetBinContent(1, 1, histo.GetBinContent(1, 1) + underflow_content)
#                        histo.SetBinError(1, 1, histo.GetBinError(1, 1) + underflow_error)
#                        # Set overflow and underflow bin content and error to zero
#                        histo.SetBinContent(nx+1, ny+1, 0)
#                        histo.SetBinError(nx+1, ny+1, 0)
#                        histo.SetBinContent(0, 0, 0)
#                        histo.SetBinError(0, 0, 0)
#                        histo.Write()
#            new_file.Close()
#            file.Close()




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--top_dir", type=str, help="Top directory where the response matrices will be saved")
    args = parser.parse_args()

    top_dir=args.top_dir
    merge_histograms()
    calcualte_and_save_response_matrices(args.top_dir)
