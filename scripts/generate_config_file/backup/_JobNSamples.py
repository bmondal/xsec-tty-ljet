from Config import *
job_name_unfolding_name_dict = {
  "tty1l_pt_all_syst": "tty_pt",
  "tty1l_eta_all_syst": "tty_eta",
  "tty1l_dr_all_syst": "tty_drphl",
  "tty1l_drphb_all_syst": "tty_drphb",
  "tty1l_drlj_all_syst": "tty_drlj",
  "tty1l_ptj1_all_syst": "tty_ptj1",
  "tty1l_pt_all_stat": "tty_pt",
  "tty1l_eta_all_stat": "tty_eta",
  "tty1l_dr_all_stat": "tty_drphl",
  "tty1l_drphb_all_stat": "tty_drphb",
  "tty1l_drlj_all_stat": "tty_drlj",
  "tty1l_ptj1_all_stat": "tty_ptj1"
}

job_name_bin_width_dict = {
  "tty1l_pt_all_syst": 15,
  "tty1l_eta_all_syst": 0.2,
  "tty1l_dr_all_syst": 0.4,
  "tty1l_drphb_all_syst": 0.4,
  "tty1l_drlj_all_syst": 0.4,
  "tty1l_ptj1_all_syst": 55,
  "tty1l_pt_all_stat": 15,
  "tty1l_eta_all_stat": 0.2,
  "tty1l_dr_all_stat": 0.4,
  "tty1l_drphb_all_stat": 0.4,
  "tty1l_drlj_all_stat": 0.4,
  "tty1l_ptj1_all_stat": 55,
}
class JobNSamples:
  def write_config(self):
    self.file = open(self.config, 'w')
    self.file.write("Job: \"{}\"\n".format(self.job_name))
    #self.file.write("  Label: \"e-jets + #mu-jets\" \n")
    self.file.write("  Label: \"Single lepton\" \n")
    #self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path))
    #self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  CmeLabel: \"13 TeV\" \n")
    self.file.write("  AtlasLabel: Internal \n")
    self.file.write("  DebugLevel: 2 \n")
    self.file.write("  HistoPath: {}\n".format(self.histo_path_SR1))
    self.file.write("  LabelX: 0.17 \n")
    self.file.write("  LegendNColumns: 2 \n")
    self.file.write("  LegendX1: 0.48 \n")
    self.file.write("  LegendX2: 0.92 \n")
    self.file.write("  LegendY: 0.87 \n")
    self.file.write("  Logo: TRUE \n")
    self.file.write("  LumiLabel: \"140 fb^{-1}\" \n")
    self.file.write("  PlotOptions: NOXERR\n")
    self.file.write("  GetChi2: TRUE \n")
    self.file.write("  ImageFormat: pdf, root, eps\n")
    self.file.write("  ReadFrom: HIST\n")
    self.file.write("  SystControlPlots: FALSE\n")
    self.file.write("  UseGammaPulls: TRUE \n")
    #self.file.write("  DoExtendedCovariances: TRUE \n")
    self.file.write("  CorrelationThreshold: 0.15\n")
    #self.file.write("  CorrectNormForNegativeIntegral: TRUE\n")
    self.file.write("  SystPruningNorm: 0.001 \n")
    self.file.write("  SystPruningShape: 0.001 \n")
    self.file.write("  MCstatThreshold: 0.001 \n")
    self.file.write("  PruningType: COMBINEDSIGNAL \n")
    self.file.write("  UnfoldingShowStatOnlyError: TRUE \n")

    self.file.write(" \n")
    self.file.write("Fit: \"myFit\" \n")
    if(self.fit_blind):
      self.file.write("  FitBlind: TRUE\n")
    else:
      self.file.write("  FitBlind: FALSE\n")
    self.file.write("  FitRegion: CRSR \n")
    self.file.write("  FitType: UNFOLDING \n")
    self.file.write("  GetGoodnessOfFit: TRUE\n")
    self.file.write("  StatOnlyFit: TRUE \n")
    # depending on number of mu, calculate the minos only for mus
    #self.file.write("  UseMinos: \"all\",")
    self.file.write("  UseMinos: ")
    for bin in range(0, self.number_of_reco_bins):
      if unfolding_name:
        self.file.write(" {0}_Bin_00{1}_mu,".format(job_name_unfolding_name_dict[self.job_name],(bin+1))) # +1 because it mus numbering starts from 1
      else:
        self.file.write(" {0}_Bin_00{1}_mu,".format("Unfolding",(bin+1))) # +1 because it mus numbering starts from 1

    self.file.write("{}".format("tty_dec-SigXsecOverSM"))
    self.file.write(" \n")
    #if (self.use_data and self.use_real_data): # Hide mu if using real data
      #self.file.write("  BlindedParameters: ")
      #for bin in range(0, self.number_of_reco_bins):
        #self.file.write(" {0}_Bin_00{1}_mu,".format(job_name_unfolding_name_dict[self.job_name],(bin + 1))) # +1 because it mus numbering starts from 1

      #self.file.write("{}".format("tty_dec-SigXsecOverSM"))
    #self.file.write(" \n") # this is for creating a newline; because last lines were being written in a single line
    self.file.write("  NumCPU: 5 \n")
    self.file.write(" \n")
    
    self.file.write(" \n")
    self.file.write("NormFactor: tty_dec-SigXsecOverSM \n")
    self.file.write("  Max: 1.5 \n")
    self.file.write("  Min: 0.5 \n")
    self.file.write("  Nominal: 1 \n")
    self.file.write("  Title: \"Norm factor for tty dec\" \n")
    self.file.write("  Samples: \"tty_dec\" \n")
    self.file.write(" \n")

    self.file.write(" \n")
    if unfolding_name: 
      self.file.write("Unfolding: \"{}\" \n".format(job_name_unfolding_name_dict[self.job_name]))
    else: 
      self.file.write("Unfolding: \"{}\" \n".format("Unfolding"))

    self.file.write("  MatrixOrientation: TRUTHONVERTICAL \n")
    self.file.write("  NumberOfTruthBins: {} \n".format(self.number_of_reco_bins))
    self.file.write("  NominalTruthSample: \"pythia\" \n")
    self.file.write("  DivideByBinWidth: TRUE \n")
    self.file.write("  DivideByLumi: 140.1 \n")
    if(normalized_xsec_unfolding):
      self.file.write("  UnfoldNormXSec: TRUE \n")
    #self.file.write("  UnfoldingChi2Type: PROB \n")
    self.file.write("  LogX: FALSE \n")
    self.file.write("  LogY: FALSE \n")
    self.file.write("  TitleX: \"{}\" \n".format(self.xaxisTitle))
    self.file.write("  TitleY: \"{}\" \n".format(self.yaxisTitle))
    self.file.write(" \n")
    # Truth pythia 8
    self.file.write("TruthSample: \"pythia\" \n")
    self.file.write("  TruthDistributionFile: {} \n".format(self.truth_distribution_file))
    self.file.write("  TruthDistributionName: {} \n".format(self.truth_distribution_name))
    self.file.write("  TruthDistributionPath: {} \n".format(self.truth_distribution_path))
    self.file.write("  LineColor: 632 \n")
    self.file.write("  Title: \"aMC@NLO+P8\" \n")
    self.file.write(" \n")
    # Truth herwig 7
    self.file.write("TruthSample: \"herwig\" \n")
    self.file.write("  TruthDistributionFile: {} \n".format(self.truth_distribution_file_H7))
    self.file.write("  TruthDistributionName: {} \n".format(self.truth_distribution_name))
    self.file.write("  TruthDistributionPath: {} \n".format(self.truth_distribution_path))
    self.file.write("  LineColor: 600 \n")
    self.file.write("  Title: \"aMC@NLO+H7\" \n")
    self.file.write(" \n")

    #SR1
    self.file.write("Region: \"SR1\" \n")
    self.file.write("  ResponseMatrixName: {} \n".format(self.response_matrix_name))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR1))
    self.file.write("  HistoName: {} \n".format(self.histo_name))
    #self.file.write("  Label: \"SR\" \n")
    self.file.write("  Label: \"SR t#bar{t}#gamma production \" \n")
    self.file.write("  NumberOfRecoBins: {} \n".format(self.number_of_reco_bins))
    self.file.write("  Type: SIGNAL \n")
    self.file.write("  DataType: DATA \n")
    self.file.write("  VariableTitle: \"{}\" \n".format(self.variable_title))
    if(write_bin_width): self.file.write("  BinWidth: {} \n".format(job_name_bin_width_dict[self.job_name]))
    self.file.write(" \n")
    # SR2
    self.file.write(" \n")
    self.file.write("Region: \"SR2\" \n")
    self.file.write("  ResponseMatrixName: {} \n".format(self.response_matrix_name))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR2))
    self.file.write("  HistoPath: {}\n".format(self.histo_path_SR2))
    self.file.write("  HistoName: {} \n".format(self.histo_name))
    #self.file.write("  Label: \"CR(tt#gamma dec)\" \n")
    self.file.write("  Label: \"CR t#bar{t}#gamma decay\" \n")
    self.file.write("  NumberOfRecoBins: {} \n".format(self.number_of_reco_bins))
    self.file.write("  Type: SIGNAL \n")
    self.file.write("  DataType: DATA \n")
    self.file.write("  VariableTitle: \"{}\" \n".format(self.variable_title))
    if(write_bin_width): self.file.write("  BinWidth: {} \n".format(job_name_bin_width_dict[self.job_name]))
    self.file.write(" \n")
    # SR3
    self.file.write(" \n")
    self.file.write("Region: \"SR3\" \n")
    self.file.write("  ResponseMatrixName: {} \n".format(self.response_matrix_name))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR3))
    self.file.write("  HistoPath: {}\n".format(self.histo_path_SR3))
    self.file.write("  HistoName: {} \n".format(self.histo_name))
    #self.file.write("  Label: \"CR(fakes)\" \n")
    self.file.write("  Label: \"CR fake #gamma\" \n")
    self.file.write("  NumberOfRecoBins: {} \n".format(self.number_of_reco_bins))
    self.file.write("  Type: SIGNAL \n")
    self.file.write("  DataType: DATA \n")
    self.file.write("  VariableTitle: \"{}\" \n".format(self.variable_title))
    if(write_bin_width): self.file.write("  BinWidth: {} \n".format(job_name_bin_width_dict[self.job_name]))
    self.file.write(" \n")
    # SR4
    self.file.write(" \n")
    self.file.write("Region: \"SR4\" \n")
    self.file.write("  ResponseMatrixName: {} \n".format(self.response_matrix_name))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR4))
    self.file.write("  HistoPath: {}\n".format(self.histo_path_SR4))
    self.file.write("  HistoName: {} \n".format(self.histo_name))
    #self.file.write("  Label: \"CR(other #gamma)\" \n")
    self.file.write("  Label: \"CR other #gamma\" \n")
    self.file.write("  NumberOfRecoBins: {} \n".format(self.number_of_reco_bins))
    self.file.write("  Type: SIGNAL \n")
    self.file.write("  DataType: DATA \n")
    self.file.write("  VariableTitle: \"{}\" \n".format(self.variable_title))
    if(write_bin_width): self.file.write("  BinWidth: {} \n".format(job_name_bin_width_dict[self.job_name]))
    self.file.write(" \n")

    # Reference sample
    if(self.write_ttbar_af2):
      self.file.write(" \n")
      self.file.write("Sample: \"ttbar_AFII\" \n")
      #self.file.write("  FillColorRGB: 255,255,51 \n")
      #self.file.write("  HistoFile: \"histograms.promptY_ttbar_AFII\" \n")
      self.file.write("  HistoFiles: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.promptY_ttbar_AFII"))
      #self.file.write("  LineColor: 31 \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      #self.file.write("  Title: \"Prompt #gamma bkg\" \n")
      self.file.write("  Type: GHOST \n")
      self.file.write("  UseMCstat: TRUE \n")
      self.file.write(" \n")
    if(self.write_pdf_ref_sample):
      # tty_prod SR1
      self.file.write(" \n")
      self.file.write("UnfoldingSample: \"tty_prod_PDF4LHC_0\" \n")
      self.file.write("  ResponseMatrixFile: {} \n".format("histograms.tty_prod_PDF4LHC_0"))
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Type: GHOST \n")
      self.file.write(" \n")
      # tty_dec
      self.file.write("Sample: \"tty_dec_PDF4LHC_0\" \n")
      self.file.write("  HistoFiles: {} \n".format("histograms.tty_dec_PDF4LHC_0"))
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Type: GHOST \n")
      self.file.write("  UseMCstat: TRUE \n")
      self.file.write(" \n")

    # tty prod
    self.file.write("UnfoldingSample: \"tty_prod\" \n")
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR1))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  FillColor: 2\n")
    self.file.write("  Regions: SR1 \n")
    #self.file.write("  Title: \"t#bar{t}#gamma(prod.)\" \n")
    self.file.write("  Title: \"t#bar{t}#gamma production\"\n")
    self.file.write(" \n")
    ## multiple unfolding samples
    self.file.write("UnfoldingSample: \"tty_prod\" \n")
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR2))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  FillColor: 2\n")
    self.file.write("  Regions: SR2 \n")
    #self.file.write("  Title: \"t#bar{t}#gamma(prod.)\" \n")
    self.file.write("  Title: \"t#bar{t}#gamma production\"\n")
    self.file.write(" \n")
    self.file.write("UnfoldingSample: \"tty_prod\" \n")
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR3))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  FillColor: 2\n")
    self.file.write("  Regions: SR3 \n")
    #self.file.write("  Title: \"t#bar{t}#gamma(prod.)\" \n")
    self.file.write("  Title: \"t#bar{t}#gamma production\"\n")
    self.file.write(" \n")
    self.file.write("UnfoldingSample: \"tty_prod\" \n")
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR4))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  FillColor: 2\n")
    self.file.write("  Regions: SR4 \n")
    #self.file.write("  Title: \"t#bar{t}#gamma(prod.)\" \n")
    self.file.write("  Title: \"t#bar{t}#gamma production\"\n")
    self.file.write(" \n")
    self.file.write(" \n")

    # data
    if self.use_data:
      if self.use_real_data:
        self.file.write(" \n")
        self.file.write("Sample: \"data\" \n")
        self.file.write("  FillColor: 9 \n")
        self.file.write("  HistoFile:  histograms.data  \n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  Title: \"Data\" \n")
        self.file.write("  Type: data \n")
        self.file.write(" \n")

      elif self.use_reweighted_pseudodata:
        self.file.write(" \n")
        self.file.write("Sample: \"data\" \n")
        self.file.write("  FillColor: 9 \n")
        self.file.write("  HistoFiles: \"{0}\",\"histograms.ttgamma_dec\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\",\"histograms.efake_tty_NLO_prod\",\"histograms.efake_tty_LO_dec\",\"histograms.efake_wjets\",\"histograms.efake_wgamma\",\"histograms.efake_zjets\",\"histograms.efake_zgamma\",\"histograms.efake_ttbar\",\"histograms.efake_diboson\",\"histograms.efake_ttv\",\"histograms.efake_singletop\",\"histograms.efake_wty\",\"histograms.efake_wt_inclusive\",\"histograms.hfake_tty_NLO_prod\",\"histograms.hfake_tty_LO_dec\",\"histograms.hfake_wjets\",\"histograms.hfake_wgamma\",\"histograms.hfake_zjets\",\"histograms.hfake_zgamma\",\"histograms.hfake_ttbar\",\"histograms.hfake_diboson\",\"histograms.hfake_ttv\",\"histograms.hfake_singletop\",\"histograms.hfake_wty\",\"histograms.hfake_wt_inclusive\",\"histograms.lepfake_negative_bin_corrected.ejet\",\"histograms.lepfake_negative_bin_corrected.mujet\",\n".format(self.reweighted_data_name))
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  Title: \"data\" \n")
        self.file.write("  Type: DATA \n")
        self.file.write(" \n")
      else:
        self.file.write(" \n")
        self.file.write("Sample: \"data\" \n")
        self.file.write("  FillColor: 9 \n")
        self.file.write("  HistoFiles: \"histograms.ttgamma_prod\",\"histograms.ttgamma_dec\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\",\"histograms.efake_tty_NLO_prod\",\"histograms.efake_tty_LO_dec\",\"histograms.efake_wjets\",\"histograms.efake_wgamma\",\"histograms.efake_zjets\",\"histograms.efake_zgamma\",\"histograms.efake_ttbar\",\"histograms.efake_diboson\",\"histograms.efake_ttv\",\"histograms.efake_singletop\",\"histograms.efake_wty\",\"histograms.efake_wt_inclusive\",\"histograms.hfake_tty_NLO_prod\",\"histograms.hfake_tty_LO_dec\",\"histograms.hfake_wjets\",\"histograms.hfake_wgamma\",\"histograms.hfake_zjets\",\"histograms.hfake_zgamma\",\"histograms.hfake_ttbar\",\"histograms.hfake_diboson\",\"histograms.hfake_ttv\",\"histograms.hfake_singletop\",\"histograms.hfake_wty\",\"histograms.hfake_wt_inclusive\",\"histograms.lepfake_negative_bin_corrected.ejet\",\"histograms.lepfake_negative_bin_corrected.mujet\",\n".format(self.reweighted_data_name))
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  Title: \"data\" \n")
        self.file.write("  Type: DATA \n")
        self.file.write(" \n")

    # tty dec
    self.file.write("Sample: \"tty_dec\" \n")
    self.file.write("  FillColor: 3 \n")
    self.file.write("  HistoFile: \"histograms.ttgamma_dec\" \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    #self.file.write("  Title: \"t#bar{t}#gamma(dec)\" \n")
    self.file.write(" Title: \"t#bar{t}#gamma decay\"\n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")

    # hfake
    self.file.write(" \n")
    self.file.write("Sample: \"hfake\" \n")
    self.file.write("  FillColor: 6 \n")
    self.file.write("  HistoFiles: \"histograms.hfake_tty_NLO_prod\",\"histograms.hfake_tty_LO_dec\",\"histograms.hfake_wjets\",\"histograms.hfake_wgamma\",\"histograms.hfake_zjets\",\"histograms.hfake_zgamma\",\"histograms.hfake_ttbar\",\"histograms.hfake_diboson\",\"histograms.hfake_ttv\",\"histograms.hfake_singletop\",\"histograms.hfake_wty\",\"histograms.hfake_wt_inclusive\",\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    #self.file.write("  Title: \"hfake\" \n")
    self.file.write("  Title: \"h-fake #gamma\"\n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")


    # efake
    self.file.write(" \n")
    self.file.write("Sample: \"efake\" \n")
    self.file.write("  FillColor: 7 \n")
    self.file.write("  HistoFiles: \"histograms.efake_tty_NLO_prod\",\"histograms.efake_tty_LO_dec\",\"histograms.efake_wjets\",\"histograms.efake_wgamma\",\"histograms.efake_zjets\",\"histograms.efake_zgamma\",\"histograms.efake_ttbar\",\"histograms.efake_diboson\",\"histograms.efake_ttv\",\"histograms.efake_singletop\",\"histograms.efake_wty\",\"histograms.efake_wt_inclusive\",\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    #self.file.write("  Title: \"efake\" \n")
    self.file.write("  Title: \"e-fake #gamma\"\n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")


    # normal sample; adding all the subsamples
    self.file.write(" \n")
    self.file.write("Sample: \"prompty\" \n")
    self.file.write("  FillColor: 5 \n")
    self.file.write("  HistoFiles: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    #self.file.write("  Title: \"prompt #gamma\" \n")
    self.file.write("  Title: \"Other #gamma\"\n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")


    self.file.write("Sample: \"lepfake\" \n")
    self.file.write("  FillColor: 4 \n")
    self.file.write("  HistoFiles: \"histograms.lepfake_negative_bin_corrected\"\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    #self.file.write("  Title: \"lepfake\" \n")
    self.file.write("  Title: \"Lep. fakes\"\n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
