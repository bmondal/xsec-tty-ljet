from getInfoFromRootFile import *
from np_title_dict import *
from np_dict_tty_ttZ_v1 import *

debug = False 

def get_title_for_simplied_btag(syst_name): # get title for lepton SIMPLIFIED model and btag ones
  splitted_string = syst_name.split("_")
  index_ = splitted_string[len(splitted_string) - 1]
  temp_string = "" 
  for i in range(1,(len(splitted_string)-1)):
    temp_string += splitted_string[i] + "_"
  return (temp_string+index_)

def get_title(np_name):
  np_title= ""
  counter = 0
  is_found = False
  for syst in np_dict:
    if np_name in syst:
      np_title= np_dict[syst]
      counter = counter +1
      is_found = True
      if(debug): print("(1) syst name: {} \t NP title : {}".format(np_name, np_title))
    else: continue
  #print("(2) syst name: {} \t np_title: {}, \t counter : {} \n".format(np_name, np_title, counter)) 
  # its fine to have 1/2/4, because up and down are repeated, pseudo-data makes it 4
  if((not is_found)):  
    print("(2) syst name: {} \t NP title : {}, \t counter : {} \n".format(np_name, np_title, counter)) 
  #counter = 5# for debug
  if(not(counter == 1 or counter == 2 or counter ==4 )):  print("(2) syst name: {} \t NP title : {}, \t counter : {} \n".format(np_name, np_title, counter)) 
  return np_title 


dict_sub_category= {}
dict_sub_category["MUON"] = "Other experimental uncertainties"
dict_sub_category["photonSF"] = "Photon"
dict_sub_category["leptonSF"] = "Other experimental uncertainties"
dict_sub_category["MET"] = "Other experimental uncertainties"
dict_sub_category["JET"] = "Jets"
dict_sub_category["EG_RESOLUTION"] = "Other experimental uncertainties"
dict_sub_category["EG_SCALE"] = "Other experimental uncertainties"
dict_sub_category["pileup"] = "Other experimental uncertainties"
dict_sub_category["weight_jvt"] = "Jets"

def find_syst_category(syst_name):
  sub_category = ""
  counter = 0
  is_found = False
  for syst in dict_sub_category:
    if syst in syst_name:
      sub_category = dict_sub_category[syst]
      counter = counter +1
      is_found = True
      if(debug): print("(1) syst name: {} \t sub_category : {}".format(syst_name, sub_category))
    else: continue
  #print("(2) syst name: {} \t sub_category : {}, \t counter : {} \n".format(syst_name, sub_category, counter)) 
  if((not is_found) or (counter > 1)):  print("(2) syst name: {} \t sub_category : {}, \t counter : {} \n".format(syst_name, sub_category, counter)) 
  return sub_category


## ********************************************************************************************************************
class WriteOtherSyst:
  def write_addition_met_syst(self):
    self.file.write(" \n")
    syst_name = "MET_SoftTrk_ResoPara"
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixPathSuffUp: \"/../{}_9999\" \n".format(syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Other experimental uncertainties \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec, prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoPathSufUp: \"/../{}_9999\" \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Other experimental uncertainties \n")

    self.file.write(" \n")
    syst_name = "MET_SoftTrk_ResoPerp"
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixPathSuffUp: \"/../{}_9999\" \n".format(syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Other experimental uncertainties \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec, prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoPathSufUp: \"/../{}_9999\" \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Other experimental uncertainties \n")

    self.file.write(" \n")

  ## ********************************************************************************************************************
  # write luminosity uncertainty in the config
  def write_luminosity(self):

    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: lumi \n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["lumi"]["ttZ_np_name"]))
    #self.file.write("  Title: Luminosity \n")
    self.file.write("  Title: \"{}\" \n".format(np_dict["lumi"]["Title"]))
    self.file.write("  Type: OVERALL \n")
    self.file.write("  OverallUp: +0.0083 \n")
    self.file.write("  OverallDown: -0.0083 \n")
    self.file.write("  Category: lumi \n")
    self.file.write("  Regions: all \n")
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Other experimental uncertainties \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: lumi \n")
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["lumi"]["ttZ_np_name"]))
    #self.file.write("  Title: Luminosity \n")
    self.file.write("  Title: \"{}\" \n".format(np_dict["lumi"]["Title"]))
    self.file.write("  Type: OVERALL \n")
    self.file.write("  OverallUp: +0.0083 \n")
    self.file.write("  OverallDown: -0.0083 \n")
    self.file.write("  Category: lumi \n")
    self.file.write("  Regions: all \n")
    self.file.write("  Samples: tty_dec, efake, hfake, prompty \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Other experimental uncertainties \n")
    self.file.write(" \n")



    # Some of the systematics only applies to Wty sample
    sub_category = "Jets"
    self.file.write(" \n")
    syst_name="JET_PunchThrough_AFII__1"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty \n")
    self.file.write("  HistoFilesUp: /../{0}up_9999/histograms.Wty,/../nominal_9999/histograms.wt_inclusive,/../nominal_9999/histograms.wjets,/../nominal_9999/histograms.wgamma,/../nominal_9999/histograms.zjets,/../nominal_9999/histograms.zgamma,/../nominal_9999/histograms.ttbar,/../nominal_9999/histograms.diboson,/../nominal_9999/histograms.ttv,/../nominal_9999/histograms.singletop_schan,/../nominal_9999/histograms.singletop_tchan \n".format(syst_name))
    self.file.write("  HistoFilesDown: /../{0}down_9999/histograms.Wty,/../nominal_9999/histograms.wt_inclusive,/../nominal_9999/histograms.wjets,/../nominal_9999/histograms.wgamma,/../nominal_9999/histograms.zjets,/../nominal_9999/histograms.zgamma,/../nominal_9999/histograms.ttbar,/../nominal_9999/histograms.diboson,/../nominal_9999/histograms.ttv,/../nominal_9999/histograms.singletop_schan,/../nominal_9999/histograms.singletop_tchan \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))

    self.file.write(" \n")
    syst_name="JET_JER_DataVsMC_AFII__1"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty \n")
    self.file.write("  HistoFilesUp: /../{0}up_9999/histograms.Wty,/../nominal_9999/histograms.wt_inclusive,/../nominal_9999/histograms.wjets,/../nominal_9999/histograms.wgamma,/../nominal_9999/histograms.zjets,/../nominal_9999/histograms.zgamma,/../nominal_9999/histograms.ttbar,/../nominal_9999/histograms.diboson,/../nominal_9999/histograms.ttv,/../nominal_9999/histograms.singletop_schan,/../nominal_9999/histograms.singletop_tchan \n".format(syst_name))
    self.file.write("  HistoFilesUpSubtractSample: /../{0}up_{1}/histograms.Wty,/../nominal_9999/histograms.wt_inclusive,/../nominal_9999/histograms.wjets,/../nominal_9999/histograms.wgamma,/../nominal_9999/histograms.zjets,/../nominal_9999/histograms.zgamma,/../nominal_9999/histograms.ttbar,/../nominal_9999/histograms.diboson,/../nominal_9999/histograms.ttv,/../nominal_9999/histograms.singletop_schan,/../nominal_9999/histograms.singletop_tchan \n".format(syst_name, "PseudoData_9999"))
    self.file.write("  HistoFilesDown: /../{0}down_9999/histograms.Wty,/../nominal_9999/histograms.wt_inclusive,/../nominal_9999/histograms.wjets,/../nominal_9999/histograms.wgamma,/../nominal_9999/histograms.zjets,/../nominal_9999/histograms.zgamma,/../nominal_9999/histograms.ttbar,/../nominal_9999/histograms.diboson,/../nominal_9999/histograms.ttv,/../nominal_9999/histograms.singletop_schan,/../nominal_9999/histograms.singletop_tchan  \n".format(syst_name))
    self.file.write("  HistoFilesDownSubtractSample: /../{0}down_{1}/histograms.Wty,/../nominal_9999/histograms.wt_inclusive,/../nominal_9999/histograms.wjets,/../nominal_9999/histograms.wgamma,/../nominal_9999/histograms.zjets,/../nominal_9999/histograms.zgamma,/../nominal_9999/histograms.ttbar,/../nominal_9999/histograms.diboson,/../nominal_9999/histograms.ttv,/../nominal_9999/histograms.singletop_schan,/../nominal_9999/histograms.singletop_tchan \n".format(syst_name, "PseudoData_9999"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))

    self.file.write(" \n")
    syst_name="JET_RelativeNonClosure_AFII__1"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty \n")
    self.file.write("  HistoFilesUp: /../{0}up_9999/histograms.Wty,/../nominal_9999/histograms.wt_inclusive,/../nominal_9999/histograms.wjets,/../nominal_9999/histograms.wgamma,/../nominal_9999/histograms.zjets,/../nominal_9999/histograms.zgamma,/../nominal_9999/histograms.ttbar,/../nominal_9999/histograms.diboson,/../nominal_9999/histograms.ttv,/../nominal_9999/histograms.singletop_schan,/../nominal_9999/histograms.singletop_tchan \n".format(syst_name))
    self.file.write("  HistoFilesDown: /../{0}down_9999/histograms.Wty,/../nominal_9999/histograms.wt_inclusive,/../nominal_9999/histograms.wjets,/../nominal_9999/histograms.wgamma,/../nominal_9999/histograms.zjets,/../nominal_9999/histograms.zgamma,/../nominal_9999/histograms.ttbar,/../nominal_9999/histograms.diboson,/../nominal_9999/histograms.ttv,/../nominal_9999/histograms.singletop_schan,/../nominal_9999/histograms.singletop_tchan \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))
    self.file.write(" \n")


    # write other normalization uncertainty
    uncalib_jet_name_list = ["uncalib_ljet","uncalib_cjet","uncalib_bjet"]
    for uncalib_jet_name in uncalib_jet_name_list:
      self.file.write("UnfoldingSystematic: {} \n".format(uncalib_jet_name))
      #self.file.write("  Title: {} \n ".format(uncalib_jet_name))
      self.file.write("  Title: {} \n ".format(np_dict[uncalib_jet_name]["Title"]))
      self.file.write("  NuisanceParameter: {} \n ".format(np_dict[uncalib_jet_name]["ttZ_np_name"]))
      self.file.write("  ResponseMatrixFilesUp: histograms.ttgamma_prod, histograms.ttgamma_prod_{0}_x0.5\n ".format(uncalib_jet_name))
      self.file.write("  Symmetrisation: ONESIDED\n ")
      self.file.write("  Samples: tty_prod\n ")
      self.file.write("  Regions: all  \n ")
      self.file.write("  Category: Instrumental \n")
      self.file.write(" \n")

      self.file.write("Systematic: {} \n".format(uncalib_jet_name))
      #self.file.write("  Title: {} \n ".format(uncalib_jet_name))
      self.file.write("  Title: {} \n ".format(np_dict[uncalib_jet_name]["Title"]))
      self.file.write("  NuisanceParameter: {} \n ".format(np_dict[uncalib_jet_name]["ttZ_np_name"]))
      self.file.write("  HistoFilesUp: histograms.ttgamma_dec, histograms.ttgamma_dec_{0}_x0.5\n ".format(uncalib_jet_name))
      self.file.write("  Symmetrisation: ONESIDED\n ")
      self.file.write("  Samples: tty_dec\n ")
      self.file.write("  Regions: all  \n ")
      self.file.write("  Category: Instrumental \n")
      self.file.write(" \n")
 
      self.file.write("Systematic: {} \n".format(uncalib_jet_name))
      #self.file.write("  Title: {} \n ".format(uncalib_jet_name))
      self.file.write("  Title: {} \n ".format(np_dict[uncalib_jet_name]["Title"]))
      self.file.write("  NuisanceParameter: {} \n ".format(np_dict[uncalib_jet_name]["ttZ_np_name"]))
      self.file.write("  HistoFilesUp: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\",\"histograms.Wty_{0}_x0.5\",\"histograms.wt_inclusive_{0}_x0.5\",\"histograms.wjets_{0}_x0.5\",\"histograms.wgamma_{0}_x0.5\",\"histograms.zjets_{0}_x0.5\",\"histograms.zgamma_{0}_x0.5\",\"histograms.ttbar_{0}_x0.5\",\"histograms.diboson_{0}_x0.5\",\"histograms.ttv_{0}_x0.5\",\"histograms.singletop_schan_{0}_x0.5\",\"histograms.singletop_tchan_{0}_x0.5\" \n".format(uncalib_jet_name))
      self.file.write("  Symmetrisation: ONESIDED\n ")
      self.file.write("  Samples: prompty\n ")
      self.file.write("  Regions: all  \n ")
      self.file.write("  Category: Instrumental \n")
      self.file.write(" \n")


  ## ********************************************************************************************************************
  #def write_config_datadriven_syst(self, syst_name):
  def write_config_datadriven_syst(self):
    self.file.write(" \n")
    syst_name="efakeSF_"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  HistoPathSufUp: \"/../{}Up_9999\" \n".format(syst_name))
    self.file.write("  HistoPathSufDown: \"/../{}Down_9999\" \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: \"{}\" \n".format(syst_name))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Samples: efake\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Fake photon\n")


    self.file.write(" \n")
    syst_name="hfakeSF_"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  HistoPathSufUp: \"/../{}Up_9999\" \n".format(syst_name))
    self.file.write("  HistoPathSufDown: \"/../{}Down_9999\" \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: \"{}\" \n".format(syst_name))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Samples: hfake\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Fake photon\n")

  ## ********************************************************************************************************************
    # lepton fake
    self.file.write(" \n")
    syst_name="weight_leptonFake_"
    self.file.write("Systematic: {} \n".format("lepfakeSF_"))
    self.file.write("  HistoPathSufUp: \"/../{}nominal_Up\" \n".format(syst_name))
    self.file.write("  HistoPathSufDown: \"/../{}nominal_Down\" \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["lepfakeSF_"]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: \"{}\" \n".format(syst_name))
    self.file.write("  Title: \"{}\" \n".format(np_dict["lepfakeSF_"]["Title"]))
    self.file.write("  Samples: lepfake\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: Fake lepton\n")

    # lepton fake
    self.file.write(" \n")
    syst_name="weight_leptonFake_"
    self.file.write("Systematic: {} \n".format("lepfakeSF_var1"))
    self.file.write("  HistoPathSufUp: \"/../{}var1\" \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["lepfakeSF_var1"]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: ONESIDED \n")
    #self.file.write("  Title: \"{}\" \n".format("lepfakeSF_var1"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["lepfakeSF_var1"]["Title"]))
    self.file.write("  Samples: lepfake\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  SubCategory: Fake lepton\n")


    self.file.write(" \n")
    syst_name="weight_leptonFake_"
    self.file.write("Systematic: {} \n".format("lepfakeSF_var2"))
    self.file.write("  HistoPathSufUp: \"/../{}var2\" \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["lepfakeSF_var2"]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: ONESIDED \n")
    #self.file.write("  Title: \"{}\" \n".format("lepfakeSF_var2"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["lepfakeSF_var2"]["Title"]))
    self.file.write("  Samples: lepfake\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  SubCategory: Fake lepton\n")


  ## ********************************************************************************************************************
  def write_config_obj_syst(self, syst_name, up_name, down_name):
    if syst_name.rstrip()=="tru":  
      return
    sub_category = find_syst_category(syst_name)
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  ResponseMatrixPathSuffUp: \"/../{}{}_9999\" \n".format(syst_name, up_name))
    self.file.write("  ResponseMatrixPathSuffDown: \"/../{}{}_9999\" \n".format(syst_name, down_name))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec, prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoPathSufUp: \"/../{}{}_9999\" \n".format(syst_name,up_name))
    self.file.write("  HistoPathSufDown: \"/../{}{}_9999\" \n".format(syst_name,down_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))


  ## ********************************************************************************************************************
  ## btag and lepton reco, id, iso systematics
  def write_btag_and_lepton_syst(self):
    ### weight syst with vectors
    syst_weight_vecs={}
    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_B"] = 45
    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_C"] = 20
    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_Light"] = 20
#    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_B_down"] = 45
#    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_C_down"] = 20
#    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_Light_down"] = 20
    
    self.file.write(" \n")
    for syst in syst_weight_vecs:
      for index_ in range(0,syst_weight_vecs[syst]):
        syst_name = syst + "_{}".format(index_)
        syst_name_up = syst + "_up_{}".format(index_)
        syst_name_down = syst + "_down_{}".format(index_)
        self.file.write(" \n")
        self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
        self.file.write("  Samples: tty_prod \n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  ResponseMatrixPathSuffUp: \"/../{}\" \n".format(syst_name_up))
        self.file.write("  ResponseMatrixPathSuffDown: \"/../{}\" \n".format(syst_name_down))
        self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        #self.file.write("  Title: {} \n".format(get_title_for_simplied_btag(syst_name)))
        self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Category: Instrumental \n")
        self.file.write("  SubCategory: b-Tagging \n")

        self.file.write(" \n")
        self.file.write("Systematic: {} \n".format(syst_name))
        self.file.write("  Samples: tty_dec, prompty\n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  HistoPathSufUp: \"/../{}\" \n".format(syst_name_up))
        self.file.write("  HistoPathSufDown: \"/../{}\" \n".format(syst_name_down))
        self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        self.file.write("  Type: HISTO \n")
        #self.file.write("  Title: {} \n".format(get_title_for_simplied_btag(syst_name)))
        self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Category: Instrumental \n")
        self.file.write("  SubCategory: b-Tagging \n")
        self.file.write(" \n")

    syst_weight_vecs_leps={}
    syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_Reco"] = 24
    #syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_Reco"] = 24
    syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_ID"] = 32
    #syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN"] = 32 
    syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_Iso"] = 17
    #syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_Iso_DOWN"] = 17

    self.file.write(" \n")
    for syst in syst_weight_vecs_leps:
      for index_ in range(0,syst_weight_vecs_leps[syst]):
        syst_name = syst + "_{}".format(index_)
        syst_name_up = syst + "_UP_{}".format(index_)
        syst_name_down = syst + "_DOWN_{}".format(index_)
        self.file.write(" \n")
        self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
        self.file.write("  Samples: tty_prod \n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  ResponseMatrixPathSuffUp: \"/../{}\" \n".format(syst_name_up))
        self.file.write("  ResponseMatrixPathSuffDown: \"/../{}\" \n".format(syst_name_down))
        self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        #self.file.write("  Title: {} \n".format(get_title_for_simplied_btag(syst_name)))
        self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Category: Instrumental \n")
        self.file.write("  SubCategory: Other experimental uncertainties \n")

        self.file.write(" \n")
        self.file.write("Systematic: {} \n".format(syst_name))
        self.file.write("  Samples: tty_dec, prompty\n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  HistoPathSufUp: \"/../{}\" \n".format(syst_name_up))
        self.file.write("  HistoPathSufDown: \"/../{}\" \n".format(syst_name_down))
        self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        self.file.write("  Type: HISTO \n")
        #self.file.write("  Title: {} \n".format(get_title_for_simplied_btag(syst_name)))
        self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Category: Instrumental \n")
        self.file.write("  SubCategory: Other experimental uncertainties \n")
        self.file.write(" \n")


  ## ********************************************************************************************************************
  ## Tree systematics
  def write_config_syst(self, syst_name):
    ##*** The reason there is no HistoPathUp/Down in the Unfolding systematics is that
    ##    The particle level will be folded with the response matrix to give the reco spectra?
    ##***
    sub_category=find_syst_category(syst_name)
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixPathSuffUp: \"/../{}up_9999\" \n".format(syst_name))
    self.file.write("  ResponseMatrixPathSuffDown: \"/../{}down_9999\" \n".format(syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    #self.file.write("  Title: \"{}\" \n".format(get_title(syst_name)))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec, prompty\n")
    self.file.write("  HistoPathSufUp: \"/../{}up_9999\" \n".format(syst_name))
    self.file.write("  HistoPathSufDown: \"/../{}down_9999\" \n".format(syst_name))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    #self.file.write("  Title: \"{}\" \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))


  ## ********************************************************************************************************************
  ## write config JET JER; remember to subtract PseudoData samples
  def write_config_syst_JET_JER(self, syst_name):
    ##*** The reason there is no HistoPathUp/Down in the Unfolding systematics is that
    ##    The particle level will be folded with the response matrix to give the reco spectra?
    ##***
    sub_category = "Jets"
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixFileUp: /../{0}up_9999/{1} \n".format(syst_name,"histograms.ttgamma_prod"))
    self.file.write("  ResponseMatrixFileUpSubtractSample: /../{0}up_{1}_9999/{2} \n".format(syst_name,"PseudoData","histograms.ttgamma_prod"))
    self.file.write("  ResponseMatrixFileDown: /../{0}down_9999/{1} \n".format(syst_name,"histograms.ttgamma_prod"))
    self.file.write("  ResponseMatrixFileDownSubtractSample: /../{0}down_{1}_9999/{2} \n".format(syst_name,"PseudoData","histograms.ttgamma_prod"))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec \n")
    self.file.write("  HistoFileUp: /../{0}up_9999/{1} \n".format(syst_name,"histograms.ttgamma_dec"))
    self.file.write("  HistoFileUpSubtractSample: /../{0}up_{1}_9999/{2} \n".format(syst_name,"PseudoData","histograms.ttgamma_dec"))
    self.file.write("  HistoFileDown: /../{0}down_9999/{1} \n".format(syst_name, "histograms.ttgamma_dec"))
    self.file.write("  HistoFileDownSubtractSample: /../{0}down_{1}_9999/{2} \n".format(syst_name,"PseudoData", "histograms.ttgamma_dec"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty \n")
    self.file.write("  HistoFilesUp: /../{0}up_9999/histograms.Wty,/../{0}up_9999/histograms.wt_inclusive,/../{0}up_9999/histograms.wjets,/../{0}up_9999/histograms.wgamma,/../{0}up_9999/histograms.zjets,/../{0}up_9999/histograms.zgamma,/../{0}up_9999/histograms.ttbar,/../{0}up_9999/histograms.diboson,/../{0}up_9999/histograms.ttv,/../{0}up_9999/histograms.singletop_schan,/../{0}up_9999/histograms.singletop_tchan \n".format(syst_name))
    self.file.write("  HistoFilesUpSubtractSample: /../{0}up_{1}/histograms.Wty,/../{0}up_{1}/histograms.wt_inclusive,/../{0}up_{1}/histograms.wjets,/../{0}up_{1}/histograms.wgamma,/../{0}up_{1}/histograms.zjets,/../{0}up_{1}/histograms.zgamma,/../{0}up_{1}/histograms.ttbar,/../{0}up_{1}/histograms.diboson,/../{0}up_{1}/histograms.ttv,/../{0}up_{1}/histograms.singletop_schan,/../{0}up_{1}/histograms.singletop_tchan \n".format(syst_name, "PseudoData_9999"))
    self.file.write("  HistoFilesDown: /../{0}down_9999/histograms.Wty,/../{0}down_9999/histograms.wt_inclusive,/../{0}down_9999/histograms.wjets,/../{0}down_9999/histograms.wgamma,/../{0}down_9999/histograms.zjets,/../{0}down_9999/histograms.zgamma,/../{0}down_9999/histograms.ttbar,/../{0}down_9999/histograms.diboson,/../{0}down_9999/histograms.ttv,/../{0}down_9999/histograms.singletop_schan,/../{0}down_9999/histograms.singletop_tchan \n".format(syst_name))
    self.file.write("  HistoFilesDownSubtractSample: /../{0}down_{1}/histograms.Wty,/../{0}down_{1}/histograms.wt_inclusive,/../{0}down_{1}/histograms.wjets,/../{0}down_{1}/histograms.wgamma,/../{0}down_{1}/histograms.zjets,/../{0}down_{1}/histograms.zgamma,/../{0}down_{1}/histograms.ttbar,/../{0}down_{1}/histograms.diboson,/../{0}down_{1}/histograms.ttv,/../{0}down_{1}/histograms.singletop_schan,/../{0}down_{1}/histograms.singletop_tchan \n".format(syst_name, "PseudoData_9999"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Title: {} \n".format(get_title(syst_name)))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Smoothing: 40 \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Category: Instrumental \n")
    self.file.write("  SubCategory: {} \n".format(sub_category))



  def write_whole_config(self):
    self.write_config()
    obj = ReadFromRootFile(self.one_ntuple_file_for_reading_trees)
    tree_list = obj.get_list_of_trees() # list containing tree names
    list_of_obj_syst = obj.get_list_of_uncer_from_nominal()

    list_of_datadriven_syst = ["efakeSF_Up","efakeSF_Down","hfakeSF_Up","hfakeSF_Down"]
    match_list=['down', 'DOWN', 'Down']
    Dict = {"UP":"DOWN", "Up":"Down", "up":"down"}
    ignore_list_btag = ["DL1r_70","DL1r_77","DL1r_85","DL1r_Continuous", "SIMPLIFIED", "weight_leptonSF_EL_SF_Reco_","weight_leptonSF_EL_SF_ID_","weight_leptonSF_EL_SF_Isol_" ] #ignoring DL1r and SIMPLIFIED systematics; we include it later, these syst contains many NPs with vector indexed
    ignore_list_fakes = ["leptonFake", "efake", "hfake"]

    previous_syst = ""
    if self.exp_tree_syst: 
      for tree in tree_list:
        # ignore truth tree
        if tree=="truth":  continue
        # only use up and generate down with that tree name
        if(tree == 'nominal'): continue
        if 'MET_SoftTrk_ResoP' in tree: continue # added by write_additonal_met_syst()
        if not any(x in tree for x in match_list):
          if "PseudoData" in tree: continue
          if "JET_JER" in tree: # use PseudoData samples for JET_JER
            current_syst = tree
            if(previous_syst == current_syst): continue
            self.write_config_syst_JET_JER(tree[:-2])
            previous_syst = current_syst
          else: # other than JET_JER
            current_syst = tree
            if(previous_syst == current_syst): continue
            self.write_config_syst(tree[:-2])
            previous_syst = current_syst

            
    if self.exp_weight_syst:
      for branch in list_of_obj_syst: # generate object systematics
        if "weight_photonSF_Trigger_UNCERT_" in branch: continue
        if(debug): print("branch name : {} \n".format( branch) )
        if any(x in branch for x in ignore_list_btag): continue
        if any(x in branch for x in ignore_list_fakes): continue
        if not any(x in branch for x in match_list):
          split_branch_str = branch.split("_") # split the string branch and get last two letter, it should be "UP/Up/up"
  
          str_containting_up = split_branch_str[len(split_branch_str)-1]
          if(self.debug): print("{}: {}".format(str_containting_up, Dict[str_containting_up]) )
          self.write_config_obj_syst(branch[:-2], str_containting_up, Dict[str_containting_up])
  
    # generate for data driven syst 
    if self.data_driven_syst: self.write_config_datadriven_syst()
    # write config for sample modelling
    if self.modelling_syst: self.write_config_modelling()
    # adding normalization uncertainty
    if self.bkg_norm_syst: self.write_bkg_norm_uncertainty()
    ## write lumi
    if self.lumi_syst: self.write_luminosity()
    ## write muR_muF
    if self.muR_muF_syst: self.write_muR_muF()
    ## write additonal met syst
    if self.met_syst: self.write_addition_met_syst()
    ## write btag syst
    if self.btag_lepton_syst: self.write_btag_and_lepton_syst()


