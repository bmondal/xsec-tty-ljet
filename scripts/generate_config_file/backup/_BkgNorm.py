from np_dict_tty_ttZ_v1 import *
class BkgNorm:
  def write_bkg_norm_uncertainty(self):

    # prompt Wty normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("wty_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["wty_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["wty_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["wty_norm"]["SubCategory"])) # correlated
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.Wty_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.Wty_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    #self.file.write("  Title: \"{}\" \n".format("Wt#gamma Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["wty_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt Wt normalization
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("wt_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["wt_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["wt_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["wt_norm"]["SubCategory"])) # correlated
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.Wty\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.wt_inclusive_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.Wty\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.wt_inclusive_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    #self.file.write("  Title: \"{}\" \n".format("Wt#gamma Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["wt_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt single top normalization  s-channel
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("singletop_schan_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["singletop_schan_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["singletop_schan_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["singletop_schan_norm"]["SubCategory"])) # correlated
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.Wty\", \"histograms.singletop_tchan\" \n".format("histograms.singletop_schan_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.Wty\", \"histograms.singletop_tchan\"  \n".format("histograms.singletop_schan_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    #self.file.write("  Title: \"{}\" \n".format("t#gamma(s,t channel) Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["singletop_schan_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt single top normalization  t-channel
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("singletop_tchan_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["singletop_tchan_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["singletop_tchan_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["singletop_tchan_norm"]["SubCategory"])) # correlated
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.Wty\", \"histograms.singletop_schan\" \n".format("histograms.singletop_tchan_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.Wty\", \"histograms.singletop_schan\"  \n".format("histograms.singletop_tchan_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    #self.file.write("  Title: \"{}\" \n".format("t#gamma(s,t channel) Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["singletop_tchan_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))


    # prompt Wy normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("wy_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["wy_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["wy_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["wy_norm"]["SubCategory"])) # correlated
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.wgamma_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.wgamma_NormVar"))
    #self.file.write("  Title: \"{}\" \n".format("W#gamma Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["wy_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt Zy normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("zy_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["zy_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["zy_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["zy_norm"]["SubCategory"])) # correlated
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.zgamma_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.zgamma_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    #self.file.write("  Title: \"{}\" \n".format("Z#gamma Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["zy_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt ttV normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("ttv_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["ttv_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["ttv_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["ttv_norm"]["SubCategory"])) # correlated
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.ttv_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.ttv_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    #self.file.write("  Title: \"{}\" \n".format("ttV Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["ttv_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

# prompt diboson normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("diboson_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["diboson_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["diboson_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["diboson_norm"]["SubCategory"])) # correlated
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.diboson_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.diboson_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    #self.file.write("  Title: \"{}\" \n".format("diboson Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["diboson_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

# prompt ttbar normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("ttbar_norm"))
    self.file.write("  NuisanceParameter: \"{}\" \n".format(np_dict["ttbar_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: \"{}\" \n".format(np_dict["ttbar_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: \"{}\" \n".format(np_dict["ttbar_norm"]["SubCategory"])) # correlated
    self.file.write("  HistoFilesUp: \"{}_Up\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.ttbar_NormVar"))
    self.file.write("  HistoFilesDown: \"{}_Down\",\"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.ttbar_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    #self.file.write("  Title: \"{}\" \n".format("ttbar Norm"))
    self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_norm"]["Title"])) # correlated
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

#    # tty_dec normalization uncertainty
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("tty_dec_norm"))
#    self.file.write("  NuisanceParameter: \"{}\" \n".format("tty_dec_norm"))
#    self.file.write("  Category: \"{}\" \n".format("tty_dec_norm"))
#    self.file.write("  SubCategory: \"{}\" \n".format("tty_dec_norm"))
#    self.file.write("  HistoFilesUp: \"{}_Up\" \n".format("histograms.ttgamma_dec_NormVar"))
#    self.file.write("  HistoFilesDown: \"{}_Down\"\n".format("histograms.ttgamma_dec_NormVar"))
#    self.file.write("  Symmetrisation: TWOSIDED\n")
#    self.file.write("  Title: \"{}\" \n".format("t#bar{t}#gamma\_dec Norm"))
#    self.file.write("  Type: HISTO \n")
#    self.file.write("  Samples: prompty\n")
#    self.file.write("  Regions: {} \n".format(self.regions))
#
