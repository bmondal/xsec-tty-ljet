import sys

if len(sys.argv) != 2:
    print("Usage: python script_name.py <path_to_your_python_file>")
    sys.exit(1)

file_path = sys.argv[1]

# Read the file contents
with open(file_path, 'r') as file:
    lines = file.readlines()

# Modify the file
new_lines = []
for line in lines:
    if "Category" in line or "SubCategory" in line:
      continue
    new_lines.append(line)
    if "NuisanceParameter" in line:
        modified_line = line.replace("NuisanceParameter", "Category").replace("ttZ_np_name", "Category")
        new_lines.append(modified_line)
        modified_line = line.replace("NuisanceParameter", "SubCategory").replace("ttZ_np_name", "SubCategory")
        new_lines.append(modified_line)

# Write the modified contents back to the file
with open(file_path, 'w') as file:
    file.writelines(new_lines)

