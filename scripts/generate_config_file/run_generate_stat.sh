#!/usr/bin/env bash
# This script is for generating file and copyting all the config file to a appropriate folder for running unfolding
function generate_config_stat_all_var {
  path_to_unfolding_inputs="~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v10_nominal_binning_v1/" # this doesn't work maybe
  save_folder="stat-all"
  mkdir -p $save_folder
  python GenerateUnfoldingTrexConfig_pt.py --filename $save_folder/tty1l_pt_all_stat.config  --write-data --fit-blind --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_eta.py --filename $save_folder/tty1l_eta_all_stat.config  --write-data --fit-blind --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_dr.py --filename $save_folder/tty1l_dr_all_stat.config   --write-data --fit-blind --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_ptj1.py --filename $save_folder/tty1l_ptj1_all_stat.config  --write-data --fit-blind --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_drphb.py --filename $save_folder/tty1l_drphb_all_stat.config   --write-data --fit-blind --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_drlj.py --filename $save_folder/tty1l_drlj_all_stat.config   --write-data --fit-blind --path-to-unfolding-hist $path_to_unfolding_inputs
}
# generate reweighted configs
function generate_config_reweighted {
  echo "Info::generate_config_reweighted()"
  path_to_unfolding_inputs="/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/"
  var_name=$1
  save_folder="stat-all"
  mkdir -p $save_folder
  #linear
  python GenerateUnfoldingTrexConfig_${var_name}.py --filename $save_folder/tty1l_${var_name}_all_stat_linear_reweighted_y1.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name histograms.ttgamma_prod.linear_reweighted_y1.0 --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_${var_name}.py --filename $save_folder/tty1l_${var_name}_all_stat_linear_reweighted_y-1.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name histograms.ttgamma_prod.linear_reweighted_y-1.0 --path-to-unfolding-hist $path_to_unfolding_inputs
  # nonlinear
  python GenerateUnfoldingTrexConfig_${var_name}.py --filename $save_folder/tty1l_${var_name}_all_stat_nonlinear_reweighted_y1.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name  ttgamma_prod_reweighted_y1.0 --path-to-unfolding-hist $path_to_unfolding_inputs
  python GenerateUnfoldingTrexConfig_${var_name}.py --filename $save_folder/tty1l_${var_name}_all_stat_nonlinear_reweighted_y-1.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name ttgamma_prod_reweighted_y-1.0 --path-to-unfolding-hist $path_to_unfolding_inputs
}


function generate_config_closure_reweighted {
  echo "Info:: generate_config_closure_reweighted()"
  path_to_unfolding_inputs="/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/"
  var_name=$1
  save_folder="stat-all"
  mkdir -p $save_folder
  #linear
  python GenerateUnfoldingTrexConfig_${var_name}.py --filename $save_folder/tty1l_${var_name}_all_stat.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name histograms.ttgamma_prod --path-to-unfolding-hist $path_to_unfolding_inputs
}

#generate_stat
function generate_config_reweighted_all_var() {
  generate_config_reweighted "pt"
  generate_config_reweighted "eta"
  generate_config_reweighted "dr"
  generate_config_reweighted "drphb"
  generate_config_reweighted "ptj1"
  generate_config_reweighted "drlj"
}
#generate_stat
function generate_config_closure_test_reweighted_all_var() {
  echo "Info:: generate_config_closure_test_reweighted_all_var()"
  generate_config_closure_reweighted "pt"
  generate_config_closure_reweighted "eta"
  generate_config_closure_reweighted "dr"
  generate_config_closure_reweighted "drphb"
  generate_config_closure_reweighted "ptj1"
  generate_config_closure_reweighted "drlj"
}

