from np_dict_tty_ttZ_v1 import *
class MuRMuFpdf:
  def write_muR_muF(self):
    #Wty muR
    if(self.wty_muR_muF):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("Wty_muR"))
      self.file.write("  Samples:  prompty\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["Wty_muR"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["Wty_muR"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["Wty_muR"]["SubCategory"]))
      self.file.write("  HistoFilesUp: \"{}\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tWy_muR_up"))
      self.file.write("  HistoFilesDown: \"{}\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.tWy_muR_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("Wty_muR"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["Wty_muR"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

      #Wty muF
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("Wty_muF"))
      self.file.write("  Samples:  prompty\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["Wty_muF"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["Wty_muF"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["Wty_muF"]["SubCategory"]))
      self.file.write("  HistoFilesUp: \"{}\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tWy_muF_up"))
      self.file.write("  HistoFilesDown: \"{}\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.tWy_muF_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("Wty_muF"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["Wty_muF"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

    #Wt muR
    if(self.wty_muR_muF):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("Wt_muR"))
      self.file.write("  Samples:  prompty\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["Wt_muR"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["Wt_muR"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["Wt_muR"]["SubCategory"]))
      self.file.write("  HistoFilesUp: \"{}\",\"histograms.Wty\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tW_muR_up"))
      self.file.write("  HistoFilesDown: \"{}\",\"histograms.Wty\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tW_muR_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("Wty_muR"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["Wt_muR"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

      #Wty muF
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("Wt_muF"))
      self.file.write("  Samples:  prompty\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["Wt_muF"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["Wt_muF"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["Wt_muF"]["SubCategory"]))
      self.file.write("  HistoFilesUp: \"{}\",\"histograms.Wty\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tW_muF_up"))
      self.file.write("  HistoFilesDown: \"{}\",\"histograms.Wty\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"histograms.ttbar\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\" \n".format("histograms.tW_muF_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("Wty_muF"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["Wt_muF"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")


    #ttbar muR
    if(self.ttbar_muR_muF):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("ttbar_muR"))
      self.file.write("  Samples:  prompty\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_muR"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["ttbar_muR"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_muR"]["SubCategory"]))
      self.file.write("  HistoFilesUp: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tt_muR_up"))
      self.file.write("  HistoFilesDown: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tt_muR_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("ttbar_muR"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_muR"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

      #ttbar muF
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("ttbar_muF"))
      self.file.write("  Samples:  prompty\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_muF"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["ttbar_muF"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_muF"]["SubCategory"]))
      self.file.write("  HistoFilesUp: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tt_muF_up"))
      self.file.write("  HistoFilesDown: \"histograms.Wty\",\"histograms.wt_inclusive\",\"histograms.wjets\",\"histograms.wgamma\",\"histograms.zjets\",\"histograms.zgamma\",\"{}\",\"histograms.diboson\",\"histograms.ttv\",\"histograms.singletop_schan\",\"histograms.singletop_tchan\"  \n".format("histograms.tt_muF_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("ttbar_muF"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_muF"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

    #tty_dec muR
    if(self.ttgamma_dec_muR_muF):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("tty_dec_muR"))
      self.file.write("  Samples:  tty_dec\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_muR"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_muR"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_muR"]["SubCategory"]))
      self.file.write("  HistoFileUp: {} \n".format("histograms.tty_dec_muR_up"))
      self.file.write("  HistoFileDown: {} \n".format("histograms.tty_dec_muR_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("tty_dec_muR"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_muR"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

      #tty_dec muF
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("tty_dec_muF"))
      self.file.write("  Samples:  tty_dec\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_muF"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_muF"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_muF"]["SubCategory"]))
      self.file.write("  HistoFileUp: {} \n".format("histograms.tty_dec_muF_up"))
      self.file.write("  HistoFileDown: {} \n".format("histograms.tty_dec_muF_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("tty_dec_muF"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_muF"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

    #tty_prod muR
    if(self.ttgamma_prod_muR_muF):
      self.file.write(" \n")
      self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_muR"))
      self.file.write("  Samples:  tty_prod\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_muR"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_muR"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_muR"]["SubCategory"]))
      self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.tty_prod_muR_up"))
      self.file.write("  ResponseMatrixFileDown: {} \n".format("histograms.tty_prod_muR_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("tty_prod_muR"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_muR"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

      #tty_prod muF
      self.file.write(" \n")
      self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_muF"))
      self.file.write("  Samples:  tty_prod\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_muF"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_muF"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_muF"]["SubCategory"]))
      self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.tty_prod_muF_up"))
      self.file.write("  ResponseMatrixFileDown: {} \n".format("histograms.tty_prod_muF_down"))
      self.file.write("  Symmetrisation: TWOSIDED\n")
      #self.file.write("  Title: \"{}\" \n".format("tty_prod_muF"))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_muF"]["Title"]))
      self.file.write("  Type: HISTO \n")
      self.file.write(" \n")

      ## pdf tty_prod
      if (self.pdf_tty_prod):
        for index in range(1,31):
          self.file.write(" \n")
          self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_PDF4LHC_{}".format(index)))
          self.file.write("  Samples:  tty_prod\n")
          self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_PDF4LHC_{}".format(index)]["ttZ_np_name"]))
          self.file.write("  Category: {} \n".format(np_dict["tty_prod_PDF4LHC_{}".format(index)]["Category"]))
          self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_PDF4LHC_{}".format(index)]["SubCategory"]))
          self.file.write("  ResponseMatrixFileUp: {} \n".format("histograms.tty_prod_PDF4LHC_{}".format(index)))
          self.file.write("  ReferenceSample: tty_prod_PDF4LHC_0 \n")
          self.file.write("  Symmetrisation: ONESIDED\n")
          #self.file.write("  Title: \"{}\" \n".format("tty_prod_PDF4LHC_{}".format(index)))
          self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_PDF4LHC_{}".format(index)]["Title"]))
          self.file.write("  Type: HISTO \n")
          self.file.write(" \n")

      ## pdf tty_dec
      if (self.pdf_tty_dec):
        for index in range(1,31):
          self.file.write(" \n")
          self.file.write("Systematic: {} \n".format("tty_dec_PDF4LHC_{}".format(index)))
          self.file.write("  Samples:  tty_dec\n")
          self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_PDF4LHC_{}".format(index)]["ttZ_np_name"]))
          self.file.write("  Category: {} \n".format(np_dict["tty_dec_PDF4LHC_{}".format(index)]["Category"]))
          self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_PDF4LHC_{}".format(index)]["SubCategory"]))
          self.file.write("  HistoFileUp: {} \n".format("histograms.tty_dec_PDF4LHC_{}".format(index)))
          self.file.write("  ReferenceSample: tty_dec_PDF4LHC_0 \n")
          self.file.write("  Symmetrisation: ONESIDED\n")
          #self.file.write("  Title: \"{}\" \n".format("tty_dec_PDF4LHC_{}".format(index)))
          self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_PDF4LHC_{}".format(index)]["Title"]))
          self.file.write("  Type: HISTO \n")
          self.file.write(" \n")

