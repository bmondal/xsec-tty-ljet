##*************************** CONFIGURATION *******************
normalized_xsec_unfolding= False 
normalized_bin_n_minus_1 =  False
absolute_xsec_unfolding = True 
input_histograms = "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/"

write_bin_width = False 
unfolding_name = False 

use_data = False
write_ttbar_af2 = False
write_pdf_ref_sample = False 
debug = False
all_syst =False
exp_tree_syst = False
exp_weight_syst = False
data_driven_syst = False
modelling_syst = False
bkg_norm_syst = False
btag_lepton_syst = False
muR_muF_syst = False
met_syst = False
lumi_syst = False
use_real_data = False 
use_reweighted_pseudodata = True 
reweighted_data_name = "histograms.ttgamma_prod" # if planning to set this option using argparse then use set the "write_bin-widht = False"
#reweighted_data_name = ""
fit_blind = True
