#!/usr/bin/python

'''
- Plot unfolded spectrum and truth spectrum in a single plot
- Also make the ration plot of Unfolded/Truth 

'''
import ROOT
import argparse

#path = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/abs-xsec-with-trexfiles/v12/v12/stat-all/tty2l_pt_all_stat/"
#path_truth = "/afs/cern.ch/user/b/bmondal/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v7/NN06/CR_sig/nominal/"


debug = False

histo_dict={}
histo_dict["tty1l_pt_all_stat"] = "hist_part_ph_pt_full_weighted"
histo_dict["tty1l_eta_all_stat"] = "hist_part_ph_eta_full_weighted"
histo_dict["tty1l_dr_all_stat"] = "hist_part_ph_drphl1_full_weighted"
histo_dict["tty1l_drphb_all_stat"] = "hist_part_drphb_full_weighted"
histo_dict["tty1l_drlj_all_stat"] = "hist_part_drlj_full_weighted"
histo_dict["tty1l_ptj1_all_stat"] = "hist_part_j1_pt_full_weighted"


def extract_name_from_path(path_):
  splitted_string = path_.split("/")
  return splitted_string[len(splitted_string) -2]


def process_line(line):
  arr = line.split(" ")
  if debug: print(arr)
  return int(arr[0]), float(arr[1]), float(arr[2]), float(arr[3])


def unfolded_hist_from_fit_file(file_path, empty_hist):
  hist = empty_hist.Clone()
  file = open(file_path)
  readline = False
  for line in file.readlines():
    if line.strip():
      if(line[0] == '1'):
        readline = True
        if debug: print("start of reading line\n")
      if readline:
        if debug: print(line)
        bin_number, nominal, err_up, err_down = process_line(line)
        hist.SetBinContent(bin_number, nominal)
        hist.SetBinError(bin_number, err_up)
        print("Bin number: {0}, nominal: {1}, up: {2}, down: {3} \n".format(bin_number, nominal, err_up, err_down))
    else:
      if debug: print("line is empty")
      break
  return hist


def correct_underflow_overflow_bin(hist):
  underflow_content = hist.GetBinContent(0)
  overflow_content = hist.GetBinContent(hist.GetNbinsX()+1)
  hist.SetBinContent(1, hist.GetBinContent(1)+underflow_content)
  hist.SetBinContent(hist.GetNbinsX(), hist.GetBinContent(hist.GetNbinsX())+overflow_content)
  return hist


def createCanvasPads():
  c = ROOT.TCanvas("c", "canvas", 800, 800)
  # Upper histogram plot is pad1
  pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
  pad1.SetBottomMargin(0)  # joins upper and lower plot
  #pad1.SetGridx()
  pad1.Draw()
  # Lower ratio plot is pad2
  c.cd()  # returns to main canvas before defining pad2
  pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
  pad2.SetTopMargin(0)  # joins upper and lower plot
  pad2.SetBottomMargin(0.2)
  #pad2.SetGridx()
  pad2.SetGridy()
  pad2.Draw()

  return c, pad1, pad2


def run(path, path_truth):
  path_truth_hist = "{}/histograms.ttgamma_incl.root".format(path_truth)
  path_unfolded_hist = "{}/Fits/Unfolding_UnfoldedResults.txt".format(path)

  print(" path : {} path_truth: {}".format(path, path_truth))
  #file_unfolded = ROOT.TFile.Open(path_unfolded_hist)
  #key_name = ""
  #for key in file_unfolded.GetListOfKeys():
  #  print("key name: {}".format(key.GetName()))
  #  if "c1" in key.GetName():
  #    key_name = key.GetName()
  #unfolded_plot_canvas = file_unfolded.Get("{}".format(key_name)) # unfolded histogram; getting it from canvas
  file_truth = ROOT.TFile.Open(path_truth_hist)
  hist_name = histo_dict[extract_name_from_path(path)]
  hist_truth_tmp = file_truth.Get("particle/{}".format(hist_name))
  hist_truth = correct_underflow_overflow_bin(hist_truth_tmp)
  #hist_truth = hist_truth_tmp  # FIXME: DANGER DANGER DANGER
  # hists with deltaR starting from 0.4
  #hist_dr_list = ["hist_part_ph_drphl_full_weighted", "hist_part_ph_drphl1_full_weighted", "hist_part_ph_drphl2_full_weighted", "hist_part_drlj_full_weighted", "hist_part_drphb_full_weighted"]
  # divide by bin width
  for i in range(0, hist_truth.GetNbinsX()):
    bin_content = 0
    bin_width = 0
    value = 0
    bin_content = hist_truth.GetBinContent(i+1)
    bin_width = hist_truth.GetBinWidth(i+1)
    ## if hist contains deltaR, in bin width calculation we should subtract 0.4 from the first bin
    #if any(hist in hist_name for hist in hist_dr_list):
    #  if i == 0:
    #    bin_width = bin_width - 0.4
    value = bin_content/(bin_width*139)
    print("bin content: {0}; bin width: {1}; value: {2}".format(bin_content, bin_width, value))
    hist_truth.SetBinContent(i+1, value)
    hist_truth.SetBinError(i+1, 0)

  #hist = unfolded_plot_canvas.FindObject("hist_part_ph_pt_full_weighted")
  hist = unfolded_hist_from_fit_file(path_unfolded_hist, hist_truth)
  hist_clone = hist.Clone()
  hist.SetLineColor(4)
  hist_truth_clone = hist_truth.Clone()
  hist_truth.SetLineColor(2)
  canvas, pad1, pad2 = createCanvasPads()
  pad1.cd()
  hist.GetXaxis().SetLabelFont(63)
  hist.GetXaxis().SetLabelSize(14)
  hist.GetYaxis().SetLabelFont(63)
  hist.GetYaxis().SetLabelSize(14)
  hist.GetYaxis().SetTitleSize(0.05)
  #hist.GetXaxis().SetTitle("p_{T}(#gamma) [GeV]")
  hist.GetYaxis().SetTitle("events/GeV")
  hist.SetStats(False)
  hist.Draw("hist e P*")
  hist_truth.Draw("hist same e")
  # legend
  legend = ROOT.TLegend(0.7,0.7, 0.9,0.9)
  legend.AddEntry(hist, "Pseudo-data unfolded", "l")
  legend.AddEntry(hist_truth, " truth", "l")
  legend.Draw()

  pad2.cd()
  hist_clone.SetStats(False)
  hist_clone.Divide(hist_truth_clone)
  hist_clone.GetYaxis().SetRangeUser(0.93,1.07)
  hist_clone.GetXaxis().SetLabelFont(63)
  hist_clone.GetXaxis().SetLabelSize(14)
  hist_clone.GetYaxis().SetLabelFont(63)
  hist_clone.GetYaxis().SetLabelSize(14)
  hist_clone.GetYaxis().SetTitleSize(0.07)
  hist_clone.GetXaxis().SetTitleSize(0.08)
  hist_clone.GetYaxis().SetTitle("Unfolded/Truth")
  hist_clone.Draw("hist e P*")

  hist_clone.GetYaxis().SetRangeUser(0.7,1.3)
  save_name = extract_name_from_path(path)+".pdf"
  canvas.SaveAs(save_name)

  # print chiSquare
  #chisquare = hist_truth.GetChiSquare(hist)
  #print("chi square : {}".format(chisquare))


if __name__== '__main__':
  parser = argparse.ArgumentParser(description ='create trex-config file for ph pt')
  parser.add_argument("--path-trex-fit", type=str, help="config file name")
  parser.add_argument("--path-truth", type=str, help="config file name")
  args = parser.parse_args()
  path = args.path_trex_fit
  path_truth = args.path_truth
  run(path, path_truth)
