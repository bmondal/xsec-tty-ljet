#!/usr/bin/env python3

import subprocess
import ROOT
from calculate_response_matrix import *

import os
import threading


def merge_tty_prod_tty_dec(path,binning_name):
  sub_dirs = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d)) ]
  total_dirs = len(sub_dirs)
  current_dir = 1

  # loop through each subdirectory
  for sub_dir in sub_dirs:
    # full path of subdirectory
    sub_dir_path = os.path.join(path,sub_dir)

    # open the ttgamma_prod.root and ttgamma_dec.root file
    if binning_name=="":
      ttgamma_prod_path = os.path.join(sub_dir_path, "histograms.ttgamma_prod.root")
      ttgamma_dec_path = os.path.join(sub_dir_path, "histograms.ttgamma_dec.root")
      # command to merge the ttgamma_prod and ttgamma_dec files
      cmd = "hadd -v 0 -f {0}/histograms.ttgamma_incl.root {1} {2}".format(sub_dir_path,ttgamma_prod_path, ttgamma_dec_path)
      print("3 hadd -v 0 -f {0}/histograms.ttgamma_incl.root {1} {2}\n".format(sub_dir_path,ttgamma_prod_path, ttgamma_dec_path))


    else:
      ttgamma_prod_path = os.path.join(sub_dir_path, "histograms.ttgamma_prod.{}.root".format(binning_name))
      ttgamma_dec_path = os.path.join(sub_dir_path, "histograms.ttgamma_dec.{}.root".format(binning_name))
      # command to merge the ttgamma_prod and ttgamma_dec files
      cmd = "hadd -v 0 -f {0}/histograms.ttgamma_incl.{1}.root {2} {3}".format(sub_dir_path,binning_name,ttgamma_prod_path, ttgamma_dec_path)
      print("3 hadd -v 0 -f {0}/histograms.ttgamma_incl.{1}.root {2} {3}\n".format(sub_dir_path,binning_name,ttgamma_prod_path, ttgamma_dec_path))


    subprocess.call(cmd, shell=True)

    # display progress
    print("{}/{} subdirectoreis processed.\n".format(current_dir,total_dirs))
    current_dir += 1

def merge_tty_prod_tty_dec_muR_muF_pdf(path_to_CR,binning_name):
  # find the list of the files in nominal folder
  list_of_files = []
  sub_dir_path = os.path.join(path_to_CR,"nominal_9999")
  for filename in os.listdir(sub_dir_path):
    # find the PDF, muR, muF files and merge
    # create only tty_prod var samples (in this case tty_dec should be nominal sample)
    if (".tty_prod_" in filename and ".root" in filename and binning_name in filename):
      ttgamma_prod_filename = filename.strip()

      if binning_name=="":
        ttgamma_dec_filename = "histograms.ttgamma_dec.root"
      else:
        ttgamma_dec_filename = "histograms.ttgamma_dec.{}.root".format(binning_name)
      ttgamma_incl_filename = ttgamma_prod_filename.replace("prod","incl_prod")

      # open the tty_prod.root and tty_dec.root file
      ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
      ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

      # command to merge the ttgamma_prod and ttgamma_dec files
      cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
      subprocess.call(cmd, shell=True)

    ## create only tty_dec var samples (in this case tty_prod should be nominal sample)
    #elif (".tty_dec_" in filename and ".root" in filename and binning_name in filename):
    #  if binning_name == "":
    #    ttgamma_prod_filename = "histograms.ttgamma_prod.root"
    #  else:
    #    ttgamma_prod_filename = "histograms.ttgamma_prod.{}.root".format(binning_name)
    #  ttgamma_dec_filename = filename.strip()
    #  ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl_dec")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge var3c samples; create only tty_prod var samples (in this case tty_dec should be nominal sample)
    #elif (".ttgamma_prod" in filename and ".root" in filename and binning_name in filename and "var3c" in filename):
    #  ttgamma_prod_filename = filename.strip()
    #  if binning_name=="":
    #    ttgamma_dec_filename = "histograms.ttgamma_dec.root"
    #  else:
    #    ttgamma_dec_filename = "histograms.ttgamma_dec.{}.root".format(binning_name)
    #  ttgamma_incl_filename = ttgamma_prod_filename.replace("prod","incl_prod")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge var3c samples; create only tty_dec var samples (in this case ttgamma_prod should be nominal sample)
    #elif (".tty_dec" in filename and ".root" in filename and binning_name in filename and "var3c" in filename):
    #  if binning_name=="":
    #    ttgamma_prod_filename = "histograms.ttgamma_prod.root"
    #  else:
    #    ttgamma_prod_filename = "histograms.ttgamma_prod.{}.root".format(binning_name)
    #  ttgamma_dec_filename = filename.strip()
    #  ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl_dec")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge H7 samples; create only tty_prod var samples (in this case tty_dec should be nominal sample)
    #elif (".ttgamma_prod" in filename and ".root" in filename and binning_name in filename and "H7" in filename):
    #  ttgamma_prod_filename = filename.strip()
    #  if binning_name=="":
    #    ttgamma_dec_filename = "histograms.ttgamma_dec.root"
    #  else:
    #    ttgamma_dec_filename = "histograms.ttgamma_dec.{}.root".format(binning_name)
    #  ttgamma_incl_filename = ttgamma_prod_filename.replace("prod","incl_prod")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge H7 samples; needed for nominal truth
    #elif (".ttgamma_prod" in filename and ".root" in filename and binning_name in filename and "H7" in filename):
    #  ttgamma_prod_filename = filename.strip()
    #  ttgamma_dec_filename = ttgamma_prod_filename.replace("prod","dec")
    #  ttgamma_incl_filename = ttgamma_prod_filename.replace("prod","incl_H7")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge H7 samples; create only tty_dec var samples (in this case tty_prod should be nominal sample)
    #elif (".ttgamma_dec" in filename and ".root" in filename and binning_name in filename and "H7" in filename):
    #  if binning_name=="":
    #    ttgamma_prod_filename = "histograms.ttgamma_prod.root"
    #  else:
    #    ttgamma_prod_filename = "histograms.ttgamma_prod.{}.root".format(binning_name)
    #  ttgamma_dec_filename = filename.strip() 
    #  ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl_dec")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge tty_prod H7 tty_dec H7 samples
    #elif (".ttgamma_dec" in filename and ".root" in filename and binning_name in filename and "H7" in filename):
    #  ttgamma_dec_filename = filename.strip() 
    #  ttgamma_prod_filename = ttgamma_dec_filename.replace("dec","prod")
    #  ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge uncalib ljets
    #elif (".ttgamma_dec" in filename and ".root" in filename and binning_name in filename and "uncalib_ljet" in filename):
    #  ttgamma_dec_filename = filename.strip() 
    #  ttgamma_prod_filename = ttgamma_dec_filename.replace("dec","prod")
    #  ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge uncalib cjets
    #elif (".ttgamma_dec" in filename and ".root" in filename and binning_name in filename and "uncalib_cjet" in filename):
    #  ttgamma_dec_filename = filename.strip() 
    #  ttgamma_prod_filename = ttgamma_dec_filename.replace("dec","prod")
    #  ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)

    ## merge uncalib bjets
    #elif (".ttgamma_dec" in filename and ".root" in filename and binning_name in filename and "uncalib_bjet" in filename):
    #  ttgamma_dec_filename = filename.strip() 
    #  ttgamma_prod_filename = ttgamma_dec_filename.replace("dec","prod")
    #  ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl")

    #  # open the tty_prod.root and tty_dec.root file
    #  ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
    #  ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

    #  # command to merge the ttgamma_prod and ttgamma_dec files
    #  cmd = "hadd -v 0 -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
    #  subprocess.call(cmd, shell=True)


    # merge tty_dec_NormVar samples; vary only tty_dec_NormVar then merge with nominal ttgamma_prod template
    elif (".ttgamma_dec" in filename and ".root" in filename and binning_name in filename and "NormVar" in filename):
      if binning_name=="":
        ttgamma_prod_filename = "histograms.ttgamma_prod.root"
      else:
        ttgamma_prod_filename = "histograms.ttgamma_prod.{}.root".format(binning_name)
      ttgamma_dec_filename = filename.strip() 
      ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl_dec")

      # open the tty_prod.root and tty_dec.root file
      ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
      ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

      # command to merge the ttgamma_prod and ttgamma_dec files
      cmd = "hadd -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
      subprocess.call(cmd, shell=True)

    # merge tty_dec_NormVar samples; vary only tty_dec_NormVar then merge with nominal ttgamma_prod template
    elif (".ttgamma_dec" in filename and ".root" in filename and binning_name in filename and "reweight" in filename):
      if binning_name=="":
        ttgamma_prod_filename = "histograms.ttgamma_prod.root"
      else:
        ttgamma_prod_filename = "histograms.ttgamma_prod.{}.root".format(binning_name)
      ttgamma_dec_filename = filename.strip() 
      ttgamma_incl_filename = ttgamma_dec_filename.replace("dec","incl_dec")

      # open the tty_prod.root and tty_dec.root file
      ttgamma_prod_path = os.path.join(sub_dir_path, ttgamma_prod_filename)
      ttgamma_dec_path = os.path.join(sub_dir_path, ttgamma_dec_filename)

      # command to merge the ttgamma_prod and ttgamma_dec files
      cmd = "hadd -f {0}/{1} {2} {3}".format(sub_dir_path,ttgamma_incl_filename,ttgamma_prod_path, ttgamma_dec_path)
      subprocess.call(cmd, shell=True)





def run(path_to_CR, binning_name):
  # hadd ttgamma_prod and ttgamma_dec
  print(" 2 {} binning {}\n".format(path_to_CR,binning_name))
  merge_tty_prod_tty_dec(path_to_CR, binning_name)

def run_rest(path_to_CR, binning_name):
  merge_tty_prod_tty_dec_muR_muF_pdf(path_to_CR, binning_name)

  

if __name__=="__main__":
  submit_merging_part = False
  submit_merging_part_nominal_only = True #this includes nominal folder, muR_muF,PDF ...
  submit_response_matrix_part = False 
  calculate_response_matrix_over_folders = True# recalcualte  response matrix over some folders (this is needed because of some corrupted file)
  calculate_response_matrix_over_file = False
  #path = "./tmp"
  #path = "/Users/buddhadeb/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/"
  #path = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/"
  path = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v12_v1/"
  #path = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v8/"
  #path = "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/"
  #binning_name = "nominal_from_fine" #binning_name = "nominal_from_fine"
  binning_name = "nominal_from_fine" #binning_name = "nominal_from_fine"
  #binning_name = "" #binning_name = "nominal_from_fine"
  #binning_name = "nominal_from_fine_uncalib_bjet_x0.5" #binning_name = "nominal_from_fine"

  #binning_name = "Nominal_Binning"
  path_to_tty_CR= os.path.expanduser(os.path.join(path,"tty_CR"))
  path_to_tty_dec_CR= os.path.expanduser(os.path.join(path,"tty_dec_CR"))
  path_to_fakes_CR= os.path.expanduser(os.path.join(path,"fakes_CR"))
  path_to_other_photons_CR= os.path.expanduser(os.path.join(path,"other_photons_CR"))
  print("{} \n".format(path_to_tty_CR))


  if submit_merging_part:
    ## Create two threads
    thread1 = threading.Thread(target=run, args=(path_to_tty_CR,binning_name))
    thread2 = threading.Thread(target=run, args=(path_to_tty_dec_CR,binning_name))
    thread3 = threading.Thread(target=run, args=(path_to_fakes_CR,binning_name))
    thread4 = threading.Thread(target=run, args=(path_to_other_photons_CR,binning_name))

    # Start the threads
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()

    # wait for both threads to finish
    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()

  if submit_merging_part_nominal_only:
    run_rest(path_to_tty_CR, binning_name)
    run_rest(path_to_tty_dec_CR, binning_name)
    run_rest(path_to_fakes_CR, binning_name)
    run_rest(path_to_other_photons_CR, binning_name)

  # when run and run_rest are done ; update with calculating response matrix
  if submit_response_matrix_part:
    ## Create two threads
    thread1 = threading.Thread(target=calculate_and_save_response_matrices_over_main_dir, args=(path_to_tty_CR,binning_name))
    thread2 = threading.Thread(target=calculate_and_save_response_matrices_over_main_dir, args=(path_to_tty_dec_CR,binning_name))
    thread3 = threading.Thread(target=calculate_and_save_response_matrices_over_main_dir, args=(path_to_fakes_CR,binning_name))
    thread4 = threading.Thread(target=calculate_and_save_response_matrices_over_main_dir, args=(path_to_other_photons_CR,binning_name))

    # Start the threads
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()

    # wait for both threads to finish
    #thread1.join()
    #thread2.join()
    #thread3.join()
    #thread4.join()
  
  if calculate_response_matrix_over_folders:
    list_of_folders_tty_CR= ["nominal_9999"]
    list_of_folders_tty_dec_CR= ["nominal_9999"]
    list_of_folders_fakes_CR= ["nominal_9999"]
    list_of_folders_other_photons_CR= ["nominal_9999"]

    for file_ in list_of_folders_tty_CR:
      calculate_and_save_response_matrices_over_sub_dir(os.path.join(path_to_tty_CR, file_),binning_name)
    for file_ in list_of_folders_tty_dec_CR:
      calculate_and_save_response_matrices_over_sub_dir(os.path.join(path_to_tty_dec_CR, file_),binning_name)
    for file_ in list_of_folders_fakes_CR:
      calculate_and_save_response_matrices_over_sub_dir(os.path.join(path_to_fakes_CR, file_),binning_name)
    for file_ in list_of_folders_other_photons_CR:
      calculate_and_save_response_matrices_over_sub_dir(os.path.join(path_to_other_photons_CR, file_),binning_name)


  if calculate_response_matrix_over_file:
    list_of_files_tty_CR= ["nominal_9999/histograms.ttgamma_incl_H7.root"]
    list_of_files_tty_dec_CR= ["nominal_9999/histograms.ttgamma_incl_H7.root"]
    list_of_files_fakes_CR= ["nominal_9999/histograms.ttgamma_incl_H7.root"]
    list_of_files_other_photons_CR = ["nominal_9999/histograms.ttgamma_incl_H7.root"]
    for file_ in list_of_files_tty_CR:
      print("Info:: going to calculate only the response matrix for these sameple: {}/{}\n".format(path_to_tty_CR,file_))
      calculate_and_save_response_matrices(os.path.join(path_to_tty_CR, file_))
    for file_ in list_of_files_tty_dec_CR:
      calculate_and_save_response_matrices(os.path.join(path_to_tty_dec_CR, file_))
    for file_ in list_of_files_fakes_CR:
      calculate_and_save_response_matrices(os.path.join(path_to_fakes_CR, file_))
    for file_ in list_of_files_other_photons_CR:
      calculate_and_save_response_matrices(os.path.join(path_to_other_photons_CR, file_))


