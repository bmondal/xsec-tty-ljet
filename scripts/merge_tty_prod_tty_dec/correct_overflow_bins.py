from constants import *

"""
"add_overflow_to_previous" is a function that takes in a 2D histogram as 
input and modifies it by adding the content and error of the overflow 
bins to the previous bin. The function loops through the bins in 
both x and y axes and adds the content and error of the overflow bins 
to the previous bins. The histogram with updated values is then returned as output.

the below implementation does this:

--- Before correction ---
0.0 0.0 0.0 0.0 0.0 0.0
18.0 7.0 8.0 9.0 37.0 0.0
5.0 6.0 7.0 8.0 9.0 0.0
4.0 5.0 6.0 7.0 8.0 0.0
3.0 4.0 5.0 6.0 10.0 0.0
2.0 3.0 4.0 5.0 17.0 0.0

(top most and right most bins are overflow bins)

--- After correction ---

0.0 0.0 0.0 0.0 0.0 0.0
18.0 7.0 8.0 9.0 37.0 0.0
5.0 6.0 7.0 8.0 9.0 0.0
4.0 5.0 6.0 7.0 8.0 0.0
3.0 4.0 5.0 6.0 10.0 0.0
2.0 3.0 4.0 5.0 17.0 0.0

"""
def add_overflow_to_previous(histogram):
    nx = histogram.GetNbinsX()
    ny = histogram.GetNbinsY()
    for i in range(1,nx+2):
        for j in range(1,ny+2):
            if i == nx+1:
                if debug: print("histoname {} \t binx, biny, bincontent: {} \t {} \t {} \n".format(histogram.GetName(), i, j,
                                                                                         histogram.GetBinContent(i, j)))
                histogram.SetBinContent(i-1, j, histogram.GetBinContent(i-1, j) + histogram.GetBinContent(i, j))
                histogram.SetBinError(i-1, j, (histogram.GetBinError(i-1, j) ** 2 + histogram.GetBinError(i, j) ** 2) ** 0.5)
            if j == ny+1:
                if debug: print("histoname {} \t binx, biny, bincontent: {} \t {} \t {} \n".format(histogram.GetName(), i, j,
                                                                                         histogram.GetBinContent(i, j)))
                histogram.SetBinContent(i, j-1, histogram.GetBinContent(i, j-1) + histogram.GetBinContent(i, j))
                histogram.SetBinError(i, j-1, (histogram.GetBinError(i, j-1) ** 2 + histogram.GetBinError(i, j) ** 2) ** 0.5)
    # add the right-top diagonal corner overflow bin to the previous diagonal bin
    # (nx+1, ny+1) to (nx,ny)
    histogram.SetBinContent(nx,ny, histogram.GetBinContent(nx,ny) + histogram.GetBinContent(nx+1, ny+1))
    histogram.SetBinError(nx,ny, (histogram.GetBinError(nx,ny) ** 2 + histogram.GetBinError(nx+1, ny+1) ** 2) ** 0.5)

    # now set the overflow bins to zero
    for i in range(1,nx+2):
        for j in range(1,ny+2):
            if i == nx+1:
                histogram.SetBinContent(i, j, 0)
                histogram.SetBinError(i, j, 0)
            if j == ny+1:
                histogram.SetBinContent(i, j, 0)
                histogram.SetBinError(i, j, 0)


    return histogram


"""
"rebin_2d_histogram" is a function to rebin a 2D histogram with a new set of bin edges. 
The original 2D histogram, "h2", is passed as an argument along with the new x-axis 
bin edges, "xedges", and y-axis bin edges, "yedges". The function creates a new 2D 
histogram, "h2_rebinned", using the new bin edges and sets the axis labels to match 
the original histogram. The contents and errors of the original histogram bins 
are added to the corresponding bin in the new histogram. The overflow bin 
contribution is also added to the last bin of the new histogram. 
The rebinned 2D histogram is then returned.
"""
def rebin_2d_histogram(h2, xedges, yedges, new_hist_name):
    # Create a new histogram with the new bin edges
    nx = len(xedges) - 1
    ny = len(yedges) - 1
    h2_rebinned = ROOT.TH2D(new_hist_name, "", nx, np.asarray(xedges), ny, np.asarray(yedges))

    # Initialize the new histogram with the same axis labels as the original
    h2_rebinned.GetXaxis().SetTitle(h2.GetXaxis().GetTitle())
    h2_rebinned.GetYaxis().SetTitle(h2.GetYaxis().GetTitle())

    # first set the bin error to zero; because in the loop it adds uncertainty on top of this
    h2_rebinned.SetBinError(0,0)
    # Loop over the original histogram bins
    for xbin in range(1, h2.GetNbinsX() + 1):
        for ybin in range(1, h2.GetNbinsY() + 1):
            xcenter = h2.GetXaxis().GetBinCenter(xbin)
            ycenter = h2.GetYaxis().GetBinCenter(ybin)
            z = h2.GetBinContent(xbin, ybin)
            error = h2.GetBinError(xbin, ybin)
            # Find the corresponding bin in the new histogram
            new_xbin = h2_rebinned.GetXaxis().FindBin(xcenter)
            new_ybin = h2_rebinned.GetYaxis().FindBin(ycenter)
            # Add the bin contents and errors to the corresponding bin in the new histogram
            newbin = h2_rebinned.GetBin(new_xbin, new_ybin)
            h2_rebinned.AddBinContent(newbin, z)
            h2_rebinned.SetBinError(newbin,
                                    math.sqrt(h2_rebinned.GetBinError(newbin) ** 2 + error ** 2))

    # Add overflow bin contribution to the last bin of the new histogram
    overflow_x = h2.GetBinContent(h2.GetNbinsX() + 1, 0)
    overflow_y = h2.GetBinContent(0, h2.GetNbinsY() + 1)
    overflow_z = h2.GetBinContent(h2.GetNbinsX() + 1, h2.GetNbinsY() + 1)
    if debug: print("overflow x, y, z: {} \t {} \t {}\n".format(overflow_x, overflow_y, overflow_z))
    error_overflow_x = h2.GetBinError(h2.GetNbinsX() + 1, 0)
    error_overflow_y = h2.GetBinError(0, h2.GetNbinsY() + 1)
    error_overflow_z = h2.GetBinError(h2.GetNbinsX() + 1, h2.GetNbinsY() + 1)
    h2_rebinned.AddBinContent(h2_rebinned.GetNbinsX(), overflow_x)
    h2_rebinned.AddBinContent(h2_rebinned.GetNbinsY(), overflow_y)
    h2_rebinned.AddBinContent(h2_rebinned.GetNbinsX() * h2_rebinned.GetNbinsY(), overflow_z)
    h2_rebinned.SetBinError(h2_rebinned.GetNbinsX(),
                            math.sqrt(h2_rebinned.GetBinError(h2_rebinned.GetNbinsX()) ** 2 + error_overflow_x ** 2))
    h2_rebinned.SetBinError(h2_rebinned.GetNbinsY(),
                            math.sqrt(h2_rebinned.GetBinError(h2_rebinned.GetNbinsY()) ** 2 + error_overflow_y ** 2))
    h2_rebinned.SetBinError(h2_rebinned.GetNbinsX() * h2_rebinned.GetNbinsY(),
                            math.sqrt(h2_rebinned.GetBinError(
                                h2_rebinned.GetNbinsX() * h2_rebinned.GetNbinsY()) ** 2 + error_overflow_z ** 2))

    return h2_rebinned
