#!/usr/bin/env bash
for CR in "tty_CR" "tty_dec_CR" "fakes_CR" "other_photons_CR"; do
  echo ">>>>>> processing $CR <<<<<<<<<<<<<"
  path_nominal="/Users/buddhadeb/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/${CR}/nominal_9999/"
  path_uncalib="/Users/buddhadeb/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v15/${CR}/nominal_9999/"
  nominal_file="${path_nominal}/histograms.ttgamma_prod.root"
  uncalib_file="${path_uncalib}/histograms.ttgamma_prod.nominal_from_fine_uncalib_x0.5.root"
  output_file="${path_uncalib}/histograms.ttgamma_prod.nominal_from_fine_uncalib_x1.5.root"

  ./make_uncalib_template.py --input_nominal_file $nominal_file --input_uncalib_file $uncalib_file --output_file $output_file
done
