#!/usr/bin/env python3
"""
this program is for creating response matrix in which uncalibrated jet uncertainty
is incorporated
lets say I have nominal tty_prod template in which there is uncalibrated jet present
n = a + b (b is the contribution of uncalibrated jet events)
I want to create a response matrix which in which following holds:
Reco = R(a+b)
Reco = R'(a+b); R' is the new response matrix in which uncalibrated jet uncertainty is included
"""

import ROOT
import argparse


def calculate_response_matrix(reco_hist, truth_hist, migration_hist):
    response_matrix = migration_hist.Clone()
    N_R = reco_hist.GetNbinsX()
    N_T = truth_hist.GetNbinsX()
    N_mig_R = migration_hist.GetNbinsX()
    N_mig_T = migration_hist.GetNbinsY()
    dumTcontent = -1.e6
    rbin_total = 0
    res_value = [[0 for x in range(1000)] for y in range(1000)]
    res_error = [[0 for x in range(1000)] for y in range(1000)]

    if ((N_R != N_T) or N_R == 0):
        print("Unequal/empty truth and/or reco")

    '''response matrix calculation...'''
    for bin_r in range(1, N_R + 1):
        rbin_total = 0.
        for bin_t in range(1, N_T + 1):
            rbin_total += migration_hist.GetBinContent(bin_r, bin_t)
        for bin_t in range(1, N_T + 1):
            dumTcontent = truth_hist.GetBinContent(bin_t)
            if not (rbin_total == 0 or dumTcontent == 0):
                res_value[bin_r - 1][bin_t - 1] = 1. * (migration_hist.GetBinContent(bin_r, bin_t)) * (
                            (reco_hist.GetBinContent(bin_r)) / (dumTcontent * rbin_total))
                res_error[bin_r - 1][bin_t - 1] = 1. * (migration_hist.GetBinError(bin_r, bin_t)) * (
                            (reco_hist.GetBinContent(bin_r)) / (dumTcontent * rbin_total))
            response_matrix.SetBinContent(bin_r, bin_t, res_value[bin_r - 1][bin_t - 1])
            response_matrix.SetBinError(bin_r, bin_t, res_error[bin_r - 1][bin_t - 1])

    return response_matrix


def run(nominal_file, uncalib_file, output_file):
    nominal_tfile = ROOT.TFile(nominal_file)
    uncalib_tfile = ROOT.TFile(uncalib_file)
    output_tfile = ROOT.TFile(output_file,"RECREATE")
    histograms_list_dict = {"hist_reco_ph_pt_full_weighted":"h2_ph_pt_reco_part_full_weighted",
                            "hist_reco_ph_eta_full_weighted":"h2_ph_eta_reco_part_full_weighted",
                            "hist_reco_ph_drphl1_full_weighted":"h2_ph_drphl1_reco_part_full_weighted",
                            "hist_reco_drphb_full_weighted":"h2_drphb_reco_part_full_weighted",
                            "hist_reco_drlj_full_weighted":"h2_drlj_reco_part_full_weighted",
                            "hist_reco_j1_pt_full_weighted":"h2_j1_pt_reco_part_full_weighted"}
    hist_response_matrix_dict = {"hist_reco_ph_pt_full_weighted":"h2_response_matrix_ph_pt",
                            "hist_reco_ph_eta_full_weighted":"h2_response_matrix_ph_eta",
                            "hist_reco_ph_drphl1_full_weighted":"h2_response_matrix_ph_dryl1",
                            "hist_reco_drphb_full_weighted":"h2_response_matrix_drphb",
                            "hist_reco_drlj_full_weighted":"h2_response_matrix_drlj",
                            "hist_reco_j1_pt_full_weighted":"h2_response_matrix_j1_pt"}

    for hist in histograms_list_dict:
        nominal_hist_reco = nominal_tfile.Get("Reco/{}".format(hist))
        nominal_hist_part = nominal_tfile.Get("particle/{}".format(hist.replace("reco","part")))
        uncalib_hist = uncalib_tfile.Get("Reco/{}".format(hist))
        hist_reco = nominal_hist_reco.Clone() # Reco (a+b)
        hist_reco.Add(uncalib_hist) # Reco (a+b+b/2)
        hist_particle = nominal_hist_part.Clone() # particle (a+b)
        hist_particle.Add(uncalib_hist) # particle (a+b+b/2) ; b/2 is same in particle as reco as there are no migration from particle to reco for uncalibrated jets
        nominal_migration_matrix = nominal_tfile.Get(histograms_list_dict[hist])
        response_matrix = calculate_response_matrix(hist_reco,hist_particle,nominal_migration_matrix)

        response_matrix.SetName(hist_response_matrix_dict[hist])
        response_matrix.Write()

    output_tfile.Close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser() 
    parser.add_argument("--input_nominal_file", type=str, help="name of the nominal file")
    parser.add_argument("--input_uncalib_file", type=str, help="name of the uncalib file")
    parser.add_argument("--output_file", type=str, help="name of the output file")

    args = parser.parse_args()

    run(args.input_nominal_file, args.input_uncalib_file, args.output_file)