#!/usr/bin/env python3
"""
I have a folder, which contains many subfolder. inside the subfolders I have many root files. Find those root files and try opening it. if you find that the root file is corrupted then store those files with the path and print those files. 
"""

import ROOT
import os
import argparse

def find_corrupted_files(folder):
    corrupted_files = []
    corrupted_folders = []
    for root, dirs, files in os.walk(folder):
        for file in files:
            if file.endswith('.root'):
                file_path = os.path.join(root, file)
                try:
                    root_file = ROOT.TFile.Open(file_path)
                    if not root_file:
                        raise IOError('Could not open file')
                    if root_file.IsZombie():
                        raise IOError('File is a zombie')
                    if not root_file.GetListOfKeys():
                        raise IOError('File has no keys')
                    if ".ttgamma_prod." in file_path or ".tty_prod." in file_path or ".tty_prod_" in file_path or ".ttgamma_dec." in file_path or ".tty_dec." in file_path or ".tty_dec_" in file_path:
                      if not any("h2_response_matrix_ph_pt" in key.GetName() for key in root_file.GetListOfKeys()):
                          raise IOError('key not found')

                    root_file.Close()
                except:
                    corrupted_files.append(file_path)
                    corrupted_folders.append(os.path.basename(root))

    return corrupted_files,corrupted_folders

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Find corrupted root files in a folder and its subdirectories')
    parser.add_argument('--folder', type=str, help='Path to the folder to search')
    #parser.add_argument('--outfile', type=str, help='corrupted folders will be stored in outfile')

    args = parser.parse_args()
    folder = args.folder

    corrupted_files,corrupted_folders = find_corrupted_files(folder)
    corrupted_folders_set = set(corrupted_folders)

#    if len(corrupted_files) > 0:
#        print('The following root files are corrupted:')
#        for file in corrupted_files:
#            print(file)
    if len(corrupted_folders_set) > 0:
        print('There are corrupted files in the following folders:')
        for folder in corrupted_folders_set:
            print(folder)

    else:
        print('No corrupted root files found.')

