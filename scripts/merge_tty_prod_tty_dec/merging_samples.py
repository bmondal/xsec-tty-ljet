"""
CURRENTLY NOT WORKING.
NEED TO FIX THE LOGIC

KEEPING THE FILE FOR LATER REFERENCE TO SOME PART OF THE CODE


"""


def merge_directory(input_dir, output_dir):
  for key in input_dir.GetListOfKeys():
    obj = key.ReadObj()
    if isinstance(obj,ROOT.TDirectory()):
      sub_dir = output_dir.mkdir(obj.GetName())
    else:
      obj.Write()

def merge_tty_prod_tty_dec(path):
  sub_dirs = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d)) ]

  # loop through each subdirectory
  for sub_dir in sub_dirs:
    # full path of subdirectory
    sub_dir_path = os.path.join(path,sub_dir)

    # open the ttgamma_prod.root and ttgamma_dec.root file
    ttgamma_prod = ROOT.TFile(os.path.join(sub_dir_path, "histograms.ttgamma_prod.Nominal_Binning.root"))
    ttgamma_dec = ROOT.TFile(os.path.join(sub_dir_path, "histograms.ttgamma_dec.Nominal_Binning.root"))

    # Create a new TFile for the merged file
    ttgamma_incl = ROOT.TFile(os.path.join(sub_dir_path, "histograms.ttgamma_incl.Nominal_Binning.root"), "RECREATE")

    # Loop over the objecs in both files and merge them into the new file
    for key in ttgamma_prod.GetListOfKeys():
      # skip the response matrices
      if "response" in key.GetName(): continue
      obj = key.ReadObj()
      # If the object is a TDirectry
      if isinstance(obj, ROOT.TDirectory):
        # Make the subdirectory
        sub_dir = ttgamma_incl.mkdir(obj.GetName())
        # loop over the subkeys
        for sub_key in obj.GetListOfKeys():
          sub_obj = sub_key.ReadObj()
          sub_dir.WriteObject(sub_obj,"{}".format(sub_obj.GetName()))
        # write the sub dir to tfile
        ttgamma_incl.WriteObject(sub_dir,"{}".format(sub_dir.GetName()))
      obj.Write()
    for key in ttgamma_dec.GetListOfKeys():
      # skip the response matrices
      if "response" in key.GetName(): continue
      obj = key.ReadObj()
      # If the object is a TDirectory
      if isinstance(obj,ROOT.TDirectory):
        # Make the subdirectory
        sub_dir = ttgamma_incl.mkdir(obj.GetName()) # FIXME no need to create the directory again
        # loop over subkeys
        for sub_key in obj.GetListOfKeys():
          sub_obj = sub_key.ReadObj()
          sub_dir.WriteObject(sub_obj,"{}".format(sub_obj.GetName()))
        # write the sub dir to tfile
        ttgamma_incl.WriteObject(sub_dir,"{}".format(sub_dir.GetName()))
      obj.Write()

    #merge_directory(ttgamma_prod, ttgamma_incl)
    #merge_directory(ttgamma_dec, ttgamma_incl)
    # Close the files
    ttgamma_prod.Close()
    ttgamma_dec.Close()
    ttgamma_incl.Close()
