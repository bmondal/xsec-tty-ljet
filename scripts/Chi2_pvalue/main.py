# Open the text file in read mode
variables = ["log_pt","log_eta","log_dr","log_drphb","log_drlj","log_ptj1"]

tty_prod= True 
tty_total= False 

measure_pvalue = False 
measure_chi2 = True 

#if tty_prod:  abs_norm = ["v30","v32"]
#if tty_total:  abs_norm = ["v31","v33"]
if tty_prod:  abs_norm = ["v30","v32_bin1_as_normbin"]
if tty_total:  abs_norm = ["v31","v33_bin1_as_normbin"]
# only pt_distributions with 2nd last bin as norm bin
#variables = ["log_pt","log_ptj1"]
#if tty_prod:  abs_norm = ["v30","v32_bin1_as_normbin_v1"]
#if tty_total:  abs_norm = ["v31","v33_bin1_as_normbin_v1"]



print("---------------------------------------------------------------------------\n")
print("Variable |\t abs P8 |\t abs H7 |\t norm P8 |\t norm H7 | \n")
print("---------------------------------------------------------------------------\n")
for var in variables:
  results = []
  for region in abs_norm:
    filename = "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/{}/syst-all-fit-data-mu-blinded/{}.txt".format(region,var)
    with open(filename, 'r') as file:
        lines = file.readlines()
    
    # List to store extracted values
    results_tmp = []
    
    # Iterate over the lines with index
    for i, line in enumerate(lines):
        clean_line = line.replace("\x1b[0m", "")
        # If the line contains the search string
        if "INFO::TRExFit::PlotUnfold: MC" in line:
            # Extract chi2, ndf, and prob values from the next three lines
            name = (clean_line.split(":")[6].split("^")[0]).strip()
            chi2 = round(float((lines[i + 1].replace("\x1b[0m", "").split(":")[6].split("^")[0]).strip()),1)
            ndf = int((lines[i + 2].replace("\x1b[0m", "").split(":")[6].split("^")[0]).strip())
            prob = round(float((lines[i + 3].replace("\x1b[0m", "").split(":")[6].split("^")[0]).strip()),2)

            
            # Store the extracted values as a tuple in the results list
            #results.append((name, chi2, ndf, prob))
            if measure_pvalue:  results_tmp.append((var, name, prob))
            if measure_chi2:  results_tmp.append((var, name, chi2,ndf))
    results.append(results_tmp)
    

# Print the results
  #print("{}".format(len(results)))
  if tty_prod and measure_pvalue:  print("{} |\t {} |\t {} |\t {} |\t {} | \n".format(var, ((results[0])[0])[2], ((results[0])[1])[2],((results[1])[0])[2], ((results[1])[1])[2]))
  if tty_total and measure_pvalue:  print("{} |\t {} |\t {} |\t {} |\t {} | \n".format(var, ((results[0])[2])[2], ((results[0])[3])[2],((results[1])[2])[2], ((results[1])[3])[2]))
  if tty_prod and measure_chi2:  print("{0} |\t {1}/{2} |\t {3}/{4} |\t {5}/{6} |\t {7}/{8} | \n".format(var, ((results[0])[0])[2], ((results[0])[0])[3],((results[0])[1])[2],((results[0])[1])[3],((results[1])[0])[2],((results[1])[0])[3] ,((results[1])[1])[2],((results[1])[1])[3]))
  if tty_total and measure_chi2:  print("{0} |\t {1}/{2} |\t {3}/{4} |\t {5}/{6} |\t {7}/{8} | \n".format(var, ((results[0])[2])[2], ((results[0])[2])[3],((results[0])[3])[2],((results[0])[3])[3],((results[1])[2])[2],((results[1])[2])[3] ,((results[1])[3])[2],((results[1])[3])[3]))
#for i, res in enumerate(results):
      #print(f"Occurrence {res[0]}")
      #print(f"  chi2: {res[1]}")
      #print(f"  ndf: {res[2]}")
      #print(f"  prob: {res[3]}")
