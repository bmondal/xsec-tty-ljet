rm tty1l_pt_all_syst
rm tty1l_ptj1_all_syst
rm tty1l_eta_all_syst
rm tty1l_dr_all_syst
rm tty1l_drlj_all_syst
rm tty1l_drphb_all_syst

#ln -s ../../../abs-xsec-with-trexfiles-local/v12/v20/syst-all/tty1l_pt_all_syst/ tty1l_pt_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v22/syst-all/tty1l_pt_all_syst/ tty1l_pt_all_syst # pt is different path
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v22/syst-all/tty1l_ptj1_all_syst/ tty1l_ptj1_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v22/syst-all/tty1l_eta_all_syst/ tty1l_eta_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v22/syst-all/tty1l_dr_all_syst/ tty1l_dr_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v22/syst-all/tty1l_drlj_all_syst/ tty1l_drlj_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v22/syst-all/tty1l_drphb_all_syst/ tty1l_drphb_all_syst

# copy configs
cp ../../../abs-xsec-with-trexfiles-local/v12/v22/syst-all/tty1l*.config .
#cp ../../../abs-xsec-with-trexfiles-local/v12/v_ljet_finer_binning_1/v1/syst-all-fit-data-mu-blinded/tty1l*.config .
