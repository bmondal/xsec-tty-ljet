#!/usr/bin/env python3
from get_norm_all_templates import *


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Loop over all the samples and systematic variation; for every sample find all the histogram templates; get the normalization of those histograms and write to a txt file. To check consistent behaviour between different version of input histograms for fitting")
    parser.add_argument("--folder_path", type=str, help="Input folder; example for dilepton, /.../Unfolding_inputs/NN06/CR_sig")
    parser.add_argument("--output_file", type=str, help="output txt file containing all the normalization values")
    parser.add_argument("--check_binning", type=str, help="check only binning. ex: 'Fine_Binning_1'")
    args = parser.parse_args()
    folder_path = args.folder_path
    output_file = args.output_file 
    binning_name = args.check_binning
    results = process_folder(folder_path,binning_name)
    write_results(results, output_file)

