python write_norm_to_file_zero_intergral_ttgamma_only.py --folder_path ~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/tty_CR/ --output_file corrupted_folders_tty_cr.txt --check_binning ''

python write_norm_to_file_zero_intergral_ttgamma_only.py --folder_path ~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/tty_dec_CR/ --output_file corrupted_folders_tty_dec_cr.txt --check_binning ''

python write_norm_to_file_zero_intergral_ttgamma_only.py --folder_path ~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/fakes_CR/ --output_file corrupted_folders_fakes_cr.txt --check_binning ''

python write_norm_to_file_zero_intergral_ttgamma_only.py --folder_path ~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/other_photons_CR/ --output_file corrupted_folders_other_photons_cr.txt --check_binning ''

