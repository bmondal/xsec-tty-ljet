#!/usr/bin/env python3
import argparse
import os
import ROOT


def get_integral(file, histogram):
    hist = file.Get(histogram)
    if not hist:
        raise Exception(f"Histogram '{histogram}' not found in file")
    if not isinstance(hist, ROOT.TH1):
        raise Exception(f"Object '{histogram}' is not a histogram")
    return hist.Integral() + hist.GetBinContent(hist.GetNbinsX() + 1)

def process_file(file):
    integrals = {}
    integrals["Reco/hist_reco_ph_eta_full_weighted"] = get_integral(file, "Reco/hist_reco_ph_eta_full_weighted")
    if ".ttgamma_prod." in file.GetName() or ".tty_prod_" in file.GetName() or ".ttgamma_prod_" in file.GetName() or ".ttgamma_dec." in file.GetName() or ".ttgamma_dec_" in file.GetName() or ".tty_dec_" in file.GetName():
      integrals["particle/hist_part_ph_eta_full_weighted"] = get_integral(file, "particle/hist_part_ph_eta_full_weighted")
      integrals["h2_ph_pt_reco_part_full_weighted"] = get_integral(file, "h2_ph_pt_reco_part_full_weighted")
    return integrals

def find_corrupted_migration_matrix(folder_path,binning_name):
    corrupted_files = []
    corrupted_migration_matrix = []
    for root, dirs, files in os.walk(folder_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            if file_path.endswith(".root") and binning_name in file_path:
              if not(".ttgamma_prod." in file_name or ".tty_prod_" in file_name or ".ttgamma_prod_" in file_name or ".ttgamma_dec." in file_name or ".ttgamma_dec_" in file_name or ".tty_dec_" in file_name):  continue
              file = None
              try:
                  file = ROOT.TFile.Open(file_path)
                  if file is None:
                      raise Exception("Failed to open file")
                  subfolder = os.path.relpath(root, folder_path)
                  integrals = process_file(file)
                  for hist_name, integral in integrals.items():
                    try:
                      if (hist_name=="h2_ph_pt_reco_part_full_weighted" and integral==0.0):
                        corrupted_migration_matrix.append("{}\t {}".format(os.path.basename(root),file_name))
                    except Exception as e:
                      print(f"error fetching migration matrix for file {file_path}")
                      corrupted_migration_matrix.append("{}\t {}".format(os.path.basename(root),file_name))
              except Exception as e:
                  print(f"Error processing file {file_path}: {str(e)}")
                  corrupted_files.append(file_path)
              finally:
                  if file is not None:
                      file.Close()
    # corrupted migration matrices
    print(">>> corrupted files : \n")
    for item in corrupted_files:
        print("{}\n".format(item))
    return corrupted_migration_matrix


def process_folder(folder_path,binning_name):
    results = []
    corrupted_files = []
    for root, dirs, files in os.walk(folder_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            if file_path.endswith(".root") and binning_name in file_path:
                if not ("ttgamma_prod" in file_name): continue #DANGER remve this
                file = None
                try:
                    file = ROOT.TFile.Open(file_path)
                    if file is None:
                        raise Exception("Failed to open file")
                    subfolder = os.path.relpath(root, folder_path)
                    integrals = process_file(file)
                    for hist_name, integral in integrals.items():
                        result = (subfolder, file_name, hist_name, integral)
                        results.append(result)
                except Exception as e:
                    print(f"Error processing file {file_path}: {str(e)}")
                    corrupted_files.append(file_path)
                finally:
                    if file is not None:
                        file.Close()
    # corrupted migration matrices
    print(">>> corrupted files : \n")
    for item in corrupted_files:
        print("{}\n".format(item))
    return results

def process_folder_return_dict(folder_path, binning_name):
    if not isinstance(binning_name, str):
        raise ValueError("binning_name should be a string")
    results = {}
    corrupted_files = []
    for root, dirs, files in os.walk(folder_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            if file_path.endswith(".root") and binning_name in file_path:
                file = None
                try:
                    file = ROOT.TFile.Open(file_path)
                    if file is None:
                        raise Exception("Failed to open file")
                    subfolder = os.path.relpath(root, folder_path)
                    integrals = process_file(file)
                    for hist_name, integral in integrals.items():
                        key = (subfolder, file_name, hist_name)
                        results[key] = integral
                except Exception as e:
                    print(f"Error processing file {file_path}: {str(e)}")
                    corrupted_files.append(file_path)
                finally:
                    if file is not None:
                        file.Close()
    print(">>> corrupted files : \n")
    for item in corrupted_files:
        print("{}\n".format(item))
    return results


def write_results(results, corrupted_migration_matrices, output_file):
    with open(output_file, "w") as f:
        f.write("subfolder,\t file,\t hist,\t integral\n")
        for result in results:
            f.write(",\t ".join(str(x) for x in result) + "\n")


def write_results_zero_integral(results, output_file):
    with open(output_file, "w") as f:
        f.write("subfolder,\t file,\t hist,\t integral\n")
        for result in results:
            #if not "ttgamma_prod" in result[1] or not "ttgamma_dec" in result[1]: continue
            print(result[3])
            if not(result[3] == 0.0): continue
            f.write(",\t ".join(str(x) for x in result) + "\n")

