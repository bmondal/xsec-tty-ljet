#!/usr/bin/env python3
from get_norm_all_templates import *
maximum_diff_allowed = 2.0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="check two different unfolding inputs whether the normalization of all the templates match or not") 
    parser.add_argument("--path1", type=str, help="path1, example: /.../Unfolding_inputs_v12_v10/NN06/CR_sig")
    parser.add_argument("--path2", type=str, help="path2, example: /.../Unfolding_inputs_v12_v11/NN06/CR_sig")
    parser.add_argument("--check_binning", type=str, help="check only binning. ex: 'Fine_Binning_1'")
    #parser.add_arguments("--outfilename", type=str, ".txt output file name")
    args = parser.parse_args()

    folder_path1 = args.path1
    folder_path2 = args.path2 
    binning_name = args.check_binning
    #output_file = args.outfilename
    results1 = process_folder_return_dict(folder_path1,binning_name)
    results2 = process_folder_return_dict(folder_path2,binning_name)
    for key, value1 in results1.items():
        value2 = results2.get(key, None)
        diff_val1_val2_percentage = 0.0
        if (value1 != 0) and value2:
          diff_val1_val2_percentage = abs(value1 - value2)*100/value1
        if value2 is None:
            print(f"{key} not found in folder 2")
        elif (diff_val1_val2_percentage > maximum_diff_allowed) and (value1 != 0):
            print(f"{key} \t differs between folders, value1: {value1}, \t value2: {value2}, \t rel_diff_value_1: {diff_val1_val2_percentage}")
    for key, value2 in results2.items():
        if key not in results1:
            print(f"{key} not found in folder 1")
