#def extract_data_from_file(filename):
#    with open(filename, 'r') as file:
#        lines = file.readlines()
#
#    # Split data based on headers
#    yields_index = lines.index("Yields in bins for unfolded data\n") + 2
#    correlations_index = lines.index(" Correlations \n") + 1
#
#    # Extract yields and uncertainties
#    yields_data = lines[yields_index:correlations_index - 2]
#    yields = [float(line.split()[1]) for line in yields_data]
#    up_uncertainties = [float(line.split()[2]) for line in yields_data]
#    down_uncertainties = [float(line.split()[3]) for line in yields_data]
#
#    return yields, up_uncertainties, down_uncertainties

def extract_data_from_file(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    # Split data based on headers
    yields_index = lines.index("Yields in bins for unfolded data\n") + 2
    correlations_index = lines.index(" Correlations \n") + 1

    # Extract yields and uncertainties
    yields_data = lines[yields_index:correlations_index - 2]

    # Let's add the troubleshooting step here
    for line in yields_data:
        try:
            split_line = line.split()
            _ = split_line[1]
        except IndexError:
            print(f"Problematic line: {line}")

    yields = [float(line.split()[1]) for line in yields_data]
    up_uncertainties = [float(line.split()[2]) for line in yields_data]
    down_uncertainties = [float(line.split()[3]) for line in yields_data]

    return yields, up_uncertainties, down_uncertainties


def calculate_inclusive_xsec(yields, bin_boundaries):
    bin_widths = [bin_boundaries[i+1] - bin_boundaries[i] for i in range(len(bin_boundaries)-1)]
    inclusive_xsec = sum([yields[i] * bin_widths[i] for i in range(len(yields))])

    return inclusive_xsec

def calculate_total_uncertainty(up_uncertainties, down_uncertainties, bin_boundaries):
    bin_widths = [bin_boundaries[i+1] - bin_boundaries[i] for i in range(len(bin_boundaries)-1)]
    total_up_uncertainty = sum([(up_uncertainties[i] * bin_widths[i])**2 for i in range(len(up_uncertainties))])**0.5
    total_down_uncertainty = sum([(down_uncertainties[i] * bin_widths[i])**2 for i in range(len(down_uncertainties))])**0.5

    return total_up_uncertainty, total_down_uncertainty

if __name__ == "__main__":

    filename ="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v_ljet_dilep_comb/v2/syst-all-fit-data-mu-blinded/tty_ljets_dilep_pt_all_syst_using_multifit/Fits/tty_pt_UnfoldedResults.txt"  
    bin_boundaries = [20, 40, 60, 75, 95, 115, 135, 160, 200, 260, 500]

    yields, up_uncertainties, down_uncertainties = extract_data_from_file(filename)
    
    inclusive_xsec = calculate_inclusive_xsec(yields, bin_boundaries)
    total_up_uncertainty, total_down_uncertainty = calculate_total_uncertainty(up_uncertainties, down_uncertainties, bin_boundaries)

    print(f"Inclusive Cross-section: {inclusive_xsec:.2f}")
    print(f"Total Up Uncertainty: +{total_up_uncertainty:.2f}")
    print(f"Total Down Uncertainty: -{total_down_uncertainty:.2f}")

