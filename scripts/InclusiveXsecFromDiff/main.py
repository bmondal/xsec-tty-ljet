import os

debug = False

def extract_data_from_file(filename, bin_count):
    if not os.path.exists(filename):
      print(f"Error: File '{filename}' does not exist.")
      sys.exit(1)
    with open(filename, 'r') as file:
        lines = file.readlines()

    # Extract yield values and uncertainties based on bin_count
    yields = [list(map(float, line.split()[1:])) for line in lines[2:2+bin_count]]

    # Extract correlations
    correlations = [list(map(float, line.split())) for line in lines[15:15+bin_count]]

    return yields, correlations


def calculate_inclusive_xsec(yields, bin_boundaries):
    # Calculate bin widths
    bin_widths = [bin_boundaries[i+1] - bin_boundaries[i] for i in range(len(bin_boundaries)-1)]
    if debug: print(f"bin widths {bin_widths}\n")

    # Multiply differential cross section with bin widths
    yields_multiplied = [value[0] * bin_widths[i] for i, value in enumerate(yields)]
    if debug: print(f"yields after multiplying with bin widths: {yields_multiplied} \n")

    # Calculate inclusive cross section
    inclusive_xsec = sum(yields_multiplied)

    # Calculate uncertainty on inclusive cross section
    up_uncertainties = [bin_widths[i]*row[0]+bin_widths[i]*row[1] for i,row in enumerate(yields)]
    down_uncertainties = [bin_widths[i]*row[0]+bin_widths[i]*row[2] for i,row in enumerate(yields)]

    if debug: print(f"up uncertainties {up_uncertainties} \n")
    if debug: print(f"down uncertainties {down_uncertainties} \n")

    # Calculate uncertainty for each bin, using the average of the up and down uncertainties.
    uncertainties = [(up_uncertainties[i] - abs(down_uncertainties[i])) / 2 for i in range(len(up_uncertainties))]
    if debug: print(f"uncertainties for each bin: {uncertainties} \n")

    # Quadrature sum of uncertainties
    inclusive_xsec_uncertainty = (sum(uncertainty**2 for uncertainty in uncertainties))**0.5

    return inclusive_xsec, inclusive_xsec_uncertainty

if __name__ == "__main__":
    tty_prod_SLDL="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v_ljet_dilep_comb/v2/"
    filename_tty_prod_pt_SLDL="{}/syst-all-fit-data-mu-blinded/tty_ljets_dilep_pt_all_syst_using_multifit/Fits/tty_pt_UnfoldedResults.txt"  
    filename_tty_prod_eta_SLDL="{}/syst-all-fit-data-mu-blinded/tty_ljets_dilep_eta_all_syst_using_multifit/Fits/tty_eta_UnfoldedResults.txt"  
    filename_tty_prod_pt_SL_SLDL="{}/syst-all-fit-data-mu-blinded/tty1l_pt_all_syst/Fits/tty_pt_UnfoldedResults.txt"  
    filename_tty_prod_pt_DL_SLDL="{}/syst-all-fit-data-mu-blinded/tty2l_pt_all_syst/Fits/tty_pt_UnfoldedResults.txt"  
    
    tty_prod_SL="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v30"
    filename_tty_prod_pt_SL ="{}/syst-all-fit-data-mu-blinded/tty1l_pt_all_syst/Fits/tty_pt_UnfoldedResults.txt"  
    filename_tty_prod_eta_SL ="{}/syst-all-fit-data-mu-blinded/tty1l_eta_all_syst/Fits/tty_eta_UnfoldedResults.txt"  
    filename_tty_prod_dr_SL ="{}/syst-all-fit-data-mu-blinded/tty1l_dr_all_syst/Fits/tty_drphl_UnfoldedResults.txt"  
    filename_tty_prod_drphb_SL ="{}/syst-all-fit-data-mu-blinded/tty1l_drphb_all_syst/Fits/tty_drphb_UnfoldedResults.txt"  
    filename_tty_prod_drlj_SL ="{}/syst-all-fit-data-mu-blinded/tty1l_drlj_all_syst/Fits/tty_drlj_UnfoldedResults.txt"  
    filename_tty_prod_ptj1_SL ="{}/syst-all-fit-data-mu-blinded/tty1l_ptj1_all_syst/Fits/tty_ptj1_UnfoldedResults.txt"  

    tty_prod_DL="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-dilep/abs-xsec-with-trexfiles-local/v12/v34/"
    filename_tty_prod_pt_DL="{}/syst-all-fit-data-mu-blinded/tty2l_pt_all_syst/Fits/tty_pt_UnfoldedResults.txt"
    filename_tty_prod_eta_DL="{}/syst-all-fit-data-mu-blinded/tty2l_eta_all_syst/Fits/tty_eta_UnfoldedResults.txt"
    filename_tty_prod_dr_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dr_all_syst/Fits/tty_min_drphl_UnfoldedResults.txt"
    filename_tty_prod_dr1_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dr1_all_syst/Fits/tty_drphl1_UnfoldedResults.txt"
    filename_tty_prod_dr2_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dr2_all_syst/Fits/tty_drphl2_UnfoldedResults.txt"
    filename_tty_prod_drphb_DL="{}/syst-all-fit-data-mu-blinded/tty2l_drphb_all_syst/Fits/tty_drphb_UnfoldedResults.txt"
    filename_tty_prod_drlj_DL="{}/syst-all-fit-data-mu-blinded/tty2l_drlj_all_syst/Fits/tty_drlj_UnfoldedResults.txt"
    filename_tty_prod_ptj1_DL="{}/syst-all-fit-data-mu-blinded/tty2l_ptj1_all_syst/Fits/tty_ptj1_UnfoldedResults.txt"
    filename_tty_prod_ptll_DL="{}/syst-all-fit-data-mu-blinded/tty2l_ptll_all_syst/Fits/tty_ptll_UnfoldedResults.txt"
    filename_tty_prod_dEtall_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dEtall_all_syst/Fits/tty_dEtall_UnfoldedResults.txt"
    filename_tty_prod_dPhill_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dPhill_all_syst/Fits/tty_dPhill_UnfoldedResults.txt"

    tty_total_SL="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v31"
    filename_tty_total_pt_SL="{}/syst-all-fit-data-mu-blinded/tty1l_pt_all_syst/Fits/tty_pt_UnfoldedResults.txt"
    filename_tty_total_eta_SL="{}/syst-all-fit-data-mu-blinded/tty1l_eta_all_syst/Fits/tty_eta_UnfoldedResults.txt"
    filename_tty_total_drphb_SL="{}/syst-all-fit-data-mu-blinded/tty1l_drphb_all_syst/Fits/tty_drphb_UnfoldedResults.txt"
    filename_tty_total_drlj_SL="{}/syst-all-fit-data-mu-blinded/tty1l_drlj_all_syst/Fits/tty_drlj_UnfoldedResults.txt"
    filename_tty_total_dr_SL="{}/syst-all-fit-data-mu-blinded/tty1l_dr_all_syst/Fits/tty_drphl_UnfoldedResults.txt"
    filename_tty_total_ptj1_SL="{}/syst-all-fit-data-mu-blinded/tty1l_ptj1_all_syst/Fits/tty_ptj1_UnfoldedResults.txt"

    tty_total_DL="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-dilep/abs-xsec-with-trexfiles-local/v12/v35/"
    filename_tty_total_pt_DL="{}/syst-all-fit-data-mu-blinded/tty2l_pt_all_syst/Fits/tty2l_pt_all_syst_UnfoldedResults.txt"
    filename_tty_total_eta_DL="{}/syst-all-fit-data-mu-blinded/tty2l_eta_all_syst/Fits/tty2l_eta_all_syst_UnfoldedResults.txt"
    filename_tty_total_dr_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dr_all_syst/Fits/tty2l_dr_all_syst_UnfoldedResults.txt"
    filename_tty_total_dr1_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dr1_all_syst/Fits/tty2l_dr1_all_syst_UnfoldedResults.txt"
    filename_tty_total_dr2_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dr2_all_syst/Fits/tty2l_dr2_all_syst_UnfoldedResults.txt"
    filename_tty_total_drphb_DL="{}/syst-all-fit-data-mu-blinded/tty2l_drphb_all_syst/Fits/tty2l_drphb_all_syst_UnfoldedResults.txt"
    filename_tty_total_drlj_DL="{}/syst-all-fit-data-mu-blinded/tty2l_drlj_all_syst/Fits/tty2l_drlj_all_syst_UnfoldedResults.txt"
    filename_tty_total_ptj1_DL="{}/syst-all-fit-data-mu-blinded/tty2l_ptj1_all_syst/Fits/tty2l_ptj1_all_syst_UnfoldedResults.txt"
    filename_tty_total_ptll_DL="{}/syst-all-fit-data-mu-blinded/tty2l_ptll_all_syst/Fits/tty2l_ptll_all_syst_UnfoldedResults.txt"
    filename_tty_total_dEtall_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dEtall_all_syst/Fits/tty2l_dEtall_all_syst_UnfoldedResults.txt"
    filename_tty_total_dPhill_DL="{}/syst-all-fit-data-mu-blinded/tty2l_dPhill_all_syst/Fits/tty2l_dPhill_all_syst_UnfoldedResults.txt"


    bin_boundaries_tty_prod_pt_SLDL = [20, 40, 60, 75, 95, 115, 135, 160, 200, 260, 500]
    bin_boundaries_tty_prod_eta_SL = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.37]

    bin_boundaries_tty_prod_dr_SL = [0.4,0.8,1.2,1.6,2.0,2.4,2.8,3.4]
    bin_boundaries_tty_prod_drphb_SL = [0.4,0.8,1.3,1.9,2.6,3.4]
    bin_boundaries_tty_prod_drlj_SL = [0.4,0.8,1.3,1.9,2.6,3.4]
    bin_boundaries_tty_prod_ptj1_SL = [25,80,140,200,300,450]

    bin_boundaries_tty_prod_pt_DL = [20, 35, 50, 70, 130, 200, 500]
    bin_boundaries_tty_prod_dEtall_DL = [0.0,0.5,1.0,1.5,2.0,2.5,3.0,4.5]
    bin_boundaries_tty_prod_dPhill_DL = [0.0, 0.4,0.8,1.2,1.6,2.0,2.4,2.8,3.2]
    bin_boundaries_tty_prod_ptll_DL = [0,30,60,90,130,210,420]


    dict_variable_bin_boundaries = {
      filename_tty_prod_pt_SLDL:bin_boundaries_tty_prod_pt_SLDL, 
      filename_tty_prod_eta_SLDL:bin_boundaries_tty_prod_eta_SL, 
      filename_tty_prod_pt_SL: bin_boundaries_tty_prod_pt_SLDL,
      filename_tty_prod_eta_SL: bin_boundaries_tty_prod_eta_SL,
      filename_tty_prod_dr_SL: bin_boundaries_tty_prod_dr_SL,
      filename_tty_prod_drphb_SL: bin_boundaries_tty_prod_drphb_SL,
      filename_tty_prod_drlj_SL: bin_boundaries_tty_prod_drlj_SL,
      filename_tty_prod_ptj1_SL: bin_boundaries_tty_prod_ptj1_SL,

      filename_tty_prod_pt_DL:bin_boundaries_tty_prod_pt_DL,
      filename_tty_prod_eta_DL:bin_boundaries_tty_prod_eta_SL,
      filename_tty_prod_dr_DL:bin_boundaries_tty_prod_dr_SL,
      filename_tty_prod_dr1_DL:bin_boundaries_tty_prod_dr_SL,
      filename_tty_prod_dr2_DL:bin_boundaries_tty_prod_dr_SL,
      filename_tty_prod_drphb_DL:bin_boundaries_tty_prod_drphb_SL,
      filename_tty_prod_drlj_DL:bin_boundaries_tty_prod_drlj_SL,
      filename_tty_prod_ptj1_DL:bin_boundaries_tty_prod_ptj1_SL,
      filename_tty_prod_ptll_DL:bin_boundaries_tty_prod_ptll_DL,
      filename_tty_prod_dEtall_DL:bin_boundaries_tty_prod_dEtall_DL,
      filename_tty_prod_dPhill_DL:bin_boundaries_tty_prod_dPhill_DL,

      filename_tty_total_pt_SL:bin_boundaries_tty_prod_pt_SLDL,
      filename_tty_total_eta_SL:bin_boundaries_tty_prod_eta_SL, 
      filename_tty_total_drphb_SL:bin_boundaries_tty_prod_drphb_SL, 
      filename_tty_total_drlj_SL:bin_boundaries_tty_prod_drlj_SL, 
      filename_tty_total_dr_SL:bin_boundaries_tty_prod_dr_SL, 
      filename_tty_total_ptj1_SL:bin_boundaries_tty_prod_ptj1_SL, 

      filename_tty_total_pt_DL:bin_boundaries_tty_prod_pt_DL,
      filename_tty_total_eta_DL:bin_boundaries_tty_prod_eta_SL,
      filename_tty_total_dr_DL:bin_boundaries_tty_prod_dr_SL,
      filename_tty_total_dr1_DL:bin_boundaries_tty_prod_dr_SL,
      filename_tty_total_dr2_DL:bin_boundaries_tty_prod_dr_SL,
      filename_tty_total_drphb_DL:bin_boundaries_tty_prod_drphb_SL,
      filename_tty_total_drlj_DL:bin_boundaries_tty_prod_drlj_SL,
      filename_tty_total_ptj1_DL:bin_boundaries_tty_prod_ptj1_SL,
      filename_tty_total_ptll_DL:bin_boundaries_tty_prod_ptll_DL,
      filename_tty_total_dEtall_DL:bin_boundaries_tty_prod_dEtall_DL,
      filename_tty_total_dPhill_DL:bin_boundaries_tty_prod_dPhill_DL,

    }

    key_names= [
      "tty_prod_pt_SLDL",
      "tty_prod_eta_SLDL",
      "tty_prod_pt_SL",
      "tty_prod_eta_SL",
      "tty_prod_dr_SL",
      "tty_prod_drphb_SL",
      "tty_prod_drlj_SL",
      "tty_prod_ptj1_SL",
      "tty_prod_pt_DL",
      "tty_prod_eta_DL",
      "tty_prod_dr_DL",
      "tty_prod_dr1_DL",
      "tty_prod_dr2_DL",
      "tty_prod_drphb_DL",
      "tty_prod_drlj_DL",
      "tty_prod_ptj1_DL",
      "tty_prod_ptll_DL",
      "tty_prod_dEtall_DL",
      "tty_prod_dPhill_DL",
      "tty_total_pt_SL",
      "tty_total_eta_SL",
      "tty_total_drphb_SL",
      "tty_total_drlj_SL",
      "tty_total_dr_SL",
      "tty_total_ptj1_SL",
      "tty_total_pt_DL",
      "tty_total_eta_DL",
      "tty_total_dr_DL",
      "tty_total_dr1_DL",
      "tty_total_dr2_DL",
      "tty_total_drphb_DL",
      "tty_total_drlj_DL",
      "tty_total_ptj1_DL",
      "tty_total_ptll_DL",
      "tty_total_dEtall_DL",
      "tty_total_dPhill_DL",
    ]

    if debug: print("length of dictionary: {} \t length of key_names: {} \n".format(len(dict_variable_bin_boundaries), len(key_names)))

    for idx, filename in enumerate(dict_variable_bin_boundaries):
      print("\n")
      print(f"==================================")
      print(f"{key_names[idx]}")
      print(f"==================================\n")
      if debug: print(f"Processing filename: {filename}")
      path = filename

      # Extract the version number using string splitting
      version = path.split("/")[-5]

      # Extract the last part of the path (the filename)
      pathname= os.path.basename(path)
      
      
      yields, correlations = extract_data_from_file(filename, len(dict_variable_bin_boundaries[filename])-1)
      
      inclusive_xsec, inclusive_xsec_uncertainty = calculate_inclusive_xsec(yields, dict_variable_bin_boundaries[filename])
      
      print(f"Inclusive xsec: {inclusive_xsec:.4f} ± {inclusive_xsec_uncertainty:.4f} \t Bin count {len(dict_variable_bin_boundaries[filename])-1}")
      if debug: print(f"Inclusive xsec: {inclusive_xsec:.4f} ± {inclusive_xsec_uncertainty:.4f} \t Bin count {len(dict_variable_bin_boundaries[filename])-1} \t filename: {pathname} \t version: {version}")
