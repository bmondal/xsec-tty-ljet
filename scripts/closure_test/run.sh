DISPLAY=0
#path="../../abs-xsec-with-trexfiles-local/v12/v15/stat-all//"
path="/home/bm863639/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v30/stat-all/"
path_truth="/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/tty_CR/nominal_9999/"
python plot_unfolded_truth.py --path-trex-fit $path/tty1l_pt_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty1l_eta_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty1l_dr_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty1l_drphb_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty1l_drlj_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty1l_ptj1_all_stat/ --path-truth  $path_truth
