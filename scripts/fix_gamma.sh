folders=(
  "tty2l_drphb_all_syst"
  "tty2l_ptj1_all_syst"
  "tty2l_pt_all_syst"
  "tty2l_drlj_all_syst"
  "tty2l_ptll_all_syst"
  "tty2l_dEtall_all_syst"
  "tty2l_dr2_all_syst"
  "tty2l_dPhill_all_syst"
  "tty2l_dr1_all_syst"
  "tty2l_dr_all_syst"
  "tty2l_eta_all_syst"
)

for item in "${folders[@]}"
do
  echo "$item"
  pushd "$item/Fits/"
  sed -i 's/t#bar{t}#gamma (dec.) modelling/"&"/g' Group*.txt
  sed -i 's/t#bar{t}#gamma (prod.) modelling/"&"/g' Group*.txt
  sed -i 's/Fake photon/"&"/g' Group*.txt
  sed -i 's/Other background modelling/"&"/g' Group*.txt
  sed -i 's/Other experimental uncertainties/"&"/g' Group*.txt
  for file in GroupedImpact_tty_*_Bin_00*.txt
  do
    echo "$file"
    sed -i '/gamma_tty/d' "$file"
  done
  popd
done

config=(
  "tty2l_eta_all_syst.config"
  "tty2l_ptj1_all_syst.config"
  "tty2l_drlj_all_syst.config"
  "tty2l_ptll_all_syst.config"
  "tty2l_pt_all_syst.config"
  "tty2l_dr1_all_syst.config"
  "tty2l_drphb_all_syst.config"
  "tty2l_dr2_all_syst.config"
  "tty2l_dEtall_all_syst.config"
  "tty2l_dPhill_all_syst.config"
  "tty2l_dr_all_syst.config"
)

setup_trex
for item in "${config[@]}"
do
  trex-fitter i $item "GroupedImpact=combine"
done
