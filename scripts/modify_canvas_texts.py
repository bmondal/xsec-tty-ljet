import ROOT

root_file="tty1l_pt_all_syst/NuisPar_Instrumental.root"
# Open the ROOT file
f = ROOT.TFile.Open(root_file)

# Get the canvas from the file
canvas = f.Get("c")

# Get the list of objects in the canvas
list_objects = canvas.GetListOfPrimitives()

# Iterate over the objects in the list
for i in range(list_objects.GetSize()):
    obj = list_objects.At(i)
    # Check if the object is a TText (which is used for axis labels and titles)
    if obj.InheritsFrom("TText"):
        # Check if the text matches "EG RESOLUTION ALL"
        if obj.GetTitle() == "EG RESOLUTION ALL":
            # Change the text to "EG res all"
            obj.SetTitle("EG res all")

# Save the canvas as a PDF
canvas.SaveAs("tmp.pdf")

# Close the file
f.Close()

