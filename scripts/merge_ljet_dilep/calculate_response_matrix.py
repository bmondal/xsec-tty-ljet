#!/usr/bin/env python3
import constants # for the constant variables throughout the program
import os
import ROOT
ROOT.gROOT.SetBatch(True)

"""
This function calculates the response matrix from the given 
reco_hist, truth_hist, and migration_hist. The response matrix 
is created by cloning the migration_hist. The number of bins for 
reco_hist, truth_hist, and migration_hist are checked to make 
sure they are equal and not empty. If they are unequal or empty, 
the function will print a message. The response matrix is 
calculated by looping over the bins of reco_hist and truth_hist 
and using the values from migration_hist and the reco_hist 
and truth_hist to set the values of the response matrix. 
The response matrix is returned at the end of the function.
"""
def calculate_response_matrix(reco_hist, truth_hist, migration_hist):
    response_matrix = migration_hist.Clone()
    N_R = reco_hist.GetNbinsX()
    N_T = truth_hist.GetNbinsX()
    N_mig_R = migration_hist.GetNbinsX()
    N_mig_T = migration_hist.GetNbinsY()
    dumTcontent = -1.e6
    rbin_total = 0
    res_value = [[0 for x in range(1000)] for y in range(1000)]
    res_error = [[0 for x in range(1000)] for y in range(1000)]

    if ((N_R != N_T) or N_R == 0):
        print("Unequal/empty truth and/or reco")

    '''response matrix calculation...'''
    for bin_r in range(1, N_R + 1):
        rbin_total = 0.
        for bin_t in range(1, N_T + 1):
            rbin_total += migration_hist.GetBinContent(bin_r, bin_t)
        for bin_t in range(1, N_T + 1):
            dumTcontent = truth_hist.GetBinContent(bin_t)
            if not (rbin_total == 0 or dumTcontent == 0):
                res_value[bin_r - 1][bin_t - 1] = 1. * (migration_hist.GetBinContent(bin_r, bin_t)) * (
                            (reco_hist.GetBinContent(bin_r)) / (dumTcontent * rbin_total))
                res_error[bin_r - 1][bin_t - 1] = 1. * (migration_hist.GetBinError(bin_r, bin_t)) * (
                            (reco_hist.GetBinContent(bin_r)) / (dumTcontent * rbin_total))
            response_matrix.SetBinContent(bin_r, bin_t, res_value[bin_r - 1][bin_t - 1])
            response_matrix.SetBinError(bin_r, bin_t, res_error[bin_r - 1][bin_t - 1])

    return response_matrix


def calculate_and_save_response_matrices(filename):
    print(">>>>>>>>>> starting to update response matrix for {} <<<<<<<<< \n".format(filename))
    # Open the original ROOT file
    f = ROOT.TFile(filename, "UPDATE")
    # ph_pt
    reco_hist_pt = f.Get("Reco/hist_reco_ph_pt_full_weighted")
    truth_hist_pt = f.Get("particle/hist_part_ph_pt_full_weighted")
    migration_hist_pt = f.Get("h2_ph_pt_reco_part_full_weighted")
    response_matrix_pt = calculate_response_matrix(reco_hist_pt, truth_hist_pt, migration_hist_pt)
    response_matrix_pt.SetName("h2_response_matrix_ph_pt")
    response_matrix_pt.Write("h2_response_matrix_ph_pt", ROOT.TObject.kOverwrite)

    # ph_eta
    reco_hist_eta = f.Get("Reco/hist_reco_ph_eta_full_weighted")
    truth_hist_eta = f.Get("particle/hist_part_ph_eta_full_weighted")
    migration_hist_eta = f.Get("h2_ph_eta_reco_part_full_weighted")
    response_matrix_eta = calculate_response_matrix(reco_hist_eta, truth_hist_eta,
                                                    migration_hist_eta)
    response_matrix_eta.SetName("h2_response_matrix_ph_eta")
    response_matrix_eta.Write("h2_response_matrix_ph_eta", ROOT.TObject.kOverwrite)


    f.Close()
    print(">>>>>>>>>> done creating response matrices <<<<<<<<< \n")



"""
This function calculates and saves response matrices for a given 
optimized binning in the ROOT files in the directory specified by 
top_dir. It checks for files that are ROOT files and contain the 
string "Fine_Binning" in its name. The function then opens the 
original ROOT file, retrieves histograms for the "Reco", "particle", 
and migration variables, and calculates the response matrix for 
both the "ph_pt" and "ph_eta" variables. The calculated response 
matrices are then saved in the same ROOT file, overwriting any 
existing response matrix with the same name.
main_dir meaning /../Unfolding_inputs_v12_v11/NN06/CR_sig
sub_dir meaning /../Unfolding_inputs_v12_v11/NN06/CR_sig//weight_bTagSF_DL1r_85_eigenvars_C_up_9999
"""
def calculate_and_save_response_matrices_over_main_dir(top_dir, optimized_binning):
    """binning: str; Update the files containing this string
    """
    num_files = 0

    for dirpath, dirnames, filenames in os.walk(top_dir):
        for filename in filenames:
            # Check if the file is a ROOT file and contains "Fine_Binning" in its name
            if filename.endswith('.root') and optimized_binning in filename and (".ttgamma_incl" in filename or ".tty_incl_" in filename):
                num_files += 1
    
    print(f"Total number of files to be processed: {num_files}")
    
    processed_files = 0
    
    for dirpath, dirnames, filenames in os.walk(top_dir):
        for filename in filenames:
            # Check if the file is a ROOT file and contains "Fine_Binning" in its name
            if filename.endswith('...root'):  continue #FIXME
            if "lepfakeSF_nominal" in os.path.basename(dirpath) or "lepfakeSF_var" in os.path.basename(dirpath):  continue #FIXME
            if filename.endswith('.root') and optimized_binning in filename and (".ttgamma_incl" in filename or ".tty_incl_" in filename):
                print(f"Starting to process file {os.path.join(dirpath,filename)}")
                calculate_and_save_response_matrices(os.path.join(dirpath,filename))                
                processed_files += 1
                print(f"Processed file {os.path.join(dirpath,filename)}")
                print(f"Processed {processed_files}/{num_files} files")


def calculate_and_save_response_matrices_over_sub_dir(sub_dir, optimized_binning):
    """binning: str; Update the files containing this string
    """
    for dirpath, dirnames, filenames in os.walk(sub_dir):
        for filename in filenames:
            # Check if the file is a ROOT file and contains "Fine_Binning" in its name
            if filename.endswith('.root') and optimized_binning in filename and (".ttgamma_incl" in filename or ".tty_incl_" in filename):
                calculate_and_save_response_matrices(os.path.join(dirpath,filename))                
                print(f"Processed file {os.path.join(dirpath,filename)}")

