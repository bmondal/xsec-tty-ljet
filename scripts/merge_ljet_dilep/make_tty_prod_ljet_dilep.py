#!/usr/bin/env python3

import subprocess
import ROOT
from calculate_response_matrix import *

import os
import threading
import sys

dict_of_tty_prod_sample_in_nominal = [
  "histograms.tty_prod_PDF4LHC_0.root"
  ,"histograms.tty_prod_PDF4LHC_1.root"
  ,"histograms.tty_prod_PDF4LHC_10.root"
  ,"histograms.tty_prod_PDF4LHC_11.root"
  ,"histograms.tty_prod_PDF4LHC_12.root"
  ,"histograms.tty_prod_PDF4LHC_13.root"
  ,"histograms.tty_prod_PDF4LHC_14.root"
  ,"histograms.tty_prod_PDF4LHC_15.root"
  ,"histograms.tty_prod_PDF4LHC_16.root"
  ,"histograms.tty_prod_PDF4LHC_17.root"
  ,"histograms.tty_prod_PDF4LHC_18.root"
  ,"histograms.tty_prod_PDF4LHC_19.root"
  ,"histograms.tty_prod_PDF4LHC_2.root"
  ,"histograms.tty_prod_PDF4LHC_20.root"
  ,"histograms.tty_prod_PDF4LHC_21.root"
  ,"histograms.tty_prod_PDF4LHC_22.root"
  ,"histograms.tty_prod_PDF4LHC_23.root"
  ,"histograms.tty_prod_PDF4LHC_24.root"
  ,"histograms.tty_prod_PDF4LHC_25.root"
  ,"histograms.tty_prod_PDF4LHC_26.root"
  ,"histograms.tty_prod_PDF4LHC_27.root"
  ,"histograms.tty_prod_PDF4LHC_28.root"
  ,"histograms.tty_prod_PDF4LHC_29.root"
  ,"histograms.tty_prod_PDF4LHC_3.root"
  ,"histograms.tty_prod_PDF4LHC_30.root"
  ,"histograms.tty_prod_PDF4LHC_4.root"
  ,"histograms.tty_prod_PDF4LHC_5.root"
  ,"histograms.tty_prod_PDF4LHC_6.root"
  ,"histograms.tty_prod_PDF4LHC_7.root"
  ,"histograms.tty_prod_PDF4LHC_8.root"
  ,"histograms.tty_prod_PDF4LHC_9.root"
  ,"histograms.tty_prod_muF_down.root"
  ,"histograms.tty_prod_muF_up.root"
  ,"histograms.tty_prod_muR_05_muF_2.root"
  ,"histograms.tty_prod_muR_2_muF_05.root"
  ,"histograms.tty_prod_muR_down.root"
  ,"histograms.tty_prod_muR_muF_down.root"
  ,"histograms.tty_prod_muR_muF_up.root"
  ,"histograms.tty_prod_muR_up.root"
  #,"histograms.ttgamma_prod.root" this will be processed before
  ,"histograms.ttgamma_prod_H7.root"
  ,"histograms.ttgamma_prod_uncalib_bjet_x0.5.root"
  ,"histograms.ttgamma_prod_uncalib_cjet_x0.5.root"
  ,"histograms.ttgamma_prod_uncalib_ljet_x0.5.root"
  ,"histograms.ttgamma_prod_uncalib_x0.5.root"
  ,"histograms.ttgamma_prod_uncalib_x1.5.root"
  ,"histograms.ttgamma_prod_var3cDown.root"
  ,"histograms.ttgamma_prod_var3cUp.root"
]



def process_other_folders(path_CR1,path_CR2,path_CR3,path_CR4,path_CR5,path_CR6, list_of_files_processed):
  sub_dirs = [d for d in os.listdir(path_CR1) if os.path.isdir(os.path.join(path_CR1,d)) ]
  total_dirs = len(sub_dirs)
  current_dir = 1

  # loop through each subdirectory
  for sub_dir in sub_dirs:
    # full path of subdirectory
    sub_dir_path_CR1 = os.path.join(path_CR1,sub_dir)
    sub_dir_path_CR2 = os.path.join(path_CR2,sub_dir)
    sub_dir_path_CR3 = os.path.join(path_CR3,sub_dir)
    sub_dir_path_CR4 = os.path.join(path_CR4,sub_dir)
    sub_dir_path_CR5 = os.path.join(path_CR5,sub_dir)
    sub_dir_path_CR6 = os.path.join(path_CR6,sub_dir)

    # open the ttgamma_prod.root and ttgamma_dec.root file
    ttgamma_prod_path_CR1 = os.path.join(sub_dir_path_CR1, "histograms.ttgamma_prod.root")
    ttgamma_prod_path_CR2 = os.path.join(sub_dir_path_CR2, "histograms.ttgamma_prod.root")
    ttgamma_prod_path_CR3 = os.path.join(sub_dir_path_CR3, "histograms.ttgamma_prod.root")
    ttgamma_prod_path_CR4 = os.path.join(sub_dir_path_CR4, "histograms.ttgamma_prod.root")
    ttgamma_prod_path_CR5 = os.path.join(sub_dir_path_CR5, "histograms.ttgamma_prod.root")
    ttgamma_prod_path_CR6 = os.path.join(sub_dir_path_CR6, "histograms.ttgamma_prod.root")
    #ttgamma_dec_path = os.path.join(sub_dir_path, "histograms.ttgamma_dec.{}.root".format(binning_name))

    if(os.path.exists(ttgamma_prod_path_CR1) and os.path.exists(ttgamma_prod_path_CR2) and os.path.exists(ttgamma_prod_path_CR3) and
       os.path.exists(ttgamma_prod_path_CR4) and os.path.exists(ttgamma_prod_path_CR5) and os.path.exists(ttgamma_prod_path_CR6) ):
       print("=== Info:: All the paths exists we are good to go \n")
       print(f"=== Info:: hadding {ttgamma_prod_path_CR1} with the rest \n")
       ttgamma_prod_path_CR1_merged = ttgamma_prod_path_CR1.replace(".ttgamma_prod.",".ttgamma_prod_particle_ljet_dilep.")
       # command to merge the ttgamma_prod and ttgamma_dec files
       cmd = "hadd -v 0 -f {0} {1} {2} {3} {4} {5} {6}".format(ttgamma_prod_path_CR1_merged, ttgamma_prod_path_CR1, ttgamma_prod_path_CR2, ttgamma_prod_path_CR3, 
                                                               ttgamma_prod_path_CR4, ttgamma_prod_path_CR5, ttgamma_prod_path_CR6)
       list_of_files_processed.append(ttgamma_prod_path_CR1_merged)
       subprocess.call(cmd, shell=True)

    elif (not os.path.exists(ttgamma_prod_path_CR1)):
      print(f"=== Error path {ttgamma_prod_path_CR1} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR2)):
      print(f"=== Error path {ttgamma_prod_path_CR2} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR3)):
      print(f"=== Error path {ttgamma_prod_path_CR3} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR4)):
      print(f"=== Error path {ttgamma_prod_path_CR4} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR5)):
      print(f"=== Error path {ttgamma_prod_path_CR5} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR6)):
      print(f"=== Error path {ttgamma_prod_path_CR6} not exists")
      sys.exit()

    # display progress
    print("{}/{} subdirectoreis processed.\n".format(current_dir,total_dirs))
    current_dir += 1

def process_nominal(path_CR1, path_CR2, path_CR3, path_CR4, path_CR5, path_CR6, list_of_files_processed):
  # find the list of the files in nominal folder
  for tty_nominal_var in dict_of_tty_prod_sample_in_nominal:
    # full path of subdirectory
    sub_dir_path_CR1 = os.path.join(path_CR1,"nominal_9999")
    sub_dir_path_CR2 = os.path.join(path_CR2,"nominal_9999")
    sub_dir_path_CR3 = os.path.join(path_CR3,"nominal_9999")
    sub_dir_path_CR4 = os.path.join(path_CR4,"nominal_9999")
    sub_dir_path_CR5 = os.path.join(path_CR5,"nominal_9999")
    sub_dir_path_CR6 = os.path.join(path_CR6,"nominal_9999")

    # open the ttgamma_prod.root and ttgamma_dec.root file
    ttgamma_prod_path_CR1 = os.path.join(sub_dir_path_CR1, "{}".format(tty_nominal_var))
    ttgamma_prod_path_CR2 = os.path.join(sub_dir_path_CR2, "{}".format(tty_nominal_var))
    ttgamma_prod_path_CR3 = os.path.join(sub_dir_path_CR3, "{}".format(tty_nominal_var))
    ttgamma_prod_path_CR4 = os.path.join(sub_dir_path_CR4, "{}".format(tty_nominal_var))
    ttgamma_prod_path_CR5 = os.path.join(sub_dir_path_CR5, "{}".format(tty_nominal_var))
    ttgamma_prod_path_CR6 = os.path.join(sub_dir_path_CR6, "{}".format(tty_nominal_var))
    #ttgamma_dec_path = os.path.join(sub_dir_path, "histograms.ttgamma_dec.{}.root".format(binning_name))

    if(os.path.exists(ttgamma_prod_path_CR1) and os.path.exists(ttgamma_prod_path_CR2) and os.path.exists(ttgamma_prod_path_CR3) and
       os.path.exists(ttgamma_prod_path_CR4) and os.path.exists(ttgamma_prod_path_CR5) and os.path.exists(ttgamma_prod_path_CR6) ):
       print("=== Info:: All the paths exists we are good to go \n")
       print(f"=== Info:: hadding {ttgamma_prod_path_CR1} with the rest \n")
       ttgamma_prod_path_CR1_merged = ttgamma_prod_path_CR1.replace(".ttgamma_prod.",".ttgamma_prod_particle_ljet_dilep.")
       # command to merge the ttgamma_prod and ttgamma_dec files
       cmd = "hadd -v 0 -f {0} {1} {2} {3} {4} {5} {6}".format(ttgamma_prod_path_CR1_merged, ttgamma_prod_path_CR1, ttgamma_prod_path_CR2, ttgamma_prod_path_CR3, 
                                                               ttgamma_prod_path_CR4, ttgamma_prod_path_CR5, ttgamma_prod_path_CR6)
       list_of_files_processed.append(ttgamma_prod_path_CR1_merged)

       subprocess.call(cmd, shell=True)
    elif (not os.path.exists(ttgamma_prod_path_CR1)):
      print(f"=== Error path {ttgamma_prod_path_CR1} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR2)):
      print(f"=== Error path {ttgamma_prod_path_CR2} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR3)):
      print(f"=== Error path {ttgamma_prod_path_CR3} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR4)):
      print(f"=== Error path {ttgamma_prod_path_CR4} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR5)):
      print(f"=== Error path {ttgamma_prod_path_CR5} not exists")
      sys.exit()
    elif (not os.path.exists(ttgamma_prod_path_CR6)):
      print(f"=== Error path {ttgamma_prod_path_CR6} not exists")
      sys.exit()


    # display progress
    print("{}/{} subdirectoreis processed.\n".format(current_dir,total_dirs))
    current_dir += 1



def run(path_to_CR1, path_to_CR2, path_to_CR3,path_to_CR4,path_to_CR5,path_to_CR6, binning_name):
  # hadd ttgamma_prod and ttgamma_dec
  list_of_files_processed = []
  print(" 2 {} binning {}\n".format(path_to_CR1,path_to_CR2,path_to_CR3,path_to_CR4,path_to_CR5,path_to_CR6))
  process_other_folders(path_to_CR1,path_to_CR2,path_to_CR3,path_to_CR4,path_to_CR5,path_to_CR6, list_of_files_processed)
  process_nominal(path_to_CR1,path_to_CR2,path_to_CR3,path_to_CR4,path_to_CR5,path_to_CR6, list_of_files_processed)
  print(list_of_files_processed)


if __name__=="__main__":
  submit_merging_part = True 
  submit_merging_part_nominal_only = False #this includes nominal folder, muR_muF,PDF ...
  submit_response_matrix_part = False 
  calculate_response_matrix_over_folders = False# recalcualte  response matrix over some folders (this is needed because of some corrupted file)
  calculate_response_matrix_over_file = False
  #path = "/Users/buddhadeb/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/"
  path_ljet = "/Users/buddhadeb/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_copy_1"
  path_dilep = "/Users/buddhadeb/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine_copy_1"

  #binning_name = "Nominal_Binning"
  path_to_tty_CR= os.path.expanduser(os.path.join(path_ljet,"tty_CR"))
  path_to_tty_dec_CR= os.path.expanduser(os.path.join(path_ljet,"tty_dec_CR"))
  path_to_fakes_CR= os.path.expanduser(os.path.join(path_ljet,"fakes_CR"))
  path_to_other_photons_CR= os.path.expanduser(os.path.join(path_ljet,"other_photons_CR"))
  path_to_dilep_SR1= os.path.expanduser(os.path.join(path_dilep,"CR_sig"))
  path_to_dilep_SR2= os.path.expanduser(os.path.join(path_dilep,"CR_bkg"))
  print("{} \n".format(path_to_tty_CR))

  list_of_files_tty_CR= []
  list_of_files_tty_dec_CR= []
  list_of_files_fakes_CR= []
  list_of_files_other_photons_CR = []

  
  if submit_merging_part:
    ## Create two threads
    thread1 = threading.Thread(target=run, args=(path_to_tty_CR,path_to_tty_dec_CR, path_to_fakes_CR, path_to_other_photons_CR, path_to_dilep_SR1, path_to_dilep_SR2, list_of_files_tty_CR))
    thread2 = threading.Thread(target=run, args=(path_to_tty_dec_CR,path_to_tty_CR, path_to_fakes_CR, path_to_other_photons_CR, path_to_dilep_SR1, path_to_dilep_SR2, list_of_files_tty_dec_CR))
    thread3 = threading.Thread(target=run, args=(path_to_fakes_CR,path_to_tty_CR,path_to_tty_dec_CR,path_to_other_photons_CR, path_to_dilep_SR1, path_to_dilep_SR2, list_of_files_fakes_CR))
    thread4 = threading.Thread(target=run, args=(path_to_other_photons_CR,path_to_fakes_CR,path_to_tty_CR,path_to_tty_dec_CR,path_to_dilep_SR1, path_to_dilep_SR2, list_of_files_other_photons_CR))

    # Start the threads
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()

    # wait for both threads to finish
    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()


  if calculate_response_matrix_over_file:
    for file_ in list_of_files_tty_CR:
      print("Info:: going to calculate only the response matrix for these sameple: {}/{}\n".format(path_to_tty_CR,file_))
      calculate_and_save_response_matrices(os.path.join(path_to_tty_CR, file_))
    for file_ in list_of_files_tty_dec_CR:
      calculate_and_save_response_matrices(os.path.join(path_to_tty_dec_CR, file_))
    for file_ in list_of_files_fakes_CR:
      calculate_and_save_response_matrices(os.path.join(path_to_fakes_CR, file_))
    for file_ in list_of_files_other_photons_CR:
      calculate_and_save_response_matrices(os.path.join(path_to_other_photons_CR, file_))


