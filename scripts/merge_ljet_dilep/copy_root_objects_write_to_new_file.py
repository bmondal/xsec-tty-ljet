#!/usr/bin/env python3
import ROOT

class CustomHadd():
    def __init__(self) -> None:
        self.list_of_root_files = []
        self.list_of_histograms= []
        self.output_file = ""
        self.list_of_merged_histograms = []
    
    def add_root_files(self,filename):
        self.list_of_root_files.append(filename)

    def output_file_name(self, filename):
        self.output_file = filename

    def add_histograms(self, histoname):
        self.list_of_histograms.append(histoname)
    
    def execute(self):
        # open all the files first
        output_tfile = ROOT.TFile.Open(self.output_file, "RECREATE")
        dict_of_file_histograms = {} 
        # open all input root files
        for tfile in self.list_of_root_files:
            tfile_tmp =  ROOT.TFile.Open(tfile)
            list_histo_tmp = []
            for histo in self.list_of_histograms:
                list_histo_tmp.append(tfile_tmp.Get(histo))
            dict_of_file_histograms[tfile] = list_histo_tmp

        
        #hadd the histograms
        for histo in dict_of_file_histograms[list(dict_of_file_histograms.keys())[0]]:
            histo_name = histo.GetName()
            histo_merged_tmp = None
            for file_ in dict_of_file_histograms:
                for histo_in_file in dict_of_file_histograms[file_]:
                    if histo_name in histo_in_file.GetName():
                      if histo_merged_tmp is None:
                        histo_merged_tmp = histo_in_file.Clone()
                      else:
                        histo_merged_tmp.Add(histo_in_file)
            self.list_of_merged_histograms.append(histo_merged_tmp)


    def copy_all_objects(self, source_dir, target_dir):
        print("=== Info:: printing list of keys \n")
        for key in source_dir.GetListOfKeys():
          print("{}\n".format(key.ReadObj().ClassName()))
        # Iterate over all keys in the directory
        for key in source_dir.GetListOfKeys():
            # Get the actual object
            obj = key.ReadObj()

            # Recurse into directories
            if obj.IsA() == ROOT.TDirectoryFile.Class():
                print("=== Info:: its a TDirectory {} \n".format(key.GetName()))
                subdir = target_dir.mkdir(key.GetName())
                self.copy_all_objects(obj, subdir)
            else:
                # Clone and write objects
                target_dir.cd()
                # if the object in the updated hist list then update it here
                if any(obj.GetName() in x.GetName() for x in self.list_of_merged_histograms):
                    for histo in self.list_of_merged_histograms:
                        if obj.GetName() in histo.GetName():
                            print("=== Info:: {} histogram needs to be replaced with {}\n".format(obj.GetName(), histo.GetName()))
                else:
                  obj_clone = obj.Clone()
                  obj_clone.Write()

    def copy_all_objects_and_update_some_hist(self, source_dir, target_dir):
        list_of_histograms_without_tdirectory = []
        for histo in self.list_of_histograms:
            if "/" in histo:
                histo_name = histo.split("/")
                list_of_histograms_without_tdirectory.append(histo_name[1])
            else:
                list_of_histograms_without_tdirectory.append(histo)
        print("=== Info:: list of histograms {}\n".format(list_of_histograms_without_tdirectory))
        #print("=== Info:: printing list of keys \n")
        for key in source_dir.GetListOfKeys():
          print("{}\n".format(key.ReadObj().ClassName()))
        # Iterate over all keys in the directory
        for key in source_dir.GetListOfKeys():
            # Get the actual object
            obj = key.ReadObj()

            # Recurse into directories
            if obj.IsA() == ROOT.TDirectoryFile.Class():
                print("=== Info:: its a TDirectory {} \n".format(key.GetName()))
                subdir = target_dir.mkdir(key.GetName())
                self.copy_all_objects(obj, subdir)
            else:
                # Clone and write objects
                target_dir.cd()
                obj_clone = obj.Clone()
                obj_clone.Write()


    def copy_root_file(self):
        # Open source and target files
        source_file = ROOT.TFile.Open(self.list_of_root_files[0])
        target_file = ROOT.TFile.Open(self.output_file, "RECREATE")

        # Call recursive function
        self.copy_all_objects_and_update_some_hist(source_file, target_file)

        # Close files
        source_file.Close()
        target_file.Close()

# Use the function
copy_root_file = CustomHadd()
copy_root_file.add_root_files("./tmp/histograms.ttgamma_prod.root")
copy_root_file.add_root_files("./tmp/histograms.ttgamma_dec.root")
copy_root_file.output_file_name("output.root")
copy_root_file.add_histograms("particle/hist_part_ph_pt_full_weighted")
copy_root_file.execute()
copy_root_file.copy_root_file()
