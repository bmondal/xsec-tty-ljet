#!/usr/bin/env bash
# use absolute path always starting from /home or /Users
#src_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v15_v2/'
#src_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v12/'
src_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples_tmp/v12/Unfolding_inputs_v13_v1/'
#dst_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/'
dst_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/'
#dst_path='/Users/buddhadeb/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_ttgamma_inclusive'
#srch_strng=".binning_1."
mkdir -p $dst_path
srch_strng=".nominal_from_fine."
replace_string="."
./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#src_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v15_v2/'
#src_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v12/'
#src_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v12_v1/'
src_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples_tmp/v12/Unfolding_inputs_v13_v1/'
#dst_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/'
#dst_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/'
dst_path='/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/'
#dst_path='/Users/buddhadeb/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_ttgamma_inclusive'
#srch_strng=".binning_1_" # this scenario is for NormVar cases
srch_strng=".nominal_from_fine_" # this scenario is for NormVar cases
replace_string="_"
./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string


#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06/CR_sig/nominal_9999'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_sig/nominal_9999'
#srch_strng=".nominal_from_fine."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06/CR_sig/nominal_9999'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_sig/nominal_9999'
#srch_strng=".nominal_from_fine_" # this scenario is for NormVar cases
#replace_string="_"
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#
#
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06/CR_bkg/nominal_9999'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_bkg/nominal_9999'
#srch_strng=".nominal_from_fine."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06/CR_bkg/nominal_9999'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_bkg/nominal_9999'
#srch_strng=".nominal_from_fine_" # this scenario is for NormVar cases
#replace_string="_"
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string




#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Optimized_binning_1'
#srch_strng=".Optimized_Binning_1."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#
#
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Optimized_binning_2'
#srch_strng=".Optimized_Binning_2."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Optimized_binning_3'
#srch_strng=".Optimized_Binning_3."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Optimized_binning_4'
#srch_strng=".Optimized_Binning_4."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Optimized_binning_5'
#srch_strng=".Optimized_Binning_5."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Optimized_binning_6'
#srch_strng=".Optimized_Binning_6."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
#
#src_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning'
#dst_path='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Optimized_binning_7'
#srch_strng=".Optimized_Binning_7."
#replace_string="."
#./copy_to_new_folder_and_rename_files.py --src_folder $src_path --dst_folder $dst_path --search_string $srch_strng --replace_string $replace_string
