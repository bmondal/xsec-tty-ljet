#!/usr/bin/env python3
import os
import shutil
import argparse


#def copy_and_rename(src_folder, dst_folder, search_string, replace_string):
#    """
#    Function that copies a directory tree and renames all files inside by searching and replacing a specific string
#    :param src_folder: Source folder path
#    :param dst_folder: Destination folder path
#    :param search_string: String to be searched in the file name
#    :param replace_string: String to replace the search string in the file name
#    """
#    for dirpath, dirnames, filenames in os.walk(src_folder):
#        for filename in filenames:
#            # construct the full file path
#            src_file_path = os.path.join(dirpath, filename)
#            # construct the destination file path, with new name and location
#            rel_path = os.path.relpath(src_file_path, src_folder)
#            dst_file_path = os.path.join(dst_folder, rel_path)
#            # create the destination folder if it doesn't exist
#            os.makedirs(os.path.dirname(dst_file_path), exist_ok=True)
#            # rename the file
#            new_file_name = filename.replace(search_string, replace_string)
#            dst_file_path = os.path.join(os.path.dirname(dst_file_path), new_file_name)
#            # copy the file
#            shutil.copy2(src_file_path, dst_file_path)

#def copy_and_rename(src_folder, dst_folder, search_string, replace_string):
#    """ 
#    Function that copies a directory tree and renames all files inside by searching and replacing a specific string
#    :param src_folder: Source folder path
#    :param dst_folder: Destination folder path
#    :param search_string: String to be searched in the file name
#    :param replace_string: String to replace the search string in the file name
#    """
#    for dirpath, dirnames, filenames in os.walk(src_folder):
#        for filename in filenames:
#            # Skip the file if it doesn't contain the search string
#            if search_string not in filename:
#                continue
#            # construct the full file path
#            src_file_path = os.path.join(dirpath, filename)
#            # construct the destination file path, with new name and location
#            rel_path = os.path.relpath(src_file_path, src_folder)
#            dst_file_path = os.path.join(dst_folder, rel_path)
#            # create the destination folder if it doesn't exist
#            os.makedirs(os.path.dirname(dst_file_path), exist_ok=True)
#            # rename the file
#            new_file_name = filename.replace(search_string, replace_string)
#            dst_file_path = os.path.join(os.path.dirname(dst_file_path), new_file_name)
#            # copy the file
#            shutil.copy2(src_file_path, dst_file_path)
#

def copy_and_rename(src_folder, dst_folder, search_string, replace_string):
    """
    Function that copies a directory tree and renames all files inside by searching and replacing a specific string
    :param src_folder: Source folder path
    :param dst_folder: Destination folder path
    :param search_string: String to be searched in the file name
    :param replace_string: String to replace the search string in the file name
    """
    for dirpath, dirnames, filenames in os.walk(src_folder):
        for filename in filenames:
            # Skip the file if it doesn't contain the search string
            if search_string not in filename:
                continue
            # construct the full file path
            src_file_path = os.path.join(dirpath, filename)
            # construct the destination file path, with new name and location
            rel_path = os.path.relpath(src_file_path, src_folder)
            dst_file_path = os.path.join(dst_folder, rel_path)
            # create the destination folder if it doesn't exist
            os.makedirs(os.path.dirname(dst_file_path), exist_ok=True)
            # rename the file
            new_file_name = filename.replace(search_string, replace_string)
            dst_file_path = os.path.join(os.path.dirname(dst_file_path), new_file_name)
            print(f"Copying {src_file_path} to {dst_file_path}")

            try:
                # copy the file
                shutil.copyfile(src_file_path, dst_file_path)
                # copy metadata (e.g., timestamps and permissions) using os.utime and os.chmod
                stat_info = os.stat(src_file_path)
                os.utime(dst_file_path, (stat_info.st_atime, stat_info.st_mtime))
                os.chmod(dst_file_path, stat_info.st_mode)
            except Exception as e:
                print(f"Error copying file: {e}")


if __name__=="__main__":
  """
  src_folder = "/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06"
  dst_folder = "/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning"
  search_string = ".000000." # I want to keep the same name in this folder
  replace_string = "."
  """


  parser = argparse.ArgumentParser(description='Rename and move histograms')
  parser.add_argument('--src_folder', type=str,
                      default='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06',
                      help='Source folder')
  parser.add_argument('--dst_folder', type=str,
                      default='/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v10/NN06_Fine_Binning',
                      help='Destination folder')
  parser.add_argument('--search_string', type=str, default='.000000.',
                      help='Search string')
  parser.add_argument('--replace_string', type=str, default='.',
                      help='Replace string')

  args = parser.parse_args()

  src_folder = args.src_folder
  dst_folder = args.dst_folder
  search_string = args.search_string
  replace_string = args.replace_string

  copy_and_rename(src_folder, dst_folder, search_string, replace_string)
