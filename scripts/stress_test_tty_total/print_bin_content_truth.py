#!/usr/bin/env python

import ROOT

def correct_underflow_overflow_bin(hist):
  underflow_content = hist.GetBinContent(0)
  overflow_content = hist.GetBinContent(hist.GetNbinsX()+1)
  hist.SetBinContent(0, hist.GetBinContent(1)+underflow_content)
  hist.SetBinContent(hist.GetNbinsX(), hist.GetBinContent(hist.GetNbinsX())+overflow_content)
  return hist

path = "/afs/cern.ch/user/b/bmondal/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_vHTCondor_splitted_by_processes_v4/NN06/CR_sig/nominal/"
#histo_File = "{}/histograms.ttgamma_prod.root".format(path)
#histo_File = "{}/ttgamma_prod_reweighted_y1.0.root".format(path)
histo_File = "{}/ttgamma_prod_reweighted_y-1.0.root".format(path)
def run():
  file = ROOT.TFile(histo_File)
  hist_tmp = file.Get("particle/hist_part_ph_pt_full_weighted")
  hist = correct_underflow_overflow_bin(hist_tmp)
  for i in range(0, hist.GetNbinsX()):
    bin_content = 0
    bin_width = 0
    value = 0
    bin_content = hist.GetBinContent(i+1)
    bin_width = hist.GetBinWidth(i+1)
    value = bin_content/(bin_width*139)
    #print("bin content: {0}; bin width: {1}; value: {2}".format(bin_content, bin_width, value))
    print("{}".format(value))


if __name__ == "__main__":
  run()