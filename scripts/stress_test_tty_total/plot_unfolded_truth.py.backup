#!/usr/bin/python

'''
- Plot unfolded spectrum and truth spectrum in a single plot
- Also make the ration plot of Unfolded/Truth 

'''

'''
  c.SetRightMargin(0.09);
  c.SetLeftMargin(0.35);
  c.SetBottomMargin(0.35);
'''

debug = False
draw_non_linear = False

import ROOT
import ctypes

from ROOT import gROOT

def graph_to_hist(graph):
  n_points = graph.GetN()
  hist = ROOT.TH1D()
  for i in range(0, n_points):
    x=ctypes.c_double()
    y=ctypes.c_double()
    graph.GetPoint(i,x,y)
    hist.Fill(x,y)
  return hist
 

class StressTest:
  def __init__(self,path_truth,path_linear_y1, path_linear_y_1, path_nonlinear_y1, path_nonlinear_y_1,output_file):
    # ----------- configurable parameters ----------------------------
    self.path_linear_y1 = path_linear_y1 
    self.path_linear_y_1 = path_linear_y_1
    self.path_nonlinear_y1 = path_nonlinear_y1 
    self.path_nonlinear_y_1 = path_nonlinear_y_1

    self.path_truth = path_truth 
    self.output_file = output_file
    #-------------
  
    self.path_unfolded_hist_y1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_linear_y1)
    self.path_unfolded_hist_y_1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_linear_y_1)
    self.path_unfolded_hist_nonlinear_y1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_nonlinear_y1)
    self.path_unfolded_hist_nonlinear_y_1 = "{}/Fits/Unfolding_UnfoldedResults.txt".format(self.path_nonlinear_y_1)

    # truth
    self.path_truth_hist = "{}/histograms.ttgamma_prod.root".format(self.path_truth)
  
  def process_line(self,line):
    arr = line.split(" ")
    if debug: print(arr)
    return int(arr[0]), float(arr[1]), float(arr[2]), float(arr[3])
  
  def unfolded_hist_from_fit_file(self,file_path, empty_hist):
    hist = empty_hist.Clone()
    file = open(file_path)
    readline = False
    for line in file.readlines():
      if line.strip():
        if(line[0] == '1'): 
          readline = True 
          if debug: print("start of reading line\n")
        if readline:
          if debug: print(line)
          bin_number, nominal, err_up, err_down = self.process_line(line)
          hist.SetBinContent(bin_number, nominal)
          hist.SetBinError(bin_number, err_up)
          print("Bin number: {0}, nominal: {1}, up: {2}, down: {3} \n".format(bin_number, nominal, err_up, err_down))
      else:
        if debug: print("line is empty")
        break
    return hist
 
  def createCanvasPads(self):
    c = ROOT.TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    #pad2.SetGridx()
    pad2.SetGridy()
    pad2.Draw()
  
    return c, pad1, pad2
  
  
  def correct_underflow_overflow_bin(self,hist):
    underflow_content = hist.GetBinContent(0)
    overflow_content = hist.GetBinContent(hist.GetNbinsX()+1)
    hist.SetBinContent(0, hist.GetBinContent(1)+underflow_content)
    hist.SetBinContent(hist.GetNbinsX(), hist.GetBinContent(hist.GetNbinsX())+overflow_content)
    return hist
  
  def run(self):
    #file_unfolded_y1 = ROOT.TFile.Open(path_unfolded_hist_y1)
    #file_unfolded_y_1 = ROOT.TFile.Open(path_unfolded_hist_y_1)
      #key_name = ""
    #for key in file_unfolded_y1.GetListOfKeys():
    #  print("key name (before): {}".format(key.GetName()))
    #  if "c" in key.GetName():
    #    key_name = key.GetName()
    #    print("key name (after): {}".format(key.GetName()))
    #unfolded_plot_canvas_y1 = file_unfolded_y1.Get("{}".format(key_name)) # unfolded histogram; getting it from canvas
    #key_name = ""
    #for key in file_unfolded_y_1.GetListOfKeys():
    #  print("key name (before): {}".format(key.GetName()))
    #  if "c" in key.GetName():
    #    key_name = key.GetName()
    #    print("key name (after): {}".format(key.GetName()))
    #unfolded_plot_canvas_y_1 = file_unfolded_y_1.Get("{}".format(key_name)) # unfolded histogram; getting it from canvas
    file_truth = ROOT.TFile.Open(self.path_truth_hist)
    hist_truth_tmp = file_truth.Get("particle/hist_part_ph_pt_full_weighted")
    hist_truth = self.correct_underflow_overflow_bin(hist_truth_tmp)
    # divide by bin width
    for i in range(0, hist_truth.GetNbinsX()):
      bin_content = 0
      bin_width = 0
      value = 0
      bin_content = hist_truth.GetBinContent(i+1)
      bin_width = hist_truth.GetBinWidth(i+1)
      value = bin_content/(bin_width*139)
      print("bin content: {0}; bin width: {1}; value: {2}".format(bin_content, bin_width, value))
      hist_truth.SetBinContent(i+1, value)
      hist_truth.SetBinError(i+1, 0)
  
    #graph_y1 = unfolded_plot_canvas_y1.FindObject("Graph_from_hist_part_ph_pt_full_weighted")
    #print(graph_y1.ls())
    #graph_y_1 = unfolded_plot_canvas_y_1.FindObject("Graph_from_hist_part_ph_pt_full_weighted")
    #print(graph_y_1.ls())
    #hist_y1 = graph_to_hist(graph_y1)
    #hist_y_1 = graph_to_hist(graph_y_1)
  
    hist_y1 = self.unfolded_hist_from_fit_file(self.path_unfolded_hist_y1, hist_truth)
    hist_y_1 = self.unfolded_hist_from_fit_file(self.path_unfolded_hist_y_1, hist_truth)
    hist_nonlinear_y1 = self.unfolded_hist_from_fit_file(self.path_unfolded_hist_nonlinear_y1, hist_truth)
    hist_nonlinear_y_1 = self.unfolded_hist_from_fit_file(self.path_unfolded_hist_nonlinear_y_1, hist_truth)

  
    hist_clone_y1 = hist_y1.Clone()
    hist_clone_y_1 = hist_y_1.Clone()
    hist_clone_nonlinear_y1 = hist_nonlinear_y1.Clone()
    hist_clone_nonlinear_y_1 = hist_nonlinear_y_1.Clone()

    hist_y1.SetLineColor(4)
    hist_y_1.SetLineColor(6)
    if draw_non_linear: hist_nonlinear_y1.SetLineColor(45)
    if draw_non_linear: hist_nonlinear_y_1.SetLineColor(3)

    hist_truth_clone = hist_truth.Clone()
    hist_truth.SetLineColor(2)
    canvas, pad1, pad2 = self.createCanvasPads()
    pad1.cd()
    hist_y1.GetXaxis().SetLabelFont(63)
    hist_y1.GetXaxis().SetLabelSize(14)
    hist_y1.GetYaxis().SetLabelFont(63)
    hist_y1.GetYaxis().SetLabelSize(14)
    hist_y1.GetYaxis().SetTitleSize(0.05)
    hist_y1.GetXaxis().SetTitle("p_{T}(#gamma) [GeV]")
    hist_y1.GetYaxis().SetTitle("events/GeV")


    hist_y1.SetStats(False)
    hist_y1.Draw("hist e")
    hist_y_1.Draw("hist same e")
    if draw_non_linear: hist_nonlinear_y1.Draw("hist same e")
    if draw_non_linear: hist_nonlinear_y_1.Draw("hist same e")
    hist_truth.Draw("hist same e")
    # legend
    legend = ROOT.TLegend(0.7,0.7, 0.9,0.9)
    legend.AddEntry(hist_y1, "1 + X", "l")
    legend.AddEntry(hist_y_1, " 1 - X", "l")
    if draw_non_linear: legend.AddEntry(hist_nonlinear_y1, "1 + 1*Obs", "l")
    if draw_non_linear: legend.AddEntry(hist_nonlinear_y_1, " 1 - 1*Obs", "l")
    legend.AddEntry(hist_truth, " truth", "l")
    legend.Draw()
    #latex = ROOT.TLatex()
    #latex.SetTextSize(0.030)
    #latex.SetTextAlign(13) #align at top
    #latex.DrawLatex(.2,.9,"#it{ATLAS} Internal")
    #latex.DrawLatex(.2,.8,"#sqrt{S} = 13 TeV, 139 fb^{-1}")
    #latex.DrawLatex(.2,.7," dilepton channel")
    pad2.cd()
    hist_clone_y1.Divide(hist_truth_clone)
    hist_clone_nonlinear_y1.Divide(hist_truth_clone)
    hist_clone_y1.GetYaxis().SetRangeUser(0.3,1.7)
    hist_clone_y1.GetYaxis().SetTitle("Unfolded/Truth")
    hist_clone_y_1.Divide(hist_truth_clone)
    hist_clone_nonlinear_y_1.Divide(hist_truth_clone)
    hist_clone_y1.SetLineColor(4)
    hist_clone_y_1.SetLineColor(6)
    if draw_non_linear: hist_clone_nonlinear_y1.SetLineColor(45)
    if draw_non_linear: hist_clone_nonlinear_y_1.SetLineColor(3)

    hist_clone_y1.GetXaxis().SetLabelFont(63)
    hist_clone_y1.GetXaxis().SetLabelSize(14)
    hist_clone_y1.GetYaxis().SetLabelFont(63)
    hist_clone_y1.GetYaxis().SetLabelSize(14)
    hist_clone_y1.GetYaxis().SetTitleSize(0.07)
    hist_clone_y1.GetXaxis().SetTitleSize(0.08)
    hist_clone_y1.GetYaxis().SetTitle("Unfolded/Truth")
    hist_clone_y1.GetXaxis().SetTitle("p_{T}(#gamma)")

    hist_clone_y1.SetStats(False)
    hist_clone_y1.Draw("hist e")
    hist_clone_y_1.Draw("hist same e")
    if draw_non_linear: hist_clone_nonlinear_y1.Draw("hist same e")
    if draw_non_linear: hist_clone_nonlinear_y_1.Draw("hist same e")
    canvas.SaveAs(self.output_file)
    canvas.SaveAs("{}.png".format(self.output_file))
  
    # print chiSquare
    #chisquare = hist_truth.GetChiSquare(hist)
    #print("chi square : {}".format(chisquare))


if __name__== '__main__':
  # paths where the unfolded and truth files are stored
  path_linear_y1 = "/home/buddha/Hep/ttgamma/Unfolding_139fb/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v5/stat//tty1l_pt_all_stat_linear_reweighted_y1/"
  path_linear_y_1 = "/home/buddha/Hep/ttgamma/Unfolding_139fb/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v5/stat//tty1l_pt_all_stat_linear_reweighted_y-1/"
  path_nonlinear_y1 = "/home/buddha/Hep/ttgamma/Unfolding_139fb/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v5/stat//tty1l_pt_all_stat_linear_reweighted_y1/"
  path_nonlinear_y_1 = "/home/buddha/Hep/ttgamma/Unfolding_139fb/xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v5/stat//tty1l_pt_all_stat_linear_reweighted_y-1/"
  path_truth = "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_CR/nominal/"

  # for pt
  stress_test_pt = StressTest(path_truth, path_linear_y1, path_linear_y_1, path_nonlinear_y1, path_nonlinear_y_1,"stress_test_pt.pdf")
  stress_test_pt.run()
