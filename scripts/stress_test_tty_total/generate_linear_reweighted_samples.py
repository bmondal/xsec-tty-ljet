#!/usr/bin/env python

"""
This scripts generate reweighted samples (histograms) from the nominal one
"""

import argparse
import ROOT

debug = False

histo_dict = {}
histo_dict["hist_reco_ph_pt_full_weighted"]=[100,300]
histo_dict["hist_reco_ph_eta_full_weighted"]=[1.2, 2.37]
histo_dict["hist_reco_ph_drphl_full_weighted"] = [1.8, 6]
histo_dict["hist_reco_ph_drphl1_full_weighted"] = [1.8, 6]
histo_dict["hist_reco_ph_drphl2_full_weighted"]= [1.8,6]
histo_dict["hist_reco_dEtall_full_weighted"] = [1.2,2.5]
histo_dict["hist_reco_dPhill_full_weighted"]= [1.75, 3.14]
histo_dict["hist_reco_Ptll_full_weighted"] = [100, 300]
histo_dict["hist_reco_drphb_full_weighted"] = [1.8, 6]
histo_dict["hist_reco_drlj_full_weighted"] = [1.8,6]
histo_dict["hist_reco_j1_pt_full_weighted"] = [100, 300]
histo_dict["hist_part_ph_pt_full_weighted"]=[100,300]
histo_dict["hist_part_ph_eta_full_weighted"]=[1.2, 2.37]
histo_dict["hist_part_ph_drphl_full_weighted"] = [1.8, 6]
histo_dict["hist_part_ph_drphl1_full_weighted"] = [1.8, 6]
histo_dict["hist_part_ph_drphl2_full_weighted"]= [1.8,6]
histo_dict["hist_part_dEtall_full_weighted"] = [1.2,2.5]
histo_dict["hist_part_dPhill_full_weighted"]= [1.75, 3.14]
histo_dict["hist_part_Ptll_full_weighted"] = [100, 300]
histo_dict["hist_part_drphb_full_weighted"] = [1.8, 6]
histo_dict["hist_part_drlj_full_weighted"] = [1.8,6]
histo_dict["hist_part_j1_pt_full_weighted"] = [100, 300]


def correct_underflow_overflow_bin(hist):
  return hist
  # FIXME: correct implementation below but very weird hack for now on
  #underflow_content = hist.GetBinContent(0)
  #overflow_content = hist.GetBinContent(hist.GetNbinsX()+1)
  #hist.SetBinContent(1, hist.GetBinContent(1)+underflow_content)
  #hist.SetBinContent(hist.GetNbinsX(), hist.GetBinContent(hist.GetNbinsX())+overflow_content)
  #return hist


class ReweightSample:
  """
  ref: section unfolding test of tty xsec measurement 36 fb-1 INT note
  Get data and MC root files
  for a particular histogram, calculate weight per bin 
        weight_i = 1 + Y*[frac_up_value - bin_center)/frac_down_value ]; where i = bin index, Y = 1, -1
  """
  def __init__(self, rootfile_mc):
    self.rootfilename_mc = rootfile_mc
    self.outputfilename = ""
    self.file_mc= ROOT.TFile(self.rootfilename_mc)
    # store the hist name and the weights per bin so that it can be applied later for particle level histos
    self.weight_dict = {} # weight_dict = {"hist_name": list_containing_weights,... }

    if (debug): # if debug print status of the constructor
      print(" in __init__ \n")
      print(" rootfilename_mc: {} \n".format(self.rootfilename_mc))


  def set_param_Y(self, param_Y):
    self.param_Y = param_Y

  def set_param_nominator_denominator(self, nominator_val, denominator_val):
    self.param_nominator = nominator_val 
    self.param_denominator = denominator_val 

  def set_output_file_prefix(self, name):
    self.outputfilename = name

  def get_reweighted_hist_particle(self, hist_mc, hist_name):
    hist_mc_clone = hist_mc.Clone()
    if(hist_mc.Integral() == 0): return hist_mc_clone # if it is empty hist, just return the empty hist
    # in particle level "_reco_" is not "_part_"
    if "reco" in hist_name: return hist_mc_clone
    if "part" in hist_name:
      hist_name_modified = hist_name.replace("part", "reco")
    print("hist name before: {} ; after: {} \n".format(hist_name, hist_name_modified))
    # find the correct hist and index from the self.weight_dict
    weight_list = self.weight_dict[hist_name_modified]
    # loop over the bins and get get weighted hist
    for i in range(0, hist_mc.GetNbinsX()+2):
      bin_content_mc = hist_mc.GetBinContent(i)
      weight = 1
      
      reweighted_bin_content_mc = bin_content_mc * weight_list[i]
      hist_mc_clone.SetBinContent(i ,reweighted_bin_content_mc)

    return hist_mc_clone

  def get_reweighted_hist(self, hist_mc, Tdir_name, hist_name):
    hist_mc_clone = hist_mc.Clone()
    if(hist_mc.Integral() == 0): return hist_mc_clone # if it is empty hist, just return the empty hist
    # loop over the bins and get get weighted hist
    weight_list = [[1] * i for i in range(0,hist_mc.GetNbinsX()+2)]
    for i in range(0, hist_mc.GetNbinsX()+2):
      bin_content_mc = hist_mc.GetBinContent(i)
      bin_center = hist_mc.GetBinCenter(i)
      weight = 1
      weight = 1 + self.param_Y * (self.param_nominator - bin_center)/self.param_denominator # calculate weight here
      reweighted_bin_content_mc = bin_content_mc * weight
      hist_mc_clone.SetBinContent(i ,reweighted_bin_content_mc)
      # update the weight in the weight list
      weight_list[i] = weight
    self.weight_dict["{}".format(hist_name)] = weight_list

    return hist_mc_clone

  # create new rootfiles and update
  def save_in_rootfile(self):
    name_reweighted = "{}.linear_reweighted_y{}.root".format(self.outputfilename, self.param_Y)
    file_reweighted = ROOT.TFile(name_reweighted, "RECREATE")
    """
    loop over Tdirectories->find histogram -> reweight
    """
    directory_list = ["Reco", "particle"]
    for dir in directory_list: 
      directory = self.file_mc.Get(dir) # get Reco or particle dir
      file_reweighted.mkdir(dir) # create dir in outfile
      dir_obj_from_file_reweighted = file_reweighted.Get(dir) # this is the directory obj from the outfile. In this object we will write our new histos. this will be saved
      for key in directory.GetListOfKeys():
        key_name = key.GetName()
        histo_obj_mc_temp = self.file_mc.Get("{}/{}".format(dir, key_name))
        histo_obj_mc = correct_underflow_overflow_bin(histo_obj_mc_temp)
        # don't do naything fro th2d
        if "h2" in key_name: continue
        # only do for reweighted /unfolding histos 
        if not "weighted" in key_name: continue
        # weighting Reco and particle is different. 
        #if "reco" in key_name: 
        if(dir == "Reco"): 
          # get the param_nominator and param_denominator
          temp_list = histo_dict[key_name]
          self.set_param_nominator_denominator(temp_list[0], temp_list[1])
          # don't do anything with the h2 hist
          hist_reweighted = self.get_reweighted_hist(histo_obj_mc, dir, key_name)
          dir_obj_from_file_reweighted.WriteObject(hist_reweighted, "{}".format(key_name), "overwrite")
        if (dir == "particle"):
          hist_reweighted = self.get_reweighted_hist_particle(histo_obj_mc, key_name)
          dir_obj_from_file_reweighted.WriteObject(hist_reweighted, "{}".format(key_name), "overwrite")


if __name__=='__main__':
  # add normalization to a test.root file and in test_hist histogram and save as test_normalized.root
  parser = argparse.ArgumentParser()
  parser.add_argument("--rootfile_mc", type = str, help="name of the MC rootfile in which histogram contains")
  parser.add_argument("--outfile", type = str, help="name of the outputfile, don't use .root at the end; because we will create up/down.root")
  parser.add_argument("--param_Y", type = float, help="value of param Y")
  #parser.add_argument("--param_nominator", type = float, help="nominator value")
  #parser.add_argument("--param_denominator", type = float, help="denominator value")
  args = parser.parse_args()

  obj = ReweightSample(args.rootfile_mc)
  obj.set_param_Y(args.param_Y) # setting y param
#  obj.set_param_nominator_denominator(args.param_nominator, args.param_denominator) 
  obj.set_output_file_prefix(args.outfile)
  obj.save_in_rootfile()
