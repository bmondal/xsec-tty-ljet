function run_stress_test_all_vars {
  var_name=$1
  path_to_trex_fitted_folders="/Users/buddhadeb/Storage/HEP/ttgamma/DiffXSec/Unfolding//xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v15/stat-all"
  path_to_nominal_unfolding_input="/Users/buddhadeb/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine/"

  path_linear_y1="$path_to_trex_fitted_folders/tty1l_${var_name}_all_stat_linear_reweighted_y1/"
  path_linear_y_1="$path_to_trex_fitted_folders/tty1l_${var_name}_all_stat_linear_reweighted_y-1/"
  path_nonlinear_y1="$path_to_trex_fitted_folders/tty1l_${var_name}_all_stat_nonlinear_reweighted_y1/"
  path_nonlinear_y_1="$path_to_trex_fitted_folders/tty1l_${var_name}_all_stat_nonlinear_reweighted_y-1/"
  path_truth="$path_to_nominal_unfolding_input/tty_CR/nominal_9999/"
  
  python plot_unfolded_truth.py  --path-trex-fit-linear-y1 $path_linear_y1  --path-trex-fit-linear-y-1 $path_linear_y_1 --path-trex-fit-nonlinear-y1 $path_nonlinear_y1 --path-trex-fit-nonlinear-y-1 $path_nonlinear_y_1 --path-truth  $path_truth
  }
run_stress_test_all_vars "pt"
run_stress_test_all_vars "eta"
run_stress_test_all_vars "dr"
run_stress_test_all_vars "ptj1"
run_stress_test_all_vars "drphb"
run_stress_test_all_vars "drlj"
