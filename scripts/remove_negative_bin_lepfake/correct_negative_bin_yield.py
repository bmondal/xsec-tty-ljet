# this works only for "Reco" subdirectory

import argparse
import ROOT

debug = False

class AddNormalization:
  def __init__(self, rootfile, histo_list):
    self.rootfilename = rootfile
    self.histo_list = histo_list
    self.outputfilename = ""
    self.file = ROOT.TFile(self.rootfilename)
    self.histo_obj=[]
    for histo in histo_list:# get the histograms from the root file and store them into a list
      self.histo_obj.append(self.file.Get("Reco/{}".format(histo)))

    self.histo_obj_up = []
    for histo in self.histo_obj: # create up and down histograms by cloning
      self.histo_obj_up.append( histo.Clone())

  def set_output_file_prefix(self, name):
    self.outputfilename = name

  def correct_negative_bin(self):
    for index in range(0, len(self.histo_obj)):
      if debug: print("histo name : {}".format(self.histo_obj[index].GetName()))
      # loop over bins and set negative bins to zero
      for bin in range(0, self.histo_obj_up[index].GetNbinsX()+2):
        bin_content = self.histo_obj_up[index].GetBinContent(bin)
        if bin_content < 0:
          if debug: print("bin content before: {}".format(self.histo_obj_up[index].GetBinContent(bin)))
          (self.histo_obj_up[index]).SetBinContent(bin, 1e-6)
          (self.histo_obj_up[index]).SetBinError(bin, 1e-6)
          if debug: print("bin content after: {}".format(self.histo_obj_up[index].GetBinContent(bin)))

  # create new rootfiles and update
  def save_in_rootfile(self):
    self.correct_negative_bin()
    if (debug): print("print bin content: {}".format(self.histo_obj_up[0].GetBinContent(6)))
    name_up = self.outputfilename+".root"
    file_up = ROOT.TFile(name_up, "RECREATE")
    # get the directory object
    directory = self.file.Get("Reco")
    file_up.mkdir("Reco")
    obj_up = file_up.Get("Reco")
    for key in directory.GetListOfKeys(): # iterate over the list of keys
      key_name = key.GetName() # get name of the key
      if(debug): print("key: {} \n".format(key_name)) 
      obj = directory.Get("{}".format(key_name)) # get directory
      if key_name in self.histo_list: # if the histogram object exists in histo list
        if(debug): print("key:{} \n".format(key_name))
        for histo in self.histo_obj_up: # overwrite the histogram object 
          if key_name in histo.GetName():
            if(debug): print ("checking whether overwriting right hist keyname: {0}, histo: {1} \n".format(key_name, histo))
            if (debug): print("print bin content: {}".format(histo.GetBinContent(6)))
            obj_up.WriteObject(histo, "{}".format(key_name), "overwrite")
        #obj_up.WriteObject(self.histo_obj_up, "{}".format(key_name), "overwrite")
        #obj_down.WriteObject(self.histo_obj_down, "{}".format(key_name), "overwrite")
      else:
        obj_up.WriteObject(obj, "{}".format(key_name))


if __name__=='__main__':
  # add normalization to a test.root file and in test_hist histogram and save as test_normalized.root
  parser = argparse.ArgumentParser()
  parser.add_argument("--rootfile", type = str, help="name of the rootfile in which histogram contains")
  parser.add_argument("--outfile", type = str, help="name of the outputfile, don't use .root at the end; because we will create up/down.root")
  args = parser.parse_args()

  histogram_list = ["hist_reco_ph_pt_full_weighted", "hist_reco_ph_eta_full_weighted", "hist_reco_ph_drphl1_full_weighted", "hist_reco_drphb_full_weighted", "hist_reco_drlj_full_weighted", "hist_reco_j1_pt_full_weighted"]
  #histogram_list = ["hist_reco_ph_pt_full_weighted"]
  #obj = AddNormalization(args.rootfile, args.histname)
  obj = AddNormalization(args.rootfile, histogram_list)
  obj.set_output_file_prefix(args.outfile)
  obj.save_in_rootfile()
