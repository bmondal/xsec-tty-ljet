#!/usr/bin/env bash
run_over_SR_ejet () {
  #lep_fake
  echo "processing lepfake nominal"
  python correct_negative_bin_yield.py --rootfile $1/nominal_9999/histograms.lepfake.$2.root --outfile $1/nominal_9999/histograms.lepfake_negative_bin_corrected.$2
  #lep_fake up
  echo "processing lepfake nominal up"
  python correct_negative_bin_yield.py --rootfile $1/weight_leptonFake_nominal_Up/histograms.lepfake.$2.root --outfile $1/weight_leptonFake_nominal_Up/histograms.lepfake_negative_bin_corrected.$2
  #lep_fake
  echo "processing lepfake nominal down"
  python correct_negative_bin_yield.py --rootfile $1/weight_leptonFake_nominal_Down/histograms.lepfake.$2.root --outfile $1/weight_leptonFake_nominal_Down/histograms.lepfake_negative_bin_corrected.$2
  #lep_fake
  echo "processing lepfake var2"
  python correct_negative_bin_yield.py --rootfile $1/weight_leptonFake_var1/histograms.lepfake.$2.root --outfile $1/weight_leptonFake_var1/histograms.lepfake_negative_bin_corrected.$2
  echo "processing lepfake var1"
  python correct_negative_bin_yield.py --rootfile $1/weight_leptonFake_var2/histograms.lepfake.$2.root --outfile $1/weight_leptonFake_var2/histograms.lepfake_negative_bin_corrected.$2
}
run_over_SR_mujet () {
  #lep_fake
  echo "processing lepfake nominal"
  python correct_negative_bin_yield.py --rootfile $1/nominal_9999/histograms.lepfake.$2.root --outfile $1/nominal_9999/histograms.lepfake_negative_bin_corrected.mujet.$2
  #lep_fake up
  echo "processing lepfake nominal up"
  python correct_negative_bin_yield.py --rootfile $1/weight_leptonFake_nominal_Up/histograms.lepfake.$2.root --outfile $1/weight_leptonFake_nominal_Up/histograms.lepfake_negative_bin_corrected.mujet.$2
  #lep_fake
  echo "processing lepfake nominal down"
  python correct_negative_bin_yield.py --rootfile $1/weight_leptonFake_nominal_Down/histograms.lepfake.$2.root --outfile $1/weight_leptonFake_nominal_Down/histograms.lepfake_negative_bin_corrected.mujet.$2
  #lep_fake
  echo "processing lepfake var2"
  python correct_negative_bin_yield.py --rootfile $1/weight_leptonFake_var1/histograms.lepfake.$2.root --outfile $1/weight_leptonFake_var1/histograms.lepfake_negative_bin_corrected.mujet.$2
  echo "processing lepfake var1"
  python correct_negative_bin_yield.py --rootfile $1/weight_leptonFake_var2/histograms.lepfake.$2.root --outfile $1/weight_leptonFake_var2/histograms.lepfake_negative_bin_corrected.mujet.$2
}

# be mindful I am using local eos folder here
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> processing tty_CR <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
run_over_SR_ejet "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v5//tty_CR/" "nominal_from_fine"
#run_over_SR_mujet "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v5//tty_CR/" "nominal_from_fine"
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> processing tty_dec_CR <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
run_over_SR_ejet "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v5//tty_dec_CR/" "nominal_from_fine"
#run_over_SR_mujet "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v5//tty_dec_CR/" "nominal_from_fine"
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> processing fake_CR <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
run_over_SR_ejet "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v5//fakes_CR/" "nominal_from_fine"
#run_over_SR_mujet "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v5//fakes_CR/" "nominal_from_fine"
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> processing other photon CR <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
run_over_SR_ejet "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v5//other_photons_CR/" "nominal_from_fine"
#run_over_SR_mujet "/home/bm863639/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_v5//other_photons_CR/" "nominal_from_fine"


# for testing
#python correct_negative_bin_yield.py --rootfile histograms.Wty.root --outfile histograms.Wty --norm 50
