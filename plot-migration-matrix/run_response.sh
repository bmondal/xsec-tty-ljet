DISPLAY=0
#pt
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", "p_{T}(#gamma) [GeV]","SR1" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_dec_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", " p_{T}(#gamma) [GeV]" ,"SR2" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/fakes_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", "p_{T}(#gamma) [GeV]","SR3" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/other_photons_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", " p_{T}(#gamma) [GeV]" ,"SR4" )'

# eta
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_eta", "|#eta(#gamma)|", "SR1" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_dec_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_eta", "|#eta(#gamma)|", "SR2" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/fakes_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_eta", "|#eta(#gamma)|", "SR3" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/other_photons_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_eta", "|#eta(#gamma)|", "SR4" )'

# dr(ph.l)
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl1", "min #Delta R(#gamma, l)", "SR1" ) '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_dec_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl1", "min #Delta R(#gamma, l)", "SR2" ) '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/fakes_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl1", "min #Delta R(#gamma, l)", "SR3" ) '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/other_photons_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl1", "min #Delta R(#gamma, l)", "SR4" ) '

# dr(ph, b)
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drphb", " min #Delta R(#gamma, b)", "SR1") '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_dec_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drphb", " min #Delta R(#gamma, b)", "SR2") '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/fakes_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drphb", " min #Delta R(#gamma, b)", "SR3") '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/other_photons_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drphb", " min #Delta R(#gamma, b)", "SR4") '
# dr(l,j)
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drlj", " min #Delta R(l,j)", "SR1") '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_dec_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drlj", " min #Delta R(l,j)", "SR2") '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/fakes_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drlj", " min #Delta R(l,j)", "SR3") '
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/other_photons_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drlj", " min #Delta R(l,j)", "SR4") '

# pt(j1)
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_j1_pt", "p_{T}(j1) [GeV]","SR1" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/tty_dec_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_j1_pt", "p_{T}(j1) [GeV]","SR2" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/fakes_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_j1_pt", "p_{T}(j1) [GeV]","SR3" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_binning_1/other_photons_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_j1_pt", "p_{T}(j1) [GeV]","SR4" )'
