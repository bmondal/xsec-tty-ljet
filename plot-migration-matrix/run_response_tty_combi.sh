DISPLAY=0
#pt ljet
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_copy_1/tty_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", "p_{T}(#gamma) [GeV]","SR1" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_copy_1/tty_dec_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", " p_{T}(#gamma) [GeV]" ,"SR2" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_copy_1/fakes_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", "p_{T}(#gamma) [GeV]","SR3" )'
root -l -q response.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_copy_1/other_photons_CR/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", " p_{T}(#gamma) [GeV]" ,"SR4" )'

#pt dilep
root -l -q response.C'("~/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11_copy_1/NN06_Nominal_from_fine/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", "p_{T}(#gamma) [GeV]","SR5" )'
root -l -q response.C'("~/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11_copy_1/NN06_Nominal_from_fine/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", " p_{T}(#gamma) [GeV]" ,"SR6" )'

