
#include "TStyle.h"
#include "TROOT.h"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasLabels.h"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasStyle.h"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasUtils.h"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasLabels.C"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasStyle.C"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasUtils.C"
#endif

void response(std::string filename, std::string histogram, std::string histogramTitle, std::string outputname_suffix){
  gStyle->SetOptStat(0);
  // Load the ATLAS style script
  #ifdef __CINT__
  gROOT->LoadMacro("/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasLabels.C");
  gROOT->LoadMacro("/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasStyle.C");
  gROOT->LoadMacro("/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasUtils.C");
  #endif
  gStyle->SetOptStat(0);
  SetAtlasStyle();

  TFile *file = TFile::Open(filename.c_str());
  TH2D* hist = (TH2D*)file->Get(histogram.c_str());

  gStyle->SetPaintTextFormat("4.3f ");
  hist->GetZaxis()->SetRangeUser(0.0, 1.0);
  TString xaxisName = "Reconstructed-level "+histogramTitle;
  TString yaxisName = "Particle-level "+histogramTitle;
  hist->GetXaxis()->SetTitle(xaxisName);
  hist->GetYaxis()->SetTitle(yaxisName);
  //hist->GetZaxis()->SetRangeUser(0.0001, 100.0);
  TCanvas* c = new TCanvas("c");
  c->cd();
  c->SetRightMargin(0.20); // Adjust this value as needed
  c->SetLeftMargin(0.20); // Adjust this value as needed
  hist->Draw("colz");

  if (outputname_suffix=="SR1")  myText(       0.23,  0.85, 1, "SR t#bar{t}#gamma production");
  if (outputname_suffix=="SR2")  myText(       0.23,  0.85, 1, "CR t#bar{t}#gamma decay");
  if (outputname_suffix=="SR3")  myText(       0.23,  0.85, 1, "CR fake #gamma");
  if (outputname_suffix=="SR4")  myText(       0.23,  0.85, 1, "CR other #gamma");


  TString saveName = "response_"+histogram+"_"+outputname_suffix+".pdf";
  //c->SetLogx();
  myText(       0.23,  0.89, 1, "#sqrt{s} = 13 TeV");


  //c->SetLogy();
  c->SaveAs(saveName);

}
