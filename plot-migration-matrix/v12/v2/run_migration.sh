DISPLAY=0
#pt
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_CR/nominal/histograms.ttgamma_prod.root","h2_ph_pt_reco_part_full_weighted", "p_{T}(#gamma) [GeV]","SR1" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_dec_CR/nominal/histograms.ttgamma_prod.root","h2_ph_pt_reco_part_full_weighted", " p_{T}(#gamma) [GeV]" ,"SR2" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/fakes_CR/nominal/histograms.ttgamma_prod.root","h2_ph_pt_reco_part_full_weighted", "p_{T}(#gamma) [GeV]","SR3" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/other_photons_CR/nominal/histograms.ttgamma_prod.root","h2_ph_pt_reco_part_full_weighted", " p_{T}(#gamma) [GeV]" ,"SR4" )'

# eta
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_CR/nominal/histograms.ttgamma_prod.root","h2_ph_eta_reco_part_full_weighted", "|#eta(#gamma)|", "SR1" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_dec_CR/nominal/histograms.ttgamma_prod.root","h2_ph_eta_reco_part_full_weighted", "|#eta(#gamma)|", "SR2" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/fakes_CR/nominal/histograms.ttgamma_prod.root","h2_ph_eta_reco_part_full_weighted", "|#eta(#gamma)|", "SR3" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/other_photons_CR/nominal/histograms.ttgamma_prod.root","h2_ph_eta_reco_part_full_weighted", "|#eta(#gamma)|", "SR4" )'

# dr(ph.l)
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_CR/nominal/histograms.ttgamma_prod.root","h2_ph_drphl1_reco_part_full_weighted", "min #Delta R(#gamma, l)", "SR1" ) '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_dec_CR/nominal/histograms.ttgamma_prod.root","h2_ph_drphl1_reco_part_full_weighted", "min #Delta R(#gamma, l)", "SR2" ) '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/fakes_CR/nominal/histograms.ttgamma_prod.root","h2_ph_drphl1_reco_part_full_weighted", "min #Delta R(#gamma, l)", "SR3" ) '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/other_photons_CR/nominal/histograms.ttgamma_prod.root","h2_ph_drphl1_reco_part_full_weighted", "min #Delta R(#gamma, l)", "SR4" ) '

# dr(ph, b)
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_CR/nominal/histograms.ttgamma_prod.root","h2_drphb_reco_part_full_weighted", " min #Delta R(#gamma, b)", "SR1") '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_dec_CR/nominal/histograms.ttgamma_prod.root","h2_drphb_reco_part_full_weighted", " min #Delta R(#gamma, b)", "SR2") '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/fakes_CR/nominal/histograms.ttgamma_prod.root","h2_drphb_reco_part_full_weighted", " min #Delta R(#gamma, b)", "SR3") '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/other_photons_CR/nominal/histograms.ttgamma_prod.root","h2_drphb_reco_part_full_weighted", " min #Delta R(#gamma, b)", "SR4") '
# dr(l,j)
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_CR/nominal/histograms.ttgamma_prod.root","h2_drlj_reco_part_full_weighted", " min #Delta R(l,j)", "SR1") '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_dec_CR/nominal/histograms.ttgamma_prod.root","h2_drlj_reco_part_full_weighted", " min #Delta R(l,j)", "SR2") '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/fakes_CR/nominal/histograms.ttgamma_prod.root","h2_drlj_reco_part_full_weighted", " min #Delta R(l,j)", "SR3") '
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/other_photons_CR/nominal/histograms.ttgamma_prod.root","h2_drlj_reco_part_full_weighted", " min #Delta R(l,j)", "SR4") '

# pt(j1)
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_CR/nominal/histograms.ttgamma_prod.root","h2_j1_pt_reco_part_full_weighted", "p_{T}(j1) [GeV]","SR1" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/tty_dec_CR/nominal/histograms.ttgamma_prod.root","h2_j1_pt_reco_part_full_weighted", "p_{T}(j1) [GeV]","SR2" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/fakes_CR/nominal/histograms.ttgamma_prod.root","h2_j1_pt_reco_part_full_weighted", "p_{T}(j1) [GeV]","SR3" )'
root -l -q migration.C'("~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v5/other_photons_CR/nominal/histograms.ttgamma_prod.root","h2_j1_pt_reco_part_full_weighted", "p_{T}(j1) [GeV]","SR4" )'
