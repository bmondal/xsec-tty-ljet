//void response(std::string filename, std::string histogram, std::string histogramTitle, std::string outputname_suffix){
//  gStyle->SetOptStat(0);
//  TFile *file = TFile::Open(filename.c_str());
//  TH2D* hist = (TH2D*)file->Get(histogram.c_str());
//
//  gStyle->SetPaintTextFormat("4.2f ");
//  TString xaxisName = "Reconstructed-level "+histogramTitle;
//  TString yaxisName = "Particle-level "+histogramTitle;
//  hist->GetXaxis()->SetTitle(xaxisName);
//  hist->GetYaxis()->SetTitle(yaxisName);
//  TCanvas* c = new TCanvas("c");
//  c->cd();
//  
//  // Loop over all bins
//  int nBinsX = hist->GetNbinsX();
//  int nBinsY = hist->GetNbinsY();
//  for (int i = 1; i <= nBinsX; ++i) {
//    for (int j = 1; j <= nBinsY; ++j) {
//      // Check if the bin is empty
//      if (hist->GetBinContent(i, j) < 0.00001) {
//        // If so, set its color to white
//        hist->GetBinContent(i, j)->SetFillColor(kWhite);
//      }
//    }
//  }
//
//  hist->Draw("textcolz");
//  TString saveName = "response_"+histogram+"_"+outputname_suffix+".pdf";
//  c->SaveAs(saveName);
//}







void migration_not_normalized(std::string filename, std::string histogram, std::string histogramTitle, std::string outputname_suffix){
  gStyle->SetOptStat(0);
  TFile *file = TFile::Open(filename.c_str());
  TH2D* hist = (TH2D*)file->Get(histogram.c_str());

  gStyle->SetPaintTextFormat("4.3f ");
  hist->GetZaxis()->SetRangeUser(0.0, 1000.0);
  TString xaxisName = "Reconstructed-level "+histogramTitle;
  TString yaxisName = "Particle-level "+histogramTitle;
  hist->GetXaxis()->SetTitle(xaxisName);
  hist->GetYaxis()->SetTitle(yaxisName);
  //hist->GetZaxis()->SetRangeUser(0.0001, 100.0);
  TCanvas* c = new TCanvas("c");
  c->cd();
  hist->Draw("textcolz");
  TString saveName = "migration_"+histogram+"_"+outputname_suffix+".pdf";
  c->SetLogx();
  //c->SetLogy();
  c->SaveAs(saveName);

}
