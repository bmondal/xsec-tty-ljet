#include "TStyle.h"
#include "TROOT.h"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasLabels.h"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasStyle.h"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasUtils.h"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasLabels.C"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasStyle.C"
#include "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasUtils.C"
#endif

bool debug = false;

void migration(std::string filename, std::string histogram, std::string histogramTitle, std::string save_name){
  //gStyle->SetTextSize(0.02); // You can adjust the size as needed
  // Load the ATLAS style script
  #ifdef __CINT__
  gROOT->LoadMacro("/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasLabels.C");
  gROOT->LoadMacro("/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasStyle.C");
  gROOT->LoadMacro("/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/atlasrootstyle/AtlasUtils.C");
  #endif
  gStyle->SetOptStat(0);
  SetAtlasStyle();

  TFile *file = TFile::Open(filename.c_str());
  if (file==nullptr) std::cout<<"===Info::Error file not found " <<filename<<std::endl;
  TH2D* hist = (TH2D*)file->Get(histogram.c_str());
  TH2D* histClone = (TH2D*)hist->Clone();
  //TH1D* histProjectionX = (TH1D*)hist->ProjectionX();
  //std::cout<<"projectin on x axis integral: "<<histProjectionX->Integral()<<std::endl;;
  int nBins = hist->GetNbinsX();
  std::vector<double> xbinContent;
  for(int binx=1; binx<=nBins; binx++){
    double totalBinValueForOneXbin = 0;
    for(int biny=1; biny<=nBins; biny++){
    totalBinValueForOneXbin = totalBinValueForOneXbin + hist->GetBinContent(binx,biny);
    if (debug)  std::cout<<"individual bin content "<<hist->GetBinContent(binx,biny)<<std::endl;
    }
    if (debug)  std::cout<<"x total bin content "<<totalBinValueForOneXbin<<std::endl;
    std::cout<<"---------------------------------"<<std::endl;
    xbinContent.push_back(totalBinValueForOneXbin);
  }
  double integral = hist->Integral();
  std::cout<<"intergral: "<<integral<<std::endl;
  for(int binx=1; binx<=nBins; binx++){
    for(int biny=1; biny<=nBins; biny++){
    double binVal = 0;
    binVal = hist->GetBinContent(binx,biny);
    std::cout<<"binVal: "<<round(binVal*100)/100<<"  xbinContent: "<<xbinContent[binx-1]<<std::endl;
    std::cout<<"binVal(x,y): "<<binVal*100/xbinContent[binx-1]<<std::endl;
    histClone->SetBinContent(binx, biny, round((double)binVal*100/xbinContent[binx-1]) );
    //histClone->SetBinContent(binx, biny, round((double)binVal*100.0)/100.0 / (round(xbinContent[binx-1]*100.0)/100.0));
    }
    std::cout<<"======================================"<<std::endl;
  }

  gStyle->SetPaintTextFormat("4.0f ");
  histClone->GetZaxis()->SetRangeUser(1.0, 100);
  TString xaxisName = "Reconstructed-level "+histogramTitle;
  TString yaxisName = "Particle-level "+histogramTitle;
  histClone->GetXaxis()->SetTitle(xaxisName);
  histClone->GetYaxis()->SetTitle(yaxisName);
  histClone->GetZaxis()->SetTitle("Percentage (%)");
  TCanvas* c = new TCanvas("c");
  //c->SetCanvasSize(1500, 1000);
  //TCanvas *c = new TCanvas("c", "Canvas Title", 1000, 920); // Both width and height are 600

  c->cd();
  histClone->SetMarkerSize(1.1);
  //c->SetTopMargin(0.10); // Adjust this value as needed
  //c->SetBottomMargin(0.15); // Adjust this value as needed
  //c->SetLeftMargin(0.15); // Adjust this value as needed
  c->SetRightMargin(0.20); // Adjust this value as needed
  c->SetLeftMargin(0.20); // Adjust this value as needed

  //TAxis *xaxis = histClone->GetXaxis();
  //for (int i = 1; i <= xaxis->GetNbins(); i++) {
  //    if (i % 2 == 0) { // For alternate bins
  //        // Format the label as needed
  //        TString label = TString::Format("%.0f", xaxis->GetBinLowEdge(i));
  //        xaxis->SetBinLabel(i, label);
  //    } else {
  //        xaxis->SetBinLabel(i, ""); // Hide the label
  //    }
  //}

  //TAxis *yaxis = histClone->GetYaxis();
  //for (int i = 1; i <= yaxis->GetNbins(); i++) {
  //    if (i % 2 == 0) { // For alternate bins
  //        // Format the label as needed
  //        TString label = TString::Format("%.0f", yaxis->GetBinLowEdge(i));
  //        yaxis->SetBinLabel(i, label);
  //    } else {
  //        yaxis->SetBinLabel(i, ""); // Hide the label
  //    }
  //}


  histClone->Draw("textcolz");
  c->Update();

  //if (save_name=="SR1")  myText(       0.23,  0.75, 1, "SR t#bar{t}#gamma production");
  //if (save_name=="SR2")  myText(       0.23,  0.75, 1, "CR t#bar{t}#gamma decay");
  //if (save_name=="SR3")  myText(       0.23,  0.75, 1, "CR fake #gamma");
  //if (save_name=="SR4")  myText(       0.23,  0.75, 1, "CR other #gamma");
  //myText(       0.23,  0.80, 1, "#sqrt{s} = 13 TeV");
  //ATLASLabel(0.23,0.85,"Simulation");

  if (save_name=="SR1")  myText(       0.23,  0.85, 1, "SR t#bar{t}#gamma production");
  if (save_name=="SR2")  myText(       0.23,  0.85, 1, "CR t#bar{t}#gamma decay");
  if (save_name=="SR3")  myText(       0.23,  0.85, 1, "CR fake #gamma");
  if (save_name=="SR4")  myText(       0.23,  0.85, 1, "CR other #gamma");
  myText(       0.23,  0.89, 1, "#sqrt{s} = 13 TeV");
  //ATLASLabel(0.20,0.96,"Simulation");

  TString saveName = "migration_"+histogram+"_"+save_name+".pdf";
  //c->SetLogx();
  //c->SetLogy();
  c->SaveAs(saveName);

  //TCanvas* c1 = new TCanvas("c1");
  //c1->cd();
  //hist->Draw("textcolz 4.1f");
  //TString saveName_original = "migration"+histogram+"1.png";
  //c1->SaveAs(saveName_original);


}
