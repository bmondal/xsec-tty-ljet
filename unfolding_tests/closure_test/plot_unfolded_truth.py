#!/usr/bin/python

'''
- Plot unfolded spectrum and truth spectrum in a single plot
- Also make the ration plot of Unfolded/Truth 

'''
import ROOT

path_truth_hist = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-ljet/abs-xsec-with-trexfiles/v11/v7/tmp/tty1l_pt_all_syst/UnfoldingHistograms/FoldedHistograms.root"
path_unfolded_hist = "/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-ljet/abs-xsec-with-trexfiles/v11/v7/tmp/tty1l_pt_all_syst/Unfolding_UnfoldedData.root"


def createCanvasPads():
  c = ROOT.TCanvas("c", "canvas", 800, 800)
  # Upper histogram plot is pad1
  pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
  pad1.SetBottomMargin(0)  # joins upper and lower plot
  pad1.SetGridx()
  pad1.Draw()
  # Lower ratio plot is pad2
  c.cd()  # returns to main canvas before defining pad2
  pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
  pad2.SetTopMargin(0)  # joins upper and lower plot
  pad2.SetBottomMargin(0.2)
  #pad2.SetGridx()
  pad2.SetGridy()
  pad2.Draw()

  return c, pad1, pad2

def run():
  file_unfolded = ROOT.TFile.Open(path_unfolded_hist)
  unfolded_plot_canvas = file_unfolded.Get("c1_n9;1") # unfolded histogram; getting it from canvas
  file_truth = ROOT.TFile.Open(path_truth_hist)
  hist_truth = file_truth.Get("Unfolding_truth_distribution;1")
  # divide by bin width
  for i in range(0, hist_truth.GetNbinsX()):
    bin_content = 0
    bin_width = 0
    value = 0
    bin_content = hist_truth.GetBinContent(i+1)
    bin_width = hist_truth.GetBinWidth(i+1)
    value = bin_content/(bin_width*139)
    print("bin content: {0}; bin width: {1}; value: {2}".format(bin_content, bin_width, value))
    hist_truth.SetBinContent(i+1, value)
    hist_truth.SetBinError(i+1, 0)

  hist = unfolded_plot_canvas.FindObject("hist_part_ph_pt_full_weighted")
  hist_clone = hist.Clone()
  hist.SetLineColor(4)
  hist_truth_clone = hist_truth.Clone()
  hist_truth.SetLineColor(2)
  canvas, pad1, pad2 = createCanvasPads()
  pad1.cd()
  hist.Draw("hist e")
  hist_truth.Draw("hist same e")
  pad2.cd()
  hist_clone.Divide(hist_truth_clone)
  hist_clone.GetYaxis().SetRangeUser(0.5,1.5)
  hist_clone.Draw("hist e")
  canvas.SaveAs("tmp.pdf")
  canvas.SaveAs("tmp.png")

  # print chiSquare
  #chisquare = hist_truth.GetChiSquare(hist)
  #print("chi square : {}".format(chisquare))


if __name__== '__main__':
  run()
