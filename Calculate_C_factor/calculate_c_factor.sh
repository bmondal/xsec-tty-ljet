#!/usr/bin/env bash
#out_file_tty_prod="c_factor_ljet_tty_prod_v4.txt"
out_file_tty_prod="c_factor_ljet_tty_prod_vMC_uncert.txt"
out_file_tty_dec="c_factor_ljet_tty_dec_v4.txt"
run_tty_prod() {
  ## tty_prod
  echo -e "fiducial_particle \t fiducial_reco_sr1 \t fiducial_reco_sr2 \t fiducial_reco_sr3 \t fiducial_reco_sr4 \t total_fiducial_in_all_regions"
  echo -e "------------------------------------------"
  echo "tty_prod_nominal"
  python calculate_c_factor.py --rootfilepath $1/  --rootfilename  nominal_9999/histograms.ttgamma_prod.Fine_Binning_1.root
  #echo "tty_prod_var3cUp"
  #python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.ttgamma_prod_var3cUp.root 
  #echo "tty_prod_var3cDown"
  #python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.ttgamma_prod_var3cDown.root 
  echo "tty_prod_muR_up"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_muR_up.Fine_Binning_1.root
  echo "tty_prod_muR_down"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_muR_down.Fine_Binning_1.root
  echo "tty_prod_muF_up"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_muF_up.Fine_Binning_1.root
  echo "tty_prod_muF_down"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_muF_down.Fine_Binning_1.root

  echo "tty_prod_muR_muF_up"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_muR_muF_up.Fine_Binning_1.root
  echo "tty_prod_muR_muF_down"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_muR_muF_down.Fine_Binning_1.root

  echo "tty_prod_muR_05_muF_2"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_muR_05_muF_2.Fine_Binning_1.root
  echo "tty_prod_muR_2_muF_05"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_muR_2_muF_05.Fine_Binning_1.root


  echo "tty_prod_PDF4LHC_0"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_0.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_1"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_1.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_2"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_2.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_3"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_3.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_4"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_4.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_5"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_5.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_6"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_6.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_7"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_7.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_8"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_8.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_9"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_9.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_10"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_10.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_11"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_11.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_12"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_12.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_13"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_13.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_14"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_14.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_15"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_15.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_16"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_16.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_17"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_17.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_18"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_18.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_19"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_19.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_20"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_20.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_21"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_21.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_22"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_22.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_23"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_23.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_24"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_24.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_25"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_25.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_26"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_26.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_27"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_27.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_28"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_28.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_29"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_29.Fine_Binning_1.root
  echo "tty_prod_PDF4LHC_30"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_prod_PDF4LHC_30.Fine_Binning_1.root

  #echo "tty_prod_H7"
  #python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.ttgamma_prod_H7.root

}

run_tty_dec() {
  ## tty_dec
  echo -e "fiducial_particle \t fiducial_reco_sr1 \t fiducial_reco_sr2 \t fiducial_reco_sr3 \t fiducial_reco_sr4 \t total_fiducial_in_all_regions"
  echo -e "------------------------------------------"

  echo "tty_dec_nominal"
  python calculate_c_factor.py --rootfilepath $1/  --rootfilename  nominal_9999/histograms.ttgamma_dec.root
  echo "tty_dec_var3cUp"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_var3c_up.root 
  echo "tty_dec_var3cDown"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_var3c_down.root 
  echo "tty_dec_muR_up"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_muR_up.root
  echo "tty_dec_muR_down"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_muR_down.root
  echo "tty_dec_muF_up"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_muF_up.root
  echo "tty_dec_muF_down"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_muF_down.root

  echo "tty_dec_muR_muF_up"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_muR_muF_up.root
  echo "tty_dec_muR_muF_down"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_muR_muF_down.root

  echo "tty_dec_muR_05_muF_2"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_muR_05_muF_2.root
  echo "tty_dec_muR_2_muF_05"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_muR_2_muF_05.root


  echo "tty_dec_PDF4LHC_0"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_0.root
  echo "tty_dec_PDF4LHC_1"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_1.root
  echo "tty_dec_PDF4LHC_2"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_2.root
  echo "tty_dec_PDF4LHC_3"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_3.root
  echo "tty_dec_PDF4LHC_4"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_4.root
  echo "tty_dec_PDF4LHC_5"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_5.root
  echo "tty_dec_PDF4LHC_6"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_6.root
  echo "tty_dec_PDF4LHC_7"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_7.root
  echo "tty_dec_PDF4LHC_8"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_8.root
  echo "tty_dec_PDF4LHC_9"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_9.root
  echo "tty_dec_PDF4LHC_10"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_10.root
  echo "tty_dec_PDF4LHC_11"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_11.root
  echo "tty_dec_PDF4LHC_12"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_12.root
  echo "tty_dec_PDF4LHC_13"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_13.root
  echo "tty_dec_PDF4LHC_14"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_14.root
  echo "tty_dec_PDF4LHC_15"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_15.root
  echo "tty_dec_PDF4LHC_16"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_16.root
  echo "tty_dec_PDF4LHC_17"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_17.root
  echo "tty_dec_PDF4LHC_18"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_18.root
  echo "tty_dec_PDF4LHC_19"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_19.root
  echo "tty_dec_PDF4LHC_20"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_20.root
  echo "tty_dec_PDF4LHC_21"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_21.root
  echo "tty_dec_PDF4LHC_22"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_22.root
  echo "tty_dec_PDF4LHC_23"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_23.root
  echo "tty_dec_PDF4LHC_24"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_24.root
  echo "tty_dec_PDF4LHC_25"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_25.root
  echo "tty_dec_PDF4LHC_26"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_26.root
  echo "tty_dec_PDF4LHC_27"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_27.root
  echo "tty_dec_PDF4LHC_28"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_28.root
  echo "tty_dec_PDF4LHC_29"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_29.root
  echo "tty_dec_PDF4LHC_30"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.tty_dec_PDF4LHC_30.root
  echo "tty_dec_H7"
  python calculate_c_factor.py --rootfilepath $1/ --rootfilename nominal_9999/histograms.ttgamma_dec_H7.root


}


# be mindful I am using local eos folder here
#run_tty_prod "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/" >> $out_file_tty_prod
run_tty_prod "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/MC_undert/" >> $out_file_tty_prod
#run_tty_dec "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11_Nominal_from_fine_v1/" >> $out_file_tty_dec
