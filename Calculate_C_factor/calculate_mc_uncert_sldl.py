import math
from typing import Tuple
# Define the path to the file
file_path_SL = 'c_factor_ljet_tty_prod_vMC_uncert.txt' 
file_path_DL = '/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-dilep/Calculate_C_factor/c_factor_dilepton_tty_prod_vMC_uncert.txt' 

debug = True

def is_float(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def get_yields_from_file(file_path,calculate_scale,calculate_pdf):
  # Open the file in read mode
  with open(file_path, 'r') as file:
      number_list = []
      
      skip_next = False  # A flag to indicate whether to skip the next line
      
      for line in file:
          
          if skip_next:  # If flag is set, skip this line and reset the flag
              skip_next = False
              continue
          
          # Set skip_next to True if the conditions are met
          if calculate_scale and "PDF4LHC" in line:
              skip_next = True
              continue
          
          if calculate_pdf and ("muR" in line or "muF" in line or "nominal" in line):
              skip_next = True
              continue
          
          # Split the line by whitespace and filter out any non-numeric parts
          numbers = [float(num) for num in line.split() if is_float(num)]
          
          if numbers:
              number_list.append(numbers[0])
      return number_list
  

def calculate_uncertainty(calculate_scale,calculate_pdf) -> Tuple[int, int]:
      number_list_SL = get_yields_from_file(file_path_SL,calculate_scale,calculate_pdf) 
      number_list_DL = get_yields_from_file(file_path_DL,calculate_scale,calculate_pdf) 
      # calculate the quad sum
      # nominal is 
      lumi = 140.1
      nominal = (number_list_SL[0]+number_list_DL[0])
      quad_sum_up = 0
      quad_sum_down = 0
      if debug: print(f"nomnal value: {round(nominal/lumi,1)}\n")
      # other ones
      if not(len(number_list_SL) == len(number_list_DL)):
        print("=== FATAL,length of array from SL and DL don't match \n")
      for index in range(1,len(number_list_SL)):
        if debug: print(f"variantion {index} : {round((number_list_SL[index]+number_list_DL[index])/lumi,1)}\n")
        if (nominal > (number_list_SL[index]+number_list_DL[index])):
          quad_sum_up = quad_sum_up + (nominal/lumi - (number_list_SL[index]+number_list_DL[index])/lumi)**2
        else:
          quad_sum_down = quad_sum_down + (nominal/lumi - (number_list_SL[index]+number_list_DL[index])/lumi)**2
      
      uncertainty_up = math.sqrt(quad_sum_up)
      uncertainty_down = math.sqrt(quad_sum_down)
      #if calculate_scale:  print(f"xsec =  {round(nominal,1)} pm {round(uncertainty,1)}(scale) \n")
      #if calculate_pdf:  print(f"xsec =  {round(nominal,1)} pm {round(uncertainty,1)}(pdf) \n")
      return nominal/lumi, uncertainty_up, uncertainty_down # uncertainty already divided by lumi

if __name__=="__main__":
  calculate_scale = True 
  calculate_pdf = False 
  print("---------------for scale variation-----------------------------")
  nominal, scale_uncert_up,scale_uncert_down  = calculate_uncertainty(calculate_scale,calculate_pdf)
  calculate_scale = False
  calculate_pdf = True 
  print("---------------for pdf variation, nominal is pdf0 here-----------------------------")
  nominal_pdf, pdf_uncert_up,pdf_uncert_down  = calculate_uncertainty(calculate_scale,calculate_pdf)
  print(f"xsec =  {round(nominal,1)} +{round(scale_uncert_up,1)}-{round(scale_uncert_down,1)}(scale) +{round(pdf_uncert_up,1)}-{round(pdf_uncert_down,1)}(pdf) \n")


