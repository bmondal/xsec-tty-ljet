#!/usr/bin/env python

"""
plot truth distribution from the trex-fitter Folded.py
"""
import ROOT
import os

var_dict = { 
    "tty1l_dr_all_syst":"tty_dr",
    "tty1l_drlj_all_syst":"tty_drlj",
    "tty1l_drphb_all_syst":"tty_drphb",
    "tty1l_eta_all_syst":"tty_eta",
    "tty1l_pt_all_syst":"tty_pt",
    "tty1l_ptj1_all_syst":"tty_ptj1"
    }


def get_last_folder_name(path):
  path = path.split("/")
  probably_last_element = path[len(path)-1]
  if "tty" in probably_last_element:
    return path[len(path)]
  else:
    return path[len(path) - 2] 


def plot(file_path, histname,save_name):
  ROOT.gROOT.SetStyle("ATLAS")
  file = ROOT.TFile(file_path)
  histogram = file.Get(histname)
  # add overflow in the last bin
  last_bin = histogram.GetNbinsX()
  histogram.SetBinContent(last_bin, histogram.GetBinContent(last_bin) + histogram.GetBinContent(last_bin+1))
  # divide by bin width
  for i in range(1, histogram.GetNbinsX() + 1):
      bin_content = histogram.GetBinContent(i)
      bin_error = histogram.GetBinError(i)
      bin_width = histogram.GetBinWidth(i)
      
      histogram.SetBinContent(i, bin_content / bin_width)
      histogram.SetBinError(i, bin_error / bin_width)

  canvas = ROOT.TCanvas()
  #canvas.SetRightMargin(0.09);
  #canvas.SetLeftMargin(0.15);
  #canvas.SetBottomMargin(0.15);
  canvas.cd()
  histogram.GetYaxis().SetTitleOffset(2.5)
  histogram.GetYaxis().SetTitle("Events / bin width")
  histogram.Draw("hist e")
  canvas.SaveAs(save_name)

def plot_for_this_trex_folder(path,var):
  file_path_prefix = path
  save_name_prefix = get_last_folder_name(file_path_prefix)
  file_path = "{}/UnfoldingHistograms/FoldedHistograms.root".format(file_path_prefix)
  histname = "{}_truth_distribution".format(var_dict[var])
  #histname = "tty_drphl_truth_distribution" # only for dr
  #histname = "Unfolding_truth_distribution"
  save_name = "{}_Unfolding_truth_distribution.pdf".format(save_name_prefix)
  plot(file_path, histname, save_name)




if __name__ == "__main__":
  # path to the TRexFitter folder
  file_path_prefix = "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding//xsec-tty-ljet/abs-xsec-with-trexfiles-local/v12/v31/syst-all-fit-data-mu-blinded"
  # list of trex-fit folders 
  var_list = [ 
    #"tty1l_dr_all_syst",
    "tty1l_drlj_all_syst",
    "tty1l_drphb_all_syst",
    "tty1l_eta_all_syst",
    "tty1l_pt_all_syst",
    "tty1l_ptj1_all_syst"
    ]
  print(var_list)
  for var in var_list:
    path = "{}/{}/".format(os.path.abspath(file_path_prefix), var)
    print(">>>>>>>>>>>>>>>>>>>>> processing for folder {} <<<<<<<<<<<<<<<".format(path))
    plot_for_this_trex_folder(path,var)


