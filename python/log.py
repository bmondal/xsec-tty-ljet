import logging

def log(message, level):
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
    if level == "warning":
        logging.warning(message)
    elif level == "info":
        logging.info(message)
    elif level == "debug":
        logging.debug(message)
    else:
        logging.error("Invalid log level")

# example
#log("This is a warning", "warning")
#log("This is info", "info")
#log("This is debug", "debug")
